import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CanastonRoutingModule } from './canaston-routing.module';
import { RegistroCanastonComponent } from './components/registro-canaston/registro-canaston.component';
import { ReporteRegistroCanastonComponent } from './components/reporte-registro-canaston/reporte-registro-canaston.component';
import { RegistroDetallesCanastonComponent } from './components/registro-detalles-canaston/registro-detalles-canaston.component';
import { SociosCanastonComponent } from './components/socios-canaston/socios-canaston.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    RegistroCanastonComponent,
    ReporteRegistroCanastonComponent,
    RegistroDetallesCanastonComponent,
    SociosCanastonComponent
  ],
  imports: [
    CommonModule,
    CanastonRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CanastonModule { }

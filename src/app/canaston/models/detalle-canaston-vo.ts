export interface DetalleCanastonVO{
  id            : number,
  idCanaston    : number,
  nombre        : string,
  costoUnitario : number,
  unidades      : number,
  costoTotal    : number,
  // reporte
  seleccionado? : boolean,
}
export interface CanastonDTO{
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
}
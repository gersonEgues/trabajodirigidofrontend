export interface SocioCanaston{
  idCanaston      : number ,
  idSocioMedidor  : number ,
  fechaRegistro   : string,
  detalle         : string,
}
export interface DetalleCanastonDTO{
  idCanaston    : number,
  nombre        : string,
  costoUnitario : number,
  unidades      : number,
}
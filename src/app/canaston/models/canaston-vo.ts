export interface CanastonVO{
  id            : number,
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
}
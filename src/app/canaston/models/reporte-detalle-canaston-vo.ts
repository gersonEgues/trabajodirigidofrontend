import { DetalleCanastonVO } from "./detalle-canaston-vo";

export interface ReporteDetalleCanastonVO{
  sumaCostoTotal      : number,
  countRows           : number,
  detalleCanastonList : DetalleCanastonVO[],
}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { DetalleCanastonDTO } from '../models/detalle-canaston-dto';
import { DetalleCanastonVO } from '../models/detalle-canaston-vo';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { ReporteDetalleCanastonVO } from '../models/reporte-detalle-canaston-vo';

@Injectable({
  providedIn: 'root'
})
export class RegistroDetallesCanastonService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/detalle-canaston",http);
  }

  public getDetalleCanaston(id:number): Observable<DetalleCanastonVO>{
    return this.http.get<DetalleCanastonVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as DetalleCanastonVO;
      })
    );
  }

  public createDetalleCanaston(detalleCanastonDTO:DetalleCanastonDTO): Observable<DetalleCanastonVO>{
    return this.http.post<DetalleCanastonVO>(`${this.url}`,detalleCanastonDTO).pipe(
      map(response => {
        return response as DetalleCanastonVO;
      })
    );
  }

  public updateDetalleCanaston(id:number,detalleCanastonDTO:DetalleCanastonDTO): Observable<DetalleCanastonVO>{
    return this.http.put<DetalleCanastonVO>(`${this.url}/${id}`,detalleCanastonDTO).pipe(
      map(response => {
        return response as DetalleCanastonVO;
      })
    );
  }

  public deleteDetalleCanaston(id:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getListaDetalleCanaston(idCanaston:number): Observable<DetalleCanastonVO[]>{
    return this.http.get<DetalleCanastonVO[]>(`${this.url}/lista-canaston/${idCanaston}`).pipe(
      map(response => {
        return response as DetalleCanastonVO[];
      })
    );
  }

  public getReporteDetalleCanaston(idCanaston:number): Observable<ReporteDetalleCanastonVO>{
    return this.http.get<ReporteDetalleCanastonVO>(`${this.url}/reporte-canaston/${idCanaston}`).pipe(
      map(response => {
        return response as ReporteDetalleCanastonVO;
      })
    );
  }
}

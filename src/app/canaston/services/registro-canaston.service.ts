import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { CanastonVO } from '../models/canaston-vo';
import { map } from 'rxjs/operators';
import { CanastonDTO } from '../models/canaston-dto';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';

@Injectable({
  providedIn: 'root'
})
export class RegistroCanastonService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/canaston",http);
  }

  public getCanaston(id:number): Observable<CanastonVO>{
    return this.http.get<CanastonVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as CanastonVO;
      })
    );
  }

  public createCanaston(canastonDTO:CanastonDTO): Observable<CanastonVO>{
    return this.http.post<CanastonVO>(`${this.url}`,canastonDTO).pipe(
      map(response => {
        return response as CanastonVO;
      })
    );
  }

  public updateCanaston(id:number,canastonDTO:CanastonDTO): Observable<CanastonVO>{
    return this.http.put<CanastonVO>(`${this.url}/${id}`,canastonDTO).pipe(
      map(response => {
        return response as CanastonVO;
      })
    );
  }

  public deleteCanaston(id:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getListaDeCanastones(): Observable<CanastonVO[]>{
    return this.http.get<CanastonVO[]>(`${this.url}/lista`).pipe(
      map(response => {
        return response as CanastonVO[];
      })
    );
  }

  public existeCanastonEnGestion(idGestion:number): Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/existe-canaston-en-gestion/${idGestion}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getCanastonDeGestion(idGestion:number): Observable<CanastonVO>{
    return this.http.get<CanastonVO>(`${this.url}/gestion/${idGestion}`).pipe(
      map(response => {
        return response as CanastonVO;
      })
    );
  }
}

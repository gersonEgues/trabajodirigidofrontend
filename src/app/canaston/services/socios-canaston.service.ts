import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { SocioCanaston } from '../models/socio-canaston';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { map } from 'rxjs/operators';
import { ReporteSocioCanastonVO } from '../models/reporte-socio-canaston-vo';

@Injectable({
  providedIn: 'root'
})
export class SociosCanastonService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/socio-canaston",http);
  }

  public getSocioCanaston(idCanaston:number,idSocioMedidor:number): Observable<SocioCanaston>{
    return this.http.get<SocioCanaston>(`${this.url}/${idCanaston}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as SocioCanaston;
      })
    );
  }

  public createSocioCanaston(socioCanaston:SocioCanaston): Observable<SocioCanaston>{
    return this.http.post<SocioCanaston>(`${this.url}`,socioCanaston).pipe(
      map(response => {
        return response as SocioCanaston;
      })
    );
  }

  public updateSocioCanaston(socioCanaston:SocioCanaston): Observable<SocioCanaston>{
    return this.http.put<SocioCanaston>(`${this.url}`,socioCanaston).pipe(
      map(response => {
        return response as SocioCanaston;
      })
    );
  }

  public deleteSocioCanaston(idCanaston:number,idSocioMedidor:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idCanaston}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public existeSocioCanaston(idCanaston:number,idSocioMedidor:number): Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/existe-socio-canaston/${idCanaston}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getSociosConCanastonEntregado(idCanaston:number): Observable<ReporteSocioCanastonVO[]>{
    return this.http.get<ReporteSocioCanastonVO[]>(`${this.url}/si-entregado/${idCanaston}`).pipe(
      map(response => {
        return response as ReporteSocioCanastonVO[];
      })
    );
  }

  public getSociosSinCanastonEntregado(idCanaston:number): Observable<ReporteSocioCanastonVO[]>{
    return this.http.get<ReporteSocioCanastonVO[]>(`${this.url}/no-entregado/${idCanaston}`).pipe(
      map(response => {
        return response as ReporteSocioCanastonVO[];
      })
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { CanastonDTO } from '../../models/canaston-dto';
import { RegistroCanastonService } from '../../services/registro-canaston.service';
import { CanastonVO } from '../../models/canaston-vo';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';

@Component({
  selector: 'app-registro-canaston',
  templateUrl: './registro-canaston.component.html',
  styleUrls: ['./registro-canaston.component.css']
})
export class RegistroCanastonComponent implements OnInit {
  aux:any = null;
  listaGestiones          : AnioVO[];
  formularioCanaston!     : FormGroup;
  
  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';

  canastonUpdate  : CanastonVO = this.aux;
  listaCanastones : CanastonVO[] = [];

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private registroCanastonService          : RegistroCanastonService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre del canastón','Fecha registro'];
    this.campoItems  = ['nombre','fechaRegistro'];

    this.getGestionesService();
    this.construirFormulario();
    this.getListaDeCanastonesService();
  }

  private construirFormulario(){
    this.formularioCanaston = this.fb.group({
      gestion       : ['0',[Validators.required],[]],
      fechaRegistro : [{value: this.generalService.getFechaActualFormateado(), disabled: false},[Validators.required],[]],
      nombre        : ['',[Validators.required],[]],
    });
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioCanaston.get(campo).valid &&
            this.formularioCanaston.get(campo).value!='0';
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioCanaston.get(campo).invalid && 
            this.formularioCanaston.get(campo).touched;
  }

  public actualizarItemEvent(item:CanastonVO){
    this.formularioCanaston.get('gestion').setValue(item.idGestion);
    this.formularioCanaston.get('gestion').disable();
    this.formularioCanaston.get('fechaRegistro').setValue(item.fechaRegistro);
    this.formularioCanaston.get('nombre').setValue(item.nombre);

    this.canastonUpdate = item;
  }

  public async eliminarItemEvent(item:CanastonVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR REGISTRO DE CANASTÓN',`¿Está seguro de eliminar el registro del canaston ${item.nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    this.deleteCanastonService(item.id);
  }

  public cancelarRegistroDeCanaston(){
    this.limpiarFormulario();
  }

  public async guardarRegistroDeCanaston(){
    if(this.formularioInvalido())
      return;

    let idGestion : number = Number(this.formularioCanaston.get('gestion').value);
    let bandera:BanderaResponseVO = await this.existeCanastonEnGestionService(idGestion);
    
    if(bandera.bandera){
      this.generalService.mensajeAlerta("Ya existe un registro de canaston para la gestión seleciconada");
      return;
    }

    let nombre:string = this.formularioCanaston.get('nombre').value;
    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR CANASTÓN',`¿Está seguro de registrar el canastón ${nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let canaston:CanastonDTO = {
      idGestion     : idGestion,
      fechaRegistro : this.formularioCanaston.get('fechaRegistro').value,
      nombre        : nombre,
    }
    this.createCanastonService(canaston);
  }

  public async actualizarRegistroDeCanaston(){
    if(this.formularioInvalido())
      return;

    let idGestion : number = Number(this.formularioCanaston.get('gestion').value);
    let nombre:string = this.formularioCanaston.get('nombre').value;
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR CANASTÓN',`¿Está seguro de actualizar el canastón ${nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let canaston:CanastonDTO = {
      idGestion     : idGestion,
      fechaRegistro : this.formularioCanaston.get('fechaRegistro').value,
      nombre        : this.formularioCanaston.get('nombre').value,
    }
    this.updateCanastonService(this.canastonUpdate.id,canaston);
  }

  private formularioInvalido():boolean{
    let invalido:boolean = false;

    if(this.formularioCanaston.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCanaston.markAllAsTouched();
      return true;
    }

    let idGestion:number = this.formularioCanaston.get('gestion').value;
    let fecha:string = this.formularioCanaston.get('fechaRegistro').value;
    let gestionFecha:number = Number(fecha.split('-')[0]);
    let gestion:number = this.getGestionFromLista(idGestion);

    if(gestionFecha!=gestion){
      this.generalService.mensajeFormularioInvalido('La gestión selecionada con la gestion de la fecha no coincide',INFO_MESSAGE.title_form_invalid);
      return true;
    }
    
    return invalido;
  }

  private getGestionFromLista(idGestion:number):number{
    let gestion:number = this.aux;
    let encontrado:boolean = false;
    let i:number = 0;
    while(!encontrado && i<this.listaGestiones.length){
      if(this.listaGestiones[i].idAnio == idGestion){
        encontrado = true;
        gestion  = this.listaGestiones[i].anio;
      }
      i++;
    }
    return gestion;
  }

  private limpiarFormulario(){
    this.formularioCanaston.reset();
    this.formularioCanaston.get('fechaRegistro').setValue(this.generalService.getFechaActualFormateado());  
    this.formularioCanaston.get('gestion').setValue('0');
    this.formularioCanaston.get('gestion').enable();
    this.canastonUpdate = this.aux;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaCanaston')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de ingresos extra');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public createCanastonService(canastonDTO:CanastonDTO){
    return this.registroCanastonService.createCanaston(canastonDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getListaDeCanastonesService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateCanastonService(id:number,canastonDTO:CanastonDTO){
    return this.registroCanastonService.updateCanaston(id,canastonDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getListaDeCanastonesService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteCanastonService(id:number){
    return this.registroCanastonService.deleteCanaston(id).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getListaDeCanastonesService();
      },
      (err)=>{
        this.generalService.mensajeAlerta('Revise los detalles asignados al canastón y los socios registrados al canastón');
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaDeCanastonesService(){
    return this.registroCanastonService.getListaDeCanastones().subscribe(
      (resp)=>{
        this.listaCanastones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public existeCanastonEnGestionService(idGestion:number):Promise<BanderaResponseVO>{
    return this.registroCanastonService.existeCanastonEnGestion(idGestion).toPromise()
      .then((resp:BanderaResponseVO)=>{
        return resp;
      })
      .catch(err=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

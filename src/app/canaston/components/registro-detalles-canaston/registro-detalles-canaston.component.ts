import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { IngresoExtraService } from 'src/app/ingreso-extra/service/ingreso-extra.service';
import { ReporteIngresoExtraService } from 'src/app/ingreso-extra/service/reporte-ingreso-extra.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RegistroCanastonService } from '../../services/registro-canaston.service';
import { RegistroDetallesCanastonService } from '../../services/registro-detalles-canaston.service';
import { DetalleCanastonDTO } from '../../models/detalle-canaston-dto';
import { DetalleCanastonVO } from '../../models/detalle-canaston-vo';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { CanastonVO } from '../../models/canaston-vo';
import { LogAltaMedidorService } from '../../../socio-medidor/services/log-alta-medidor.service';

@Component({
  selector: 'app-registro-detalles-canaston',
  templateUrl: './registro-detalles-canaston.component.html',
  styleUrls: ['./registro-detalles-canaston.component.css']
})
export class RegistroDetallesCanastonComponent implements OnInit {
  aux:any = null;
  listaGestiones          : AnioVO[];
  formularioCanaston!     : FormGroup;
  formularioGestion!     : FormGroup;

  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';

  listaDetalleCanaston                 : DetalleCanastonVO[] = [];  
  detalleCanastonUpdate                : DetalleCanastonVO = this.aux;
  canastonSeleccionadoRegistrarDetalle : CanastonVO  = this.aux;
  canastonSeleccionadoListar           : CanastonVO = this.aux;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private registroCanastonService          : RegistroCanastonService,
              private registroDetallesCanastonService  : RegistroDetallesCanastonService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre ítem','Costo unitario','Unidades','Costo total'];
    this.campoItems  = ['nombre','costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    // para listar
    this.formularioGestion = this.fb.group({
      gestion        : ['0',[Validators.required],[]]
    });


    // para crear
    this.formularioCanaston = this.fb.group({
      gestion         : ['0',[Validators.required],[]],
      nombreCanaston  : [{value:'Canastón...',disabled:true},[Validators.required],[]],
      detalleCanaston : this.fb.array([],[Validators.required],[])
    });
    this.aniadirDetalleCanaston();
  }

  public get detalleCanastonList() : FormArray {
    return this.formularioCanaston.get('detalleCanaston') as FormArray;
  }

  public nuevoDetalleCanaston():FormGroup{
    return this.fb.group({
      nombre        : ['', [Validators.required],[]],
      costoUnitario : ['', [Validators.required],[]],
      unidades      : ['', [Validators.required],[]],
    });
  }

  public aniadirDetalleCanaston():void{
    this.detalleCanastonList.push(this.nuevoDetalleCanaston());
  }

  public eliminarDetalleCanaston(index:number):void{
    this.detalleCanastonList.removeAt(index);
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioCanaston.get(campo).valid &&
            this.formularioCanaston.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioCanaston.get(campo).invalid && 
            this.formularioCanaston.get(campo).touched;
  }

  public campoInvalidoDetalleGestion(campo:string) : boolean{
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public campoValidoDetalleGestion(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public detalleCanastonValido(index:number,campo:string) : boolean{
    return (this.detalleCanastonList.at(index) as FormGroup).controls[campo].valid;
  }

  public detalleCanastonInvalido(index:number,campo:string) : boolean{   
    return (this.detalleCanastonList.at(index) as FormGroup).controls[campo].invalid && 
           (this.detalleCanastonList.at(index) as FormGroup).controls[campo].touched;
  }



  public cancelarRegistroDeCanaston(){
    this.limpiarFormulario();
  }

  public async guardarRegistroDeCanaston(){
    if(this.formularioCanaston.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCanaston.markAllAsTouched();
      return;
    }

    if(this.canastonSeleccionadoRegistrarDetalle==this.aux){
      this.generalService.mensajeAlerta("No existe un canastón para la gestión seleccionada");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR DETALLES DE CANASTÓN',`¿Está seguro de registrar los detalles ingreados para el ${this.canastonSeleccionadoRegistrarDetalle.nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    for (let i = 0; i < this.detalleCanastonList.length; i++) {
      let ingresoExtra:AbstractControl = this.detalleCanastonList.at(i);
      
      let item:DetalleCanastonDTO = {
        idCanaston     : this.canastonSeleccionadoRegistrarDetalle.id,
        nombre        : ingresoExtra.get('nombre').value,
        costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
        unidades      : Number(ingresoExtra.get('unidades').value),
      }
      this.createDetalleCanastonService(item);
    }
  }

  public async actualizarRegistroDeCanaston(){
    if(this.formularioCanaston.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCanaston.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR DETALLES DE CANASTÓN',`¿Está seguro de actualizar los detalles ingreados para el ${this.canastonSeleccionadoListar.nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

      let ingresoExtra:AbstractControl = this.detalleCanastonList.at(0);
      
      let item:DetalleCanastonDTO = {
        idCanaston    : this.canastonSeleccionadoListar.id,
        nombre        : ingresoExtra.get('nombre').value,
        costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
        unidades      : Number(ingresoExtra.get('unidades').value),
      }
      this.updateDetalleCanastonService(this.detalleCanastonUpdate.id,item);
    
  }

  private limpiarFormulario(){
    this.formularioCanaston.reset();
    this.formularioCanaston.get('gestion').setValue('0');
    this.formularioCanaston.get('gestion').enable();
    this.formularioCanaston.get('nombreCanaston').setValue('Canastón...');
    this.detalleCanastonList.clear();
    this.detalleCanastonUpdate = this.aux;
    this.canastonSeleccionadoRegistrarDetalle = this.aux;
    this.aniadirDetalleCanaston();
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public changeGestionRegistroDetalleCanaston(){
    let idGestion:number = Number(this.formularioCanaston.get('gestion').value);
    this.getCanastonDeGestionParaRegistroDeDetalleService(idGestion);
  }

  public changeGestionListarDetalleCanaston(){
    let idGestion:number = Number(this.formularioGestion.get('gestion').value);
    this.getCanastonDeGestionService(idGestion);
  }

  public actualizarItemEvent(item:DetalleCanastonVO){
    this.formularioCanaston.get('gestion').setValue(this.canastonSeleccionadoListar.idGestion);
    this.formularioCanaston.get('gestion').disable();
    this.formularioCanaston.get('nombreCanaston').setValue(this.canastonSeleccionadoListar.nombre);

    this.detalleCanastonList.at(0).get('nombre').setValue(item.nombre);
    this.detalleCanastonList.at(0).get('costoUnitario').setValue(item.costoUnitario);
    this.detalleCanastonList.at(0).get('unidades').setValue(item.unidades);

    this.detalleCanastonUpdate = item;
  }

  public async eliminarItemEvent(item:DetalleCanastonVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR DETALLE DE CANASTÓN',`¿Está seguro de eliminar el siguiente detalle ${item.nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    this.deleteDetalleCanastonService(item.id);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaDetalleCanaston')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de detalles del canaston');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public createDetalleCanastonService(detalleCanastonDTO:DetalleCanastonDTO){
    return this.registroDetallesCanastonService.createDetalleCanaston(detalleCanastonDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = Number(this.formularioGestion.get('gestion').value);
          this.getCanastonDeGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateDetalleCanastonService(id:number,detalleCanastonDTO:DetalleCanastonDTO){
    return this.registroDetallesCanastonService.updateDetalleCanaston(id,detalleCanastonDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid){
          let idGestion:number = Number(this.formularioGestion.get('gestion').value);
          this.getCanastonDeGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteDetalleCanastonService(id:number){
    return this.registroDetallesCanastonService.deleteDetalleCanaston(id).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid){
          let idGestion:number = Number(this.formularioGestion.get('gestion').value);
          this.getCanastonDeGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaDetalleCanastonService(idCanaston:number){
    return this.registroDetallesCanastonService.getListaDetalleCanaston(idCanaston).subscribe(
      (resp)=>{      
        this.listaDetalleCanaston = resp;
        if(this.listaDetalleCanaston.length>0){
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCanastonDeGestionService(idGestion:number){
    return this.registroCanastonService.getCanastonDeGestion(idGestion).subscribe(
      (resp)=>{
        if(resp){
          this.canastonSeleccionadoListar = resp;
          this.getListaDetalleCanastonService(resp.id);
        }else{
          this.listaDetalleCanaston = [];
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCanastonDeGestionParaRegistroDeDetalleService(idGestion:number){
    return this.registroCanastonService.getCanastonDeGestion(idGestion).subscribe(
      (resp)=>{
        if(resp){
          this.canastonSeleccionadoRegistrarDetalle = resp;
          this.formularioCanaston.get('nombreCanaston').setValue(resp.nombre);
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.canastonSeleccionadoRegistrarDetalle = this.aux;
          this.formularioCanaston.get('nombreCanaston').setValue('No existe un canastón para esta gestión');
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { IngresoExtraService } from 'src/app/ingreso-extra/service/ingreso-extra.service';
import { ReporteIngresoExtraService } from 'src/app/ingreso-extra/service/reporte-ingreso-extra.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RegistroCanastonService } from '../../services/registro-canaston.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { CanastonVO } from '../../models/canaston-vo';
import { LecturaMedidorPorUsuarioService } from 'src/app/consumo-agua-socio/services/lectura-medidor-por-usuario.service';
import { DatosSocioMedidorVO } from 'src/app/consumo-agua-socio/models/datosSocioMedidorVO';
import { LecturaSocioMedidorValidator } from 'src/app/consumo-agua-socio/utils/lectura-meidor-socio.validator';
import { SociosCanastonService } from '../../services/socios-canaston.service';
import { SocioCanaston } from '../../models/socio-canaston';
import { ReporteSocioCanastonVO } from '../../models/reporte-socio-canaston-vo';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
@Component({
  selector: 'app-socios-canaston',
  templateUrl: './socios-canaston.component.html',
  styleUrls: ['./socios-canaston.component.css']
})
export class SociosCanastonComponent implements OnInit {
  aux:any = null;
  listaGestiones          : AnioVO[];
  formularioRegistroSocioCanaston!     : FormGroup;
  formularioSocioCanaston!     : FormGroup;
  
  // tabla
  tituloItemsReporte : string[] = [];
  campoItemsReporte  : string[] = [];
  idItemReporte      : string   = 'idsocio';

  canastonSeleccionadoRegistroSocioCanaston : CanastonVO = this.aux;
  canastonSeleccionadoReporteSocioCanaston  : CanastonVO = this.aux;
  datosSocioMedidor!   : DatosSocioMedidorVO;
  banderaSocioConCanastonAsignado : boolean = true;
  
  socioCanastonUpdate : ReporteSocioCanastonVO = this.aux;
  listaReporteSociosCanaston : ReporteSocioCanastonVO[];
  





  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private ingresoExtraService              : IngresoExtraService,
              private registroCanastonService          : RegistroCanastonService,
              private lecturaMedidorPorUsuarioService  : LecturaMedidorPorUsuarioService,
              private reporteIngresoExtraService       : ReporteIngresoExtraService,
              private sociosCanastonService            : SociosCanastonService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItemsReporte = ['#','Cod. Socio','Nombres','Apellido Pat.','Apellido Mat.','Cod. Medidor','Cod. Categoria','nombreCategoria','Detalle','Fecha registro'];
    this.campoItemsReporte  = ['codigoSocio','nombre','apellidoPaterno','apellidoMaterno','codigoMedidor','codigoCategoria','nombreCategoria','detalle','fechaRegistro'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    this.formularioRegistroSocioCanaston = this.fb.group({
      gestion        : ['0',[Validators.required],[]],
      nombreCanaston : [{value:'',disabled:true},[Validators.required],[]],
      fechaRegistro  : [{value: this.generalService.getFechaActualFormateado(), disabled: false},[Validators.required],[]],
      codigoMedidor  : ['',[Validators.required],[LecturaSocioMedidorValidator.codigoMediorValido(this.lecturaMedidorPorUsuarioService)]],
      detalle        : [{value:'Sin detalle', disabled:false},[Validators.required],[]],
    });

    this.formularioSocioCanaston = this.fb.group({
      gestion : ['0',[Validators.required],[]],
      estado  : ['con-canaston',[Validators.required],[]],
    });
  }

  public cancelarRegistroDeSocioCanaston(){
    this.limpiarFormulario();
  }

  public async guardarRegistroDeSocioCanaston(){
    if(this.formularioRegistroSocioCanaston.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioRegistroSocioCanaston.markAllAsTouched();
      return;
    }

    if(this.canastonSeleccionadoRegistroSocioCanaston==this.aux){
      this.generalService.mensajeFormularioInvalido('No se seleccionó un canastón de una gestión');
      return;
    }

    let bandera:BanderaResponseVO = await this.existeSocioCanaston(this.canastonSeleccionadoRegistroSocioCanaston.id,this.datosSocioMedidor.idSocioMedidor);
    let nombre:string = this.datosSocioMedidor.nombreCompletoSocio;
      let codigo:string = this.datosSocioMedidor.codigoMedidor;
    if(bandera.bandera){      
      this.generalService.mensajeFormularioInvalido(`El/La soci@ ${nombre} con código de medidor ${codigo}, ya se le asigno el canastón`);
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ASIGNAR CANASTÓN',`¿Está seguro de registrar entrega del canastón al socio ${this.datosSocioMedidor.nombreCompletoSocio}, con código de medidor ${codigo} ?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let socioCanaston:SocioCanaston = {
      idCanaston      : this.canastonSeleccionadoRegistroSocioCanaston.id ,
      idSocioMedidor  : this.datosSocioMedidor.idSocioMedidor ,
      fechaRegistro   : this.formularioRegistroSocioCanaston.get('fechaRegistro').value,
      detalle         : this.formularioRegistroSocioCanaston.get('detalle').value,
    }

    this.createSocioCanastonService(socioCanaston);
  }

  public async actualizarRegistroDeSocioCanaston(){
    if(this.formularioRegistroSocioCanaston.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioRegistroSocioCanaston.markAllAsTouched();
      return;
    }

    let nombreCompleto:string = `${this.socioCanastonUpdate.nombre} ${this.socioCanastonUpdate.apellidoPaterno} ${this.socioCanastonUpdate.apellidoMaterno}`;
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR ASIGNACION DE CANASTÓN',`¿Está seguro de actualizar la entrega del canastón al socio ${nombreCompleto}, con código de medidor ${this.socioCanastonUpdate.codigoMedidor} ?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let socioCanaston:SocioCanaston = {
      idCanaston      : this.socioCanastonUpdate.idCanaston ,
      idSocioMedidor  : this.socioCanastonUpdate.idSocioMedidor ,
      fechaRegistro   : this.formularioRegistroSocioCanaston.get('fechaRegistro').value,
      detalle         : this.formularioRegistroSocioCanaston.get('detalle').value,
    }
    this.updateSocioCanastonService(socioCanaston);
  }

  public changeGestionRegistroSocioCanaston(){
    let idGestion:number = Number(this.formularioRegistroSocioCanaston.get('gestion').value);
    this.getCanastonDeGestionParaAsignarCanastonASocio(idGestion);
  }

  public buscarDatosSocioMedidorService(){
    let codigoMedidor:string =  this.formularioRegistroSocioCanaston.get('codigoMedidor')?.value;   

    if(codigoMedidor){     
      this.getDatosSocioMedidorByCodigoMedidorService(codigoMedidor);
    }else{
      
    }
  }

  private limpiarFormulario(){
    this.formularioRegistroSocioCanaston.get('gestion').enable();
    this.formularioRegistroSocioCanaston.get('codigoMedidor').enable();

    this.formularioRegistroSocioCanaston.get('codigoMedidor').reset();
    this.formularioRegistroSocioCanaston.get('fechaRegistro').setValue(this.generalService.getFechaActualFormateado());
    this.formularioRegistroSocioCanaston.get('detalle').setValue('Sin detalle');
    
    this.socioCanastonUpdate  = this.aux;
    this.datosSocioMedidor = this.aux;
  }

  public changeGestionSocioCanaston(){
    let idGestion:number = Number(this.formularioSocioCanaston.get('gestion').value);
    let estadoSocioCanaston:string = this.formularioSocioCanaston.get('estado').value;

    if(estadoSocioCanaston == 'con-canaston')
      this.banderaSocioConCanastonAsignado = true;
    else
      this.banderaSocioConCanastonAsignado = false;

    this.getCanastonDeGestionParaMostrarReporte(idGestion);
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioRegistroSocioCanaston?.get(campo)?.valid &&
            this.formularioRegistroSocioCanaston?.get(campo)?.value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioRegistroSocioCanaston.get(campo)?.invalid && 
            this.formularioRegistroSocioCanaston.get(campo)?.touched;
  }

  public campoValidoSocioCanaston(campo:string) : boolean{
    return  this.formularioSocioCanaston?.get(campo)?.valid &&
            this.formularioSocioCanaston?.get(campo)?.value!=0;
  }

  public campoInvalidoSocioCanaston(campo:string) : boolean{   
    return  this.formularioSocioCanaston.get(campo)?.invalid && 
            this.formularioSocioCanaston.get(campo)?.touched;
  }

  public actualizarItemEvent(item:ReporteSocioCanastonVO){
    this.limpiarFormulario();
    this.formularioRegistroSocioCanaston.get('gestion').disable();
    this.formularioRegistroSocioCanaston.get('codigoMedidor').disable();

    this.formularioRegistroSocioCanaston.get('fechaRegistro').setValue(item.fechaRegistro);
    this.formularioRegistroSocioCanaston.get('codigoMedidor').setValue(item.codigoMedidor);
    this.formularioRegistroSocioCanaston.get('detalle').setValue(item.detalle);
    
    this.socioCanastonUpdate = item;
  }

  public async eliminarItemEvent(item:ReporteSocioCanastonVO){
    let nombreCompleto:string = `${item.nombre} ${item.apellidoPaterno} ${item.apellidoMaterno}`;
    
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR EL REGISTRO DE ASIGNACIÓN DE CANASTÓN',`¿Está seguro de eliminar el registro de asignacion del canastón al socio ${nombreCompleto}, con código de medidor ${item.codigoMedidor}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    this.deleteSocioCanastonService(item.idCanaston,item.idSocioMedidor);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaSociosDeCanaston')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de socios - canaston');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getCanastonDeGestionParaAsignarCanastonASocio(idGestion:number){
    return this.registroCanastonService.getCanastonDeGestion(idGestion).subscribe(
      (resp)=>{
        if(resp){
          this.canastonSeleccionadoRegistroSocioCanaston = resp;
          this.formularioRegistroSocioCanaston.get('nombreCanaston').setValue(resp.nombre);
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.canastonSeleccionadoRegistroSocioCanaston = this.aux;
          this.formularioRegistroSocioCanaston.get('nombreCanaston').setValue('No existe un canastón para esta gestión');
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCanastonDeGestionParaMostrarReporte(idGestion:number){
    return this.registroCanastonService.getCanastonDeGestion(idGestion).subscribe(
      (resp)=>{
        if(resp){
          this.canastonSeleccionadoReporteSocioCanaston = resp;          
          let estadoSocioCanaston:string = this.formularioSocioCanaston.get('estado').value;
          if(estadoSocioCanaston == 'con-canaston'){
            this.getReporteSociosConCanastonEntregadoService(this.canastonSeleccionadoReporteSocioCanaston.id);
          }else{
            this.getReporteSociosSinCanastonEntregadoService(this.canastonSeleccionadoReporteSocioCanaston.id);
          }

        }else{
          this.listaReporteSociosCanaston = [];
          this.canastonSeleccionadoReporteSocioCanaston = this.aux;
          this.generalService.mensajeAlerta('No existe registro de canastón para la gestión seleccionada');
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getDatosSocioMedidorByCodigoMedidorService(codigoMedidor:string){
    this.lecturaMedidorPorUsuarioService.getDatosSocioMedidorByCodigoMedidor(codigoMedidor).subscribe(
      (resp)=>{
        this.datosSocioMedidor = resp;             
        if(this.datosSocioMedidor){
          
        }else{
          this.datosSocioMedidor = this.aux;
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createSocioCanastonService(socioCanaston:SocioCanaston){
    return this.sociosCanastonService.createSocioCanaston(socioCanaston).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioSocioCanaston.valid && this.formularioSocioCanaston.get('gestion').value!='0'){
          let idGestion:number = Number(this.formularioSocioCanaston.get('gestion').value);
          this.getCanastonDeGestionParaMostrarReporte(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateSocioCanastonService(socioCanaston:SocioCanaston){
    return this.sociosCanastonService.updateSocioCanaston(socioCanaston).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioSocioCanaston.valid && this.formularioSocioCanaston.get('gestion').value!='0'){
          let idGestion:number = Number(this.formularioSocioCanaston.get('gestion').value);
          this.getCanastonDeGestionParaMostrarReporte(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteSocioCanastonService(idCanaston:number,idSocioMedidor:number){
    return this.sociosCanastonService.deleteSocioCanaston(idCanaston,idSocioMedidor).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioSocioCanaston.valid && this.formularioSocioCanaston.get('gestion').value!='0'){
          let idGestion:number = Number(this.formularioSocioCanaston.get('gestion').value);
          this.getCanastonDeGestionParaMostrarReporte(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public existeSocioCanaston(idCanaston:number,idSocioMedidor:number):Promise<BanderaResponseVO>{
    return this.sociosCanastonService.existeSocioCanaston(idCanaston,idSocioMedidor).toPromise()
      .then(resp=>{
        return resp;
      })
      .catch(err=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }

  public getReporteSociosConCanastonEntregadoService(idCanaston:number){
    return this.sociosCanastonService.getSociosConCanastonEntregado(idCanaston).subscribe(
      (resp)=>{
        this.listaReporteSociosCanaston = [];
        this.listaReporteSociosCanaston = resp;

        if(this.listaReporteSociosCanaston.length>0)
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);        
        else
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteSociosSinCanastonEntregadoService(idCanaston:number){
    return this.sociosCanastonService.getSociosSinCanastonEntregado(idCanaston).subscribe(
      (resp)=>{
        this.listaReporteSociosCanaston = [];
        this.listaReporteSociosCanaston = resp;

        if(this.listaReporteSociosCanaston.length>0)
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);        
        else
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

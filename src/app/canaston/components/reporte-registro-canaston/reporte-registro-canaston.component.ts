import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RegistroDetallesCanastonService } from '../../services/registro-detalles-canaston.service';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { ReporteDetalleCanastonVO } from '../../models/reporte-detalle-canaston-vo';
import { DetalleCanastonVO } from '../../models/detalle-canaston-vo';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { RegistroCanastonService } from '../../services/registro-canaston.service';

@Component({
  selector: 'app-reporte-registro-canaston',
  templateUrl: './reporte-registro-canaston.component.html',
  styleUrls: ['./reporte-registro-canaston.component.css']
})
export class ReporteRegistroCanastonComponent implements OnInit {
  aux:any = null;
  listaGestiones     : AnioVO[];
  formularioGestion! : FormGroup;
  
  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';

  reporteDetalleCanaston    : ReporteDetalleCanastonVO = this.aux;
  listaItemsDetalleCanaston : DetalleCanastonVO[] = [];
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private registroDetallesCanastonService  : RegistroDetallesCanastonService,
              private registroCanastonService          : RegistroCanastonService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre','Costo unitario','Unidades','Costo total'];
    this.campoItems  = ['costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    this.formularioGestion = this.fb.group({
      gestion : ['0',[Validators.required],[]]
    });
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public changeGestionReporte(){
    let idGestion:number = this.formularioGestion.get('gestion').value;
    this.getCanastonDeGestionService(idGestion);
  }

  public seleccionarItem(item:DetalleCanastonVO){
    this.listaItemsDetalleCanaston.forEach(element => {
      if(item.id == element.id)
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('idTableReporteCanaston')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de reporte items de cabaston');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getCanastonDeGestionService(idGestion:number){
    return this.registroCanastonService.getCanastonDeGestion(idGestion).subscribe(
      (resp)=>{
        if(resp){
          this.getReporteDetalleCanastonService(resp.id);

        }else{
          this.reporteDetalleCanaston = this.aux;
          this.listaItemsDetalleCanaston = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteDetalleCanastonService(idCanaston:number){    
    this.registroDetallesCanastonService.getReporteDetalleCanaston(idCanaston).subscribe(
      (resp)=>{               
        if(!resp || resp.detalleCanastonList.length==0){
          this.reporteDetalleCanaston = this.aux;
          this.listaItemsDetalleCanaston = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.reporteDetalleCanaston = resp;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          this.listaItemsDetalleCanaston = this.reporteDetalleCanaston.detalleCanastonList;
        }               
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

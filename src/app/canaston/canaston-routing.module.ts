import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { RegistroCanastonComponent } from './components/registro-canaston/registro-canaston.component';
import { ReporteRegistroCanastonComponent } from './components/reporte-registro-canaston/reporte-registro-canaston.component';
import { ROL } from '../shared/enums-mensajes/roles';
import { RegistroDetallesCanastonComponent } from './components/registro-detalles-canaston/registro-detalles-canaston.component';
import { SociosCanastonComponent } from './components/socios-canaston/socios-canaston.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'registro-canaston', 
        component: RegistroCanastonComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'registro-detalles-canaston', 
        component: RegistroDetallesCanastonComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'socios-canaston', 
        component: SociosCanastonComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-canaston', 
        component: ReporteRegistroCanastonComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'reporte-egresos-extra' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanastonRoutingModule { }

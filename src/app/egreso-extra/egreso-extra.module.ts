import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EgresoExtraRoutingModule } from './egreso-extra-routing.module';
import { EgresoExtraComponent } from './components/egreso-extra/egreso-extra.component';
import { ReporteEgresoExtraComponent } from './components/reporte-egreso-extra/reporte-egreso-extra.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ReporteEgresoExtraRangoFechaComponent } from './components/reporte-egreso-extra-rango-fecha/reporte-egreso-extra-rango-fecha.component';


@NgModule({
  declarations: [
    EgresoExtraComponent,
    ReporteEgresoExtraComponent,
    ReporteEgresoExtraRangoFechaComponent
  ],
  imports: [
    CommonModule,
    EgresoExtraRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class EgresoExtraModule { }

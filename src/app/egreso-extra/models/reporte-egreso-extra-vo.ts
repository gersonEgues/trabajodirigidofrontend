import { EgresoExtraVO } from "./egreso-extra-vo";

export interface ReporteEgresoExtraVO {
  sumaCostoTotal   : number,
  countRows        : number,
  egresoExtraList  : EgresoExtraVO[],  
}
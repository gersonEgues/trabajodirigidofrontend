export interface EgresoExtraDTO {
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
  costoUnitario : number,
  unidades      : number
}
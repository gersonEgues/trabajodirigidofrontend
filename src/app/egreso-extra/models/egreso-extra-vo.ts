export interface EgresoExtraVO {
  id            : number,
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
  costoUnitario : number,
  unidades      : number,
  costoTotal    : number,

  selecionado   : boolean,
}
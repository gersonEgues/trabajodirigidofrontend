import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { EgresoExtraVO } from '../models/egreso-extra-vo';
import { EgresoExtraDTO } from '../models/egreso-extra-dto';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EgresoExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/egreso-extra",http);
  }

  public getEgresoExtra(id:number) : Observable<EgresoExtraVO> {
    return this.http.get<any>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as EgresoExtraVO;
      })
    );
  }

  public createEgresoExtra(ingresoExtraDTO:EgresoExtraDTO) : Observable<EgresoExtraVO> {
    return this.http.post<any>(`${this.url}`,ingresoExtraDTO).pipe(
      map(response => {
        return response as EgresoExtraVO;
      })
    );
  }

  public updateEgresoExtra(id:number,ingresoExtraDTO:EgresoExtraDTO) : Observable<EgresoExtraVO> {
    return this.http.put<any>(`${this.url}/${id}`,ingresoExtraDTO).pipe(
      map(response => {
        return response as EgresoExtraVO;
      })
    );
  }

  public deleteEgresoExtra(id:number) : Observable<BanderaResponseVO> {
    return this.http.delete<any>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

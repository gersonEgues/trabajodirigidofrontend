import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReporteIngresoExtraVO } from 'src/app/ingreso-extra/models/reporte-ingreso-extra-vo';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { ReporteEgresoExtraVO } from '../models/reporte-egreso-extra-vo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class ReporteEgresoExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-egreso-extra",http);
  }

  public getReporteEgresoExtraRangoFecha(rangoFecha:RangoFechaDTO) : Observable<ReporteEgresoExtraVO> {
    return this.http.put<any>(`${this.url}/rango-fecha`,rangoFecha).pipe(
      map(response => {
        return response as ReporteEgresoExtraVO;
      })
    );
  }

  public getReporteEgresoByGestion(id:number) : Observable<ReporteEgresoExtraVO> {
    return this.http.put<any>(`${this.url}/gestion/${id}`,null).pipe(
      map(response => {
        return response as ReporteEgresoExtraVO;
      })
    );
  }
}

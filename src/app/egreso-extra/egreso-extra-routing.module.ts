import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EgresoExtraComponent } from './components/egreso-extra/egreso-extra.component';
import { ReporteEgresoExtraComponent } from './components/reporte-egreso-extra/reporte-egreso-extra.component';
import { ROL } from '../shared/enums-mensajes/roles';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ReporteEgresoExtraRangoFechaComponent } from './components/reporte-egreso-extra-rango-fecha/reporte-egreso-extra-rango-fecha.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'registro-egresos-extra', 
        component: EgresoExtraComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-ingresos-extra-rango-fecha', 
        component: ReporteEgresoExtraRangoFechaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-egresos-extra-gestion', 
        component: ReporteEgresoExtraComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'reporte-egresos-extra' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EgresoExtraRoutingModule { }

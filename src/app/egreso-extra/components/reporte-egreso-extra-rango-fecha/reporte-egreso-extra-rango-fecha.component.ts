import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { ReporteEgresoExtraVO } from '../../models/reporte-egreso-extra-vo';
import { EgresoExtraVO } from '../../models/egreso-extra-vo';
import { ReporteEgresoExtraService } from '../../services/reporte-egreso-extra.service';

@Component({
  selector: 'app-reporte-egreso-extra-rango-fecha',
  templateUrl: './reporte-egreso-extra-rango-fecha.component.html',
  styleUrls: ['./reporte-egreso-extra-rango-fecha.component.css']
})
export class ReporteEgresoExtraRangoFechaComponent implements OnInit {
  aux:any = null;
  listaGestiones     : AnioVO[];
  formularioGestion! : FormGroup;

  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';
  
  reporteEgresoExtraVO : ReporteEgresoExtraVO = this.aux;
  listaEgresoExtraVO   : EgresoExtraVO[] = [];
  rangoFechaDTO!        : RangoFechaDTO;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reporteEgresoExtraService        : ReporteEgresoExtraService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre ingreso extra','Fecha registro','Costo unitario','Unidades','Costo tot.'];
    this.campoItems  = ['fechaRegistro','costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    this.formularioGestion = this.fb.group({
      gestion : ['0',[Validators.required],[]]
    });
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public seleccionarItem(item:EgresoExtraVO){
    this.listaEgresoExtraVO.forEach(element => {
      if(element.id==item.id)
        element.selecionado = true;
      else
        element.selecionado = false;
    });
  }

  public getReporteIngresoExatraEnRangoFecha(rangoFechaDTO:RangoFechaDTO){
    if(!rangoFechaDTO.rangoValido){
      this.reporteEgresoExtraVO = this.aux;
      this.listaEgresoExtraVO = [];
      return;
    }
      
    this.rangoFechaDTO = rangoFechaDTO;
    this.getReporteEgresoExtraRangoFechaService(rangoFechaDTO);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('idTableReporteEgresoExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de reporte de ingresos extra');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getReporteEgresoExtraRangoFechaService(rangoFecha:RangoFechaDTO){
    return this.reporteEgresoExtraService.getReporteEgresoExtraRangoFecha(rangoFecha).subscribe(
      (resp)=>{
        if(resp!=this.aux){
          this.reporteEgresoExtraVO = resp;
          this.listaEgresoExtraVO = this.reporteEgresoExtraVO.egresoExtraList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteEgresoExtraVO = this.aux;
          this.listaEgresoExtraVO = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { EgresoExtraService } from '../../services/egreso-extra.service';
import { ReporteEgresoExtraService } from '../../services/reporte-egreso-extra.service';
import { EgresoExtraDTO } from '../../models/egreso-extra-dto';
import { ReporteEgresoExtraVO } from '../../models/reporte-egreso-extra-vo';
import { EgresoExtraVO } from '../../models/egreso-extra-vo';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
@Component({
  selector: 'app-egreso-extra',
  templateUrl: './egreso-extra.component.html',
  styleUrls: ['./egreso-extra.component.css']
})
export class EgresoExtraComponent implements OnInit {
  aux:any = null;
  listaGestiones          : AnioVO[];
  formularioEgresoExtra!  : FormGroup;
  formularioGestion!      : FormGroup;
  
  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';
  
  reporteEgresoExtraVO : ReporteEgresoExtraVO = this.aux;
  listaEgresoExtraVO   : EgresoExtraVO[] = [];
  egresoExtraUpdate    : EgresoExtraVO   = this.aux;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private egresoExtraService               : EgresoExtraService,
              private reporteEgresoExtraService        : ReporteEgresoExtraService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre ingreso extra','Fecha registro','Costo unitario','Unidades','Costo total'];
    this.campoItems  = ['nombre','fechaRegistro','costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }


  private construirFormulario(){
    this.formularioEgresoExtra = this.fb.group({
      gestion       : ['0',[Validators.required],[]],
      fechaRegistro : [{value: this.generalService.getFechaActualFormateado(), disabled: false},[Validators.required],[]],
      egresosExtra : this.fb.array([],[Validators.required],[])
    });
    this.aniadirEgresoExtra();

    this.formularioGestion = this.fb.group({
      gestion : ['0',[Validators.required],[]]
    });
  }
  
  public get egresosExtraList() : FormArray {
    return this.formularioEgresoExtra.get('egresosExtra') as FormArray;
  }

  public nuevoEgresoExtra():FormGroup{
    return this.fb.group({
      nombre        : ['', [Validators.required],[]],
      costoUnitario : ['', [Validators.required],[]],
      unidades      : ['', [Validators.required],[]],
    });
  }

  public aniadirEgresoExtra():void{
    this.egresosExtraList.push(this.nuevoEgresoExtra());
  }

  public eliminarEgresoExtra(index:number):void{
    this.egresosExtraList.removeAt(index);
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioEgresoExtra.get(campo).valid &&
            this.formularioEgresoExtra.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioEgresoExtra.get(campo).invalid && 
            this.formularioEgresoExtra.get(campo).touched;
  }

  public campoValidoGestion(campo:string) : boolean{
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public campoInvalidoGestion(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public egresoExtraValido(index:number,campo:string) : boolean{
    return (this.egresosExtraList.at(index) as FormGroup).controls[campo].valid;
  }

  public egresoExtraInvalido(index:number,campo:string) : boolean{   
    return (this.egresosExtraList.at(index) as FormGroup).controls[campo].invalid && 
           (this.egresosExtraList.at(index) as FormGroup).controls[campo].touched;
  }


  public async guardarRegistroEgresoExtra(){
    if(this.formularioInvalido())
    return;

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR EL EGRESO EXTRA','¿Está seguro de registrar el/los egreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    for (let i = 0; i < this.egresosExtraList.length; i++) {
      let ingresoExtra:AbstractControl = this.egresosExtraList.at(i);
      
      let item:EgresoExtraDTO = {
        idGestion     : Number(this.formularioEgresoExtra.get('gestion').value),
        fechaRegistro : this.formularioEgresoExtra.get('fechaRegistro').value,
        nombre        : ingresoExtra.get('nombre').value,
        costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
        unidades      : Number(ingresoExtra.get('unidades').value),
      }
      this.createEgresoExtraService(item);
    }
  }

  public async actualizarRegistroEgresoExtra(){
    if(this.formularioInvalido())
    return;

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR EL EGRESO EXTRA','¿Está seguro de actualizar el/los egreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let ingresoExtra:AbstractControl = this.egresosExtraList.at(0);
    let item:EgresoExtraDTO = {
      idGestion     : Number(this.formularioEgresoExtra.get('gestion').value),
      fechaRegistro : this.formularioEgresoExtra.get('fechaRegistro').value,
      nombre        : ingresoExtra.get('nombre').value,
      costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
      unidades      : Number(ingresoExtra.get('unidades').value),
    }
    this.updateEgresoExtraService(this.egresoExtraUpdate.id,item);
  }

  public cancelarRegistroEgresoExtra(){
    this.limpiarFormulario();
  }

  private formularioInvalido():boolean{
    let invalido:boolean = false;

    if(this.formularioEgresoExtra.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioEgresoExtra.markAllAsTouched();
      return true;
    }

    let idGestion:number = this.formularioEgresoExtra.get('gestion').value;
    let fecha:string = this.formularioEgresoExtra.get('fechaRegistro').value;
    let gestionFecha:number = Number(fecha.split('-')[0]);
    let gestion:number = this.getGestionFromLista(idGestion);

    if(gestionFecha!=gestion){
      this.generalService.mensajeFormularioInvalido('La gestión selecionada con la gestion de la fecha no coincide',INFO_MESSAGE.title_form_invalid);
      return true;
    }
    
    return invalido;
  }

  private getGestionFromLista(idGestion:number):number{
    let gestion:number = this.aux;
    let encontrado:boolean = false;
    let i:number = 0;
    while(!encontrado && i<this.listaGestiones.length){
      if(this.listaGestiones[i].idAnio == idGestion){
        encontrado = true;
        gestion  = this.listaGestiones[i].anio;
      }
      i++;
    }
    return gestion;
  }

  private limpiarFormulario(){
    this.formularioEgresoExtra.reset();
    this.formularioEgresoExtra.get('fechaRegistro').setValue(this.generalService.getFechaActualFormateado());  
    this.formularioEgresoExtra.get('gestion').setValue('0');
    this.egresoExtraUpdate = this.aux;
    this.egresosExtraList.clear();
    this.aniadirEgresoExtra();
  }

  public actualizarItemEvent(item:EgresoExtraVO){
    this.formularioEgresoExtra.get('gestion').setValue(item.idGestion);
    this.formularioEgresoExtra.get('fechaRegistro').setValue(item.fechaRegistro);
    this.egresosExtraList.at(0).get('nombre').setValue(item.nombre);
    this.egresosExtraList.at(0).get('costoUnitario').setValue(item.costoUnitario);
    this.egresosExtraList.at(0).get('unidades').setValue(item.unidades);

    this.egresoExtraUpdate = item;
  }

  public async eliminarItemEvent(item:EgresoExtraVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR EL EGRESO EXTRA','¿Está seguro de eliminar el/los egreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    this.deleteEgresoExtraService(item.id);
  }

  public changeGestionListaIngresoExtra(){
    let idGestion:number = this.formularioGestion.get('gestion').value;
    this.getReporteEgresoByGestionService(idGestion);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaEgresoExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de egresos extra');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{       
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public createEgresoExtraService(ingresoExtraDTO:EgresoExtraDTO) {
    return this.egresoExtraService.createEgresoExtra(ingresoExtraDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteEgresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateEgresoExtraService(id:number,ingresoExtraDTO:EgresoExtraDTO) {
    return this.egresoExtraService.updateEgresoExtra(id,ingresoExtraDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteEgresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteEgresoExtraService(id:number) {
    return this.egresoExtraService.deleteEgresoExtra(id).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteEgresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteEgresoByGestionService(id:number) {
    return this.reporteEgresoExtraService.getReporteEgresoByGestion(id).subscribe(
      (resp)=>{
        if(resp!=this.aux){
          this.reporteEgresoExtraVO = resp;
          this.listaEgresoExtraVO = this.reporteEgresoExtraVO.egresoExtraList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteEgresoExtraVO = this.aux;
          this.listaEgresoExtraVO = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{       
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { AuthGuard } from 'src/app/login/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children:[
      { 
        path:'socios',
        loadChildren: () => import('../../socios/socios.module').then(m => m.SociosModule),
        canActivate : [AuthGuard]
      },
      { 
        path:'usuarios',
        loadChildren: () => import('../../usuarios/usuarios.module').then(m => m.UsuariosModule),
        canActivate : [AuthGuard]
      },
      { 
        path:'lectura-medidor',
        loadChildren: () => import('../../consumo-agua-socio/consumo-agua-socio.module').then(m => m.ConsumoAguaSocioModule),
        canActivate : [AuthGuard]
      },
      { 
        path:'cuenta-socio',
        loadChildren: () => import('../../cuenta-socio/cuenta-socio.module').then(m => m.CuentaSocioModule),
        canActivate : [AuthGuard] 
      },
      {
        path:'eventos',
        loadChildren: () => import('../../eventos/eventos.module').then(m => m.EventosModule),
        canActivate : [AuthGuard]
      },
      {
        path:'aportes-eventuales',
        loadChildren: () => import('../../aportes-eventuales/aportes-eventuales.module').then(m => m.AportesEventualesModule),
        canActivate : [AuthGuard]
      },
      { 
        path:'socio-medidor',
        loadChildren: () => import('../../socio-medidor/socio-medidor.module').then(m => m.SocioMedidorModule),
        canActivate : [AuthGuard]
      },
      {
        path:'cobros-extra',
        loadChildren: () => import('../../cobros-extra/cobros-extra.module').then(m => m.CobrosExtraModule),
        canActivate : [AuthGuard]
      },
      {
        path:'ingresos-extra',
        loadChildren: () => import('../../ingreso-extra/ingreso-extra.module').then(m => m.IngresoExtraModule),
        canActivate : [AuthGuard]
      },
      {
        path:'egresos-extra',
        loadChildren: () => import('../../egreso-extra/egreso-extra.module').then(m => m.EgresoExtraModule),
        canActivate : [AuthGuard]
      },
      {
        path:'mesa-directiva',
        loadChildren: () => import('../../mesa-directiva/mesa-directiva.module').then(m => m.MesaDirectivaModule),
        canActivate : [AuthGuard] 
      },
      {
        path:'tanque-de-agua',
        loadChildren: () => import('../../tanque-de-agua/tanque-de-agua.module').then(m => m.TanqueDeAguaModule),
        canActivate : [AuthGuard] 
      },
      {
        path:'canaston',
        loadChildren: () => import('../../canaston/canaston.module').then(m => m.CanastonModule),
        canActivate : [AuthGuard]
      },
      { 
        path:'informe-ingresos-economicos',
        loadChildren: () => import('../../informe-ingresos-economicos/informe-ingresos-economicos.module').then(m => m.InformeIngresosEconomicosModule),
        canActivate : [AuthGuard] 
      },
      { 
        path:'reporte-economico',
        loadChildren: () => import('../../informe-economico/informe-economico.module').then(m => m.InformeEconomicoModule),
        canActivate : [AuthGuard] 
      },
      { 
        path:'configuraciones',
        loadChildren: () => import('../../configuraciones/configuraciones.module').then(m => m.ConfiguracionesModule),
        canActivate : [AuthGuard] 
      },
      {
        path: '**',
        redirectTo: 'socios' // 'path'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserLayoutRoutingModule { }

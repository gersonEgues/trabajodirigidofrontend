import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLayoutRoutingModule } from './user-layout-routing.module';
import { UserComponent } from './user/user.component';
import { SidenavModule } from '../../sidenav/sidenav.module';


@NgModule({
  declarations: [
    UserComponent
  ],
  imports: [
    CommonModule,
    UserLayoutRoutingModule,
    SidenavModule
  ]
})
export class UserLayoutModule { }

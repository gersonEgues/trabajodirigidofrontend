import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuestLayoutRoutingModule } from './guest-layout-routing.module';
import { GuestComponent } from './guest/guest.component';

import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    GuestComponent
  ],
  imports: [
    CommonModule,
    GuestLayoutRoutingModule,
    SharedModule
  ]
})
export class GuestLayoutModule { }

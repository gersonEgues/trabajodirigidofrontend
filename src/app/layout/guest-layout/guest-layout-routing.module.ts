import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuestComponent } from './guest/guest.component';

const routes: Routes = [
  {
    path: '',
    component: GuestComponent, // sobre 'GuestComponent' se renderizara  todos los hijos 'ModuloInvitadoModule'
    children:[
      { 
        path:'home',
        loadChildren: () => import('../../home/home.module').then(m => m.HomeModule) 
      },
      { 
        path:'login',
        loadChildren: () => import('../../login/login.module').then(m => m.LoginModule) 
      },
      {
        path: '**',
        redirectTo: 'home' // 'path'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestLayoutRoutingModule { }

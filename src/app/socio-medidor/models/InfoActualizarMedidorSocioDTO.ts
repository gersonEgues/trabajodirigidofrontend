export interface InfoActualizarMedidorSocioDTO {
  idSocioMedidor  : number,
  idSocio         : number,
  idCategoria     : number,
  idTanqueAgua    : number,
  lecturaInicial  : number,
  lecturaActual   : number,
  direccion       : string,
} 
import { MedidorDataVO } from "./medidorDataVO";

export interface SocioDataVO {
  id               : number,
  codigo           : string,
  nombre           : string,
  apellidoPaterno  : string,
  apellidoMaterno  : string,
  countItems       : number,
  listaDeMedidores : MedidorDataVO[],

  // tratado
  idSocioMedidor  : number
}

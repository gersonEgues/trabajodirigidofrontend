import { MedidorDataVO } from "./medidorDataVO";

export interface LecturaMedidorSocioDataTratadoVO {
  numberSocio?      : number, 
  indexItemSocio?   : number, 
  indexItemMedidor? : number,
  seleccionado      : boolean,

  // socio
  idSocio?                    : number,
  codigoSocio?                : string,
  nombreCompletoSocio?        : string,
  countItemsSocio?            : number,
  countItemsMedidoresDeSocio? : number,

  // medidor
  idMedidor?                   : number|string,
  idSocioMedidor?              : number|string,
  codigoMedidor?               : string,
  lecturaInicialMedidor?       : number|string,
  lecturaActualMedidor?        : number|string,
  propiedadCooperativaMedidor? : string,
  idCategoriaMedidor?          : number|string,
  nombreCategoriaMedidor?      : string,
  countItemsLecturaPeriodo?    : number|string,
  volumenConsumidoMedidor?     : number|string,

  //lectura periodo
  nroLecturaMedidor?                    : number|string,
  nombreLecturaMedidor?                 : string,
  costoM3LecturaMedidor?                : number|string,
  volumenMinimoM3LecturaMedidor?        : number|string,
  costoMinimoM3LecturaMedidor?          : number|string,
  lecturaAnteriorPeriodoLecturaMedidor? : number|string,
  lecturaActualPeriodoLecturaMedidor?   : number|string,
  consumoVolumenM3LecturaMedidor?       : number|string,
  costoPeriodoLecturaMedidor?           : number|string,
  consumoMinimo?                        : string,
  fechaRegistroLecturaMedidor?          : string,
}

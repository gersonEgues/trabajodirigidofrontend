import { MedidorDataVO } from "./medidorDataVO";

export interface ReporteMedidorDataVO {
  sumaTotalM3           : number,
  sumaTotalCostoPeriodo : number,
  countRows             : number,
  listaMedidoresDeSocio : MedidorDataVO[]
} 
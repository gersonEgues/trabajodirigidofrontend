export interface LogBajaMedidorDTO {
  idLogBajaMedidor? : number,
  idUsuario         : number,
  idMedidor         : number,
  fechaRegistro     : string,
  motivo            : string,
  ubicacion         : string,
} 
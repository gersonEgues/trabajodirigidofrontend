import { LecturaMedidorVO } from "./lecturaMedidorVO";
import { SocioDataVO } from "./socioDataVO";

export interface MedidorDataVO {
  id                        : number,
  idSocioMedidor            : number,
  codigo                    : string,
  lecturaInicial            : number,
  lecturaActual             : number,
  propiedadCooperativa      : boolean,
  idCategoria               : number,
  nombreCategoria           : string,
  countItems                : number,
  volumenConsumido          : number,
  socio?                    : SocioDataVO,
  listaLecturaSocioMedidor  : LecturaMedidorVO[]
} 
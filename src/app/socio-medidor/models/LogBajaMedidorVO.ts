export interface LogBajaMedidorVO {
    idLogBajaMedidor : number,
    idMedidor        : number,
    idUsuario        : number,
    codigo           : number,
    lecturaInicial   : number,
    lecturaActual    : number,
    disponible       : boolean,
    estado           : boolean,
    fechaRegistro    : string,
    motivo           : string,
    ubicacion        : string
} 
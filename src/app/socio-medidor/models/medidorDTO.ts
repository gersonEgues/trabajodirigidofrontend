export interface MedidorDTO {
  idUsuario            : number,
  nroSerie             : string,
  codigo               : string ,
  lecturaInicial       : number,
  lecturaActual        : number,
  propiedadCooperativa : boolean,
  direccion            : string
} 
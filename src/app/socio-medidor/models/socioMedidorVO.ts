import { MedidorAsignadoVO } from "./medidorAsignadoVO";

export interface SocioMedidorVO {
  idSocio         : number,
  codigo          : string,
  nombreCompleto  : string,
  direccion       : string,
  medidores       : MedidorAsignadoVO[];
}
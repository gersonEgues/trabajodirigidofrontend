export interface InfoBajaMedidorSocioDTO {
  idSocioMedidor : number,
  idSocio        : number,
  idMedidor      : number,
  fecha          :string,
  motivo         : string,
} 
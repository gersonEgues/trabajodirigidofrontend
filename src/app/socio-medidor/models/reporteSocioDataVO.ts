import { SocioDataVO } from "./socioDataVO";

export interface ReporteSocioDataVO {
  sumaTotalM3           : number,
  sumaTotalCostoPeriodo : number,
  countRows             : number,
  listaDeSocios         : SocioDataVO[],
}

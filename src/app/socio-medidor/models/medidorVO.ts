export interface MedidorVO {
  idMedidor            : number,
  idUsuario            : number,
  codigo               : string,
  nroSerie             : string,
  lecturaInicial       : number,
  lecturaActual        : number,
  fechaRegistro        : string,
  disponible           : boolean,
  estado               : boolean,
  propiedadCooperativa : boolean,
  direccion            : string
  seleccionado?        : boolean;
} 
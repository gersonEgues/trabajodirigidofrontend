export interface LecturaMedidorVO {
  nro                    : number,
  nombre                 : string,
  costoM3                : number,
  volumenMinimoM3        : number,
  costoMinimoM3          : number,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  consumoVolumenM3       : number,
  costoPeriodo           : number,
  fechaRegistro          : string,
  consumoMinimo          : boolean|string,
  seleccionado?          : boolean,

  // busqueda de lectura de medidor por codigo de medidor:

  index?               : number, 
  countItems?          : number,
  codigoSocio?         : string,
  nombreCompletoSocio? : string,
} 
export interface MedidorAsignadoVO {
  idSocioMedidor            : number,
  idSocio                   : number,
  idTanqueAgua              : number,
  nombreTanque              : string,
  volumenActualTanqueAguaM3 : number,
  lecturaInicial            : number,
  lecturaActual             : number,
  fechaAsignacionSM         : string,    
  estadoSocioMedidor        : boolean,//** 
  direccionSM               : string,  
  idMedidor                 : number,
  codigo                    : number,
  propiedadCooperativa      : boolean,
  fechaRegistro             : string,
  estadoMedidor             : boolean, // *
  direccion                 : string,
  idCategoria               : number,
  codigoCategoria           : string,
  nombreCategoria           : string,
  descripcionCategoria      : string,
  registroSocioCobroExtra?  : boolean,
}
import { MedidorAsignadoVO } from "./medidorAsignadoVO";

export interface SocioMedidorTratadoVO {
  indexGlobal : number,
  indexSocio  : number,
  seleccionado : boolean;

  // socio medidor
  idSocio         : number,
  codigoSocio     : string,
  nombreCompleto  : string,
  direccionSocio  : string,
  countMedidores  : number,

  // medidor
  idSocioMedidor          : number,
  idTanqueAgua            : number,
  lecturaInicial          : number,
  fechaAsignacionSM       : string,    
  estadoSocioMedidor      : boolean, // *
  direccionSM             : string,  
  idMedidor               : number,
  codigoMedidor           : number,
  propiedadCooperativa    : boolean,
  lecturaActual           : number,
  fechaRegistro           : string,
  estadoMedidor           : boolean, // *
  direccionMedidor        : string,
  idCategoria             : number,
  codigoCategoria         : string,
  nombreCategoria         : string,
  descripcionCategoria    : string,
  registroSocioCobroExtra : boolean,
}
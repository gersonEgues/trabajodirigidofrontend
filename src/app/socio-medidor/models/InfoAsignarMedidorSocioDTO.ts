export interface InfoAsignarMedidorSocioDTO {
  idSocio         : number,
  idMedidor       : number,
  idCategoria     : number,
  idTanqueAgua    : number,
  fechaAsignacion : string,
  lecturaInicial  : number,
  lecturaActual   : number,
  estado          : boolean,
  direccion       : string,
} 
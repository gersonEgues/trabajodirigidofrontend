import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistroMedidorComponent } from './components/registro-medidor/registro-medidor.component';
import { BajaMedidorComponent } from './components/baja-medidor/baja-medidor.component';
import { ListaMedidoresAsignadoASocioComponent } from './components/lista-medidores-asignado-a-socio/lista-medidores-asignado-a-socio.component';
import { AsignarSocioMedidorComponent } from './components/asignar-socio-medidor/asignar-socio-medidor.component';
import { MedidorDeSocioComponent } from './components/medidor-de-socio/medidor-de-socio.component';
import { ListaMedidoresComponent } from './components/lista-medidores/lista-medidores.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path:'medidores-asignados',
        component: ListaMedidoresAsignadoASocioComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'lista-medidores',
        component: ListaMedidoresComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'medidores-de-socio',
        component: MedidorDeSocioComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'registro-medidor',
        component: RegistroMedidorComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'asignar-socio-medidor',
        component: AsignarSocioMedidorComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'baja-medidor',
        component: BajaMedidorComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path:'**', 
        redirectTo : 'medidores-asignados' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocioMedidorRoutingModule { }

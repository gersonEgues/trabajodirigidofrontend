import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { SocioMedidorRoutingModule } from './socio-medidor-routing.module';

import { SociosModule } from '../socios/socios.module';
import { SharedModule } from '../shared/shared.module';


import { RegistroMedidorComponent } from './components/registro-medidor/registro-medidor.component';
import { BajaMedidorComponent } from './components/baja-medidor/baja-medidor.component';
import { ListaMedidoresSinAsignarASocioComponent } from './components/lista-medidores-sin-asignar-a-socio/lista-medidores-sin-asignar-a-socio.component';
import { ListaMedidoresAsignadoASocioComponent } from './components/lista-medidores-asignado-a-socio/lista-medidores-asignado-a-socio.component';
import { AsignarSocioMedidorComponent } from './components/asignar-socio-medidor/asignar-socio-medidor.component';
import { MedidorDeSocioComponent } from './components/medidor-de-socio/medidor-de-socio.component';
import { ListaMedidoresComponent } from './components/lista-medidores/lista-medidores.component';

@NgModule({
  declarations: [
    RegistroMedidorComponent,
    BajaMedidorComponent,
    ListaMedidoresSinAsignarASocioComponent,
    ListaMedidoresAsignadoASocioComponent,
    AsignarSocioMedidorComponent,
    MedidorDeSocioComponent,
    ListaMedidoresComponent,
  ],
  imports: [
    CommonModule,
    SocioMedidorRoutingModule,
    SociosModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SocioMedidorModule { }

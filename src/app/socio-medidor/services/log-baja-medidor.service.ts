import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LogBajaMedidorDTO } from '../models/LogBajaMedidorDTO';
import { LogBajaMedidorVO } from '../models/LogBajaMedidorVO';
import { CountItemVO } from 'src/app/shared/models/countItemVO';
import { PageDTO } from '../../shared/models/pageDTO';

@Injectable({
  providedIn: 'root'
})
export class LogBajaMedidorService  extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/log-baja-medidor",http);
  }

  public getCountLogBajaMedidores() : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/count`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getLogBajaMedidor(idLogBajaMedidor:number) : Observable<LogBajaMedidorVO> {
    return this.http.get<LogBajaMedidorVO>(`${this.url}/${idLogBajaMedidor}`).pipe(
      map(response => {
        return response as LogBajaMedidorVO;
      })
    );
  }

  public getLogBajaMedidores(pageDTO:PageDTO) : Observable<LogBajaMedidorVO[]> {
    return this.http.get<LogBajaMedidorVO[]>(`${this.url}/lista/${pageDTO.page}/${pageDTO.size}`).pipe(
      map(response => {
        return response as LogBajaMedidorVO[];
      })
    );
  }

  public createLogBajaMedidor(logBajaMedidorDTO:LogBajaMedidorDTO) : Observable<LogBajaMedidorVO>{
    return this.http.post<LogBajaMedidorVO>(`${this.url}`,logBajaMedidorDTO).pipe(
      map(resp => {
        return resp as LogBajaMedidorVO;
      })
    );
  }

  public updateLogBajaMedidor(logBajaMedidorDTO:LogBajaMedidorDTO) : Observable<LogBajaMedidorVO>{
    return this.http.put<LogBajaMedidorVO>(`${this.url}`,logBajaMedidorDTO).pipe(
      map(resp => {
        return resp as LogBajaMedidorVO;
      })
    );
  }
}

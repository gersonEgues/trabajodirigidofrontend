import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MedidorDataVO } from '../models/medidorDataVO';
import { CountItemVO } from '../../shared/models/countItemVO';
import { PageEventDTO } from '../../shared/models/pageEventDTO';
import { SocioDataVO } from '../models/socioDataVO';
import { ReporteSocioDataVO } from '../models/reporteSocioDataVO';
import { ReporteMedidorDataVO } from '../models/reporteMedidorDataVO';

@Injectable({
  providedIn: 'root'
})
export class ReportesService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-lectura-periodo",http);
  }

  public getReporteLecturaPorCodigoDeSocio(codigoSocio:string,idAnio:number) : Observable<ReporteSocioDataVO> {
    return this.http.get<ReporteSocioDataVO>(`${this.url}/lista/codigo-socio/${codigoSocio}/${idAnio}`).pipe(
      map(response => {
        return response as ReporteSocioDataVO;
      })
    );
  }

  public getReporteLecturaPorCodigoDeMedidor(codigoMedidor:string,idAnio:number) : Observable<ReporteMedidorDataVO> {
    return this.http.get<ReporteMedidorDataVO>(`${this.url}/lista/codigo-medidor/${codigoMedidor}/${idAnio}`).pipe(
      map(response => {
        return response as ReporteMedidorDataVO;
      })
    );
  }


  public getCountSociosReporteLecturaPorGestion() : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/count/socios`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  
  public getReporteLecturaPorGestion(pageDTO:PageEventDTO,idAnio:number) : Observable<ReporteSocioDataVO> {
    return this.http.get<ReporteSocioDataVO>(`${this.url}/lista/gestion/${pageDTO.pageIndex}/${pageDTO.pageSize}/${idAnio}`).pipe(
      map(response => {
        return response as ReporteSocioDataVO;
      })
    );
  }

  public getReporteLecturaPorGestionConsumoMinimo(idAnio:number) : Observable<ReporteSocioDataVO> {
    return this.http.get<ReporteSocioDataVO>(`${this.url}/lista/gestion/minimo/${idAnio}`).pipe(
      map(response => {
        return response as ReporteSocioDataVO;
      })
    );
  }

  public getReporteLecturaPorGestionRangoMes(page:PageEventDTO,idAnio:number,idMesInicio:number,idMesFin:number) : Observable<ReporteSocioDataVO> {
    return this.http.get<ReporteSocioDataVO>(`${this.url}/lista/gestion/mes/${page.pageIndex}/${page.pageSize}/${idAnio}/${idMesInicio}/${idMesFin}`).pipe(
      map(response => {
        return response as ReporteSocioDataVO;
      })
    );
  }

  public getReporteLecturaRangoMesConsumoMinimo(idAnio:number,idMesInicio:number,idMesFin:number) : Observable<ReporteSocioDataVO> {
    return this.http.get<ReporteSocioDataVO>(`${this.url}/lista/gestion/mes/minimo/${idAnio}/${idMesInicio}/${idMesFin}`).pipe(
      map(response => {
        return response as ReporteSocioDataVO;
      })
    );
  }
}

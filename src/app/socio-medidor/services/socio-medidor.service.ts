import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SocioMedidorVO } from '../models/socioMedidorVO';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { CountItemVO } from '../../shared/models/countItemVO';
import { MedidorAsignadoVO } from '../models/medidorAsignadoVO';
import { InfoAsignarMedidorSocioDTO } from '../models/InfoAsignarMedidorSocioDTO';
import { InfoBajaMedidorSocioDTO } from '../models/InfoBajaMedidorSocioDTO';
import { InfoActualizarMedidorSocioDTO } from '../models/InfoActualizarMedidorSocioDTO';

@Injectable({
  providedIn: 'root'
})

export class SocioMedidorService  extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/socio-medidor",http);
  }

  public getCountMedidoresAsignadosASocios(estadoSocio:boolean=true, estadoSocioMedidor:boolean=true) : Observable<CountItemVO> {
    let url : string = `${this.url}/count/${estadoSocio}/${estadoSocioMedidor}`;
    return this.http.get<CountItemVO>(url).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getMedidoresAsignadosASocios(pageDTO:PageDTO,estadoSocio:boolean=true, estadoSocioMedidor:boolean=true, estadoMedidor:boolean=true) : Observable<SocioMedidorVO[]> {
    let url : string = `${this.url}/${estadoSocio}/${estadoSocioMedidor}/${estadoMedidor}?page=${pageDTO.page}&size=${pageDTO.size}`;
    return this.http.get<SocioMedidorVO[]>(url).pipe(
      map(response => {
        return response as SocioMedidorVO[];
      })
    );
  }

  public getSociosConSusMedidoresParaPeriodoCobroExtra( pageDTO:PageDTO,
                                                        idPeriodoCobroExtra:number,
                                                        estadoSocio:boolean=true,
                                                        estadoSocioMedidor:boolean=true,
                                                        estadoMedidor:boolean=true) : Observable<SocioMedidorVO[]> {
    let url : string = `${this.url}/cobro-extra/${estadoSocio}/${estadoSocioMedidor}/${estadoMedidor}/${idPeriodoCobroExtra}?page=${pageDTO.page}&size=${pageDTO.size}`;
    return this.http.get<SocioMedidorVO[]>(url).pipe(
      map(response => {
        return response as SocioMedidorVO[];
      })
    );
  }

  public getListaMedidoresFiltradoPorEstadoYDisponibilidad(estado:boolean=true, disponible:boolean=true) : Observable<MedidorAsignadoVO[]> {
    return this.http.get<MedidorAsignadoVO[]>(`${this.url}/medidores/${estado}/${disponible}`).pipe(
      map(response => {
        return response as MedidorAsignadoVO[];
      })
    );
  }

  public getMedidoresDeSocioConBajaSocioMedidor(idSocio:number) : Observable<MedidorAsignadoVO[]> {
    let url : string = `${this.url}/socio-baja-medidor/${idSocio}`;
    return this.http.get<MedidorAsignadoVO[]>(url).pipe(
      map(response => {
        return response as MedidorAsignadoVO[];
      })
    );
  }

  // get medidores con datos del socio
  public getMedidorerPorCodigoDeSocio(codigo:string) : Observable<SocioMedidorVO[]> {
    let url : string = `${this.url}/codigo/${codigo}`;
    return this.http.get<SocioMedidorVO[]>(url).pipe(
      map(response => {
        return response as SocioMedidorVO[];
      })
    );
  }

  // solo trae datos del medidor
  public getMedidorerPorIdDeSocio(idSocio:number) : Observable<MedidorAsignadoVO[]> {
    let url : string = `${this.url}/busqueda/id/${idSocio}`;
    return this.http.get<MedidorAsignadoVO[]>(url).pipe(
      map(response => {
        return response as MedidorAsignadoVO[];
      })
    );
  }

  public updateMedidor(idSocioMedidor:number,idSocio:number,idMedidor:number) : Observable<SocioMedidorVO>{
    let url:string = `${this.url}/dar-de-baja-asignacion-medidor/${idSocioMedidor}/${idSocio}/${idMedidor}`
    return this.http.put<SocioMedidorVO>(url,null).pipe(
      map(resp => {
        return resp as SocioMedidorVO;
      })
    );
  }

  //---------------------------------------------
  public asignarMedidorSocio(infoAsignarMedidorSocioDTO:InfoAsignarMedidorSocioDTO) : Observable<MedidorAsignadoVO>{
    return this.http.post<MedidorAsignadoVO>(`${this.url}/asignar-medidor-socio`,infoAsignarMedidorSocioDTO).pipe(
      map(resp => {
        return resp as MedidorAsignadoVO;
      })
    );
  }

  public desasignarMedidorSocio(infoBajaMedidorSocioDTO:InfoBajaMedidorSocioDTO) : Observable<SocioMedidorVO>{
    return this.http.put<SocioMedidorVO>(`${this.url}/baja-medidor-socio`,infoBajaMedidorSocioDTO).pipe(
      map(resp => {
        return resp as SocioMedidorVO;
      })
    );
  }

  public actualizarMedidorSocio(infoActualizarMedidorSocioDTO:InfoActualizarMedidorSocioDTO) : Observable<SocioMedidorVO>{
    return this.http.put<SocioMedidorVO>(`${this.url}/actualizar-medidor-socio`,infoActualizarMedidorSocioDTO).pipe(
      map(resp => {
        return resp as SocioMedidorVO;
      })
    );
  }
}

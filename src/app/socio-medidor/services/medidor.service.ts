import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { MedidorVO } from '../models/medidorVO';
import { MedidorDTO } from '../models/medidorDTO';
import { PageDTO } from '../../shared/models/pageDTO';
import { CountItemVO } from 'src/app/shared/models/countItemVO';
import { LogBajaMedidorDTO } from '../models/LogBajaMedidorDTO';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';


@Injectable({
  providedIn: 'root'
})
export class MedidorService  extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/medidor",http);
  }

  public getMedidor(idMedidor:number) : Observable<MedidorVO> {
    return this.http.get<MedidorVO>(`${this.url}/${idMedidor}`).pipe(
      map(response => {
        return response as MedidorVO;
      })
    );
  }

  public  getCountMedidoresSinAsignar(estado:boolean=true, disponible:boolean=true) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/sin-asignar/count/${disponible}/${estado}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getMedidoresSinAsignar(estado:boolean, disponible:boolean, pageDTO:PageDTO) : Observable<MedidorVO[]> {
    let url : string = `${this.url}/sin-asignar/${disponible}/${estado}?page=${pageDTO.page}&size=${pageDTO.size}`;
    return this.http.get<MedidorVO[]>(url).pipe(
      map(response => {
        return response as MedidorVO[];
      })
    );
  }

  public getCountMedidoresAsidnadosASocios() : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/asignados-a-socios/count`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getMedidoresAsignadosASocios(pageDTO:PageDTO) : Observable<MedidorVO[]> {
    let url : string = `${this.url}/asignados-a-socios?page=${pageDTO.page}&size=${pageDTO.size}`;
    return this.http.get<MedidorVO[]>(url).pipe(
      map(response => {
        return response as MedidorVO[];
      })
    );
  }

  public getCountMedidoresPorSuEstado(estado:boolean=true) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/estado/count/${estado}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getMedidoresPorSuEstado(estado:boolean=true, pageDTO:PageDTO) : Observable<MedidorVO[]> {
    let url : string = `${this.url}/estado/lista/${estado}?page=${pageDTO.page}&size=${pageDTO.size}`;
    return this.http.get<MedidorVO[]>(url).pipe(
      map(response => {
        return response as MedidorVO[];
      })
    );
  }

  public createMedidor(medidor:MedidorDTO) : Observable<MedidorVO>{
    return this.http.post<MedidorVO>(`${this.url}`,medidor).pipe(
      map(resp => {
        return resp as MedidorVO;
      })
    );
  }

  public updateMedidor(medidor:MedidorVO) : Observable<MedidorVO>{
    return this.http.put<MedidorVO>(`${this.url}`,medidor).pipe(
      map(resp => {
        return resp as MedidorVO;
      })
    );
  }

  public deleteMedidor(idMedidor:number) : Observable<CountItemVO>{
    return this.http.delete<CountItemVO>(`${this.url}/${idMedidor}`).pipe(
      map(resp => {
        return resp as CountItemVO;
      })
    );
  }

  public codigoMedidorDisponible(codigoMedidor:string) : Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/codigo-medidor-disponible/${codigoMedidor}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public codigoMedidorDisponibleUpdate(codigoMedidor:string,codigoExclude:string) : Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/codigo-medidor-disponible-update/${codigoMedidor}/${codigoExclude}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { PageDTO } from '../../../shared/models/pageDTO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MedidorVO } from '../../models/medidorVO';
import { MedidorService } from '../../services/medidor.service';
import { MedidorDTO } from '../../models/medidorDTO';
import { CountItemVO } from '../../../shared/models/countItemVO';
import { map } from 'rxjs/operators';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { User } from 'src/app/login/models/user';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-registro-medidor',
  templateUrl: './registro-medidor.component.html',
  styleUrls: ['./registro-medidor.component.css']
})

export class RegistroMedidorComponent implements OnInit {
  formularioRegistroMedidor! : FormGroup;
  bandera                    : boolean = false;
  medidorSeleccionado!       : MedidorVO;
  banderaUpdate              : boolean = false;

  aux:any = null;
  medidorDisponible          : boolean = this.aux;
  estadoMedidor              : boolean = true;

  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'idMedidor';
  countItems          : number      = 0;
  listaMedidores      : MedidorVO[] = [];
  pageDTO!            : PageDTO;

  userSession:User = this.aux;

  @ViewChild('tablaListaMedidoresRegSocio') tablaListaMedidoresRegSocio! : TablePaginatorComponent;

  constructor(private fb             : FormBuilder,
              private generalService : GeneralService,
              private medidorService : MedidorService,
              private dialog         : MatDialog,
              private router         : Router) {
  }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10, 
      sort : "",
    }            
    this.userSession = this.generalService.getDatosDeUsuarioConSessionActiva();
    this.construirFormulario();
    this.configurarDatosTablaListaMedidores();
    this.getCountMedidoresPorSuEstadoService(this.estadoMedidor);
    this.getMedidoresPorSuEstadoService(this.estadoMedidor,this.pageDTO);
  }

  private construirFormulario(){
    let nombreCompletoUsr:string = this.userSession.nombre + ' ' + this.userSession.apellidoPaterno + ' ' + this.userSession.apellidoMaterno;

    this.formularioRegistroMedidor = this.fb.group({
      fechaRegistro         : [{value: this.generalService.getFechaActual(), disabled: true},[Validators.required],[]],
      responsableRegistro   : [{value: nombreCompletoUsr , disabled: true},[Validators.required],[]],
      medidores             : this.fb.array([],[Validators.required],[])
    });
    this.aniadirMedidor();
  }
  
  public get medidores() : FormArray {
    return this.formularioRegistroMedidor.get('medidores') as FormArray;
  }

  public nuevoMedidor():FormGroup{
    return this.fb.group({
      nroSerieMedidor         : ['', [Validators.required],[]],
      codigoMedidor           : ['', [Validators.required],[this.codigoMedidorDisponible.bind(this)]],
      lecturaActual           : ['', [Validators.required,Validators.min(0)],[]],
      direccion               : ['', [Validators.required],[]],
      opcionPropietario       : [true, [Validators.required],[]],
    });
  }

  public aniadirMedidor():void{
    this.medidores.push(this.nuevoMedidor());
  }

  public eliminarMedidor(index:number):void{
    this.medidores.removeAt(index);
  }

  public medidorValido(index:number,campo:string) : boolean{
    return (this.medidores.at(index) as FormGroup).controls[campo].valid;
  }

  public medidorInvalido(index:number,campo:string) : boolean{   
    return (this.medidores.at(index) as FormGroup).controls[campo].invalid && 
           (this.medidores.at(index) as FormGroup).controls[campo].touched;
  }

  public async registrarMedidorDeSocio(){        
    if(this.formularioRegistroMedidor.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioRegistroMedidor.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE MEDIDOR','¿Está seguro de registrar los medidores ingresados?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.bandera = true;
    for (let medidor of this.medidores.controls) {
      let medidorDTO : MedidorDTO = {
        idUsuario            : this.userSession.id,
        nroSerie             : medidor.get('nroSerieMedidor')?.value,
        codigo               : medidor.get('codigoMedidor')?.value,
        lecturaInicial       : medidor.get('lecturaActual')?.value,
        lecturaActual        : medidor.get('lecturaActual')?.value,
        propiedadCooperativa : medidor.get('opcionPropietario')?.value,
        direccion            : medidor.get('direccion')?.value,
      }
      let medidorResp : any = await this.registrarMedidor(medidorDTO);
    }


    if(this.bandera){
      this.generalService.mensajeCorrecto("Se creo de manera correcta","Exito :");
      this.limpiarTodosLosCampos();

      this.getCountMedidoresPorSuEstadoService(this.estadoMedidor);
    this.getMedidoresPorSuEstadoService(this.estadoMedidor,this.pageDTO);
    }
  }

  public async actualizarDatosDeMedidor(){
    if(this.formularioRegistroMedidor.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioRegistroMedidor.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR MEDIDOR','¿Está seguro de actualizar los datos del medidor','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.medidorSeleccionado.codigo               = this.medidores.at(0).get('codigoMedidor')?.value;
    this.medidorSeleccionado.nroSerie             = this.medidores.at(0).get('nroSerieMedidor')?.value;
    //this.medidorSeleccionado.lecturaInicial     = this.medidores.at(0).get('lecturaInicial')?.value;
    this.medidorSeleccionado.lecturaActual        = this.medidores.at(0).get('lecturaActual')?.value;
    this.medidorSeleccionado.propiedadCooperativa = this.medidores.at(0).get('opcionPropietario')?.value;
    this.medidorSeleccionado.direccion            = this.medidores.at(0).get('direccion')?.value;    
    this.actualizarDatosDeMedidorService();
  }

  public cancelarRegistroSocio(){
    this.limpiarTodosLosCampos();
    this.banderaUpdate = false;

    this.formularioRegistroMedidor.get('codigo')?.clearAsyncValidators();
    this.formularioRegistroMedidor.get('codigo')?.setAsyncValidators(this.codigoMedidorDisponible.bind(this));
  }

  private limpiarTodosLosCampos(){
    let nombreCompletoUsr:string = this.userSession.nombre + ' ' + this.userSession.apellidoPaterno + ' ' + this.userSession.apellidoMaterno;
    this.formularioRegistroMedidor.reset();
    (this.formularioRegistroMedidor.get('medidores') as FormArray).at(0).get('codigoMedidor')?.clearAsyncValidators();
    this.formularioRegistroMedidor.get('fechaRegistro')?.setValue(this.generalService.getFechaActual());
    this.formularioRegistroMedidor.get('responsableRegistro')?.setValue(nombreCompletoUsr);
    (this.formularioRegistroMedidor.get('medidores') as FormArray).at(0).get('opcionPropietario')?.setValue(true);

    while(this.medidores.length > 1){
      this.medidores.removeAt(1);
    }
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    this.tablaListaMedidoresRegSocio.genearteArchivoXLSX("Lista de socios por aporte eventual");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  public getCountMedidoresPorSuEstadoService(estado:boolean){
    this.medidorService.getCountMedidoresPorSuEstado(estado).subscribe(
      (resp:CountItemVO)=>{
        this.countItems = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getMedidoresPorSuEstadoService(estado:boolean, pageDTO : PageDTO){
    this.medidorService.getMedidoresPorSuEstado(estado,pageDTO).subscribe(
      (resp)=>{
        this.listaMedidores = resp;        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    ); 
  }

  private registrarMedidor(medidorDTO : MedidorDTO):Promise<any>{   
    return this.medidorService.createMedidor(medidorDTO).toPromise()
      .then(data=>{     
      })
      .catch(err=>{
        this.bandera = false;
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
    });  
  }

  public actualizarDatosDeMedidorService(){
    this.medidorService.updateMedidor(this.medidorSeleccionado).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo correctamente","Exito :");
        this.banderaUpdate = false;
        this.limpiarTodosLosCampos();   
        this.getMedidoresPorSuEstadoService(this.estadoMedidor,this.pageDTO);             
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private deleteMedidor(idMedidor : number){
    this.medidorService.deleteMedidor(idMedidor).subscribe(
      (resp)=>{
        this.getCountMedidoresPorSuEstadoService(this.estadoMedidor);
        this.getMedidoresPorSuEstadoService(this.estadoMedidor,this.pageDTO);

        this.generalService.mensajeCorrecto("Se elimino el medidor de manera correcta.","Exito :");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private codigoMedidorDisponible(control: AbstractControl) {
    return this.medidorService.codigoMedidorDisponible(control.value).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }

  private codigoMedidorDisponibleUpdate(control: AbstractControl) {
    return this.medidorService.codigoMedidorDisponibleUpdate(control.value,this.medidorSeleccionado.codigo).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }

  //------------ Tabla para visualizar los medidores -------------------------
  private configurarDatosTablaListaMedidores() : void{
    this.tituloItems = ['#','Código','Nro. de Serie','Lec. Inicial','Lec. Actual','Fecha Reg.','Disponible','Prop. Cooperativa',"Direccion"];
    this.campoItems = ['codigo','nroSerie','lecturaInicial','lecturaActual','fechaRegistro','disponible','propiedadCooperativa','direccion'];

    this.pageDTO = {
      page : 0,
      size : 10,
      sort : '',
    }    
    //this.getCountSocios(this.estadoSocio);
  }

  // Event Output
  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.getMedidoresPorSuEstadoService(this.estadoMedidor,this.pageDTO);
  }
  
  public actualizarMedidorEvent(medidor:MedidorVO){  
    this.medidorSeleccionado = medidor;
    this.banderaUpdate = true;    
  
    (this.formularioRegistroMedidor.get('medidores') as FormArray).at(0).get('codigoMedidor')?.clearAsyncValidators();
    (this.formularioRegistroMedidor.get('medidores') as FormArray).at(0).get('codigoMedidor')?.setAsyncValidators(this.codigoMedidorDisponibleUpdate.bind(this));

    this.setDatosEnFormularioParaActualizar(medidor);
  }

  private setDatosEnFormularioParaActualizar(medidor:MedidorVO){
    this.formularioRegistroMedidor.get('fechaRegistro')?.setValue(medidor.fechaRegistro);
    this.formularioRegistroMedidor.get('responsableRegistro')?.setValue(this.generalService.getNombreCompletoUsuarioSession());
    this.medidores.at(0).get('codigoMedidor')?.setValue(medidor.codigo);
    this.medidores.at(0).get('nroSerieMedidor')?.setValue(medidor.nroSerie);
    this.medidores.at(0).get('lecturaInicial')?.setValue(medidor.lecturaInicial);
    this.medidores.at(0).get('lecturaActual')?.setValue(medidor.lecturaActual);
    this.medidores.at(0).get('direccion')?.setValue(medidor.direccion);
    this.medidores.at(0).get('opcionPropietario')?.setValue(medidor.propiedadCooperativa);
  }

  public deleteMedidorEvent(medidor:MedidorVO){
    this.deleteMedidor(medidor.idMedidor);
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

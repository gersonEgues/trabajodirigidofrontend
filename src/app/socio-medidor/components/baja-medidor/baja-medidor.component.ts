import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MedidorVO } from '../../models/medidorVO';
import { GeneralService } from '../../../shared/services/general.service';
import { LogBajaMedidorDTO } from '../../models/LogBajaMedidorDTO';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { LogBajaMedidorService } from '../../services/log-baja-medidor.service';
import { LogBajaMedidorVO } from '../../models/LogBajaMedidorVO';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ListaMedidoresSinAsignarASocioComponent } from '../lista-medidores-sin-asignar-a-socio/lista-medidores-sin-asignar-a-socio.component';
import { User } from 'src/app/login/models/user';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
@Component({
  selector: 'app-baja-medidor',
  templateUrl: './baja-medidor.component.html',
  styleUrls: ['./baja-medidor.component.css']
})
export class BajaMedidorComponent implements OnInit {
  aux:any = null;

  // app-buscar-medidor
  medidorSeleccionado! : MedidorVO;

  medidorSeleccionadoUpdate! : LogBajaMedidorVO;

  //lista baja medidores
  tituloItems           : string[] = [];
  campoItems            : string[] = [];
  idItem                : string   = 'idLogBajaMedidor';
  listaLogBajaMedidores : LogBajaMedidorVO[] = [];
  countMedidor          : number = 0;  
  
  pageDTO!                    : PageDTO;
  LogBajaMedidorSeleccionado! : LogBajaMedidorVO;
  banderaUpdate               : boolean = false;
  formularioBajaMedidor!      : FormGroup
  
  userSession:User = this.aux;


  @ViewChild('medidoresDisponiblesComponent') medidoresDisponiblesComponent!  : ListaMedidoresSinAsignarASocioComponent;  
  @ViewChild('tablaBajaMedidor') tablaBajaMedidor! : TablePaginatorComponent;


  constructor(private fb                    : FormBuilder,
              private generalService        : GeneralService,
              private logBajaMedidorService : LogBajaMedidorService,
              private dialog                : MatDialog,
              private router                : Router) {     
    
  }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : 'apellido_paterno,apellido_materno,nombre',
    }
    this.userSession = this.generalService.getDatosDeUsuarioConSessionActiva();
    this.construirFormulario();
    this.configurarDatosTablaListaSocios();
    this.getCountLogBajaMedidoresService();
    this.getLogBajaMedidoresService(this.pageDTO);
  }

  private construirFormulario(){
    let nombreCompletoUsr:string = this.userSession.nombre + ' ' + this.userSession.apellidoPaterno + ' ' + this.userSession.apellidoMaterno;

    this.formularioBajaMedidor = this.fb.group({
      fechaRegistro     : [{value:this.generalService.getFechaActualFormateado(), disabled: true},[Validators.required],[]],
      responsableBaja   : [{value: nombreCompletoUsr, disabled: true},[Validators.required],[]],
      motivo            : ['',[Validators.required],[]],
      ubicacionDeRetiro : ['',[Validators.required],[]],
    });
  }

  private configurarDatosTablaListaSocios() : void {
    this.tituloItems  = ['#','Código','Nro. de Serie','Fecha Registro','Lectura Inicial','Lectura Actual','Motivo Baja','Ubicacion','Disponible','Estado'];
    this.campoItems   = ['codigo','nroSerie','fechaRegistro','lecturaInicial','lecturaActual','motivo','ubicacion','disponible','estado'];
  }

  public campoValido(campo:string):boolean{
    return this.formularioBajaMedidor.controls[campo].valid;
  }

  public campoInvalido(campo:string):boolean{
    return  this.formularioBajaMedidor.controls[campo].invalid &&
            this.formularioBajaMedidor.controls[campo].touched;
  }

  public async registrarBajaMedidor(){
    if(this.medidorSeleccionado==undefined){
      this.generalService.mensajeAlerta("Seleccione un medidor");
    }
    if(this.formularioBajaMedidor.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioBajaMedidor.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO BAJA MEDIDOR','¿Está seguro de dar de baja a este medidor?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let LogBajaMedidor:LogBajaMedidorDTO = this.getBajaMedidorDTO(this.banderaUpdate);
    this.createLogBajaMedidorService(LogBajaMedidor);
  }

  public async updateBajaMedidor(){
    if(this.formularioBajaMedidor.invalid){
      this.generalService.mensajeAlerta("Llene todos los campos");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR BAJA MEDIDOR','¿Está seguro de actualizar los datos de baja de este medidor?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
  
    let LogBajaMedidor:LogBajaMedidorDTO = this.getBajaMedidorDTO(this.banderaUpdate);
    this.updateLogBajaMedidorService(LogBajaMedidor);
  }
  

  public cancelarRegistroMedidor():void{
    this.resetFormulario();
  }

  private resetFormulario(){
    this.formularioBajaMedidor.reset();

    this.formularioBajaMedidor.get('responsableBaja')?.setValue('juan');
    this.formularioBajaMedidor.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());

    let aux:any=undefined;
    this.medidorSeleccionado = aux;

    this.banderaUpdate = false;    
    this.medidorSeleccionadoUpdate = aux;
  }

  private getBajaMedidorDTO(banderaUpdate:boolean):LogBajaMedidorDTO{
    let logBajaMedidor:LogBajaMedidorDTO = {
      idUsuario     : this.userSession.id,
      idMedidor     : (this.banderaUpdate)?this.medidorSeleccionadoUpdate.idMedidor:this.medidorSeleccionado.idMedidor,
      fechaRegistro : this.getValueFromFormulario('fechaRegistro'),
      motivo        : this.getValueFromFormulario('motivo'),
      ubicacion     : this.getValueFromFormulario('ubicacionDeRetiro')
    }

    if(banderaUpdate){
      logBajaMedidor.idLogBajaMedidor = this.medidorSeleccionadoUpdate.idLogBajaMedidor;
    }
    return logBajaMedidor;
  }

  private setValuesBajaMedidor():void{
    this.setValueToForm('fechaRegistro',this.medidorSeleccionadoUpdate.fechaRegistro);
    this.setValueToForm('responsableBaja','juan');
    this.setValueToForm('motivo',this.medidorSeleccionadoUpdate.motivo);
    this.setValueToForm('ubicacionDeRetiro',this.medidorSeleccionadoUpdate.ubicacion);
  }

  private setValueToForm(campo:string, value:any):void{
    this.formularioBajaMedidor.get(campo)?.setValue(value);
  }

  private getValueFromFormulario(campo:string):any{
    return this.formularioBajaMedidor.get(campo)?.value;
  }

  private limpiarFormulario(){
    this.formularioBajaMedidor.reset();
    this.formularioBajaMedidor.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());
    this.formularioBajaMedidor.get('responsableBaja')?.setValue('juan');
    this.banderaUpdate = false;
  }

  public genearteArchivoXLSX():void{
    this.tablaBajaMedidor.genearteArchivoXLSX("lista de socios por aportes eventual");
  }

  // -------------------- 
  // | Consumo API-REST |
  // --------------------
  private createLogBajaMedidorService(logBajaMedidorDTO:LogBajaMedidorDTO){
    this.logBajaMedidorService.createLogBajaMedidor(logBajaMedidorDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.medidoresDisponiblesComponent.reloadTabla();

        this.getCountLogBajaMedidoresService();
        this.getLogBajaMedidoresService(this.pageDTO);

        this.generalService.mensajeCorrecto("Se dio de baja de manera correcta","Exito");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private updateLogBajaMedidorService(logBajaMedidorDTO:LogBajaMedidorDTO){
    this.logBajaMedidorService.updateLogBajaMedidor(logBajaMedidorDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getLogBajaMedidoresService(this.pageDTO);
        this.generalService.mensajeCorrecto("Se actualizo de manera correcta","Exito :");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getCountLogBajaMedidoresService(){
    this.logBajaMedidorService.getCountLogBajaMedidores().subscribe(
      (resp)=>{
        this.countMedidor = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getLogBajaMedidoresService(pageDTO:PageDTO){
    this.logBajaMedidorService.getLogBajaMedidores(pageDTO).subscribe(
      (resp)=>{
        this.listaLogBajaMedidores = [];
        this.listaLogBajaMedidores = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // app-buscar-medidor events
  public seleccionarMedidorEvent(medidor:MedidorVO){
    this.medidorSeleccionado = medidor;    
  }

  // table events
  public newPageOrChangeSizePage(pageEvent:PageEvent):void{
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
    }
    this.getLogBajaMedidoresService(this.pageDTO);
  }

  public selectMedidorEvent(medidor:MedidorVO):void{
  }

  public updateMedidorEvent(medidor:LogBajaMedidorVO):void{
    this.medidorSeleccionadoUpdate = medidor;
    this.banderaUpdate = true;

    this.setValuesBajaMedidor();
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

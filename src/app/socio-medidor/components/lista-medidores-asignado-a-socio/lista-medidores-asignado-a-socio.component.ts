import { Component, OnInit,ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { TablePaginatorMultipleRowsV2Component } from 'src/app/shared/components/table-paginator-multiple-rows-v2/table-paginator-multiple-rows-v2.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { PageDTO } from '../../../shared/models/pageDTO';
import { SocioMedidorVO } from '../../models/socioMedidorVO';
import { SocioMedidorService } from '../../services/socio-medidor.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-lista-medidores-asignado-a-socio',
  templateUrl: './lista-medidores-asignado-a-socio.component.html',
  styleUrls: ['./lista-medidores-asignado-a-socio.component.css']
})
export class ListaMedidoresAsignadoASocioComponent implements OnInit {
  estadoSocio        : boolean = true;
  estadoSocioMedidor : boolean = true;
  estadoMedidor      : boolean = true;

  // tabla
  tituloItemsTabla     : string[] = [];
  campoItemsFiltrado   : string[] = [];
  campoSubItemsFiltrado: string[] = [];
  idItemFiltrado       : string = 'idSocio_i';
  idItem               : string = 'idSocio';
  countSocios          : number = 0;
  idCampoFilaDividida  : string = '';
  listaDeSociosMedidor : SocioMedidorVO[]=[];
  pageDTO!             : PageDTO;

  camposItem           : string[] = [];
  idCampoSubItem       : string = "medidores";
  camposSubItem        : string[] = [];
  
  formularioFiltro!  : FormGroup;

  @ViewChild('tablaMedidoresDeSocio') tablaMedidoresDeSocio! : TablePaginatorMultipleRowsV2Component;

  constructor(private fb                  : FormBuilder,
              private generalService      : GeneralService,
              private socioMedidorService : SocioMedidorService,
              private router              : Router) { }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : 'apellido_paterno,apellido_materno,nombre',
    }

    this.construirFormularioFiltro();
    this.configurarDatosTablaListaSocios();
    this.cambioBusquedaFiltroEvent('estadoSocio');
  }

  private construirFormularioFiltro(){
    this.formularioFiltro = this.fb.group({
      estadoSocio             : ['1',[Validators.required],[]],
      estadoAsignacionMedidor : ['1',[Validators.required],[]],
      estadoMedidor           : ['1',[Validators.required],[]],
    });
  }

  private configurarDatosTablaListaSocios() : void {
    this.tituloItemsTabla      = ['#','Cód. Socio','Soc. Activo','Nombre Completo','Dir. Socio','Cod. Medidor','Dir. Asig.','Cod. Cat','Categoria','Tanque','Fech. Asig','Med. Activo','Prop. Coop.'];
    
    this.camposItem          = ['idSocio','codigo','estado','nombreCompleto','direccion'];
    this.campoItemsFiltrado    = ['indexGlobal','codigo_i','estado_i','nombreCompleto_i','direccion_i'];
    
    this.camposSubItem       = ['codigo','direccionSM','codigoCategoria','nombreCategoria','nombreTanque','fechaAsignacionSM','estadoMedidor','propiedadCooperativa'];
    this.campoSubItemsFiltrado = ['codigo_si','direccionSM_si','codigoCategoria_si','nombreCategoria_si','nombreTanque_si','fechaAsignacionSM_si','estadoMedidor_si','propiedadCooperativa_si'];
  }

  public formularioFiltroValido(campo:string) : boolean{
    return this.formularioFiltro.controls[campo].valid
  }

  public formularioFiltroInvalido(campo:string) : boolean{   
    return this.formularioFiltro.controls[campo].invalid && 
           this.formularioFiltro.controls[campo].touched;
  }

  public getFormValue(campo:string){
    return this.formularioFiltro.get(campo).value;
  }

  // Event table
  public selectMedidor(socioMedidorVO : SocioMedidorVO){
    
  }

  public newPageOrChangeSizePage(page:PageEvent){
    this.pageDTO.page = page.pageIndex;
    this.pageDTO.size = page.pageSize;
    
    this.getMedidoresAsignadosASocio(this.estadoSocio,this.estadoSocioMedidor,this.estadoMedidor,this.pageDTO);
  }

  public busquedaPoCodigoDeSocio(textoBusqueda:string){
    this.tablaMedidoresDeSocio.buscarItem(textoBusqueda,'codigo_i');
  }

  public busquedaPoCodigoDeMedidor(textoBusqueda:string){
    this.tablaMedidoresDeSocio.buscarItem(textoBusqueda,'codigo_si');
  } 

  public cambioBusquedaFiltroEvent(opcion:string):void{
    if(this.getFormValue('estadoSocio')=='0' && opcion=='estadoSocio'){
      this.formularioFiltro.get('estadoAsignacionMedidor').disable();
      this.formularioFiltro.get('estadoAsignacionMedidor').setValue('0');
    }else if(this.getFormValue('estadoSocio')=='1' && opcion=='estadoSocio'){
      this.formularioFiltro.get('estadoAsignacionMedidor').enable();
      this.formularioFiltro.get('estadoAsignacionMedidor').setValue('1');
    }

    if(this.getFormValue('estadoAsignacionMedidor')=='1'){
      this.formularioFiltro.get('estadoMedidor').disable();
      this.formularioFiltro.get('estadoMedidor').setValue('1');
    }else if(this.getFormValue('estadoAsignacionMedidor')=='0'){
      this.formularioFiltro.get('estadoMedidor').enable();
    }
    
    this.estadoSocio = this.getFormValue('estadoSocio');
    this.estadoSocioMedidor = this.getFormValue('estadoAsignacionMedidor');
    this.estadoMedidor = this.getFormValue('estadoMedidor');

    this.getCountMedidoresAsignadosASocio(this.estadoMedidor,this.estadoSocioMedidor);    
  }

  public genearteArchivoXLSX():void{
    this.tablaMedidoresDeSocio.genearteArchivoXLSX("Lista de medidores asignados a socios");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  private getCountMedidoresAsignadosASocio(estadoSocio:boolean, estadoSocioMedidor:boolean){
    this.socioMedidorService.getCountMedidoresAsignadosASocios(estadoSocio,estadoSocioMedidor).subscribe(
      (resp)=>{
        if(resp){
          this.countSocios = resp.countItem;
          this.getMedidoresAsignadosASocio(this.estadoSocio,this.estadoSocioMedidor,this.estadoMedidor,this.pageDTO);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getMedidoresAsignadosASocio(estadoSocio:boolean, estadoSocioMedidor:boolean, estadoMedidor:boolean, pageDTO:PageDTO){
    this.socioMedidorService.getMedidoresAsignadosASocios(pageDTO,estadoSocio,estadoSocioMedidor,estadoMedidor).subscribe(
      (resp)=>{
        this.listaDeSociosMedidor = resp;
        if(this.listaDeSociosMedidor.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MedidorService } from '../../services/medidor.service';
import { MedidorVO } from '../../models/medidorVO';
import { CountItemVO } from 'src/app/shared/models/countItemVO';
import { PageEvent } from '@angular/material/paginator';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-lista-medidores',
  templateUrl: './lista-medidores.component.html',
  styleUrls: ['./lista-medidores.component.css']
})
export class ListaMedidoresComponent implements OnInit {
  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'idMedidor';
  countItems!         : CountItemVO;
  listaMedidores      : MedidorVO[] = [];
  pageDTO!            : PageDTO;

  formularioEstadoMedidor! : FormGroup;

  @ViewChild('tablaListaDeMedidores') tablaListaDeMedidores! : TablePaginatorComponent;

  constructor(private fb             : FormBuilder,
              private medidorService : MedidorService,
              private generalService : GeneralService,
              private router         : Router) { }
  
  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : '',
    }

    this.construirFormulario(); 
    this.configurarDatosTablaListaMedidores();  
  }

  private configurarDatosTablaListaMedidores() : void{
    this.tituloItems = ['#','Código','Nro. de Serie','Lec. Inicial','Lec. Actual','Estado','Disponible','Prop. Cooperativa',"Direccion"];
    this.campoItems = ['codigo','nroSerie','lecturaInicial','lecturaActual','estado','disponible','propiedadCooperativa','direccion'];
  } 

  public construirFormulario(){
    this.formularioEstadoMedidor = this.fb.group({
      estado     : ['none',[Validators.required],[]],
      disponible : ['none',[Validators.required],[]],
    });
  }

  public changeFiltroMedidor(){    
    let estadoMedidor:string = (this.formularioEstadoMedidor.get('estado').value);
    if(estadoMedidor=='1'){// medidor activo
      this.listaMedidores = [];
      this.formularioEstadoMedidor.get('disponible').enable();
      let disponibilidadMedidor:string = this.formularioEstadoMedidor.get('disponible').value;
      

      if(disponibilidadMedidor=='1'){ // medidores disponibles (no asignado)
        this.getCountMedidoresSinAsignarService();
        this.getMedidoresSinAsignarService(true,true,this.pageDTO);
      }else if(disponibilidadMedidor=='0'){ // no disponible (asignado)
        this.getCountMedidoresAsidnadosASociosService();
        this.getMedidoresAsignadosASociosService(this.pageDTO);
      }
    }else if(estadoMedidor=='0'){ // medidor dado de baja
      this.formularioEstadoMedidor.get('disponible').setValue('none');
      this.formularioEstadoMedidor.get('disponible').disable();
      this.getCountMedidoresPorSuEstadoService(false);
      this.getMedidoresPorSuEstadoService(false,this.pageDTO);
    }
  }

  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.changeFiltroMedidor();
  }

  public genearteArchivoXLSX():void{
    this.tablaListaDeMedidores.genearteArchivoXLSX("Lista de medidores");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  public getCountMedidoresSinAsignarService(estado:boolean=true,disponible:boolean=true){
    this.medidorService.getCountMedidoresSinAsignar(estado,disponible).subscribe(
      (resp)=>{
        this.countItems = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getMedidoresSinAsignarService(estado:boolean=true,disponible:boolean=true, pageDTO:PageDTO){
    return this.medidorService.getMedidoresSinAsignar(estado,disponible,pageDTO).subscribe(
      (resp)=>{
        this.listaMedidores = resp;
        if(this.listaMedidores.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountMedidoresAsidnadosASociosService(){
    this.medidorService.getCountMedidoresAsidnadosASocios().subscribe(
      (resp)=>{
        this.countItems = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getMedidoresAsignadosASociosService(pageDTO:PageDTO){
    return this.medidorService.getMedidoresAsignadosASocios(pageDTO).subscribe(
      (resp)=>{
        this.listaMedidores = resp;
        if(this.listaMedidores.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountMedidoresPorSuEstadoService(estado:boolean=true){
    this.medidorService.getCountMedidoresPorSuEstado(estado).subscribe(
      (resp)=>{
        this.countItems = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getMedidoresPorSuEstadoService(estado:boolean=true, pageDTO:PageDTO){
    return this.medidorService.getMedidoresPorSuEstado(estado,pageDTO).subscribe(
      (resp)=>{
        this.listaMedidores = resp;
        if(this.listaMedidores.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

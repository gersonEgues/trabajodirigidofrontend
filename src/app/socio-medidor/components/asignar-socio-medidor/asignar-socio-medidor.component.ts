import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BuscarSocioComponent } from 'src/app/shared/components/buscar-socio/buscar-socio.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MedidorAsignadoVO } from '../../models/medidorAsignadoVO';
import { MedidorVO } from '../../models/medidorVO';
import { SocioMedidorService } from '../../services/socio-medidor.service';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { InfoAsignarMedidorSocioDTO } from '../../models/InfoAsignarMedidorSocioDTO';
import { InfoBajaMedidorSocioDTO } from '../../models/InfoBajaMedidorSocioDTO';
import { InfoActualizarMedidorSocioDTO } from '../../models/InfoActualizarMedidorSocioDTO';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { CategoriaVO } from 'src/app/configuraciones/models/categoriaVO';
import { RegistroTanqueAguaService } from 'src/app/tanque-de-agua/services/registro-tanque-agua.service';
import { TanqueAguaVO } from '../../../tanque-de-agua/models/tanqueAguaVO';
import { ListaMedidoresSinAsignarASocioComponent } from '../lista-medidores-sin-asignar-a-socio/lista-medidores-sin-asignar-a-socio.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { SocioVO } from 'src/app/shared/models/socio-vo';

@Component({
  selector: 'app-asignar-socio-medidor',
  templateUrl: './asignar-socio-medidor.component.html',
  styleUrls: ['./asignar-socio-medidor.component.css']
})
export class AsignarSocioMedidorComponent implements OnInit {
  medidorSeleccionado!           : MedidorVO;
  medidorSeleccionadoActualizar! : MedidorAsignadoVO;
  medidorSeleccionadoDesasignar! : MedidorAsignadoVO;

  banderaGuardar       : boolean = true;
  banderaActualizar    : boolean = false;
  banderaDesasignar    : boolean = false;

  // Asignar Medidor
  formularioAsignarMedidor! : FormGroup;
  listaItemsCategoria! : CategoriaVO[];

  // Buscar Socio
  tituloItems          : string[] = [];
  campoItems           : string[] = [];
  socioSeleccionado!   : SocioVO;

  // Tabla para medidores de socio
  tituloItemsMedidor     : string[] = [];
  campoItemsMedidor      : string[] = [];
  idItemMedidor          : string = '';
  listaMedidoresDeSocio! : MedidorAsignadoVO[];

  listaTanqueAgua!       : TanqueAguaVO[];
  
  @ViewChild('buscarSocioComponent') buscarSocioComponent!                   : BuscarSocioComponent;
  @ViewChild('medidoresDisponiblesComponent') medidoresDisponiblesComponent! : ListaMedidoresSinAsignarASocioComponent;
  @ViewChild('medidoresDeSocioTable') medidoresDeSocioTable!                 : TablaComponent;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private socioMedidorService              : SocioMedidorService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private registroTanqueAguaService        : RegistroTanqueAguaService,
              private dialog                           : MatDialog,
              private router                           : Router) {
    this.configurarDatosComponenteBuscador();
    this.configurarDatosTablaMedidoresDeSocio();

    this.getListaCategoriasService();
    this.getListaTanqueAguaService();
  } 

  ngOnInit(): void {
    this.construirFormulario();
  }

  private construirFormulario(){
    this.formularioAsignarMedidor = this.fb.group({
      lecturaActual   : ['',[Validators.required],[]],
      fechaAsignacion : [{value:this.generalService.getFechaActualFormateado(), disabled: false},[Validators.required],[]],
      categoria       : ['0',[Validators.required],[]],
      tanqueAgua      : ['0',[Validators.required],[]],
      direccion       : ['',[Validators.required],[]],
    });
  }

  public campoValido(campo:string):boolean{
    return this.formularioAsignarMedidor.controls[campo].valid &&
           this.formularioAsignarMedidor.controls[campo].value!="0";
  }

  public campoInvalido(campo:string):boolean{
    return  this.formularioAsignarMedidor.controls[campo].invalid &&
            this.formularioAsignarMedidor.controls[campo].touched;
  }

  private validarSemanticaFormulario():boolean{
    let bandera:boolean = false;
    

    if(this.banderaGuardar && this.invalidoSocioMediorParaAsignar()){
      this.generalService.mensajeAlerta("Tiene que seleccionar un socio y un medidor disponible");
      bandera = true;
    }

    if(this.banderaActualizar && this.invalidoSocioMediorParaActualizar()){
      this.generalService.mensajeAlerta("Tiene que seleccionar un socio y su medidor para poder actualizar");
      bandera = true;
    }

    if(this.banderaDesasignar && this.invalidoSocioMediorParaDesasignar()){
      this.generalService.mensajeAlerta("Tiene que seleccionar un socio y su medidor para poder desasignar");
      bandera = true;
    }

    if(this.formularioAsignarMedidor.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioAsignarMedidor.markAllAsTouched();
      bandera = true;
    }

    return bandera;
  }

  private invalidoSocioMediorParaAsignar():boolean{
    return this.socioSeleccionado==undefined || this.medidorSeleccionado==undefined;
  }

  private invalidoSocioMediorParaActualizar():boolean{
    return this.socioSeleccionado==undefined || this.medidorSeleccionadoActualizar==undefined;
  }

  private invalidoSocioMediorParaDesasignar():boolean{
    return this.socioSeleccionado==undefined || this.medidorSeleccionadoDesasignar==undefined;
  }

  private getInfoAsignarMedidorSocioDTO():InfoAsignarMedidorSocioDTO{
    let infoAsignarMedidorSocioDTO:InfoAsignarMedidorSocioDTO = {
      idSocio         : this.socioSeleccionado.idSocio,
      idMedidor       : this.medidorSeleccionado.idMedidor,
      idCategoria     : this.formularioAsignarMedidor.get('categoria')?.value,
      idTanqueAgua    : this.formularioAsignarMedidor.get('tanqueAgua')?.value,
      fechaAsignacion : this.formularioAsignarMedidor.get('fechaAsignacion')?.value,
      lecturaInicial  : this.formularioAsignarMedidor.get('lecturaActual')?.value,
      lecturaActual   : this.formularioAsignarMedidor.get('lecturaActual')?.value,
      estado          : true,
      direccion       : this.formularioAsignarMedidor.get('direccion')?.value,
    }
    return infoAsignarMedidorSocioDTO;
  }

  private getInfoActualizarMedidorSocioDTO():InfoActualizarMedidorSocioDTO{
    let infoActualizarMedidorSocioDTO : InfoActualizarMedidorSocioDTO = {
      idSocioMedidor  : this.medidorSeleccionadoActualizar.idSocioMedidor,
      idSocio         : this.socioSeleccionado.idSocio,
      idCategoria     : this.formularioAsignarMedidor.get('categoria')?.value,
      idTanqueAgua    : this.formularioAsignarMedidor.get('tanqueAgua')?.value,
      lecturaInicial  : this.formularioAsignarMedidor.get('lecturaActual')?.value,
      lecturaActual   : this.formularioAsignarMedidor.get('lecturaActual')?.value,
      direccion       : this.formularioAsignarMedidor.get('direccion')?.value,
    }
    return infoActualizarMedidorSocioDTO;
  }

  private getInfoDesasignarMedidorSocioDTO():InfoBajaMedidorSocioDTO{
    let infoBajaMedidorSocioDTO:InfoBajaMedidorSocioDTO = {
      idSocioMedidor : this.medidorSeleccionadoDesasignar.idSocioMedidor,
      idSocio        : this.medidorSeleccionadoDesasignar.idSocio,
      idMedidor      : this.medidorSeleccionadoDesasignar.idMedidor,
      fecha          : this.formularioAsignarMedidor.get('fechaAsignacion')?.value,
      motivo         : this.formularioAsignarMedidor.get('direccion')?.value,
    }
    return infoBajaMedidorSocioDTO;
  }

  private setFormularioParaGuardar(){
    this.formularioAsignarMedidor.reset();

    this.formularioAsignarMedidor.get('lecturaActual')?.enable();
    this.formularioAsignarMedidor.get('lecturaActual')?.setValue('');
    this.formularioAsignarMedidor.get('categoria')?.setValue('0');
    this.formularioAsignarMedidor.get('tanqueAgua')?.setValue('0');
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(this.generalService.getFechaActualFormateado());
    this.formularioAsignarMedidor.get('direccion')?.setValue('');
  }

  private setMedidorSocioEnFormularioParaEditar(medidor:MedidorAsignadoVO){
    this.formularioAsignarMedidor.reset();

    this.formularioAsignarMedidor.get('lecturaActual')?.enable();
    this.formularioAsignarMedidor.get('lecturaActual')?.setValue(medidor.lecturaActual);

    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(medidor.fechaAsignacionSM);
    
    this.formularioAsignarMedidor.get('categoria')?.enable();
    this.formularioAsignarMedidor.get('categoria')?.setValue(medidor.idCategoria);
    
    this.formularioAsignarMedidor.get('tanqueAgua')?.enable();
    this.formularioAsignarMedidor.get('tanqueAgua')?.setValue(medidor.idTanqueAgua);
    
    this.formularioAsignarMedidor.get('direccion')?.setValue(medidor.direccionSM);    
  }

  private setMedidorSocioEnFormularioParaDesasignar(medidor:MedidorAsignadoVO){
    this.formularioAsignarMedidor.reset();
    
    this.formularioAsignarMedidor.get('lecturaActual')?.setValue(''); 
    this.formularioAsignarMedidor.get('lecturaActual')?.disable();    
    
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue('');
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(this.generalService.getFechaActualFormateado());
    
    this.formularioAsignarMedidor.get('categoria')?.setValue(''); 
    this.formularioAsignarMedidor.get('categoria')?.disable();    
    
    this.formularioAsignarMedidor.get('tanqueAgua')?.setValue(''); 
    this.formularioAsignarMedidor.get('tanqueAgua')?.disable();    
    
    this.formularioAsignarMedidor.get('direccion')?.setValue('');
  }

  public async guardarRegistroSocioMedidor(){
    let bandera:boolean =  this.validarSemanticaFormulario();
    if(bandera){ return; }

    let nombreCompleto : string = this.socioSeleccionado.nombre + ' ' + this.socioSeleccionado.apellidoPaterno + ' ' + this.socioSeleccionado.apellidoMaterno; 
    let codigoMedidor  : string = this.medidorSeleccionado.codigo;
    let afirmativo = await this.getConfirmacion('ASIGNAR MEDIDOR',`¿Está seguro asignar a  ${nombreCompleto.toUpperCase()} el medidor con codigo : ${codigoMedidor} ?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ return; }

    let infoAsignarMedidorSocioDTO:InfoAsignarMedidorSocioDTO = this.getInfoAsignarMedidorSocioDTO();
    this.asignarMedidorSocioService(infoAsignarMedidorSocioDTO);
  }

  public async guardarActualizarMedidor(){
    let bandera:boolean =  this.validarSemanticaFormulario();
    if(bandera){ return; }

    let afirmativo = await this.getConfirmacion('ACTUALIZAR ASIGNACIÓN DE MEDIDOR','¿Está seguro de actualizar los datos de asigación?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ return; }

    let infoActualizarMedidorSocioDTO : InfoActualizarMedidorSocioDTO = this.getInfoActualizarMedidorSocioDTO();
    this.actualizarMedidorSocioService(infoActualizarMedidorSocioDTO);
  }

  public async guardarDesasignarMedidor(){
    let bandera:boolean =  this.validarSemanticaFormulario();
    if(bandera){ return; }

    let afirmativo:any = await this.getConfirmacion('DESASIGNAR MEDIDOR','¿Está seguro de desasignar este medidor al socio?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ return; }

    let infoBajaMedidorSocioDTO : InfoBajaMedidorSocioDTO = this.getInfoDesasignarMedidorSocioDTO();
    this.desasignarAsignacionDeMedidorService(infoBajaMedidorSocioDTO);

  }

  public cancelarRegistroSocioMedidor(){
    this.limpiarTodosLosFormulariosYBusquedas();
  }

  private limpiarTodosLosFormulariosYBusquedas():void{
    let aux:any = undefined;
    this.formularioAsignarMedidor.reset();

    this.banderaGuardar = true;
    this.banderaActualizar = this.banderaDesasignar = false;

    this.socioSeleccionado = this.medidorSeleccionado = this.medidorSeleccionadoActualizar = this.medidorSeleccionadoDesasignar = aux;
    
    this.buscarSocioComponent.limpiarBusqueda();

    this.medidoresDisponiblesComponent.limpiarTabla();
    this.medidoresDisponiblesComponent.reloadTabla();

    this.formularioAsignarMedidor.get('categoria')?.setValue('0');
    this.formularioAsignarMedidor.get('tanqueAgua')?.setValue('0');
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(this.generalService.getFechaActualFormateado());
  }

  private limpiarFormularioRegistro():void{
    let aux:any = undefined;
    this.formularioAsignarMedidor.reset();
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(this.generalService.getFechaActualFormateado());

    this.banderaGuardar = true;
    this.banderaActualizar = this.banderaDesasignar = false;

    this.socioSeleccionado = this.medidorSeleccionado = this.medidorSeleccionadoActualizar = this.medidorSeleccionadoDesasignar = aux;
    
    this.formularioAsignarMedidor.get('lecturaActual')?.enable();
    this.formularioAsignarMedidor.get('categoria')?.enable();
    this.formularioAsignarMedidor.get('tanqueAgua')?.enable();

    this.formularioAsignarMedidor.get('categoria')?.setValue('0');
    this.formularioAsignarMedidor.get('tanqueAgua')?.setValue('0');
    this.formularioAsignarMedidor.get('fechaAsignacion')?.setValue(this.generalService.getFechaActualFormateado());

    this.medidoresDisponiblesComponent.limpiarTabla();
  }
   
  
  // Buscador
  private configurarDatosComponenteBuscador(){
    this.tituloItems = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Telefono'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','telefono'];
  }

  // Tabla medidores de socio
  private configurarDatosTablaMedidoresDeSocio(){
    this.tituloItemsMedidor = ['#','Código','L. Inicial','Fecha asignacion','Direccion','Prop. Coope.','Categoria','Tanque'];
    this.campoItemsMedidor = ['codigo','lecturaInicial','fechaAsignacionSM','direccionSM','propiedadCooperativa','nombreCategoria','nombreTanque'];
    this.idItemMedidor = 'idSocioMedidor';
  }

  // events - buscador medidor - medidores disponibles para asignar
  public socioSeleccionadoEvent(socio:SocioVO){
    this.socioSeleccionado = socio;
    this.getMedidorDeSocioService(socio.idSocio);
  }

  public nuevaBusquedaBuscadorSocio(busqueda:string){ 
    let aux:any = undefined;
    this.socioSeleccionado = aux;
    this.medidoresDeSocioTable.limpiarListaItems();
    this.limpiarFormularioRegistro();
  }

  // events tabla medidor de socio
  public actualizarAsignacionDeMedidorEvent(medidor:MedidorAsignadoVO):void{
    let aux:any = undefined;
    this.medidorSeleccionadoDesasignar = this.medidorSeleccionado =  aux;

    this.medidorSeleccionadoActualizar =  medidor;
    this.setMedidorSocioEnFormularioParaEditar(medidor);    
    this.banderaActualizar = true;
    this.banderaDesasignar = this.banderaGuardar = false; 
  }

  public desasignarAsignacionDeMedidorEvent(medidor:MedidorAsignadoVO):void{
    this.banderaDesasignar = true;
    this.banderaActualizar = this.banderaGuardar = false; 
    
    let aux:any = undefined;
    this.medidorSeleccionadoActualizar = this.medidorSeleccionado =  aux;

    this.medidorSeleccionadoDesasignar =  medidor;
    this.setMedidorSocioEnFormularioParaDesasignar(medidor);
  }

  // events - medidor disponible
  public seleccionarMedidorEvent(medidor:MedidorVO){
    let aux:any = undefined;
    this.medidorSeleccionadoActualizar = this.medidorSeleccionadoDesasignar =  aux;

    this.medidorSeleccionado = medidor;
    this.setFormularioParaGuardar();
    this.banderaGuardar = true;
    this.banderaActualizar = this.banderaDesasignar = false;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private asignarMedidorSocioService(infoAsignarMedidorSocioDTO:InfoAsignarMedidorSocioDTO):void{
    this.socioMedidorService.asignarMedidorSocio(infoAsignarMedidorSocioDTO).subscribe(
      (resp:MedidorAsignadoVO)=>{        
        this.generalService.mensajeCorrecto("Se asigno de manera correcta el medidor al socio.","Exito");
        this.limpiarTodosLosFormulariosYBusquedas();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private actualizarMedidorSocioService(infoActualizarMedidorSocioDTO:InfoActualizarMedidorSocioDTO):void{
    this.socioMedidorService.actualizarMedidorSocio(infoActualizarMedidorSocioDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo correctamente","Exito :");
        this.limpiarTodosLosFormulariosYBusquedas();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private desasignarAsignacionDeMedidorService(infoBajaMedidorSocioDTO:InfoBajaMedidorSocioDTO):void{
    this.socioMedidorService.desasignarMedidorSocio(infoBajaMedidorSocioDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se desasigno de manera correcta el medidor del socio","Exito :");
        this.limpiarTodosLosFormulariosYBusquedas();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  private getMedidorDeSocioService(idSocio:number):void{
    this.socioMedidorService.getMedidorerPorIdDeSocio(idSocio).subscribe(
      (resp:MedidorAsignadoVO[])=>{
        this.listaMedidoresDeSocio = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  private getListaCategoriasService(){
    this.mesAnioCategoriaCostoAguaService.getCategorias().subscribe(
      (resp)=>{
        this.listaItemsCategoria = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getListaTanqueAguaService(){
    this.registroTanqueAguaService.getListaTanqueAgua(true).subscribe(
      (resp)=>{
        this.listaTanqueAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  confirm dialog
  private async getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string){
    let bandera:boolean = false;
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    let resp:any = await dialogRef.afterClosed().toPromise()
      .then(resp=>{
        bandera = (resp)?true:false;
      })
      .catch(err=>{
        bandera = false;
    });
    return bandera;
  }
}

import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { GeneralService } from 'src/app/shared/services/general.service';
import { PageDTO } from '../../../shared/models/pageDTO';
import { MedidorService } from '../../services/medidor.service';
import { MedidorVO } from '../../models/medidorVO';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-lista-medidores-sin-asignar-a-socio',
  templateUrl: './lista-medidores-sin-asignar-a-socio.component.html',
  styleUrls: ['./lista-medidores-sin-asignar-a-socio.component.css']
})
export class ListaMedidoresSinAsignarASocioComponent implements OnInit {
  // tabla
  tituloItems        : string[] = [];
  campoItems         : string[] = [];
  idItem             : string = 'idMedidor';
  listaMedidores     : MedidorVO[]=[];
  countMedidor       : number = 0;
  pageDTO!           : PageDTO;

  // No asignado aun a un socio
  medidorDisponible        : boolean = true;
  // Medidor aun activo
  estadoMedidor     : boolean = true;

  @Output('seleccionarMedidor') seleccionarMedidor   : EventEmitter<any> = new EventEmitter();
  @ViewChild('tablaMedidoresComponent') tablaMedidoresComponent! : TablePaginatorComponent;

  constructor(private generalService : GeneralService,
              private medidorService : MedidorService,
              private router         : Router) { }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : 'codigo'
    }

    this.configurarDatosTablaListaSocios();
    this.getCountMedidoresSinAsignarService(this.medidorDisponible,this.estadoMedidor);
    this.getMedidoresSinAsignarService(this.medidorDisponible,this.estadoMedidor,this.pageDTO);
  }

  private configurarDatosTablaListaSocios() : void {
    this.tituloItems  = ['#','Código','Nro. de Serie','Fecha Registro','Lectura Inicial','Lectura Actual','Direccion','Disponible','Estado'];
    this.campoItems   = ['codigo','nroSerie','fechaRegistro','lecturaInicial','lecturaActual','direccion','disponible','estado'];
  }

  public selecionarMedidor(medidor:MedidorVO){
    this.seleccionarMedidor.emit(medidor);  
  }

  public newPageOrChangeSizePage(page:PageEvent){
    this.pageDTO.page = page.pageIndex;
    this.pageDTO.size = page.pageSize;    
    this.getMedidoresSinAsignarService(this.medidorDisponible,this.estadoMedidor,this.pageDTO);
  }

  public reloadTabla(){
    this.getMedidoresSinAsignarService(this.medidorDisponible,this.estadoMedidor,this.pageDTO);
  }

  public limpiarTabla(){
    this.tablaMedidoresComponent.deseleccionarItems();
  }

  // input-buscador
  public buscarMedidores(texto:string){
    if(texto=='' || texto==undefined || texto==null){
      this.limpiarTabla();
      return;
    }

    this.tablaMedidoresComponent.buscarItem(texto,'codigo');
  }

  // Consumo API-REST
  private getCountMedidoresSinAsignarService(estadoSocio:boolean, estadoSocioMedidor:boolean){
    this.medidorService.getCountMedidoresSinAsignar(estadoSocio,estadoSocioMedidor).subscribe(
      (resp)=>{
        this.countMedidor = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getMedidoresSinAsignarService(estadoSocio:boolean, estadoSocioMedidor:boolean, pageDTO:PageDTO){
    this.medidorService.getMedidoresSinAsignar(estadoSocio,estadoSocioMedidor,pageDTO).subscribe(
      (resp)=>{
        this.listaMedidores = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

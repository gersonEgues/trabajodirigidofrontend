import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { InputFormComponent } from 'src/app/shared/components/input-form/input-form.component';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MedidorAsignadoVO } from '../../models/medidorAsignadoVO';
import { SocioMedidorVO } from '../../models/socioMedidorVO';
import { SocioMedidorService } from '../../services/socio-medidor.service';
import { TablePaginatorMultipleRowsV2Component } from 'src/app/shared/components/table-paginator-multiple-rows-v2/table-paginator-multiple-rows-v2.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-medidor-de-socio',
  templateUrl: './medidor-de-socio.component.html',
  styleUrls: ['./medidor-de-socio.component.css']
})
export class MedidorDeSocioComponent implements OnInit {
  @Input('opcionImprimir') opcionImprimir : boolean = true;

  // tabla
  tituloItems!  : string[];
  campoItems!   : string[];
  //idItem!       : string;
  listaItems!   : MedidorAsignadoVO[];


  // tabla
  tituloItemsTabla     : string[] = [];
  campoItemsFiltrado   : string[] = [];
  campoSubItemsFiltrado: string[] = [];
  idItemFiltrado       : string = 'idSocio_i';
  idItem               : string = 'idSocio';
  countSocios          : number = 0;
  idCampoFilaDividida  : string = '';
  listaDeSociosMedidor : SocioMedidorVO[]=[];
  pageDTO!             : PageDTO;
  
  camposItem           : string[] = [];
  idCampoSubItem       : string = "medidores";
  camposSubItem        : string[] = [];

  @ViewChild('inputFormComponentCodigoSocio') inputFormComponentCodigoSocio! : InputFormComponent;
  @ViewChild('tablaListaSocios') tablaListaSocios!                           : TablePaginatorMultipleRowsV2Component;

  constructor(private generalService      : GeneralService,
              private socioMedidorService : SocioMedidorService,
              private router              : Router) { 
  }

  ngOnInit(): void {
    this.configurarDatosTablaListaSocios();
  }

  private configurarDatosTablaListaSocios() : void {
    this.tituloItemsTabla      = ['#','Cód. Socio','Soc. Activo','Nombre Completo','Dir. Socio','Cod. Medidor','Cod. Cat','Categoria','Lec. Inicial','Lec. Act.','Tanque','Asig. Activa','Dir. Asig.','Med. Activo','Dir. Medidor','Fecha asignacion ','Prop. Coop.'];
    
    this.camposItem            = ['idSocio','codigo','estado','nombreCompleto','direccion'];
    this.campoItemsFiltrado    = ['indexGlobal','codigo_i','estado_i','nombreCompleto_i','direccion_i'];
    
    this.camposSubItem         = ['codigo','codigoCategoria','nombreCategoria','lecturaInicial','lecturaActual','nombreTanque','estadoSocioMedidor','direccionSM','idMedidor','propiedadCooperativa','fechaAsignacionSM','estadoMedidor','direccion'];
    this.campoSubItemsFiltrado = ['codigo_si','codigoCategoria_si','nombreCategoria_si','lecturaInicial_si','lecturaActual_si','nombreTanque_si','estadoSocioMedidor_si','direccionSM_si','estadoMedidor_si','direccion_si','fechaAsignacionSM_si','propiedadCooperativa_si'];
  }

  // event buscador
  public busquedaPorCodigoDeSocio(textoBusqueda:string){
    if(textoBusqueda=="" || textoBusqueda==null || textoBusqueda==undefined || textoBusqueda.trim()==''){
      this.tablaListaSocios.limpiarItemsDeTabla();      
    }else{
      this.buscarMedidoresPorCodigoDeSocioService(textoBusqueda);
      this.tablaListaSocios.deseleccionarItem();
    }
  }

  // consumo API-REST
  private buscarMedidoresPorCodigoDeSocioService(codigoSocio:string):void{
    this.socioMedidorService.getMedidorerPorCodigoDeSocio(codigoSocio).subscribe(
      (resp)=>{        
        this.listaDeSociosMedidor = resp;
        if(this.listaDeSociosMedidor.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },  
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

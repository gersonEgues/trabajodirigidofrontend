import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  imagePaths:string[];
  

  constructor() { }

  ngOnInit(): void {
    this.buildImagePaths();
  }

  private buildImagePaths(){
    this.imagePaths = ['./assets/img1.jpg','./assets/img2.jpeg','./assets/img3.jpg'];
  }

  public girarIzquierda(){
    let aux:string = this.imagePaths[0];
    for (let i = 0; i<this.imagePaths.length-1 ; i++) {
      this.imagePaths[i] = this.imagePaths[i+1];
    }
    this.imagePaths[2] = aux;   
  }

  public girarDerecha(){
    let aux:string = this.imagePaths[2];
    for (let i = this.imagePaths.length-1; i>0 ; i--) {
      this.imagePaths[i] = this.imagePaths[i-1];
    }
    this.imagePaths[0] = aux;    
  }
}

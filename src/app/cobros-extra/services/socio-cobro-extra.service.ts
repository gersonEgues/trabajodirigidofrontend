import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { SocioCobroExtra } from '../models/socioCobroExtra';

@Injectable({
  providedIn: 'root'
})
export class SocioCobroExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/socio-cobro-extra",http);
  }

  public createSocioCobroExtraTodos(idPeriodoCobroExtra:number): Observable<BanderaResponseVO>{
    return this.http.post<BanderaResponseVO>(`${this.url}/create-todos/${idPeriodoCobroExtra}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public deleteSocioCobroExtraTodos(idPeriodoCobroExtra:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/todos/${idPeriodoCobroExtra}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }


  public createSocioCobroExtra(idPeriodoCobroExtra:number,idSocioMedidor:number): Observable<SocioCobroExtra>{
    return this.http.post<SocioCobroExtra>(`${this.url}/${idPeriodoCobroExtra}/${idSocioMedidor}`,null).pipe(
      map(response => {
        return response as SocioCobroExtra;
      })
    );
  }
  
  public deleteSocioCobroExtra(idPeriodoCobroExtra:number,idSocioMedidor:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idPeriodoCobroExtra}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

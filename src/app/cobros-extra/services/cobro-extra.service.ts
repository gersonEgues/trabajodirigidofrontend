import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CobroExtraDTO } from '../models/cobroExtraDTO';
import { CobroExtraVO } from '../models/cobroExtraVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';

@Injectable({
  providedIn: 'root'
})
export class CobroExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/cobro-extra",http);
  }

  public getCobroExtra(idCobroExtra:number): Observable<CobroExtraVO>{
    return this.http.get<CobroExtraVO>(`${this.url}/${idCobroExtra}`).pipe(
      map(response => {
        return response as CobroExtraVO;
      })
    );
  }

  public getListaCobroExtra(bandera:boolean): Observable<CobroExtraVO[]>{
    return this.http.get<CobroExtraVO[]>(`${this.url}/lista/${bandera}`).pipe(
      map(response => {
        return response as CobroExtraVO[];
      })
    );
  }

  public getListaCobroExtraDePeriodo(ideriodo:number,todos:boolean): Observable<CobroExtraVO[]>{
    return this.http.get<CobroExtraVO[]>(`${this.url}/lista/${ideriodo}/${todos}`).pipe(
      map(response => {
        return response as CobroExtraVO[];
      })
    );
  }

  public createCobroExtra(cobroExtraDTO:CobroExtraDTO): Observable<CobroExtraVO>{
    return this.http.post<CobroExtraVO>(`${this.url}`,cobroExtraDTO).pipe(
      map(response => {
        return response as CobroExtraVO;
      })
    );
  }

  public updateCobroExtra(cobroExtraVO:CobroExtraVO ): Observable<CobroExtraVO>{
    return this.http.put<CobroExtraVO>(`${this.url}`,cobroExtraVO).pipe(
      map(response => {
        return response as CobroExtraVO;
      })
    );
  }

  public habilitarCobroExtra(idCobroExtra:number): Observable<CobroExtraVO>{
    return this.http.put<CobroExtraVO>(`${this.url}/habilitar/${idCobroExtra}`,null).pipe(
      map(response => {
        return response as CobroExtraVO;
      })
    );
  }

  public inhabilitarCobroExtra(idCobroExtra:number): Observable<CobroExtraVO>{
    return this.http.put<CobroExtraVO>(`${this.url}/inhabilitar/${idCobroExtra}`,null).pipe(
      map(response => {
        return response as CobroExtraVO;
      })
    );
  }

  public deleteCobroExtra(idCobroExtra:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idCobroExtra}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

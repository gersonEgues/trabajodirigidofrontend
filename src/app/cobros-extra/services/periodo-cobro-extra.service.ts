import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PeriodoCobroExtraDataVO } from '../models/periodoCobroExtraDataVO';
import { PeriodoCobroExtraVO } from '../models/periodoCobroExtraVO';
import { PeriodoCobroExtraDTO } from '../models/periodoCobroExtraDTO';
import { PeriodoDataVO } from '../models/periodoDataVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';



@Injectable({
  providedIn: 'root'
})
export class PeriodoCobroExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/periodo-cobro-extra",http);
  }

  public getListaPeriodoCobroExtra(idAnio:number,idCobroExtra:number): Observable<PeriodoCobroExtraDataVO[]>{
    return this.http.get<PeriodoCobroExtraDataVO[]>(`${this.url}/lista/${idAnio}/${idCobroExtra}`).pipe(
      map(response => {
        return response as PeriodoCobroExtraDataVO[];
      })
    );
  }

  public getPeriodoCobroExtra(idPeriodoCobroExtra:number): Observable<PeriodoCobroExtraVO>{
    return this.http.get<PeriodoCobroExtraVO>(`${this.url}/${idPeriodoCobroExtra}`).pipe(
      map(response => {
        return response as PeriodoCobroExtraVO;
      })
    );
  }

  public createPeriodoCobroExtra(periodoCobroExtraDTO:PeriodoCobroExtraDTO): Observable<PeriodoCobroExtraVO>{
    return this.http.post<PeriodoCobroExtraVO>(`${this.url}`,periodoCobroExtraDTO).pipe(
      map(response => {
        return response as PeriodoCobroExtraVO;
      })
    );
  }

  public updatePeriodoCobroExtra(periodoCobroExtraVO:PeriodoCobroExtraVO): Observable<PeriodoCobroExtraVO>{
    return this.http.put<PeriodoCobroExtraVO>(`${this.url}`,periodoCobroExtraVO ).pipe(
      map(response => {
        return response as PeriodoCobroExtraVO;
      })
    );
  }

  public getPeriodosConSusCobrosExtraDeGestion(idGestion:number): Observable<PeriodoDataVO[]>{
    return this.http.get<PeriodoDataVO[]>(`${this.url}/gestion/${idGestion}`).pipe(
      map(response => {
        return response as PeriodoDataVO[];
      })
    );
  }

  public deletePeriodoCobroExtra(idPeriodoCobroExtra:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idPeriodoCobroExtra}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

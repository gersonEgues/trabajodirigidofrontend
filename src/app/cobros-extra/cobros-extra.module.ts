import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobrosExtraRoutingModule } from './cobros-extra-routing.module';
import { TipoCobroExtraComponent } from './components/tipo-cobro-extra/tipo-cobro-extra.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PeriodoCobroExtraComponent } from './components/periodo-cobro-extra/periodo-cobro-extra.component';
import { VistaPeriodoConCobrosExtraComponent } from './components/vista-periodo-con-cobros-extra/vista-periodo-con-cobros-extra.component';
import { AsignarCobroExtraASocioComponent } from './components/asignar-cobro-extra-a-socio/asignar-cobro-extra-a-socio.component';


@NgModule({
  declarations: [
    TipoCobroExtraComponent,
    PeriodoCobroExtraComponent,
    VistaPeriodoConCobrosExtraComponent,
    AsignarCobroExtraASocioComponent
  ],
  imports: [
    CommonModule,
    CobrosExtraRoutingModule,
    SharedModule,
    AngularMaterialModule, 
    FormsModule,
    ReactiveFormsModule,
    
  ]
})
export class CobrosExtraModule { }

import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { CobroExtraService } from '../../services/cobro-extra.service';
import { MesAnioCategoriaCostoAguaService } from '../../../configuraciones/services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from '../../../configuraciones/models/anioVO';
import { CobroExtraVO } from '../../models/cobroExtraVO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PeriodoDeAnioVO } from 'src/app/configuraciones/models/periodoDeAnioVO';
import { SocioMedidorService } from '../../../socio-medidor/services/socio-medidor.service';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { SocioMedidorVO } from 'src/app/socio-medidor/models/socioMedidorVO';
import { SocioMedidorTratadoVO } from 'src/app/socio-medidor/models/socioMedidorTratado';
import { MedidorAsignadoVO } from 'src/app/socio-medidor/models/medidorAsignadoVO';
import { SocioCobroExtraService } from '../../services/socio-cobro-extra.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-asignar-cobro-extra-a-socio',
  templateUrl: './asignar-cobro-extra-a-socio.component.html',
  styleUrls: ['./asignar-cobro-extra-a-socio.component.css']
})
export class AsignarCobroExtraASocioComponent implements OnInit {
  aux:any = undefined;
  listaTituloPeriodoCobroExtra : string[];

  lengthList                    : number = 0;
  pageSize                      : number = 0;
  pageSizeOptions               : number[] = [10,20,50,100]

  
  formularioSeleccionPeriodoCobroExtra! : FormGroup;
  formularioBusqueda!                   : FormGroup;

  listaGestiones         : AnioVO[]=[];
  listaPeriodosDeGestion : PeriodoDeAnioVO[]=[];
  listaCobroExtra        : CobroExtraVO[]=[];

  countSocios                 : number = 0;
  listaDeSociosMedidor        : SocioMedidorVO[]=[];
  listaDeSociosMedidorTratado : SocioMedidorTratadoVO[]=[];
  pageDTO!                    : PageDTO;

  listaTituloItems       : string[];
  listaCampoItemsSocio   : string[];
  listaCampoItemsMedidor : string[];

  constructor(private fb                               : FormBuilder,
              private cobroExtraService                : CobroExtraService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private generalService                   : GeneralService,
              private socioMedidorService              : SocioMedidorService,
              private socioCobroExtraService           : SocioCobroExtraService,
              private dialog                           : MatDialog,
              private router                           : Router) { }

  ngOnInit(): void {
    this.getAniosService();
    this.construirFormularioSelccionPeriodoCobroExtra();
    this.construirTituloTabla();

    this.pageDTO = {
      page : 0,
      size : 10,
    }  
  }

  // tabla periodo cobro extra
  private construirFormularioSelccionPeriodoCobroExtra(){
    this.formularioSeleccionPeriodoCobroExtra = this.fb.group({
      gestion    :  [ '0', [Validators.required],[]],
      periodo    :  [ '0', [Validators.required],[]],
      cobroExtra :  [ '0', [Validators.required],[]],
    });

    this.formularioBusqueda = this.fb.group({
      codigo  :  ['', [],[]],
    });
  }

  public campoValidoSeleccionPeriodoCobroExtra(campo:string):boolean{
    return  this.formularioSeleccionPeriodoCobroExtra.get(campo)!.valid && 
            this.formularioSeleccionPeriodoCobroExtra.get(campo)!.value!='0';
  }

  public campoInvalidoSeleccionPeriodoCobroExtra(campo:string):boolean{
    return  this.formularioSeleccionPeriodoCobroExtra.get(campo)!.touched && 
            this.formularioSeleccionPeriodoCobroExtra.get(campo)!.value == '0';
  }

  public changeSelectorGestionMesTipoCobroExtra(opcion:string){
    let idGestion    : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('gestion')?.value);
    this.listaDeSociosMedidorTratado = [];
    this.listaDeSociosMedidor = [];

    if(idGestion!=0 && opcion=='gestion'){
      this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').reset();
      this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').setValue('0');
      this.formularioSeleccionPeriodoCobroExtra.get('periodo').reset();
      this.formularioSeleccionPeriodoCobroExtra.get('periodo').setValue('0');
      this.getPeriodosDeAnioService(idGestion);
    }

    let idPeriodo    : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('periodo')?.value);
    if(idGestion!=0 && idPeriodo!=0 && opcion=='periodo'){
      this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').reset();
      this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').setValue('0');
      this.getListaCobroExtraDePeriodoService(idPeriodo,false);
    }

    let idCobroExtra : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra')?.value);
    if(idGestion!=0 && idPeriodo!=0 && idCobroExtra!=0){         
      let idCobroExtra : number = (Number)(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').value);
      this.getSociosConSusMedidoresParaPeriodoCobroExtra(this.pageDTO,idCobroExtra);
      this.getCountMedidoresAsignadosASocio();  
    }
  }

  public construirTituloTabla(){
    this.listaTituloItems       = ['Nro.','Cod. Socio','Nombre','# Med.','Cod. Medidor','Dirección','Lectura Actual','Código Cat.','Nombre Cat.','Seleccionar'];
    this.listaCampoItemsSocio   = ['indexSocio','codigoSocio','nombreCompleto','countMedidores'];
    this.listaCampoItemsMedidor = ['codigoMedidor','direccionSM','lecturaActual','codigoCategoria','nombreCategoria'];
  }
  
  public tratarListaDeSociosMedidor(){
    let indexGlobal : number = 0;
    let indexSocio  : number = 1;
    this.listaDeSociosMedidorTratado = [];

    for (let i = 0; i < this.listaDeSociosMedidor.length; i++) {
      let socioMedidorVO:SocioMedidorVO = this.listaDeSociosMedidor[i];      
      let listaMedidores:MedidorAsignadoVO[] = socioMedidorVO.medidores;
      for (let j = 0; j < listaMedidores.length; j++) {
        let medidor:MedidorAsignadoVO = listaMedidores[j];
        
        let socioMedidorTratadoVO:SocioMedidorTratadoVO = {
          indexGlobal : indexGlobal,
          indexSocio  : indexSocio,
          seleccionado : false,

          // socio medidor
          idSocio         : socioMedidorVO.idSocio,
          codigoSocio     : socioMedidorVO.codigo,
          nombreCompleto  : socioMedidorVO.nombreCompleto,
          direccionSocio  : socioMedidorVO.direccion,
          countMedidores  : listaMedidores.length,

          // medidor
          idSocioMedidor          : medidor.idSocioMedidor,
          idTanqueAgua            : medidor.idTanqueAgua,
          lecturaInicial          : medidor.lecturaInicial,
          fechaAsignacionSM       : medidor.fechaAsignacionSM,    
          estadoSocioMedidor      : medidor.estadoSocioMedidor, 
          direccionSM             : medidor.direccionSM,  
          idMedidor               : medidor.idMedidor,
          codigoMedidor           : medidor.codigo,
          propiedadCooperativa    : medidor.propiedadCooperativa,
          lecturaActual           : medidor.lecturaActual,
          fechaRegistro           : medidor.fechaRegistro,
          estadoMedidor           : medidor.estadoMedidor,
          direccionMedidor        : medidor.direccion,
          idCategoria             : medidor.idCategoria,
          codigoCategoria         : medidor.codigoCategoria,
          nombreCategoria         : medidor.nombreCategoria,
          descripcionCategoria    : medidor.descripcionCategoria,
          registroSocioCobroExtra : medidor.registroSocioCobroExtra
        }
        this.listaDeSociosMedidorTratado.push(socioMedidorTratadoVO);
        indexGlobal += 1;
      }
      indexGlobal = 0;
      indexSocio += 1;
    }
  }

  public seleccionarItem(socio:SocioMedidorTratadoVO){
    this.limpiarItemsSeleccionados();
    this.listaDeSociosMedidorTratado.forEach(socioMedidor => {
      if(socioMedidor.indexSocio==socio.indexSocio){
        socioMedidor.seleccionado = true;
      }
    });
  }

  private limpiarItemsSeleccionados(){
    this.listaDeSociosMedidorTratado.forEach(socioMedidor => {
      socioMedidor.seleccionado = false;
    });
  }

  public async checkTipoCobroExtraPeriodo(event:any,socio:SocioMedidorTratadoVO){
    let idPeriodoCobroExtra:number = Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra').value);
    let cobroExtraVO:CobroExtraVO = this.getCobroExtra(idPeriodoCobroExtra);
      

    if(event.target.checked){     
      let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR A MEDIDOR CON COBRO EXTRA',`¿Está seguro de asignar al socio ${socio.nombreCompleto}, medidor : "${socio.codigoMedidor}", el cobro extra : "${cobroExtraVO.nombre} - ${cobroExtraVO.detalle}"?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${socio.codigoMedidor}`);     
        element.checked=false;
        return;
      }
      
      this.createSocioCobroExtra(idPeriodoCobroExtra,socio.idSocioMedidor);
    }else{
      let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR REGISTRO A MEDIDOR CON COBRO EXTRA',`¿Está seguro de desasignar al socio ${socio.nombreCompleto},  medidor : "${socio.codigoMedidor}", el cobro extra : "${cobroExtraVO.nombre} - ${cobroExtraVO.detalle}"?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){ 
        let element = <HTMLInputElement>document.getElementById(`check${socio.codigoMedidor}`);     
        element.checked=true;
        return;
      }
      this.deleteSocioCobroExtra(idPeriodoCobroExtra,socio.idSocioMedidor);
    }
  }

  private getCobroExtra(idPeriodoCobroExtra:number):CobroExtraVO{
    let cobroExtraVO:CobroExtraVO;
    this.listaCobroExtra.forEach(cobroExtra => {
      if(cobroExtra.idPeriodoCobroExtra == idPeriodoCobroExtra){
        cobroExtraVO = cobroExtra;
      }
    });
    return cobroExtraVO;
  }

  public buscarSocioPorCodigo(codigoSocio:string){  
    this.listaDeSociosMedidorTratado.forEach(socio => {
      if(socio.codigoSocio.includes(codigoSocio) && codigoSocio!=''){
        socio.seleccionado = true;
      }else{
        socio.seleccionado = false;
      }
    });
  }

  public buscarSocioPorNombre(nombre:string){  
    nombre = nombre.toLowerCase();
    this.listaDeSociosMedidorTratado.forEach(socio => {
      if(socio.nombreCompleto.toLowerCase().includes(nombre) && nombre!=''){
        socio.seleccionado = true;
      }else{
        socio.seleccionado = false;
      }
    });
  }

  //---- paginator -----
  public cambioSizePaginaOrPagina(pageEvent:PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.changeSelectorGestionMesTipoCobroExtra('cobroExtra');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  // mes anio categoria costo agua service
  public getAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodosDeAnioService(idGestion:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnio(idGestion).subscribe(
      (resp)=>{
        this.listaPeriodosDeGestion = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // cobro extra service
  public getListaCobroExtraDePeriodoService(ideriodo:number,todos:boolean){
    this.cobroExtraService.getListaCobroExtraDePeriodo(ideriodo,todos).subscribe(
      (resp)=>{
        this.listaCobroExtra = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  // socio medidor service
  private getCountMedidoresAsignadosASocio(){
    this.socioMedidorService.getCountMedidoresAsignadosASocios().subscribe(
      (resp)=>{
        this.countSocios = resp.countItem;
        this.lengthList = this.countSocios;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getSociosConSusMedidoresParaPeriodoCobroExtra(pageDTO:PageDTO,idCobroExtra:number){
    this.socioMedidorService.getSociosConSusMedidoresParaPeriodoCobroExtra(pageDTO,idCobroExtra).subscribe(
      (resp)=>{
        this.listaDeSociosMedidor = resp;
        this.tratarListaDeSociosMedidor();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //socio cobro extra
  public createSocioCobroExtra(idPeriodoCobroExtra:number,idSocioMedidor:number){
    this.socioCobroExtraService.createSocioCobroExtra(idPeriodoCobroExtra,idSocioMedidor).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto('Registro exitoso!!!');
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  public deleteSocioCobroExtra(idPeriodoCobroExtra:number,idSocioMedidor:number){
    this.socioCobroExtraService.deleteSocioCobroExtra(idPeriodoCobroExtra,idSocioMedidor).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto('Se elminino el registro de manera correcta!!!');
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

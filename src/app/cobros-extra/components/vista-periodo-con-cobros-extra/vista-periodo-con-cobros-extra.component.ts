import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { PeriodoCobroExtraService } from '../../services/periodo-cobro-extra.service';
import { MesAnioCategoriaCostoAguaService } from '../../../configuraciones/services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from '../../../configuraciones/models/anioVO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PeriodoDataVO } from '../../models/periodoDataVO';
import { CobroExtraDataVO } from '../../models/cobroExtraDataVO';
import { PeriodoDataTratadoVO } from '../../models/periodoDataTratadoVO ';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';


@Component({
  selector: 'app-vista-periodo-con-cobros-extra',
  templateUrl: './vista-periodo-con-cobros-extra.component.html',
  styleUrls: ['./vista-periodo-con-cobros-extra.component.css']
})
export class VistaPeriodoConCobrosExtraComponent implements OnInit {
  aux:any = undefined;

  // tabla costo periodo extra
  listaTituloPeriodoCobroExtra : string[];
  listaPeriodoDataVO!          : PeriodoDataVO[];
  listaPeriodoDataTratadoVO!   : PeriodoDataTratadoVO[];

  formularioGestion! : FormGroup;
  listaGestiones     : AnioVO[]=[];
  
  constructor(private fb                               : FormBuilder,
              private periodoCobroExtraService         : PeriodoCobroExtraService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private router                           : Router) { }

  ngOnInit(): void {
    this.getAniosService();
    this.construirTablaPeriodoCobroExtra();
    this.construirFormularioSelccionPeriodoCobroExtra();
  }

  private construirFormularioSelccionPeriodoCobroExtra(){
    this.formularioGestion = this.fb.group({
      gestion    :  [ '0', [Validators.required],[]],
    });
  }

  public campoValido(campo:string):boolean{
    return  this.formularioGestion.get(campo)!.valid && 
            this.formularioGestion.get(campo)!.value!='0';
  }

  public campoInvalido(campo:string):boolean{
    return  this.formularioGestion.get(campo)!.touched && 
            this.formularioGestion.get(campo)!.value == '0';
  }

  public changeGestionCobroExtra(){
    let idGestion    : number = Number(this.formularioGestion.get('gestion')?.value);
    this.listaPeriodoDataTratadoVO = [];
    this.getPeriodosConSusCobrosExtraDeGestionService(idGestion);
  }

  public construirTablaPeriodoCobroExtra(){    
    this.listaTituloPeriodoCobroExtra = ['#','Nro.Mes','Nombere Mes','Cobro extra','Costo','Todos'];
  }

  public tratarListaPeriodoDataVO(){
    let iGlobal : number=0;
    let iMes    : number=1;
    this.listaPeriodoDataTratadoVO = [];
    for (let i = 0; i < this.listaPeriodoDataVO.length; i++) {
      let periodoDataVO : PeriodoDataVO = this.listaPeriodoDataVO[i];
      
      
      for (let j = 0; j < periodoDataVO.cobroExtraDataList.length; j++) {
        let cobroExtraDataVO:CobroExtraDataVO = periodoDataVO.cobroExtraDataList[j];
        
        let periodoDataTratadoVO:PeriodoDataTratadoVO = {
          indexGlobal  : iGlobal,
          indexMes     : iMes, 
          seleccionado : false,

          // PeriodoDataVO
          idPeriodo            : periodoDataVO.idPeriodo,
          idmes                : periodoDataVO.idmes,
          nroMes               : periodoDataVO.nroMes,
          nombreMes            : periodoDataVO.nombreMes,
          countItemsCobroExtra : periodoDataVO.cobroExtraDataList.length,

          // CobroExtraDataVO
          idCobroExtra     : cobroExtraDataVO.idCobroExtra,
          nombreCobroExtra : cobroExtraDataVO.nombre,
          costoCobroExtra  : cobroExtraDataVO.costo,
          todos            : cobroExtraDataVO.todos,
        }
        this.listaPeriodoDataTratadoVO.push(periodoDataTratadoVO);

        iGlobal += 1;
      }
      iGlobal = 0;
      iMes += 1;
    }
  }

  public seleccionarItem(periodoEvent:PeriodoDataTratadoVO){
    this.limpiarItemsSeleccionados();
    this.listaPeriodoDataTratadoVO.forEach(periodo => {
      if(periodo.idPeriodo==periodoEvent.idPeriodo){
        periodo.seleccionado = true;
      }
    });
  }

  private limpiarItemsSeleccionados(){
    this.listaPeriodoDataTratadoVO.forEach(periodo => {
      periodo.seleccionado = false;
    });
  }

  public stringFromBoolean(bandera:boolean){
    return (bandera)?'SI':'NO';
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaCobroExtraGestion')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de cobro extra por gestión');
  }  
  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------  
  public getAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodosConSusCobrosExtraDeGestionService(idGestion:number) {
    return this.periodoCobroExtraService.getPeriodosConSusCobrosExtraDeGestion(idGestion).subscribe(
      (resp)=>{
        this.listaPeriodoDataVO = resp;
        if(this.listaPeriodoDataVO.length>0){
          this.tratarListaPeriodoDataVO();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

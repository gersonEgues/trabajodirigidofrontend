import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { PeriodoCobroExtraService } from '../../services/periodo-cobro-extra.service';
import { PeriodoCobroExtraDTO } from '../../models/periodoCobroExtraDTO';
import { PeriodoCobroExtraVO } from '../../models/periodoCobroExtraVO';
import { PeriodoCobroExtraDataVO } from '../../models/periodoCobroExtraDataVO';
import { CobroExtraService } from '../../services/cobro-extra.service';
import { MesAnioCategoriaCostoAguaService } from '../../../configuraciones/services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from '../../../configuraciones/models/anioVO';
import { CobroExtraVO } from '../../models/cobroExtraVO';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocioCobroExtraService } from '../../services/socio-cobro-extra.service';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-periodo-cobro-extra',
  templateUrl: './periodo-cobro-extra.component.html',
  styleUrls: ['./periodo-cobro-extra.component.css']
})
export class PeriodoCobroExtraComponent implements OnInit {
  aux:any = undefined;

  // tabla costo periodo extra
  listaTituloPeriodoCobroExtra : string[] = [];
  listaCampoPeriodoCobroExtra  : string[] = [];
  idPeriodoCobroExtra!         : string;
  listaPeriodoCobroExtra       : PeriodoCobroExtraDataVO[]=[];

  formularioSeleccionPeriodoCobroExtra! : FormGroup;
  listaGestiones  : AnioVO[]=[];
  listaCobroExtra : CobroExtraVO[] = [];

  formularioEstablecerPeriodoCobroExtra! : FormGroup;

  @ViewChild('tablaPeriodoCobroExtra') tablaPeriodoCobroExtra! : TablaComponent;

  constructor(private fb                               : FormBuilder,
              private periodoCobroExtraService         : PeriodoCobroExtraService,
              private cobroExtraService                : CobroExtraService,
              private socioCobroExtraService           : SocioCobroExtraService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private router                           : Router) { }

  ngOnInit(): void {
    this.getAniosService();
    this.getListaCobroExtraService();

    this.construirTablaPeriodoCobroExtra();
    this.construirFormularioSelccionPeriodoCobroExtra();
    this.construirFormularioEstablecerPeriodoCobroExtra();
  }

  // tabla periodo cobro extra
  private construirFormularioSelccionPeriodoCobroExtra(){
    this.formularioSeleccionPeriodoCobroExtra = this.fb.group({
      gestion    :  [ '0', [Validators.required],[]],
      cobroExtra :  [ '0', [Validators.required],[]],
    });
  }

  private construirFormularioEstablecerPeriodoCobroExtra(){
    this.formularioEstablecerPeriodoCobroExtra = this.fb.group({
      costo :  ['', [Validators.required],[]],
      todos :  [false, [Validators.required],[]],
    });
  }

  public construirTablaPeriodoCobroExtra(){
    this.listaTituloPeriodoCobroExtra = ['#','Gestión','Mes','Nombre','Costo','Todos'];
    this.listaCampoPeriodoCobroExtra  = ['anio','nombreMes','nombreCobroExtra','costo','todos',];
    this.idPeriodoCobroExtra          = 'idPeriodo';
  }

  public campoValidoSeleccionPeriodoCobroExtra(campo:string):boolean{
    return  this.formularioSeleccionPeriodoCobroExtra.get(campo)!.valid && 
            this.formularioSeleccionPeriodoCobroExtra.get(campo)!.value!='0';
  }

  public campoInvalidoSeleccionPeriodoCobroExtra(campo:string):boolean{
    return  this.formularioSeleccionPeriodoCobroExtra.get(campo)!.touched && 
            this.formularioSeleccionPeriodoCobroExtra.get(campo)!.value == '0';
  }

  public campoValidoEstablecerPeriodoCobroExtra(campo:string):boolean{
    return  this.formularioEstablecerPeriodoCobroExtra.get(campo)!.valid
  }

  public campoInvalidoEstablecerPeriodoCobroExtra(campo:string):boolean{
    if(campo=='costo'){
      return  this.formularioEstablecerPeriodoCobroExtra.get(campo)!.touched &&
      this.formularioEstablecerPeriodoCobroExtra.get(campo)!.value=='';
    }else{
      return  this.formularioEstablecerPeriodoCobroExtra.get(campo)!.touched && 
              (this.formularioEstablecerPeriodoCobroExtra.get(campo)!.value != true && 
              this.formularioEstablecerPeriodoCobroExtra.get(campo)!.value != false);
    }
  }

  public changeGestionCobroExtra(){
    let idGestion    : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('gestion')?.value);
    let idCobroExtra : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra')?.value);

    if(idGestion!=0 && idCobroExtra!=0){
      this.getListaPeriodoCobroExtraService(idGestion,idCobroExtra);
    }
  }

  public async guardarPeriodoCobroExtra(){
    let idGestion    : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('gestion')?.value);
    let idCobroExtra : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra')?.value);

    if(this.formularioEstablecerPeriodoCobroExtra.invalid || idGestion==0 || idCobroExtra==0){
      if(idGestion==0 || idCobroExtra==0){
        this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
        this.formularioSeleccionPeriodoCobroExtra.markAllAsTouched();
      }

      if(this.formularioEstablecerPeriodoCobroExtra.invalid){
        this.generalService.mensajeFormularioInvalido("Establesca un costo de cobro extra");
        this.formularioEstablecerPeriodoCobroExtra.markAllAsTouched();
      }
      return;
    }



    let hayPeriodosSeleccionados:boolean = false;

    this.listaPeriodoCobroExtra.forEach((cobroExtra:PeriodoCobroExtraDataVO) => {
      if(cobroExtra.check){
        hayPeriodosSeleccionados = true;
      }
    });

    if(!hayPeriodosSeleccionados){
      this.generalService.mensajeAlerta('No hay periodos seleccionados');
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('GUARDAR COBRO EXTRA PARA LOS PERIODOS SELECCIONADOS ','¿Está seguro de guardar el costo extra para todos los periodos seleccionados?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    for (let i = 0; i < this.listaPeriodoCobroExtra.length; i++) {
      let periodoCobroExtra:PeriodoCobroExtraDataVO = this.listaPeriodoCobroExtra[i];
      
      if(periodoCobroExtra.check && periodoCobroExtra.idPeriodoCobroExtra!=null){// ya se registro anteriormente
        let continuar:Boolean = await this.getConfirmacion('CONFIRMAR ACCION ',`El  periodo ${periodoCobroExtra.nombreMes} - ${periodoCobroExtra.anio}, ya fue registrado, probablemente ya guarde datos de los socios con respecto a este cobro extra, ¿Continuar y eliminar esos datos?`,'Sí, estoy seguro','No, saltar este periodo');
        
        if(continuar){ 
          let periodoCobroExtraVO:PeriodoCobroExtraVO = {
            idPeriodoCobroExtra : periodoCobroExtra.idPeriodoCobroExtra,
            costo               : this.formularioEstablecerPeriodoCobroExtra.get('costo')?.value,
            todos               : this.formularioEstablecerPeriodoCobroExtra.get('todos')?.value,
          }        
          this.updatePeriodoCobroExtraService(periodoCobroExtraVO);

          let resp:any = await this.deleteSocioCobroExtraTodosService(periodoCobroExtra.idPeriodoCobroExtra)
            .then(resp=>{
              if(this.formularioEstablecerPeriodoCobroExtra.get('todos')?.value==true){
                this.createSocioCobroExtraTodosService(periodoCobroExtra.idPeriodoCobroExtra);
              }
            })
            .catch(err=>{
              this.generalService.mensajeError(err.message);
            });
        }
      }else if(periodoCobroExtra.check && periodoCobroExtra.idPeriodoCobroExtra==null){// nuevo registro
        let periodoCobroExtraDTO:PeriodoCobroExtraDTO = {
          idPeriodo    : periodoCobroExtra.idPeriodo,
          idCobroExtra : Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra')?.value),
          costo        : this.formularioEstablecerPeriodoCobroExtra.get('costo')?.value,
          todos        : this.formularioEstablecerPeriodoCobroExtra.get('todos')?.value,
        }     

        let resp:any = await this.createPeriodoCobroExtraService(periodoCobroExtraDTO)
          .then(resp=>{
            if(this.formularioEstablecerPeriodoCobroExtra.get('todos')?.value==true){
              this.createSocioCobroExtraTodosService(resp.idPeriodoCobroExtra);
            }
          })
          .catch(err=>{   
            this.generalService.mensajeError(err.message);
          });
      }
    }

    this.changeGestionCobroExtra();
    this.limpiarFormulario();
  }
  
  public limpiarFormulario(){
    this.formularioEstablecerPeriodoCobroExtra.reset();
    this.formularioEstablecerPeriodoCobroExtra.get('todos')?.setValue(false);
  } 

  public async deleteCobroExtra(periodoCobroExtraVO:PeriodoCobroExtraDataVO){
    if(periodoCobroExtraVO.idPeriodoCobroExtra==null){
      this.generalService.mensajeAlerta("Este periodo no fue asignado con un cobro extra");
      return;
    }


    let afirmativo:Boolean = await this.getConfirmacion(`ELIMINAR COBRO EXTRA DEL PERIODO ${periodoCobroExtraVO.nombreMes.toUpperCase()} - ${periodoCobroExtraVO.anio}`,`¿Está seguro de eliminar el cobro extra del periodo ${periodoCobroExtraVO.nombreMes} - ${periodoCobroExtraVO.anio}`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){ 
      return;
    }

    this.deletePeriodoCobroExtraService(periodoCobroExtraVO);
  }

  public async reloadPedriodoCobroExtraTodos(periodoCobroExtraVO:PeriodoCobroExtraDataVO){  
    if(periodoCobroExtraVO.idPeriodoCobroExtra==null){
      this.generalService.mensajeAlerta("Este periodo no fue asignado con un cobro extra");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion(`RECARGAR COBRO EXTRA DEL PERIODO ${periodoCobroExtraVO.nombreMes.toUpperCase()} - ${periodoCobroExtraVO.anio}`,`¿Está seguro de recargar el cobro extra del periodo ${periodoCobroExtraVO.nombreMes} - ${periodoCobroExtraVO.anio} para todos los socios`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){ 
      return;
    }

    //this.deletePeriodoCobroExtraService(periodoCobroExtraVO);
    this.createSocioCobroExtraTodosService(periodoCobroExtraVO.idPeriodoCobroExtra);
  }

  public reloadTablaCobroExtra(idAnio:number,idCobroExtra:number){
    this.listaPeriodoCobroExtra = [];
    this.getListaPeriodoCobroExtraService(idAnio,idCobroExtra);
  }

  public genearteArchivoXLSX():void{
    this.tablaPeriodoCobroExtra.genearteArchivoXLSX("lista de cobro extra por gestión");
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  // periodo cobro extra
  public getListaPeriodoCobroExtraService(idAnio:number,idCobroExtra:number){
    this.periodoCobroExtraService.getListaPeriodoCobroExtra(idAnio,idCobroExtra).subscribe(
      (resp)=>{
        this.listaPeriodoCobroExtra = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodoCobroExtraService(idPeriodoCobroExtra:number){
    this.periodoCobroExtraService.getPeriodoCobroExtra(idPeriodoCobroExtra).subscribe(
      (resp)=>{
        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createPeriodoCobroExtraService(periodoCobroExtraDTO:PeriodoCobroExtraDTO){
    return this.periodoCobroExtraService.createPeriodoCobroExtra(periodoCobroExtraDTO).toPromise();
  }

  public updatePeriodoCobroExtraService(periodoCobroExtraVO:PeriodoCobroExtraVO){
    this.periodoCobroExtraService.updatePeriodoCobroExtra(periodoCobroExtraVO).subscribe(
      (resp)=>{
        let idGestion    : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('gestion')?.value);
        let idCobroExtra : number = Number(this.formularioSeleccionPeriodoCobroExtra.get('cobroExtra')?.value);
        this.reloadTablaCobroExtra(idGestion,idCobroExtra);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deletePeriodoCobroExtraService(periodoCobroExtra:PeriodoCobroExtraDataVO){
    this.periodoCobroExtraService.deletePeriodoCobroExtra(periodoCobroExtra.idPeriodoCobroExtra).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se elimino correctamente el cobro extra del periodo seleccionado");
        this.reloadTablaCobroExtra(periodoCobroExtra.idAnio,periodoCobroExtra.idCobroExtra);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // cobro extra service
  public getListaCobroExtraService(){
    this.cobroExtraService.getListaCobroExtra(true).subscribe(
      (resp)=>{
        this.listaCobroExtra = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // socio cobro extra
  public createSocioCobroExtraTodosService(idPeriodoCobroExtra:number){
    this.socioCobroExtraService.createSocioCobroExtraTodos(idPeriodoCobroExtra).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteSocioCobroExtraTodosService(idPeriodoCobroExtra:number){
    return this.socioCobroExtraService.deleteSocioCobroExtraTodos(idPeriodoCobroExtra).toPromise();
  }


  // mes anio categoria costo agua service
  public getAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

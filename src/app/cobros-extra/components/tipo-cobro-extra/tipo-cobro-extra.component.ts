import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CobroExtraDTO } from 'src/app/cobros-extra/models/cobroExtraDTO';
import { CobroExtraVO } from 'src/app/cobros-extra/models/cobroExtraVO';
import { CobroExtraService } from 'src/app/cobros-extra/services/cobro-extra.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-cobro-extra',
  templateUrl: './tipo-cobro-extra.component.html',
  styleUrls: ['./tipo-cobro-extra.component.css']
})
export class TipoCobroExtraComponent implements OnInit {
  aux:any = undefined;

  formularioCobroExtra! : FormGroup;
  cobroExtraUpdate      : CobroExtraVO = this.aux;

  // tabla lista 
  listaTituloCobroExtra : string[] = [];
  listaCampoCobroExtra  : string[] = [];
  idCobroExtra!         : string;
  listaCobroExtra       : CobroExtraVO[]=[];
  
  banderaHabilitado:boolean = true;

  constructor(private fb                : FormBuilder,
              private cobroExtraService : CobroExtraService,
              private generalService    : GeneralService,
              private dialog            : MatDialog,
              private router            : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.construirTablaCobroExtra();
    this.getListaCobroExtraService(this.banderaHabilitado);
  }

  private construirFormulario(){
    this.formularioCobroExtra = this.fb.group({
      cobroExtra :  this.fb.array([],[Validators.required],[])
    });
    this.addItemsCobroExtra();
  }

  public addItemsCobroExtra(){
    this.getItemsCobroExtra().push(this.nuevoItemCobroExtra())
  }

  public removeItemCobroExtra(i:number){
    this.getItemsCobroExtra().removeAt(i);
  }
  
  public getItemsCobroExtra() : FormArray{
    return this.formularioCobroExtra.get('cobroExtra') as FormArray;
  }

  private nuevoItemCobroExtra(){
    return this.fb.group({
      nombre  : [ '', [Validators.required],[]],
      detalle : [ '', [Validators.required],[]],
    });
  }

  public campoValidoItemCobroExtra(i:number,campo:string):boolean{
    return this.getItemsCobroExtra().at(i).get(campo)!.valid;
  }

  public campoInvalidoItemCobroExtra(i:number,campo:string):boolean{
    return  this.getItemsCobroExtra().at(i).get(campo)!.invalid &&
            this.getItemsCobroExtra().at(i).get(campo)!.touched;
  }

  public async guardarTipoCobroExtra(){
    if(this.formularioCobroExtra.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCobroExtra.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE TIPOS DE COBRO EXTRA','¿Está seguro de registrar los tipos de cobro extra?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.getItemsCobroExtra().controls.forEach(cobroExtra => {
      let cobroExtraDTO:CobroExtraDTO = {
        nombre  : cobroExtra.get('nombre')?.value,
        detalle : cobroExtra.get('detalle')?.value,
      }
      this.createCobroExtraService(cobroExtraDTO);
    });

    this.limpiarFormulario();
  }

  public async actualizarTipoCobroExtra(){
    if(this.formularioCobroExtra.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCobroExtra.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR TIPO DE COBRO EXTRA','¿Está seguro de actualizar el tipo de cobro extra?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    let cobroExtraVO:CobroExtraVO = {
     idCobroExtra : this.cobroExtraUpdate.idCobroExtra,
     nombre       : this.getItemsCobroExtra().at(0).get('nombre')?.value,
     detalle      : this.getItemsCobroExtra().at(0).get('detalle')?.value,
     activo       : true
    }

    this.updateCobroExtraService(cobroExtraVO);
    this.limpiarFormulario();
  }
  
  public cancelarGuardarTipoCobroExtra(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioCobroExtra.reset();
    this.cobroExtraUpdate = this.aux;

    while(this.getItemsCobroExtra().length!=1){
      this.getItemsCobroExtra().removeAt(1);
    }
  }

  // tabla lista de cobros extra
  public construirTablaCobroExtra(){
    this.listaTituloCobroExtra = ['#','Nombre del tipo de cobro extra','Código de cobro extra'];
    this.listaCampoCobroExtra  = ['detalle','nombre'];
    this.idCobroExtra          = 'idCobroExtra';
  }

  public updateCobroExtraEvent(cobroExtra:CobroExtraVO){
    this.cobroExtraUpdate = cobroExtra;
    this.getItemsCobroExtra().at(0).get('nombre')?.setValue(cobroExtra.nombre);
    this.getItemsCobroExtra().at(0).get('detalle')?.setValue(cobroExtra.detalle);
  }

  public changeHabilitar(){
    this.banderaHabilitado = !this.banderaHabilitado;
    this.getListaCobroExtraService(this.banderaHabilitado);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaTiposCobroExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'tipos de cobro extra');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  public getListaCobroExtraService(bandera:boolean){
    this.cobroExtraService.getListaCobroExtra(bandera).subscribe(
      (resp)=>{
        this.listaCobroExtra = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createCobroExtraService(cobroExtraDTO:CobroExtraDTO){
    this.cobroExtraService.createCobroExtra(cobroExtraDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se creo correctamente el tipo de cobro extra");
        this.listaCobroExtra.push(resp);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateCobroExtraService(cobroExtraVO:CobroExtraVO ){
    this.cobroExtraService.updateCobroExtra(cobroExtraVO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo correctamene el tipo de cobro extra");
        this.getListaCobroExtraService(this.banderaHabilitado);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

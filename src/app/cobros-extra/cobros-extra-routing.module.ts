import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeriodoCobroExtraComponent } from './components/periodo-cobro-extra/periodo-cobro-extra.component';
import { TipoCobroExtraComponent } from './components/tipo-cobro-extra/tipo-cobro-extra.component';
import { VistaPeriodoConCobrosExtraComponent } from './components/vista-periodo-con-cobros-extra/vista-periodo-con-cobros-extra.component';
import { AsignarCobroExtraASocioComponent } from './components/asignar-cobro-extra-a-socio/asignar-cobro-extra-a-socio.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'periodo-cobro-extra',
        component: PeriodoCobroExtraComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'vista-periodos-con-sus-cobros-extra',
        component: VistaPeriodoConCobrosExtraComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'asignar-cobro-extra-a-socio',
        component: AsignarCobroExtraASocioComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'registro-tipo-cobro-extra',
        component: TipoCobroExtraComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', 
        redirectTo : 'vista-periodos-con-sus-cobros-extra'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CobrosExtraRoutingModule { }

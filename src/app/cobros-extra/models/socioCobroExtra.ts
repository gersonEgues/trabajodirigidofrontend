export interface SocioCobroExtra {
  idSocioCobroExtra   : number,
  idSocioMedidor      : number,
  idPeriodoCobroExtra : number,
  cancelado           : boolean,
  echaRegistro        : string,
}

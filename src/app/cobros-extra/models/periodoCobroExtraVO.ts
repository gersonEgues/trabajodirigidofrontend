import { PeriodoCobroExtraDTO } from "./periodoCobroExtraDTO";

export interface PeriodoCobroExtraVO extends PeriodoCobroExtraDTO {
  idPeriodoCobroExtra : number,
}
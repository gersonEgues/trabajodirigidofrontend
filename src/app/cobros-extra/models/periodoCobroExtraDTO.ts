export interface PeriodoCobroExtraDTO {
  idPeriodo?          : number,
  idCobroExtra?       : number,
  costo               : number,
  todos               : boolean,
}
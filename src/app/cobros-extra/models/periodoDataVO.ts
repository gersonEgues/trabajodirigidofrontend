import { CobroExtraDataVO } from "./cobroExtraDataVO";

export interface PeriodoDataVO {
  idPeriodo          : number,
  idmes              : number,
  nroMes             : number,
  nombreMes          : string,
  cobroExtraDataList : CobroExtraDataVO[],
} 
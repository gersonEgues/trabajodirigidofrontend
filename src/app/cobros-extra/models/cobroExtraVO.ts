import { CobroExtraDTO } from "./cobroExtraDTO";

export interface CobroExtraVO extends CobroExtraDTO {
  idPeriodoCobroExtra? : number,
  idCobroExtra         : number,
  activo               : boolean,
}
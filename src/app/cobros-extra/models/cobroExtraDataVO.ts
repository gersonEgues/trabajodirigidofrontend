export interface CobroExtraDataVO {
  idCobroExtra : number,
  nombre       : string,
  costo        : number,
  todos        : boolean,
}
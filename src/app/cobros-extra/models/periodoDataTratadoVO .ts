import { CobroExtraDataVO } from "./cobroExtraDataVO";

export interface PeriodoDataTratadoVO {
  indexGlobal  :  number,
  indexMes     : number,  
  seleccionado : boolean,  

  // PeriodoDataVO
  idPeriodo            : number,
  idmes                : number,
  nroMes               : number,
  nombreMes            : string,
  countItemsCobroExtra : number,

  // CobroExtraDataVO
  idCobroExtra     : number,
  nombreCobroExtra : string,
  costoCobroExtra  : number,
  todos            : boolean,  
} 
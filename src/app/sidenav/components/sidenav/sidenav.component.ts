import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree'
import { ModuloFuncion } from '../../models/modulo-funcion';
import { Menu } from '../../models/menu';
import { SidenavService } from '../../services/sidenav.service';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { GeneralService } from '../../../shared/services/general.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
  host:{
    '(window:resize)': 'onResize($event)'
  }
})

export class SidenavComponent implements OnInit {
  aux               : undefined = null;
  menu              : ModuloFuncion[] = [];
  menuItem          : string = this.aux;
  subMenuItemAntes  : string = this.aux;
  subMenuItemActual : string = this.aux;

  private _transformer = (node: ModuloFuncion, level: number) => {
    return {
      expandable: !!node.funcion && node.funcion.length > 0,
      id: node.id,
      nombre: node.nombre,
      path: node.path,
      icono: node.icono,
      permiso: node.permiso,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<Menu>( node => node.level, node => node.expandable );
  treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.funcion);
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  banderaSidenavAbierto:boolean = true;

  @ViewChild('sidenav') sidenav     : MatSidenav;


  constructor(private sidenavService : SidenavService,
              private dialog         : MatDialog,
              private router         : Router,
              private generalService : GeneralService) {
    if(window.innerWidth<800){
      this.banderaSidenavAbierto = false;
    }
  }

  hasChild = (_: number, node: Menu) => node.expandable;

  ngOnInit(): void {
    this.menu = this.sidenavService.getMenuDeUsuario();
    this.dataSource.data = this.menu;
  }

  public async salir(){
    let afirmativo:Boolean = await this.getConfirmacion('SALIR DE LA APLICACIÓN',`¿Está seguro de salir de la aplicacion`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){    
      return;
    }
    localStorage.removeItem('token');
    this.router.navigate(['/guest/home']);
  }

  public clickMenu(id:string){
  }

  public clickSubMenu(ruta:string,id:string){
    if(this.subMenuItemAntes==this.aux){
      this.subMenuItemAntes = id;
      this.subMenuItemActual = id;
    }else{
      this.subMenuItemAntes = this.subMenuItemActual;
      this.subMenuItemActual = id;
    }

    let itemAntes  : HTMLElement = document.getElementById(`sub-menu${this.subMenuItemAntes}`);
    let itemActual : HTMLElement = document.getElementById(`sub-menu${this.subMenuItemActual}`);

    if(itemAntes)
      itemAntes.className = 'submenu-text';

    if(itemActual)
      itemActual.className = 'submenu-text-selected';

    this.router.navigate([ruta]);
    if(window.innerWidth<800)
      this.banderaSidenavAbierto = false;
  }

  public onResize(event){    
    if(event.target.innerWidth<800)
      this.banderaSidenavAbierto = false;
    else
      this.banderaSidenavAbierto = true;
  }

  public openSideNav(){
    this.sidenav.toggle();
    this.banderaSidenavAbierto = this.sidenav.opened;    
  }

  public getUserName():string{
    return this.generalService.getNombreCompletoUsuarioSession();
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

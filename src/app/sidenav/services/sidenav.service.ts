import { Injectable } from '@angular/core';
import { ModuloFuncion } from '../models/modulo-funcion';
import { UserToken } from 'src/app/login/models/userToken';
import jwt_decode from "jwt-decode";
import { ROL } from 'src/app/shared/enums-mensajes/roles';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {
  menu:ModuloFuncion[] = [
    { 
      id:1,
      nombre:'Usuarios',
      path:'usuarios',
      icono:'people_alt',
      permiso:0,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:11,
          nombre:'Lista de usuarios',
          path:'/user/usuarios/lista-de-usuarios',
          icono:'format_list_numbered',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },        
        {
          id:12,
          nombre:'Roles de usuario',
          path:'/user/usuarios/roles',
          icono:'settings',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:13,
          nombre:'Registro de usuarios',
          path:'/user/usuarios/registro-de-usuarios',
          icono:'person_add',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
      ]
    },
    {
      id:2,
      nombre:'Socios',
      path:'socios',
      icono:'groups',
      permiso:0,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:21,
          nombre:'Lista de socios',
          path:'/user/socios/lista-de-socios',
          icono:'format_list_numbered',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:22,
          nombre:'Buscar socio',
          path:'/user/socios/busqueda-socio',
          icono:'manage_search',
          permiso:3,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:23,
          nombre:'Registro de socios',
          path:'/user/socios/registro-socio',
          icono:'person_add',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
      ]
    },
    {
      id:3,
      nombre:'Medidor',
      path:'socio-medidor',
      icono:'watch_later',
      permiso:0,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:31,
          nombre:'Lista de socios con sus medidores',
          path:'/user/socio-medidor/medidores-asignados',
          icono:'list',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:32,
          nombre:'Lista de medidores',
          path:'/user/socio-medidor/lista-medidores',
          icono:'list',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:33,
          nombre:'Buscar medidores de socio',
          path:'/user/socio-medidor/medidores-de-socio',
          icono:'search',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },        
        {
          id:34,
          nombre:'Asignar/Desasignar medidor a socio',
          path:'/user/socio-medidor/asignar-socio-medidor',
          icono:'assignment_ind',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },    
        {
          id:35,
          nombre:'Dar baja a medidor',
          path:'/user/socio-medidor/baja-medidor',
          icono:'auto_delete',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE],
          funcion:[],
        },  
        {
          id:36,
          nombre:'Registro de medidor',
          path:'/user/socio-medidor/registro-medidor',
          icono:'person_add',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },  
      ]
    },
    {
      id:4,
      nombre:'Lectura de medidor',
      path:'lectura-medidor',
      icono:'water_drop',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:41,
          nombre:'Lectura detallada de medidor',
          path:'/user/lectura-medidor/lectura-agua-socio',
          icono:'grading',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:42,
          nombre:'Lectura rapida de medidor',
          path:'/user/lectura-medidor/lectura-medidor-socio-guest',
          icono:'fact_check',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:43,
          nombre:'Historial lectura de medidor',
          path:'',
          icono:'manage_search',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:431,
              nombre:'Busqueda por código de socio',
              path:'/user/lectura-medidor/busqueda-por-codigo-de-socio',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:432,
              nombre:'Busqueda por código de medidor',
              path:'/user/lectura-medidor/busqueda-por-codigo-de-medidor',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:433,
              nombre:'Busqueda por gestión',
              path:'/user/lectura-medidor/busqueda-por-gestion',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:334,
              nombre:'Busqueda en rango mes',
              path:'/user/lectura-medidor/busqueda-por-mes',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },        
            {
              id:435,
              nombre:'Busqueda por gestión, consumo mínimo',
              path:'/user/lectura-medidor/busqueda-por-gestion-minimo',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:436,
              nombre:'Busqueda en rango mes, consumo mínimo',
              path:'/user/lectura-medidor/busqueda-por-mes-minimo',
              icono:'search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },             
          ]
        },
      ]
    },
    {
      id:5,
      nombre:'Cobrar a socio',
      path:'cuenta-socio',
      icono:'paid',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:51,
          nombre:'Consumo de agua - Soc. Activos',
          path:'',
          icono:'water_drop',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:511,
              nombre:'Realizar cobro',
              path:'/user/cuenta-socio/pagar-consumo-agua',
              icono:'payments',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:512,
              nombre:'Historial de pagos',
              path:'/user/cuenta-socio/historial-pago-consumo-agua',
              icono:'manage_search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
          ]
        },
        {
          id:52,
          nombre:'Consumo de agua - Soc. Baja',
          path:'',
          icono:'person_off',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:521,
              nombre:'Realizar cobro',
              path:'/user/cuenta-socio/pagar-consumo-agua-socios-o-medidores-con-baja',
              icono:'payments',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:522,
              nombre:'Historial de pagos',
              path:'/user/cuenta-socio/historial-pago-consumo-agua-socios-o-medidores-con-baja',
              icono:'manage_search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
          ]
        },
        {
          id:53,
          nombre:'Eventos realizados',
          path:'',
          icono:'person_pin_circle',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:531,
              nombre:'Realizar cobro',
              path:'/user/cuenta-socio/pagar-falta-retraso-reuniones',
              icono:'payments',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:532,
              nombre:'Historial de pagos',
              path:'/user/cuenta-socio/historial-falta-retraso-reunion',
              icono:'manage_search',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:54,
          nombre:'Aportes eventuales',
          path:'',
          icono:'money',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:541,
              nombre:'Realizar cobro',
              path:'/user/cuenta-socio/cobro-aporte-eventual',
              icono:'payments',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:542,
              nombre:'Historial pagos',
              path:'/user/cuenta-socio/reporte-socio-aporte-eventual',
              icono:'article',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        }
      ]
    },
    {
      id:6,
      nombre:'Eventos',
      path:'eventos',
      icono:'notifications_active',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:61,
          nombre:'Crear evento',
          path:'/user/eventos/crear-evento',
          icono:'event_available',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:62,
          nombre:'Seguimiento',
          path:'/user/eventos/seguimiento-evento',
          icono:'edit_note',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:63,
          nombre:'Determinaciones',
          path:'/user/eventos/determinaciones-evento',
          icono:'rule',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:64,
          nombre:'Acta',
          path:'/user/eventos/acta-evento',
          icono:'file_copy',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:65,
          nombre:'licencias para socios',
          path:'/user/eventos/licencia',
          icono:'assignment_add',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:66,
          nombre:'Tipos de eventos',
          path:'/user/eventos/tipo-evento',
          icono:'view_comfy',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
      ]
    },
    {
      id:7,
      nombre:'Aportes eventuales',
      path:'cobro-extra',
      icono:'fact_check',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
      funcion:[
        {
          id:71,
          nombre:'Registro de aporte eventual',
          path:'/user/aportes-eventuales/registro-aporte-eventual',
          icono:'view_list',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:72,
          nombre:'Asignar aporte eventual a socio',
          path:'/user/aportes-eventuales/asignar-socio-a-aporte-eventual',
          icono:'people',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        }
      ]
    },
    {
      id:8,
      nombre:'Ingresos extra',
      path:'ingresos-extra',
      icono:'input',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:81,
          nombre:'Ingresos extra',
          path:'/user/ingresos-extra/registro-ingresos-extra',
          icono:'playlist_add_check',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        }
      ]
    },
    {
      id:9,
      nombre:'Egresos extra',
      path:'egresos-extra',
      icono:'output',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:91,
          nombre:'Egresos extra',
          path:'/user/egresos-extra/registro-egresos-extra',
          icono:'playlist_add_check',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        }
      ]
    },
    {
      id:10,
      nombre:'Mesa directiva',
      path:'mesaDirectiva',
      icono:'account_balance',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:101,
          nombre:'Integrantes',
          path:'/user/mesa-directiva/miembro-mesa-directiva',
          icono:'people',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:102,
          nombre:'Configuración',
          path:'/user/mesa-directiva/mesa-directiva',
          icono:'settings',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        }
      ]
    },
    {
      id:11,
      nombre:'Tanque de agua',
      path:'tanque-de-agua',
      icono:'local_drink',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:111,
          nombre:'Ingreso de agua al tanque',
          path:'/user/tanque-de-agua/registro-ingreso-agua-tanque',
          icono:'water_drop',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:112,
          nombre:'Reporte del tanque de agua',
          path:'/user/tanque-de-agua/reporte-tanque-agua',
          icono:'assignment',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1121,
              nombre:'Estado actual',
              path:'/user/tanque-de-agua/estado-actual',
              icono:'search',
              permiso:1,
              seleccionado:false,
              funcion:[],
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
            },
            {
              id:1122,
              nombre:'Reporte de ingreso de agua',
              path:'/user/tanque-de-agua/ingreso-tanque-agua',
              icono:'search',
              permiso:1,
              seleccionado:false,
              funcion:[],
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
            }
          ]
        },
        {
          id:113,
          nombre:'Registro de tanque',
          path:'/user/tanque-de-agua/registro-tanque',
          icono:'grading',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        }
      ]
    },
    {
      id:12,
      nombre:'Canaston',
      path:'canaston',
      icono:'add_shopping_cart',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:121,
          nombre:'Registro de canaston',
          path:'/user/canaston/registro-canaston',
          icono:'view_comfy',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:122,
          nombre:'Detalles de canaston',
          path:'/user/canaston/registro-detalles-canaston',
          icono:'input',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        },
        {
          id:123,
          nombre:'Socios & canaston',
          path:'/user/canaston/socios-canaston',
          icono:'shopping_cart',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        }
      ]
    },
    {
      id:16,
      nombre:'Configurar cobros extra por periodo',
      path:'cobro-extra',
      icono:'edit_note',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
      funcion:[
        {
          id:161,
          nombre:'Establecer cobro extra por periodo',
          path:'/user/cobros-extra/periodo-cobro-extra',
          icono:'date_range',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE,ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:162,
          nombre:'Asignar cobro extra a socios',
          path:'/user/cobros-extra/asignar-cobro-extra-a-socio',
          icono:'fact_check',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },      
        {
          id:163,
          nombre:'Tipos de cobros extra',
          path:'/user/cobros-extra/registro-tipo-cobro-extra',
          icono:'assignment',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:164,
          nombre:'Reporte periodo con sus cobros extra',
          path:'/user/cobros-extra/vista-periodos-con-sus-cobros-extra',
          icono:'view_list',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        }
      ]
    },
    {
      id:13,
      nombre:'Reporte de ingresos economicos',
      path:'informe-ingresos-economicos',
      icono:'file_present',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:131,
          nombre:'Consumo de agua',
          path:'',
          icono:'water_drop',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1311,
              nombre:'Reporte en rango fecha',
              path:'/user/informe-ingresos-economicos/ingreso-cobro-lectura-periodo',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:1312,
              nombre:'Reporte por periodo',
              path:'/user/informe-ingresos-economicos/ingreso-cobro-lectura-periodo-rango-periodo',
              icono:'view_timeline',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },            
            {
              id:1314,
              nombre:'Reporte mensual en rango fecha',
              path:'/user/informe-ingresos-economicos/reporte-ingreso-filtro-mes-lectura-periodo-rango-mes',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:1315,
              nombre:'Reporte mensual en rango periodo',
              path:'/user/informe-ingresos-economicos/reporte-ingreso-filtro-mes-lectura-periodo-rango-periodo',
              icono:'view_timeline',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:132,
          nombre:'Multa por falta o retraso a eventos',
          path:'',
          icono:'emoji_people',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1321,
              nombre:'Reporte en rango fecha',
              path:'/user/informe-ingresos-economicos/ingreso-cobro-multa-reunion',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:133,
          nombre:'Aportes eventuales',
          path:'',
          icono:'person_search',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1331,
              nombre:'Reporte en rango fecha',
              path:'/user/informe-ingresos-economicos/ingreso-cobro-aportes-eventuales',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:134,
          nombre:'Ingresos extra',
          path:'',
          icono:'monetization_on',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1341,
              nombre:'Reporte en rango fecha',
              path:'/user/ingresos-extra/reporte-ingresos-extra-rango-fecha',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:1342,
              nombre:'Reporte por gestión',
              path:'/user/ingresos-extra/reporte-ingresos-extra-by-gestion',
              icono:'view_timeline',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        }
      ]
    },
    {
      id:18,
      nombre:'Reporte de deudas economicas',
      path:'informe-ingresos-economicos',
      icono:'file_present',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:181,
          nombre:'Consumo de agua',
          path:'',
          icono:'water_drop',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1813,
              nombre:'Reporte por periodo',
              path:'/user/informe-ingresos-economicos/deuda-cobro-lectura-periodo-rango-periodo',
              icono:'money_off',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:1816,
              nombre:'Reporte por mes',
              path:'/user/informe-ingresos-economicos/reporte-deuda-filtro-mes-lectura-periodo-rango-periodo',
              icono:'money_off',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:182,
          nombre:'Multa por falta o retraso a eventos',
          path:'',
          icono:'emoji_people',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1822,
              nombre:'Reporte en rango fecha',
              path:'/user/informe-ingresos-economicos/deudas-por-falta-o-retrazo-a-reunion',
              icono:'money_off',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:183,
          nombre:'Aportes eventuales',
          path:'',
          icono:'person_search',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1332,
              nombre:'Reporte en rango fecha',
              path:'/user/informe-ingresos-economicos/deuda-de-socios-por-aportes',
              icono:'money_off',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        }
      ]
    },
    {
      id:14,
      nombre:'Reporte de egresos economicos',
      path:'informe-ingresos-economicos',
      icono:'add_card',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:141,
          nombre:'Agua de tanque',
          path:'',
          icono:'warehouse',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1411,
              nombre:'Rango fecha',
              path:'/user/tanque-de-agua/reporte-ingreso-agua-rango-fecha',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              funcion:[],
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
            },
            {
              id:1412,
              nombre:'Reporte por gestión',
              path:'/user/tanque-de-agua/reporte-ingreso-agua-gestion',
              icono:'format_list_numbered_rtl',
              permiso:1,
              seleccionado:false,
              funcion:[],
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
            }
          ]
        }, 
        {
          id:142,
          nombre:'Canastón',
          path:'',
          icono:'emoji_food_beverage',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1421,
              nombre:'Reporte por gestión',
              path:'/user/canaston/reporte-canaston',
              icono:'format_list_numbered_rtl',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        },
        {
          id:143,
          nombre:'Egresos extra',
          path:'',
          icono:'money_off',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[
            {
              id:1431,
              nombre:'Rango fecha',
              path:'/user/egresos-extra/reporte-ingresos-extra-rango-fecha',
              icono:'date_range',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            },
            {
              id:1432,
              nombre:'Gestión',
              path:'/user/egresos-extra/reporte-egresos-extra-gestion',
              icono:'format_list_numbered_rtl',
              permiso:1,
              seleccionado:false,
              roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
              funcion:[],
            }
          ]
        }
      ]
    },
    {
      id:15,
      nombre:'Reporte economico general',
      path:'informe-ingresos-economicos',
      icono:'attach_money',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
      funcion:[
        {
          id:151,
          nombre:'Reporte por gestión',
          path:'/user/reporte-economico/reporte-economico-general',
          icono:'checklist_rtl',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA, ROL.MIEMBRO_MESA_DIRECTIVA],
          funcion:[],
        }        
      ]
    },
    {
      id:17,
      nombre:'Configuraciones',
      path:'configuraciones',
      icono:'settings',
      permiso:1,
      seleccionado:false,
      roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
      funcion:[
        {
          id:171,
          nombre:'Mes',
          path:'/user/configuraciones/configuracion-mes',
          icono:'today',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:172,
          nombre:'Gestión',
          path:'/user/configuraciones/configuracion-gestion',
          icono:'calendar_month',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:173,
          nombre:'Categorías de medidores',
          path:'/user/configuraciones/configuracion-categoria',
          icono:'space_dashboard',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
        {
          id:174,
          nombre:'Costo de agua',
          path:'/user/configuraciones/configuracion-costo-agua',
          icono:'attach_money',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE],
          funcion:[],
        },
        {
          id:175,
          nombre:'Datos de la asociación de agua',
          path:'/user/configuraciones/datos-comite-de-agua',
          icono:'dataset',
          permiso:1,
          seleccionado:false,
          roles: [ROL.ADMIN, ROL.PRESIDENTE, ROL.SECRETARIA],
          funcion:[],
        },
      ]
    }
  ];

  constructor() { }

  public getMenuDeUsuario():ModuloFuncion[]{
    let rolesDeUsuario:string[] = this.getRolesDeUsuario();
    let menuDeUsuario:ModuloFuncion[] = this.getMenuFiltradoPorRoles(this.menu,rolesDeUsuario);
    return menuDeUsuario;
  }

  private getMenuFiltradoPorRoles(menu:ModuloFuncion[],rolesUsuario:string[]):ModuloFuncion[]{
    let menuRespuesta:ModuloFuncion[] = [];
    for (let i = 0; i < menu.length; i++) {
      if(this.existeRol(menu[i].roles,rolesUsuario)){       
        let subMenu : ModuloFuncion[] = this.getMenuFiltradoPorRoles(menu[i].funcion,rolesUsuario);
        
        let menuClon:ModuloFuncion = {
          id           : menu[i].id,
          nombre       : menu[i].nombre,
          path         : menu[i].path,
          icono        : menu[i].icono,
          permiso      : menu[i].permiso,
          seleccionado : false,
          roles        : menu[i].roles,
          funcion      : subMenu,
        }
        menuRespuesta.push(menuClon);
      }
    }
    return menuRespuesta;
  }

  private existeRol(rolesRuta:string[],rolesUsuario:string[]):boolean{
    let existe:boolean = false;
    let i:number = 0;
    while (i<rolesUsuario.length && !existe) {
      let rol:string = rolesUsuario[i++];
      existe = rolesRuta.includes(rol);
    }
    return existe;
  }

  private getRolesDeUsuario():string[]{
    let token        : string = localStorage.getItem('token'); 
    let decoded      : UserToken = jwt_decode(token); 
    let rolesUsuario : string[]=[];
    decoded.user.roles.forEach(rol => {
      rolesUsuario.push(rol.nombre);
    });
    return rolesUsuario;
  }
}

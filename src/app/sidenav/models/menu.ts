export interface Menu {
  expandable: boolean;
  id:number,
	nombre:string,
	path:string,
	icono:string,
  permiso:number
  level: number;
}

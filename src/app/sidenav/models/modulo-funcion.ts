export interface ModuloFuncion{
  id           : number,
  nombre       : string,
  path         : string,
  icono        : string,
  permiso      : number,
  seleccionado : boolean,
  roles        : string[],
  funcion?     : ModuloFuncion[]
}


import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RootServiceService {
  protected url: string;
  
  constructor(protected endpoint: String, protected httpClient: HttpClient) {
    this.url = (environment.apiUrl + endpoint);
  }
}

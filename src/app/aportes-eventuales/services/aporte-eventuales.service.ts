import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AporteEventualVO } from '../models/aporte-eventual-vo';
import { PageDTO } from '../../shared/models/pageDTO';
import { AporteEventualDTO } from '../models/aporte-eventual-dto';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { CountItemVO } from '../../shared/models/countItemVO';
import { SocioAporteEventualDataVO } from '../models/socio-aporte-eventual-data-vo';

@Injectable({
  providedIn: 'root'
})
export class AporteEventualesService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/aporte-eventual",http);
  }

  public getAporteEventual(id:number): Observable<AporteEventualVO>{
    return this.http.get<AporteEventualVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as AporteEventualVO;
      })
    );
  }

  public getListaAporteEventual(pageDTO:PageDTO): Observable<AporteEventualVO[]>{
    return this.http.get<AporteEventualVO[]>(`${this.url}/lista/${pageDTO.page}/${pageDTO.size}`).pipe(
      map(response => {
        return response as AporteEventualVO[];
      })
    );
  }

  public getCountListaAporteEventual(): Observable<CountItemVO>{
    return this.http.get<CountItemVO>(`${this.url}/count`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public createAporteEventual(aporteEventualDTO:AporteEventualDTO): Observable<AporteEventualVO>{
    return this.http.post<AporteEventualVO>(`${this.url}`,aporteEventualDTO).pipe(
      map(response => {
        return response as AporteEventualVO;
      })
    );
  }

  public updateAporteEventual(id:number,aporteEventualVO:AporteEventualDTO): Observable<AporteEventualVO>{
    return this.http.put<AporteEventualVO>(`${this.url}/${id}`,aporteEventualVO).pipe(
      map(response => {
        return response as AporteEventualVO;
      })
    );
  }

  public deleteAporteEventual(id:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  // socio - aporte eventual
  public buscarAportesEventuales(texto:string): Observable<AporteEventualVO[]>{
    return this.http.get<AporteEventualVO[]>(`${this.url}/busqueda/${texto}`).pipe(
      map(response => {
        return response as AporteEventualVO[];
      })
    );
  }

  public getSociosAportesEventuales(idAporteEventual:number): Observable<SocioAporteEventualDataVO[]>{
    return this.http.get<SocioAporteEventualDataVO[]>(`${this.url}/socio-aporte-eventual/${idAporteEventual}`).pipe(
      map(response => {
        return response as SocioAporteEventualDataVO[];
      })
    );
  }

  public deleteSocioAporteEventualRegistro(idSocioMedidor:number,idAporteEventual:number): Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/socio-aporte-eventual/${idSocioMedidor}/${idAporteEventual}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public createSocioAporteEventualRegistro(idSocioMedidor:number,idAporteEventual:number): Observable<BanderaResponseVO>{
    return this.http.post<BanderaResponseVO>(`${this.url}/socio-aporte-eventual/${idSocioMedidor}/${idAporteEventual}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SocioAporteEventualDTO } from '../models/socio-aporte-eventual-dto';
import { SocioAporteEventualDataVO } from '../models/socio-aporte-eventual-data-vo';
import { SocioAporteEventualVO } from '../models/socio-aporte-eventual-vo';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { ReporteAporteEventualVO } from '../models/reporte-aporte-eventual-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class SocioAporteEventualService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/socio-aporte-eventual",http);
  }

  public cobrarSocioAporteEventual(socioAporteEventualDTO:SocioAporteEventualDTO): Observable<SocioAporteEventualVO>{
    return this.http.post<SocioAporteEventualVO>(`${this.url}`,socioAporteEventualDTO).pipe(
      map(response => {
        return response as SocioAporteEventualVO;
      })
    );
  }

  public cancelarCobroSocioAporteEventual(socioAporteEventualDTO:SocioAporteEventualDTO): Observable<SocioAporteEventualVO>{
    return this.http.put<SocioAporteEventualVO>(`${this.url}`,socioAporteEventualDTO).pipe(
      map(response => {
        return response as SocioAporteEventualVO;
      })
    );
  }

  public getSociosQueNoCancelaronAporteEventual(idAporteEventual:number): Observable<SocioAporteEventualDataVO[]>{
    return this.http.get<SocioAporteEventualDataVO[]>(`${this.url}/lista/no-cancelaron/${idAporteEventual}`).pipe(
      map(response => {
        return response as SocioAporteEventualDataVO[];
      })
    );
  }

  public getSociosQueSiCancelaronAporteEventual(idAporteEventual:number): Observable<SocioAporteEventualDataVO[]>{
    return this.http.get<SocioAporteEventualDataVO[]>(`${this.url}/lista/si-cancelaron/${idAporteEventual}`).pipe(
      map(response => {
        return response as SocioAporteEventualDataVO[];
      })
    );
  }

  // historial ingreso y deudas por fecha
  public getReporteIngresoRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<ReporteAporteEventualVO>{
    return this.http.put<ReporteAporteEventualVO>(`${this.url}/reporte/ingreso-fecha`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteAporteEventualVO;
      })
    );
  }

  public getReporteDeudaRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<ReporteAporteEventualVO>{
    return this.http.put<ReporteAporteEventualVO>(`${this.url}/reporte/deuda-fecha`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteAporteEventualVO;
      })
    );
  }

  // documento recibo
  public updateNombreDocumentoReciboSocioAporteEventual(idAporteEventual:number,idSocioMedidor:number,nombreDocumentoPDF:string): Observable<BanderaResponseVO>{
    return this.http.put<BanderaResponseVO>(`${this.url}/nombre_documento_recibo/${idAporteEventual}/${idSocioMedidor}/${nombreDocumentoPDF}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public createDocumentoReciboSocioAporteEventual(fd:FormData): Observable<any> { 
    return this.http.post<any>(`${this.url}/documento-recibo`,fd).pipe(
      map(response => {
        return response as any;
      })
    );
  }
  
  public descargarDocumentoReciboSocioAporteEventual(idAporteEventual:number,idSocioMedidor:number): Observable<any> { 
    return this.http.get(`${this.url}/documento-recibo/${idAporteEventual}/${idSocioMedidor}`,{responseType: 'blob'});
  }
}
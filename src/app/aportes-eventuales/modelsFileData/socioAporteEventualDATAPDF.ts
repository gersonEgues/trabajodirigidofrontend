import { DatosGeneralesComiteVO } from '../../shared/models/datosGeneralesComiteVO';
export interface SocioAporteEventualDATAPDF {
  nroRecibo             : number,
  codigomedidor         : string,
  fechaActualHibirido   : string,
  fechaActual           : string,
  horaActual            : string,
  nombreAporteEventual  : string,
  codigoSocio           : string,
  nombreCompletoSocio   : string,
  direccionSocio        : string,
  montoAporteNumero     : number,
  montoAporteLetra      : string,
  observacion           : string,
  nombreEncargadoComite : string,
  rolEncargadoComite    : string,
  datosComiteDeAgua     : DatosGeneralesComiteVO;
} 
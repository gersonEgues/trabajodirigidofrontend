import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AportesEventualesRoutingModule } from './aportes-eventuales-routing.module';
import { RegistroAportesEventualesComponent } from './components/registro-aportes-eventuales/registro-aportes-eventuales.component';
import { CobroAportesEventualesComponent } from './components/cobro-aportes-eventuales/cobro-aportes-eventuales.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PdfViewerModule }  from  'ng2-pdf-viewer';
import { ReporteSocioAportesEventualesComponent } from './components/reporte-socio-aportes-eventuales/reporte-socio-aportes-eventuales.component';
import { AsignarAporteEventualASocioComponent } from './components/asignar-aporte-eventual-a-socio/asignar-aporte-eventual-a-socio.component';

@NgModule({
  declarations: [
    RegistroAportesEventualesComponent,
    CobroAportesEventualesComponent,
    ReporteSocioAportesEventualesComponent,
    AsignarAporteEventualASocioComponent
  ],
  imports: [
    CommonModule,
    AportesEventualesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    PdfViewerModule,
  ]
})
export class AportesEventualesModule { }

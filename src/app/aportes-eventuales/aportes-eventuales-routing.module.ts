import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistroAportesEventualesComponent } from './components/registro-aportes-eventuales/registro-aportes-eventuales.component';
import { CobroAportesEventualesComponent } from './components/cobro-aportes-eventuales/cobro-aportes-eventuales.component';
import { ReporteSocioAportesEventualesComponent } from './components/reporte-socio-aportes-eventuales/reporte-socio-aportes-eventuales.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { AsignarAporteEventualASocioComponent } from './components/asignar-aporte-eventual-a-socio/asignar-aporte-eventual-a-socio.component';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'registro-aporte-eventual', 
        component: RegistroAportesEventualesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'asignar-socio-a-aporte-eventual', 
        component: AsignarAporteEventualASocioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', 
        redirectTo : 'registro-aporte-eventual' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AportesEventualesRoutingModule { }

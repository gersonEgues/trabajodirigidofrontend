export interface SocioAporteEventualDataVO{
  idsocio           : number,
  codigoSocio       : string,
  nombreSocio       : string,
  apellidoPaterno   : string,
  apellidoMaterno   : string,
  idSocioMedidor    : number,
  idMedidor         : number,
  codigoMedidor     : string,
  direccion         : string,
  idAporteEventual  : number,
  cancelado         : boolean,
  fechaCancelado    : string,
  numeroRecibo      : number,
  nombreRecibo      : string,

  // 
  seleccionado   : boolean,
}
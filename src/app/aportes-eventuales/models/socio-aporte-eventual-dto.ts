export interface SocioAporteEventualDTO{
  idAporteEventual  : number,
  idSocioMedidor    : number,
  fechaRegistro     : string,
}
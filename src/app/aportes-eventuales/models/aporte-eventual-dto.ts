export interface AporteEventualDTO{
  nombre         : string,
  descripcion    : string,
  fechaRegistro  : string,
  montoAporte    : number
}
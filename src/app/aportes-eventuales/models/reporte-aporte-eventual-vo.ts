import { AporteEventualVO } from './aporte-eventual-vo';

export interface ReporteAporteEventualVO{
  countRows          : number,
  sumaTotal          : number,
  aporteEventualList : AporteEventualVO[];
}
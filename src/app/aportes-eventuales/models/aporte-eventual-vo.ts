import { SocioAporteEventualDataVO } from './socio-aporte-eventual-data-vo';
export interface AporteEventualVO{
  idAporteEventual : number,
  nombre           : string,
  descripcion      : string,
  fechaRegistro    : string,
  montoAporte      : number,

  // historial
  countRows                   : number,
  sumaTotal                   : number,
  socioAporteEventualDataList : SocioAporteEventualDataVO[];
}
export interface ReporteAporteEventualVOTratado{
  indexGlobal : number,
  indexItem   : number,

  nombre           : string,
  fechaRegistro    : string,
  montoAporte      : number,
  countRows        : number,
  sumaTotal        : number,

  // socios aportes eventuales data
  idSocioMedidor    : number,
  codigoSocio       : string,
  nombreSocio       : string,
  apellidoPaterno   : string,
  apellidoMaterno   : string,
  codigoMedidor     : string,
  direccion         : string,
  fechaCancelado    : string,
  numeroRecibo      : number,

  // aux
  seleccionado : boolean,
}
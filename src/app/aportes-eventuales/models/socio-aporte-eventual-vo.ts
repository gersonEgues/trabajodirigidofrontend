export interface SocioAporteEventualVO{
  idAporteEventual : number;
  idSocioMedidor   : number;
  nombreRecibo     : string;
  numeroRecibo     : number;
  fechaRegistro    : string;
  cancelado        : boolean;
}
import { Injectable } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { SocioAporteEventualDATAPDF } from '../modelsFileData/socioAporteEventualDATAPDF';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { SocioAporteEventualService } from '../services/socio-aporte-eventual.service';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class ReciboAporteEventualPdfGeneratorService {

  constructor(private generalService                : GeneralService,
              private socioAporteEventualService : SocioAporteEventualService,
              private router                        : Router) { }
  
  public async generarReciboAporteEventualDeSocioPDF(idAporteEventual:number,idSocioMedidor:number,socioAporteEventualDATAPDF:SocioAporteEventualDATAPDF){
    let nombrePDF = `${socioAporteEventualDATAPDF.nombreCompletoSocio.toUpperCase()} - ${socioAporteEventualDATAPDF.nombreAporteEventual.toUpperCase()} - ${socioAporteEventualDATAPDF.codigomedidor.toUpperCase()}.pdf`;
    let fd = new FormData();
    
    //const pdfDefinitionOpen = pdfMake.createPdf(await this.getPdfDefinition(socioAporteEventualDATAPDF));
    const pdfDefinitionDownload = pdfMake.createPdf(await this.getPdfDefinition(socioAporteEventualDATAPDF));
    const pdfDefinitionUpload = pdfMake.createPdf(await this.getPdfDefinition(socioAporteEventualDATAPDF));
    
    //pdfDefinitionOpen.open();
    pdfDefinitionDownload.download(nombrePDF); 
    pdfDefinitionUpload.getBlob((blob) => {      
      fd.append('file',blob,nombrePDF);
      // actualiza en la tabale el nombre del pdf por el cual se lo encontrara
      this.updateNombreDocumentoReciboSocioAporteEventualService(idAporteEventual,idSocioMedidor,nombrePDF);
      // crea el pdf
      this.createDocumentoReciboSocioAporteEventualService(fd);
    });
  }

  private async getPdfDefinition(socioAporteEventualDATAPDF:SocioAporteEventualDATAPDF){
    const pdfDefinition:any = {
      pageSize: 'A5',
      pageOrientation: 'landscape',
      pageMargins: [ 30, 40, 30, 10 ],
      info :{
        title: `Recibo de constancia por ${socioAporteEventualDATAPDF.nombreAporteEventual.toUpperCase()}`,
        author: 'Asociacion de Agua Potable Cabaña Violeta',
        subject: 'recibo de aporte eventual de socio',
        keywords: 'aporte eventual',
      },
      content : [
        {
          margin : [0,0,0,15],
          columns : [
            {
              width : '25%',
              margin : 0,
              style : 'small',
              columns : [
                [
                  {
                    text : `${socioAporteEventualDATAPDF.datosComiteDeAgua.nombreComite}`,
                  },
                  {
                    text : `Personería Juridica ${socioAporteEventualDATAPDF.datosComiteDeAgua.personeriaJuridica}. Fundado el ${this.generalService.getFechaHibrido(socioAporteEventualDATAPDF.datosComiteDeAgua.fechaFundacion)}`,
                  },
                  {
                    text : `Telf.: ${socioAporteEventualDATAPDF.datosComiteDeAgua.telefono1} - ${socioAporteEventualDATAPDF.datosComiteDeAgua.telefono2}`,
                  },
                  {
                    text : `${socioAporteEventualDATAPDF.datosComiteDeAgua.nombreDeparatmento} - Bolivia`,
                  }
                ],
              ]
            },
            {
              width : '55%',
              style: 'header',
              margin : 0,
              columns : [
                [
                  {
                    image: await this.generalService.getBase64ImageFromURL('assets/icono.png'),
                    fit: [30, 30],
                    margin : [0,0,0,5]
                  },
                  {
                    text: `RECIBO DE APORTE POR "${socioAporteEventualDATAPDF.nombreAporteEventual.toUpperCase()}"`,
                  }
                ]
              ]
            },
            {
              width : '20%',
              margin : 0,
              columns : [              
                [
                  {
                    text : `Recibo AE. Nro: ${socioAporteEventualDATAPDF.nroRecibo}`,
                    style : 'subheader_rigth'
                  },
                  {
                    text:`Cod. Medidor: ${socioAporteEventualDATAPDF.codigomedidor}`,
                    style : 'quote_bold'
                  },
                  {
                    text  : `${socioAporteEventualDATAPDF.datosComiteDeAgua.nombreDeparatmento}, ${this.generalService.getFechaActualHibrido()}`,
                    style : 'quote'
                  },
                ]                
              ],
            }
          ]
        },
        {
          canvas: 
          [
              {
                  type: 'line',
                  x1: 0, 
                  y1: 10,
                  x2: 535, 
                  y2: 10,
                  lineWidth: 0.7
              },
          ]
        },
        {
          width : '100%',
          margin : [0,7,0,0],
          style : 'subheader',
          columns : [
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/user.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `[${socioAporteEventualDATAPDF.codigoSocio}] ${socioAporteEventualDATAPDF.nombreCompletoSocio.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/ubicacion.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `${socioAporteEventualDATAPDF.direccionSocio.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
          ]
        },
        {
        canvas: 
        [
            {
                type: 'line',
                x1: 0, 
                y1: 10,
                x2: 535, 
                y2: 10,
                lineWidth: 0.7
            },
        ]
        },
        {
          width : '100%',
          margin : [0,20,0,40],
          columns : [
            [
              {
                text : `Por medio de este recibo se da fe que el/la socio ${socioAporteEventualDATAPDF.nombreCompletoSocio} cancelo el monto de ${socioAporteEventualDATAPDF.montoAporteNumero} Bs. (${socioAporteEventualDATAPDF.montoAporteLetra} Bolivianos), por concepto de : ${socioAporteEventualDATAPDF.nombreAporteEventual}, en fecha ${socioAporteEventualDATAPDF.fechaActual}.`
              },
              {
                margin : [0,20,0,0],
                style : 'quote_bold',
                columns:[
                  [
                    {
                      text: `Son ${socioAporteEventualDATAPDF.montoAporteLetra} ${socioAporteEventualDATAPDF.montoAporteNumero}/100 Bolivianos.`
                    },
                    {
                      text:  `Fecha y hora de impresión ${socioAporteEventualDATAPDF.fechaActual} ${socioAporteEventualDATAPDF.horaActual}`
                    },
                    {
                      text: `Observaciones : ${socioAporteEventualDATAPDF.observacion}`
                    }
                  ]
                ]
              }
            ]
          ]
        },
        {
          width : '100%',
          style : 'medium_bold_center',
          margin : [0,0,0,30],
          columns : [
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Entregué  conforme',
                  },
                  {
                    text : `${socioAporteEventualDATAPDF.nombreCompletoSocio.toUpperCase()}`,
                  },
                  {
                    text : 'Socio consumidor',
                  },
                ]
              ]
            },
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Recibí conforme',
                  },
                  {
                    text : `${socioAporteEventualDATAPDF.nombreEncargadoComite.toUpperCase()}`,
                  },
                  {
                    text : `${socioAporteEventualDATAPDF.rolEncargadoComite}`,
                  },
                ]
              ]
            },
          ]
        },
        {
          text: 'Nota: Este documento no es una factura, es un recibo como constancia del aporte efectuado por parte del socio',
          style : 'small'
        }
      ],
      styles: {
        header: {
          fontSize  : 13,
          alignment : 'center',
          bold      : true,
        },
        subheader: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_left: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_rigth: {
          fontSize: 11,
          alignment : 'right',
          bold: true
        },
        quote: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
        },
        quote_bold: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
          bold : true,
        },
        quote_bold_center: {
          fontSize  : 9,
          alignment : 'center',
          italics: true,
          bold : true,
        },
        medium_bold_center: {
          fontSize: 9,
          alignment : 'center',
          bold : true,
        },
        small: {
          fontSize: 8,
          alignment : 'left',
        }
      }
    }
    return pdfDefinition;
  }
  
  //-------------------
  // Consumo API-REST |
  //-------------------
  private updateNombreDocumentoReciboSocioAporteEventualService(idAporteEventual:number,idSocioMedidor:number,nombreDocumentoPDF:string){
    this.socioAporteEventualService.updateNombreDocumentoReciboSocioAporteEventual(idAporteEventual,idSocioMedidor,nombreDocumentoPDF).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  private createDocumentoReciboSocioAporteEventualService(fd:FormData){
    this.socioAporteEventualService.createDocumentoReciboSocioAporteEventual(fd).subscribe(
      (resp)=>{},
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

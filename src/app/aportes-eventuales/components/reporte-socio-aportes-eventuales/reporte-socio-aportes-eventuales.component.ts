import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { Observable } from 'rxjs';
import { AporteEventualVO } from '../../models/aporte-eventual-vo';
import { SocioAporteEventualDataVO } from '../../models/socio-aporte-eventual-data-vo';
import { SocioAporteEventualVO } from '../../models/socio-aporte-eventual-vo';
import { MatDialog } from '@angular/material/dialog';
import { AporteEventualesService } from '../../services/aporte-eventuales.service';
import { SocioAporteEventualService } from '../../services/socio-aporte-eventual.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { SocioAporteEventualDTO } from '../../models/socio-aporte-eventual-dto';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-reporte-socio-aportes-eventuales',
  templateUrl: './reporte-socio-aportes-eventuales.component.html',
  styleUrls: ['./reporte-socio-aportes-eventuales.component.css']
})
export class ReporteSocioAportesEventualesComponent implements OnInit {
  aux:any=undefined;
  
  // Buscador
  listaAportesEventualesBusqueda! : Observable<AporteEventualVO[]>;
  aporteEventualSeleccionado      : AporteEventualVO = this.aux;
  
  //Tabla lista de socios - aporte eventual   
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'idSocioMedidor';

  listaSociosSinCancelarAporte : SocioAporteEventualDataVO[] = [];
  socioAporteEventualVO        : SocioAporteEventualVO = this.aux
  
  constructor(private generalService                : GeneralService,
              private dialog                        : MatDialog,
              private aporteEventualesService       : AporteEventualesService,
              private socioAporteEventualService    : SocioAporteEventualService,
              private router                        : Router) { }

  ngOnInit(): void {
    this.configurarTituloTablas();
  }

  private configurarTituloTablas(){
    this.tituloItems = ['#','Cod. Socio','Nombre','Apellido paterno','Apellido materno','Cod.Medidor','Direccion','Fecha cancelado','Numero recibo'];
    this.campoItems  = ['codigoSocio','nombreSocio','apellidoPaterno','apellidoMaterno','codigoMedidor','direccion','fechaCancelado','numeroRecibo'];
    this.idItem      = 'idSocioMedidor';
  }

  // ----- buscador eventos ------
  public seleccionarEventoBusqueda(evento:AporteEventualVO){
    this.limpiarDatos();
    this.aporteEventualSeleccionado = evento;
    if(this.aporteEventualSeleccionado!=this.aux)
      this.getSociosQueSiCancelaronAporteEventual(this.aporteEventualSeleccionado.idAporteEventual); 
  }

  public limpiarDatos(){
    this.aporteEventualSeleccionado = this.aux;
  }

  public seleccionarItem(itemIn:SocioAporteEventualDataVO){
    this.listaSociosSinCancelarAporte.forEach(item => {
      if(itemIn.idSocioMedidor == item.idSocioMedidor){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public async cancelarCobroAporteEventual(item:SocioAporteEventualDataVO){
    let afirmativo:Boolean = await this.getConfirmacion(`CANCELAR COBRO DE APORTE EVENTUAL A SOCIO`,`¿Está seguro de cancelar pago del aporte eventual ${this.aporteEventualSeleccionado.nombre} al socio ${item.nombreSocio} ${item.apellidoPaterno} ${item.apellidoMaterno} ?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){           
        return;
      }

      let socioAporteEventualDTO:SocioAporteEventualDTO = {
        idAporteEventual  : this.aporteEventualSeleccionado.idAporteEventual,
        idSocioMedidor    : item.idSocioMedidor,
        fechaRegistro     : this.generalService.getFechaActualFormateado(),
      }
      this.cancelarCobroSocioAporteEventual(socioAporteEventualDTO);
  }

  public descargarCobroAporteEventual(socioAporteEventualDATA:SocioAporteEventualDataVO){
    this.descargarDocumentoReciboSocioAporteEventualService(socioAporteEventualDATA.idAporteEventual,socioAporteEventualDATA.idSocioMedidor);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }



  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('sociosAporteEventual')
    this.generalService.genearteArchivoXLSX(tableElemnt,'socios que si cancelaron el aporte eventual');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public buscarAporteEventualService(texto:string) {
    this.listaAportesEventualesBusqueda = this.aporteEventualesService.buscarAportesEventuales(texto);
  }

  public getSociosQueSiCancelaronAporteEventual(idAporteEventual:number){
    return this.socioAporteEventualService.getSociosQueSiCancelaronAporteEventual(idAporteEventual).subscribe(
      (resp)=>{
        if(resp){
          this.listaSociosSinCancelarAporte = resp;
          if(this.listaSociosSinCancelarAporte.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarDatos();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarDatos();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cancelarCobroSocioAporteEventual(socioAporteEventualDTO:SocioAporteEventualDTO){
    return this.socioAporteEventualService.cancelarCobroSocioAporteEventual(socioAporteEventualDTO).subscribe(
      (resp)=>{
        if(resp){
          this.socioAporteEventualVO = resp;   
          this.generalService.mensajeCorrecto(INFO_MESSAGE.success_delete);
          this.getSociosQueSiCancelaronAporteEventual(this.aporteEventualSeleccionado.idAporteEventual);
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarDatos();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // documento recibo
  public descargarDocumentoReciboSocioAporteEventualService(idAporteEventual:number,idSocioMedidor:number){
    this.socioAporteEventualService.descargarDocumentoReciboSocioAporteEventual(idAporteEventual,idSocioMedidor).subscribe(
      (resp)=>{
        if(resp){
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url); 
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }               
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

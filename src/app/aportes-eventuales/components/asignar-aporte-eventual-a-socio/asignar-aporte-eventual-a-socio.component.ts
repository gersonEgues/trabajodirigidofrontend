import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { AporteEventualVO } from '../../models/aporte-eventual-vo';
import { BuscadorComponent } from 'src/app/shared/components/buscador/buscador.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { AporteEventualesService } from '../../services/aporte-eventuales.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { SocioAporteEventualDataVO } from '../../models/socio-aporte-eventual-data-vo';

@Component({
  selector: 'app-asignar-aporte-eventual-a-socio',
  templateUrl: './asignar-aporte-eventual-a-socio.component.html',
  styleUrls: ['./asignar-aporte-eventual-a-socio.component.css']
})
export class AsignarAporteEventualASocioComponent implements OnInit {
  aux : any = undefined;

  // Buscador
  listaAportesEventualesBusqueda! : Observable<AporteEventualVO[]>;
  aporteEventualSeleccionado      : AporteEventualVO = this.aux;
  @ViewChild('buscardorComponent') buscardorComponent! : BuscadorComponent; 
  
  
  //Tabla lista de socios - aporte eventual   
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'idSocioMedidor';

  socioAporteList : SocioAporteEventualDataVO[] = [];

  constructor(private generalService          : GeneralService,
              private aporteEventualesService : AporteEventualesService,
              private dialog                  : MatDialog,
              private router                  : Router) { }

  ngOnInit(): void {
    this.configurarTituloTablas();
  }

  private configurarTituloTablas(){
    this.tituloItems = ['#','Cod. Socio','Nombre','Apellido paterno','Apellido materno','Cod.Medidor','Direccion','Fecha cancelado','Aporte cancelado','Numero recibo','Asignado'];
    this.campoItems  = ['codigoSocio','nombreSocio','apellidoPaterno','apellidoMaterno','codigoMedidor','direccion'];
    this.idItem      = 'idSocioMedidor';
  }

  // ----- buscador eventos ------
  public seleccionarEventoBusqueda(evento:AporteEventualVO){
    this.limpiarDatos();
    this.aporteEventualSeleccionado = evento;
    if(this.aporteEventualSeleccionado!=this.aux)
      this.getSociosAportesEventuales(this.aporteEventualSeleccionado.idAporteEventual); 
  }

  public limpiarDatos(){
    this.socioAporteList = [];
  }

  public seleccionarItem(itemIn:SocioAporteEventualDataVO){
    this.socioAporteList.forEach(item => {
      if(itemIn.idSocioMedidor == item.idSocioMedidor){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public async checkSocioAporte(item:SocioAporteEventualDataVO,evento:any){
    if(evento.target.checked){ // asignar aporte eventual al socio
      let afirmativo:Boolean = await this.getConfirmacion(`ASIGNAR APORTE EVENTUAL A SOCIO`,`¿Está seguro de asignar el aporte eventual : ${this.aporteEventualSeleccionado.nombre}? al socio : ${item.nombreSocio} ${item.apellidoPaterno} ${item.apellidoMaterno}`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${item.idSocioMedidor}`);     
        element.checked=false;
        return;
      }
      this.createSocioAporteEventualRegistro(item.idSocioMedidor,this.aporteEventualSeleccionado.idAporteEventual);

    }else{ // desasignar aporte eventual al socio
      let afirmativo:Boolean = await this.getConfirmacion(`DESASIGNAR APORTE EVENTUAL DE SOCIO`,`¿Está seguro de desasignar el aporte eventual : ${this.aporteEventualSeleccionado.nombre}? del socio : ${item.nombreSocio} ${item.apellidoPaterno} ${item.apellidoMaterno}`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${item.idSocioMedidor}`);     
        element.checked=true;
        return;
      }
      this.deleteSocioAporteEventualRegistro(item.idSocioMedidor,this.aporteEventualSeleccionado.idAporteEventual);
    }
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('sociosAporteEventual')
    this.generalService.genearteArchivoXLSX(tableElemnt,'tabla socio - aporte eventual');
  }  

  //-------------------
  // Consumo API-REST |
  //-------------------
  // Evento
  public buscarAporteEventualService(texto:string){
    this.listaAportesEventualesBusqueda = this.aporteEventualesService.buscarAportesEventuales(texto);
  }

  public getSociosAportesEventuales(idAporteEventual:number){
    return this.aporteEventualesService.getSociosAportesEventuales(idAporteEventual).subscribe(
      (resp)=>{
        if(resp){
          this.socioAporteList = resp;
          if(this.socioAporteList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarDatos();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarDatos();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteSocioAporteEventualRegistro(idSocioMedidor:number,idAporteEventual:number) {
    return this.aporteEventualesService.deleteSocioAporteEventualRegistro(idSocioMedidor,idAporteEventual).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se desasigno con exito !!!");
        this.getSociosAportesEventuales(this.aporteEventualSeleccionado.idAporteEventual);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createSocioAporteEventualRegistro(idSocioMedidor:number,idAporteEventual:number) {
    return this.aporteEventualesService.createSocioAporteEventualRegistro(idSocioMedidor,idAporteEventual).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se asigno con exito !!!");
        this.getSociosAportesEventuales(this.aporteEventualSeleccionado.idAporteEventual);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

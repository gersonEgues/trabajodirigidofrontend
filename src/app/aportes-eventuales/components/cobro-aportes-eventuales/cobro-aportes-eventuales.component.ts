import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AporteEventualVO } from '../../models/aporte-eventual-vo';
import { SocioAporteEventualDTO } from '../../models/socio-aporte-eventual-dto';
import { ReciboAporteEventualPdfGeneratorService } from '../../servicesFilesData/recibo-aporte-eventual-pdf-generator.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { Router } from '@angular/router';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { Observable } from 'rxjs';
import { AporteEventualesService } from '../../services/aporte-eventuales.service';
import { SocioAporteEventualDataVO } from '../../models/socio-aporte-eventual-data-vo';
import { SocioAporteEventualService } from '../../services/socio-aporte-eventual.service';
import { SocioAporteEventualVO } from '../../models/socio-aporte-eventual-vo';
import { SocioAporteEventualDATAPDF } from '../../modelsFileData/socioAporteEventualDATAPDF';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-cobro-aportes-eventuales',
  templateUrl: './cobro-aportes-eventuales.component.html',
  styleUrls: ['./cobro-aportes-eventuales.component.css']
})
export class CobroAportesEventualesComponent implements OnInit {
  aux:any=undefined;
  
  // Buscador
  listaAportesEventualesBusqueda! : Observable<AporteEventualVO[]>;
  aporteEventualSeleccionado      : AporteEventualVO = this.aux;
  
  //Tabla lista de socios - aporte eventual   
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'idSocioMedidor';

  listaSociosSinCancelarAporte : SocioAporteEventualDataVO[] = [];
  socioAporteEventualVO        : SocioAporteEventualVO = this.aux

  
  constructor(private generalService                          : GeneralService,
              private dialog                                  : MatDialog,
              private aporteEventualesService                 : AporteEventualesService,
              private socioAporteEventualService              : SocioAporteEventualService,
              private reciboAporteEventualPdfGeneratorService : ReciboAporteEventualPdfGeneratorService,
              private router                                  : Router) { }

  ngOnInit(): void {
    this.configurarTituloTablas();
  }

  private configurarTituloTablas(){
    this.tituloItems = ['#','Cod. Socio','Nombre','Apellido paterno','Apellido materno','Cod.Medidor','Direccion','Seleccionar'];
    this.campoItems  = ['codigoSocio','nombreSocio','apellidoPaterno','apellidoMaterno','codigoMedidor','direccion'];
    this.idItem      = 'idSocioMedidor';
  }

  // ----- buscador eventos ------
  public seleccionarEventoBusqueda(evento:AporteEventualVO){
    this.limpiarDatos();
    this.aporteEventualSeleccionado = evento;
    if(this.aporteEventualSeleccionado!=this.aux)
      this.getSociosQueNoCancelaronAporteEventual(this.aporteEventualSeleccionado.idAporteEventual); 
  }

  public limpiarDatos(){
    this.aporteEventualSeleccionado = this.aux;
  }

  public seleccionarItem(itemIn:SocioAporteEventualDataVO){
    this.listaSociosSinCancelarAporte.forEach(item => {
      if(itemIn.idSocioMedidor == item.idSocioMedidor){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public async checkSocioAporte(item:SocioAporteEventualDataVO,evento:any){
    if(evento.target.checked){ // asignar aporte eventual al socio
      let afirmativo:Boolean = await this.getConfirmacion(`COBRO DE APORTE EVENTUAL A SOCIO`,`¿Está seguro de cobrar el aporte eventual ${this.aporteEventualSeleccionado.nombre} al socio ${item.nombreSocio} ${item.apellidoPaterno} ${item.apellidoMaterno} ?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${item.idSocioMedidor}`);     
        element.checked=false;
        return;
      }

      let socioAporteEventualDTO:SocioAporteEventualDTO = {
        idAporteEventual  : this.aporteEventualSeleccionado.idAporteEventual,
        idSocioMedidor    : item.idSocioMedidor,
        fechaRegistro     : this.generalService.getFechaActualFormateado(),
      }
      this.cobrarSocioAporteEventual(socioAporteEventualDTO,item);
    }
  }

  public descargarReciboPDFSocioAporteEventualEvent(socioAporteEventualDATA:SocioAporteEventualDataVO){
    this.descargarDocumentoReciboSocioAporteEventualService(socioAporteEventualDATA.idAporteEventual,socioAporteEventualDATA.idSocioMedidor);
  }

  // GENERATE PDF
  public generarReciboAporteEventualDeSocioPDF(socioAporteEventualVO : SocioAporteEventualVO,socioAporteEventualData:SocioAporteEventualDataVO){
    
    let socioAporteEventualDATAPDF:SocioAporteEventualDATAPDF = {
      nroRecibo             : socioAporteEventualVO.numeroRecibo,
      codigomedidor         : socioAporteEventualData.codigoMedidor,
      fechaActualHibirido   : this.generalService.getFechaActualHibrido(),
      fechaActual           : this.generalService.getFechaActual(),
      horaActual            : this.generalService.getHoraActualFormateado(),
      nombreAporteEventual  : this.aporteEventualSeleccionado.nombre,
      codigoSocio           : socioAporteEventualData.codigoSocio,
      nombreCompletoSocio   : `${socioAporteEventualData.nombreSocio} ${socioAporteEventualData.apellidoPaterno} ${socioAporteEventualData.apellidoMaterno}`,
      direccionSocio        : socioAporteEventualData.direccion,
      montoAporteNumero     : this.aporteEventualSeleccionado.montoAporte,
      montoAporteLetra      : this. generalService.getNumeroLetra(this.aporteEventualSeleccionado.montoAporte),
      observacion           : 'niguna',
      nombreEncargadoComite : this.generalService.getNombreCompletoUsuarioSession(),
      rolEncargadoComite    : this.generalService.getRolCompletoUsuarioSession(),
      datosComiteDeAgua     : this.generalService.getDatosComiteDeAgua(),
    }
    
    this.reciboAporteEventualPdfGeneratorService.generarReciboAporteEventualDeSocioPDF(this.aporteEventualSeleccionado.idAporteEventual,socioAporteEventualVO.idSocioMedidor,socioAporteEventualDATAPDF);
  } 

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('sociosAporteEventual')
    this.generalService.genearteArchivoXLSX(tableElemnt,'socios que no cancelaron el aporte eventual');
  } 
   
  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public buscarAporteEventualService(texto:string) {
    this.listaAportesEventualesBusqueda = this.aporteEventualesService.buscarAportesEventuales(texto);
  }

  public getSociosQueNoCancelaronAporteEventual(idAporteEventual:number){
    return this.socioAporteEventualService.getSociosQueNoCancelaronAporteEventual(idAporteEventual).subscribe(
      (resp)=>{
        if(resp){
          this.listaSociosSinCancelarAporte = resp;
          if(this.listaSociosSinCancelarAporte.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarDatos();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarDatos();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cobrarSocioAporteEventual(socioAporteEventualDTO:SocioAporteEventualDTO,socioAporteEventualData:SocioAporteEventualDataVO){
    return this.socioAporteEventualService.cobrarSocioAporteEventual(socioAporteEventualDTO).subscribe(
      (resp)=>{
        if(resp){
          this.socioAporteEventualVO = resp;   
          this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);          
          this.getSociosQueNoCancelaronAporteEventual(this.aporteEventualSeleccionado.idAporteEventual);

          this.generarReciboAporteEventualDeSocioPDF(this.socioAporteEventualVO,socioAporteEventualData);
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarDatos();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  // documento recibo
  public descargarDocumentoReciboSocioAporteEventualService(idAporteEventual:number,idSocioMedidor:number){
    this.socioAporteEventualService.descargarDocumentoReciboSocioAporteEventual(idAporteEventual,idSocioMedidor).subscribe(
      (resp)=>{
        if(resp){
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url); 
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }             
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

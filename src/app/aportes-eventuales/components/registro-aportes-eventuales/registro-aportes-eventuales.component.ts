import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AporteEventualDTO } from '../../models/aporte-eventual-dto';
import { AporteEventualVO } from '../../models/aporte-eventual-vo';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { Router } from '@angular/router';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { AporteEventualesService } from '../../services/aporte-eventuales.service';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-registro-aportes-eventuales',
  templateUrl: './registro-aportes-eventuales.component.html',
  styleUrls: ['./registro-aportes-eventuales.component.css']
})
export class RegistroAportesEventualesComponent implements OnInit {
  aux:any=undefined;
  formularioAporteEventual! : FormGroup;

  aporteEventualEditar:AporteEventualVO = this.aux;

  // tabla eventos
  tituloAporteEventual          : string[] = [];
  campoAporteEventual           : string[] = [];
  idAporteEventual              : string = 'idAporteEventual';
  listaItemsAporteEventual!     : AporteEventualVO[];
  countlistaItemsAporteEventual : number = 0;
  pageDTO!                      : PageDTO;
  
  textoBusqueda! : string;

  @ViewChild('tablaAporteEventual') tablaAporteEventual! : TablePaginatorComponent;
  
  constructor(private fb                      : FormBuilder,
              private generalService          : GeneralService,
              private dialog                  : MatDialog,
              private aporteEventualesService : AporteEventualesService,
              private router                  : Router) { }

  ngOnInit(): void {
    this.construirFormularioAporteEventual()

    this.pageDTO = {
      page : 0,
      size : 10,
      sort : '',
    }
    this.construirTablaListaAporteEventual();
    this.getCountListaAporteEventualService();
    this.getListaAporteEventualService(this.pageDTO);
  }

  public construirFormularioAporteEventual(){
    this.formularioAporteEventual = this.fb.group({
      nombre        : ['',[Validators.required],[]],
      descripcion   : ['',[Validators.required],[]],
      montoAporte   : ['',[Validators.required],[]],
      fechaRegistro : [{value:this.generalService.getFechaActualFormateado(),disabled:false},[Validators.required],[]],
    });
  }

  public construirTablaListaAporteEventual(){
    this.tituloAporteEventual = ['#','Nombre','Descripción','Fecha creación','Monto de aporte'];
    this.campoAporteEventual = ['nombre','descripcion','fechaRegistro','montoAporte'];
  }


  public campoAporteEventualValido(campo:string) : boolean{
    return this.formularioAporteEventual.controls[campo].valid;
  }

  public campoAporteEventualInvalido(campo:string) : boolean{   
    return  (this.formularioAporteEventual.controls[campo].invalid && this.formularioAporteEventual.controls[campo].touched);
  }

  public async guardarAporteEventual(){
    if(this.formularioAporteEventual.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioAporteEventual.markAllAsTouched();
      return;
    }

    let nombreVar      : string =  this.formularioAporteEventual.get('nombre')?.value;
    let descripcionVar : string =  this.formularioAporteEventual.get('descripcion')?.value;
    let montoAporteVar : number =  Number(this.formularioAporteEventual.get('montoAporte')?.value);
    let fechaVar       : string =  this.formularioAporteEventual.get('fechaRegistro')?.value;

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE APORTE EVENTUAL',`¿Está seguro de registrar el aporet eventual ${nombreVar}?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){ 
      return;
    }

    let aporteEventualDTO:AporteEventualDTO = {
      nombre        : nombreVar,
      descripcion   : descripcionVar,
      montoAporte   : montoAporteVar,
      fechaRegistro : fechaVar,
    }

    this.createAporteEventualService(aporteEventualDTO);
  }

  public async actualizarAporteEventual(){
    if(this.formularioAporteEventual.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioAporteEventual.markAllAsTouched();
      this.formularioAporteEventual.markAllAsTouched();
      return;
    }

    let nombreVar      : string =  this.formularioAporteEventual.get('nombre')?.value;
    let descripcionVar : string =  this.formularioAporteEventual.get('descripcion')?.value;
    let montoAporteVar : number =  Number(this.formularioAporteEventual.get('montoAporte')?.value);
    let fechaVar       : string =  this.formularioAporteEventual.get('fechaRegistro')?.value;

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZACIÓN DE APORTE EVENTUAL',`¿Está seguro de actualizar el aporet eventual ${nombreVar}?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){ 
      return;
    }

    let idAporteEventual:number = this.aporteEventualEditar.idAporteEventual;

    let aporteEventualDTO:AporteEventualDTO = {
      nombre           : nombreVar,
      descripcion      : descripcionVar,
      montoAporte      : montoAporteVar,
      fechaRegistro    : fechaVar,
    }

    this.updateAporteEventualService(idAporteEventual,aporteEventualDTO);
  }

  public cancelarAporteEventual(){
    this.limpiarFormulario();
  }

  public buscarNombreEvento(){
    this.tablaAporteEventual.buscarItem(this.textoBusqueda,'descripcion');
  }

  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.getCountListaAporteEventualService();
    this.getListaAporteEventualService(this.pageDTO);
  }

  private limpiarFormulario(){
    this.formularioAporteEventual.reset();
    this.formularioAporteEventual.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());
    this.aporteEventualEditar = this.aux;
  }
  
  // component lista aporte eventual
  public updateAporteEventual(aporteEventualVO:AporteEventualVO){
    this.aporteEventualEditar = aporteEventualVO;
    this.formularioAporteEventual.get('nombre')?.setValue(aporteEventualVO.nombre);
    this.formularioAporteEventual.get('descripcion')?.setValue(aporteEventualVO.descripcion);
    this.formularioAporteEventual.get('montoAporte')?.setValue(aporteEventualVO.montoAporte);
    this.formularioAporteEventual.get('fechaRegistro')?.setValue(aporteEventualVO.fechaRegistro);
  }

  public async deleteAporteEventual(aporteEventualVO:AporteEventualVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR APORTE EVENTUAL',`¿Está seguro de eliminar el aporet eventual ${aporteEventualVO.nombre}?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){ 
      return;
    }

    this.deleteAporteEventualService(aporteEventualVO.idAporteEventual);
    this.limpiarFormulario();
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public isSecretaria():boolean{
    return this.generalService.isSecretaria();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaAporteEventual')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de aportes eventuales');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public createAporteEventualService(aporteEventualDTO:AporteEventualDTO){
    return this.aporteEventualesService.createAporteEventual(aporteEventualDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
        this.limpiarFormulario();
        this.getCountListaAporteEventualService();
        this.getListaAporteEventualService(this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateAporteEventualService(id:number,aporteEventualDTO:AporteEventualDTO){
    return this.aporteEventualesService.updateAporteEventual(id,aporteEventualDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_update);
        this.limpiarFormulario();
        this.getCountListaAporteEventualService();
        this.getListaAporteEventualService(this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteAporteEventualService(id:number){
    return this.aporteEventualesService.deleteAporteEventual(id).subscribe(
      (resp)=>{
        this.getCountListaAporteEventualService();
        this.getListaAporteEventualService(this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaAporteEventualService(pageDTO:PageDTO){
    return this.aporteEventualesService.getListaAporteEventual(pageDTO).subscribe(
      (resp)=>{       
        this.listaItemsAporteEventual = resp;
        this.tablaAporteEventual.deseleccionarItems();

        if(resp){
          this.listaItemsAporteEventual = resp;
          this.tablaAporteEventual.deseleccionarItems();
          if(this.listaItemsAporteEventual.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.tablaAporteEventual.deseleccionarItems();          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.tablaAporteEventual.deseleccionarItems();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountListaAporteEventualService(){
    this.aporteEventualesService.getCountListaAporteEventual().subscribe(
      (resp)=>{
        this.countlistaItemsAporteEventual = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

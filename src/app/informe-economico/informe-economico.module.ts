import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformeEconomicoRoutingModule } from './informe-economico-routing.module';
import { ReporteIngresoEconomicoComponent } from './components/reporte-ingreso-economico/reporte-ingreso-economico.component';
import { ReporteEgresoEconomicoComponent } from './components/reporte-egreso-economico/reporte-egreso-economico.component';
import { ReporteEonomicoComponent } from './components/reporte-eonomico/reporte-eonomico.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
  
    ReporteIngresoEconomicoComponent,
       ReporteEgresoEconomicoComponent,
       ReporteEonomicoComponent
  ],
  imports: [
    CommonModule,
    InformeEconomicoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class InformeEconomicoModule { }

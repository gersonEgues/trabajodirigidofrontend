import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { ReporteEgresoVO } from '../models/reporte-egreso-vo';

@Injectable({
  providedIn: 'root'
})
export class ReporteEgresoEconomicoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-egreso-economico",http);
  }
  
  public getReporteEgresoEconomico(gestion:number):Observable<ReporteEgresoVO>{
    return this.http.get<ReporteEgresoVO>(`${this.url}/${gestion}`).pipe(
      map(response => {
        return response as ReporteEgresoVO;
      })
    );
  }
}

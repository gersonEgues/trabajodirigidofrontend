import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { ReporteIngresoVO } from '../models/reporte-ingreso-vo';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReporteIngresoEconomicoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-ingreso-economico",http);
  }
  
  public getReporteIngresoEconomico(gestion:number,idGestion:number):Observable<ReporteIngresoVO>{
    return this.http.get<ReporteIngresoVO>(`${this.url}/${gestion}/${idGestion}`).pipe(
      map(response => {
        return response as ReporteIngresoVO;
      })
    );
  }
}

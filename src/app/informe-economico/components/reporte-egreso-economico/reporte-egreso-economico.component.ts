import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReporteEgresoVO } from '../../models/reporte-egreso-vo';
import { ReporteEgresoEconomicoService } from '../../services/reporte-egreso-economico.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-reporte-egreso-economico',
  templateUrl: './reporte-egreso-economico.component.html',
  styleUrls: ['./reporte-egreso-economico.component.css']
})
export class ReporteEgresoEconomicoComponent implements OnInit {
  aux           : any = undefined;
 
  // tabla
  tituloItems         : string[] = [];
  
  reporteEgresoVO : ReporteEgresoVO=this.aux;

  @Input('gestion') gestion     : number = this.aux;
  @Input('idGestion') idGestion : number = this.aux;

  
  constructor(private generalService                   : GeneralService,
              private reporteEgresoEconomicoService    : ReporteEgresoEconomicoService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Item','Egreso'];
  }

  ngOnChanges(changes: SimpleChanges) {    
    if(changes.gestion.currentValue){
      let gestion   : number = changes.gestion.currentValue;      
      this.getReporteEgresoEconomico(gestion);
    }
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaReporteEgreso')
    this.generalService.genearteArchivoXLSX(tableElemnt,'reporte de ingreos de agua al tanque - rango fecha');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public getReporteEgresoEconomico(gestion:number){
    this.reporteEgresoEconomicoService.getReporteEgresoEconomico(gestion).subscribe(
      (resp)=>{       
        if(resp!=this.aux){
          this.reporteEgresoVO = resp; 
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

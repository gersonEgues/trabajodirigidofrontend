import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-reporte-eonomico',
  templateUrl: './reporte-eonomico.component.html',
  styleUrls: ['./reporte-eonomico.component.css']
})
export class ReporteEonomicoComponent implements OnInit {
  aux           : any = undefined;
  formulario!   : FormGroup;
  listaGestion! : AnioVO[];

  idGestion : number = this.aux;
  gestion   : number = this.aux;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirformularioGestion();
    this.getListaAniosService();
  }

  private construirformularioGestion(){
    this.formulario = this.fb.group({     
      gestion  : ['0', [Validators.required],[]]
    });
  }

  public changeGestion(){
    let idGestion:number = this.formulario.get('gestion').value;
    let gestion:number = this.getGestion(idGestion); 
    
    this.idGestion = idGestion;
    this.gestion   = gestion;
  }

  private getGestion(idGestion:number):number{
    let gestion:number = 0;
    this.listaGestion.forEach(item => {
      if(item.idAnio == idGestion){
        gestion = item.anio;
      }
    });
    return gestion;
  }

  public campoValido():boolean{
    return this.formulario.get('gestion').valid && 
           this.formulario.get('gestion').value != '0';
  }

  public campoInvalido():boolean{
    return this.formulario.get('gestion').touched && 
           this.formulario.get('gestion').invalid;
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getListaAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestion = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

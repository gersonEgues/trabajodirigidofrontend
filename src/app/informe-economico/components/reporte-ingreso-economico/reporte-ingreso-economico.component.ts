import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReporteIngresoEconomicoService } from '../../services/reporte-ingreso-economico.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { ReporteIngresoVO } from '../../models/reporte-ingreso-vo';

@Component({
  selector: 'app-reporte-ingreso-economico',
  templateUrl: './reporte-ingreso-economico.component.html',
  styleUrls: ['./reporte-ingreso-economico.component.css']
})
export class ReporteIngresoEconomicoComponent implements OnInit {
  aux           : any = undefined;
 
  // tabla
  tituloItems         : string[] = [];
  
  reporteIngresoVO : ReporteIngresoVO = this.aux;

  @Input('idGestion') idGestion : number = this.aux;
  @Input('gestion') gestion     : number = this.aux;

  constructor(private generalService                   : GeneralService,
              private reporteIngresoEconomicoService   : ReporteIngresoEconomicoService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Item','Ingreso neto','Ingreso fijo','Deuda fijo','Total fijo'];
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.gestion.currentValue && changes.idGestion.currentValue){
      let gestion   : number = changes.gestion.currentValue;
      let idGestion : number = changes.idGestion.currentValue;
      
      this.getReporteIngresoEconomico(gestion,idGestion);
    }
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaReporteIngreso')
    this.generalService.genearteArchivoXLSX(tableElemnt,'reporte general de ingresos economicos gestion ' + this.gestion);
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public getReporteIngresoEconomico(gestion:number,idGestion:number){
    this.reporteIngresoEconomicoService.getReporteIngresoEconomico(gestion,idGestion).subscribe(
      (resp)=>{
        if(resp!=this.aux){                 
          this.reporteIngresoVO = resp; 
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }  
}

import { IngresoNetoDetallado } from "./ingreso-neto-detallado";

export interface ReporteIngresoNetoDetallado {
  countRows                :  number;
  sumaIngreso              :  number;
  ingresoNetoDetalladoList :  IngresoNetoDetallado[];
}
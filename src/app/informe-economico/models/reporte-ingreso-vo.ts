import { ReporteIngresoNetoDetallado } from './reporte-ingreso-neto-detallado';
export interface ReporteIngresoVO {
  // consumo de agua - lectura de agua
  ingresoNetoConsumoAgua                 : number,
  reporteIngresoNetoConsumoAguaDetallado : ReporteIngresoNetoDetallado,
  ingresoFijoConsumoAgua                 : number,
  deudaIngresoFijoConsumoAgua            : number,
  totalFijoConsumoAgua                   : number,

  // cobro extra
  ingresoNetoCobroExtra                 : number,
  reporteIngresoNetoCobroExtraDetallado : ReporteIngresoNetoDetallado,
  ingresoFijoCobroExtra                 : number,
  deudaIngresoFijoCobroExtra            : number,
  totalFijoCobroExtra                   : number,

  // multa por eventos realizados
  ingresoNetoEventosRealizados           : number,
  reporteIngresoNetoMultaEventoDetallado : ReporteIngresoNetoDetallado,
  ingresoFijoEventosRealizados           : number,
  deudaIngresoFijoEventosRealizados      : number,
  totalFijoEventosRealizados             : number,

  // aporte eventual
  ingresoNetoAporteEventual                 : number,
  reporteIngresoNetoAporteEventualDetallado : ReporteIngresoNetoDetallado;
  ingresoFijoAporteEventual                 : number,
  deudaIngresoFijoAporteEventual            : number,
  totalFijoAporteEventual                   : number,

  // ingreso extra
  ingresoExtra : number,

  // totales
  sumaTotalIngresoNetoConsumoAgua     : number,
  sumaTotalIngresoFijoCobroExtra      : number,
  sumaTotalDeudaFijoEventosRealizados : number,
  sumaTotalFijoAporteEventual         : number,
}
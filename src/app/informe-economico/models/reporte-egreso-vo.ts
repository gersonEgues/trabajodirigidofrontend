export interface ReporteEgresoVO {
  egresoPagoAguaParaTanque : number,
  egresoCanaston           : number,
  egresoExtra              : number,
  sumaTotalEgreso          : number,
}
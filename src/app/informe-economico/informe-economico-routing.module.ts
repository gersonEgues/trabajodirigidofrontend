import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROL } from '../shared/enums-mensajes/roles';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ReporteEonomicoComponent } from './components/reporte-eonomico/reporte-eonomico.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'reporte-economico-general', 
        component: ReporteEonomicoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'crear-evento' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformeEconomicoRoutingModule { }

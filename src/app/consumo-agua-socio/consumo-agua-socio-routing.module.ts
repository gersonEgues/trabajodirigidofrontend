import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LecturaAguaSocioComponent } from './components/lectura-agua-socio/lectura-agua-socio.component';
import { LecturaMedidorSocioComponent } from './components/lectura-medidor-socio/lectura-medidor-socio.component';
import { BusquedaPorGestionComponent } from './components/filtros/busqueda-por-gestion/busqueda-por-gestion.component';
import { BusquedaPorMesComponent } from './components/filtros/busqueda-por-mes/busqueda-por-mes.component';
import { BusquedaPorGestionConsumoMinimoComponent } from './components/filtros/busqueda-por-gestion-consumo-minimo/busqueda-por-gestion-consumo-minimo.component';
import { BusquedaPorMesConsumoMinimoComponent } from './components/filtros/busqueda-por-mes-consumo-minimo/busqueda-por-mes-consumo-minimo.component';
import { BusquedaPorCodigoDeSocioComponent } from './components/filtros/busqueda-por-codigo-de-socio/busqueda-por-codigo-de-socio.component';
import { BusquedaPorCodigoDeMedidorComponent } from './components/filtros/busqueda-por-codigo-de-medidor/busqueda-por-codigo-de-medidor.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'lectura-agua-socio', 
        component: LecturaAguaSocioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'lectura-medidor-socio-guest',
        component: LecturaMedidorSocioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-codigo-de-socio', 
        component: BusquedaPorCodigoDeSocioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-codigo-de-medidor', 
        component: BusquedaPorCodigoDeMedidorComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-gestion', 
        component: BusquedaPorGestionComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-mes', 
        component: BusquedaPorMesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-gestion-minimo', 
        component: BusquedaPorGestionConsumoMinimoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'busqueda-por-mes-minimo', 
        component: BusquedaPorMesConsumoMinimoComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'busqueda-por-codigo-de-socio' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsumoAguaSocioRoutingModule { }

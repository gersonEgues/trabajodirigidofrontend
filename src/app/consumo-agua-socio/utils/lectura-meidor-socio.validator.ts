import {AbstractControl} from '@angular/forms'
import { LecturaMedidorPorUsuarioService } from '../services/lectura-medidor-por-usuario.service';
import { map } from 'rxjs/operators'

export class LecturaSocioMedidorValidator {
  static codigoMediorValido(lecturaMedidorPorUsuarioService:LecturaMedidorPorUsuarioService){
    return (control: AbstractControl) => {
      const value = control.value;
      return lecturaMedidorPorUsuarioService.getDatosSocioMedidorByCodigoMedidor(value)
      .pipe(
        map(response => {
          return response!=null ? null : {error : true};
        })
      );
    };
  }
}
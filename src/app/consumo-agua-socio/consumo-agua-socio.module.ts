import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ConsumoAguaSocioRoutingModule } from './consumo-agua-socio-routing.module';
import { SociosModule } from '../socios/socios.module';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { LecturaAguaSocioComponent } from './components/lectura-agua-socio/lectura-agua-socio.component';
import { LecturaMedidorSocioComponent } from './components/lectura-medidor-socio/lectura-medidor-socio.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { BusquedaPorGestionComponent } from './components/filtros/busqueda-por-gestion/busqueda-por-gestion.component';
import { BusquedaPorMesComponent } from './components/filtros/busqueda-por-mes/busqueda-por-mes.component';
import { BusquedaPorMesConsumoMinimoComponent } from './components/filtros/busqueda-por-mes-consumo-minimo/busqueda-por-mes-consumo-minimo.component';
import { BusquedaPorGestionConsumoMinimoComponent } from './components/filtros/busqueda-por-gestion-consumo-minimo/busqueda-por-gestion-consumo-minimo.component';
import { BusquedaPorCodigoDeSocioComponent } from './components/filtros/busqueda-por-codigo-de-socio/busqueda-por-codigo-de-socio.component';
import { BusquedaPorCodigoDeMedidorComponent } from './components/filtros/busqueda-por-codigo-de-medidor/busqueda-por-codigo-de-medidor.component';


@NgModule({
  declarations: [
    LecturaAguaSocioComponent,
    LecturaMedidorSocioComponent,
    BusquedaPorGestionComponent,
    BusquedaPorMesComponent,
    BusquedaPorMesConsumoMinimoComponent,
    BusquedaPorGestionConsumoMinimoComponent,
    BusquedaPorCodigoDeSocioComponent,
    BusquedaPorCodigoDeMedidorComponent,
  ],
  imports: [
    CommonModule,
    ConsumoAguaSocioRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SociosModule,
    SharedModule,
    AngularMaterialModule, 
    NgxImageZoomModule 
  ]
})
export class ConsumoAguaSocioModule { }

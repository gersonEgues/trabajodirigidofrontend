import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocioMedidorService } from '../../../socio-medidor/services/socio-medidor.service';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { MedidorAsignadoVO } from '../../../socio-medidor/models/medidorAsignadoVO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConsumoAguaSocioService } from '../../services/consumo-agua-socio.service';
import { PeriodoAnioConCostoAguaConLecturaVO } from '../../models/PeriodoAnioConCostoAguaConLecturaVO';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { LecturaPeriodoDTO } from '../../models/lecturaPeriodoDTO';
import { MedidorService } from 'src/app/socio-medidor/services/medidor.service';
import { MedidorVO } from '../../../socio-medidor/models/medidorVO';
import { LecturaPeridoMinMax } from '../../models/lecturaPeridoMinMaxVO';
import { VolumenCostoGestion } from '../../models/volumenCostoGestionVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { LecturaPeriodoVO } from '../../models/lecturaPeriodoVO';
import { ValidatorLecturaService } from '../../services/validator-lectura.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { SocioVO } from 'src/app/shared/models/socio-vo';

@Component({
  selector: 'app-lectura-agua-socio',
  templateUrl: './lectura-agua-socio.component.html',
  styleUrls: ['./lectura-agua-socio.component.css']
})
export class LecturaAguaSocioComponent implements OnInit {
  aux:undefined = null;
  // Buscar Socio
  tituloItems          : string[] = [];
  campoItems           : string[] = [];
  socioSeleccionado!   : SocioVO;

  // medidores de socio
  tituloItemsMedidor     : string[] = [];
  campoItemsMedidor      : string[] = [];
  idItemMedidor          : string = '';
  listaMedidoresDeSocio! : MedidorAsignadoVO[];

  medidorSeleccionado!   : MedidorAsignadoVO;

  // periodos
  formularioGestion!   : FormGroup; 
  listaItemsGestiones! : any[];
  listaItemsPeriodo!   : AnioVO[];

  tituloItemsPeriodo! : string[];
  campoItemsPeriodo!  : string[];
  idItemPeriodo!      : string;

  // Periodos con lectura
  tituloItemsPeriodoLectura! : string[];
  campoItemsPeriodoLectura!  : string[];
  idItemPeriodoLectura!      : string;
  listaItemsPeriodoLectura!  : any[];

  periodoAnioConCostoAguaConLecturaSeleccionado! : PeriodoAnioConCostoAguaConLecturaVO;


  banderaHabilitarGestion : boolean = false;

  // PeriodoAnioConCostoAguaConLectura
  tituloItemsPeriodoAnioConCostoAguaConLectura! : string[];
  campoItemsPeriodoAnioConCostoAguaConLectura!  : string[];
  idItemPeriodoAnioConCostoAguaConLectura!      : string;
  ListaPeriodoAnioConCostoAguaConLectura!       : PeriodoAnioConCostoAguaConLecturaVO[];

  // PeriodoAnioConCostoAguaConLectura
  formularioRegistroPeriodoLectura!   : FormGroup; 
  periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado!       : PeriodoAnioConCostoAguaConLecturaVO;
  periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar! : PeriodoAnioConCostoAguaConLecturaVO;

  lecturaPeridoMinMax  : LecturaPeridoMinMax[] = [];
  volumenCostoGestion! : VolumenCostoGestion;

  @ViewChild('tablaMedidoresSocioComponent') tablaMedidoresSocioComponent!                    : TablaComponent;
  @ViewChild('tablaPeriodosComponent') tablaPeriodosComponent!                                : TablaComponent;
  @ViewChild('tablaPeriodosRegistroLecturaComponent') tablaPeriodosRegistroLecturaComponent!  : TablaComponent;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private socioMedidorService              : SocioMedidorService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private consumoAguaSocioService          : ConsumoAguaSocioService,
              private dialog                           : MatDialog,
              private medidorService                   : MedidorService,
              private validatorLecturaService          : ValidatorLecturaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configurarDatosComponenteBuscador();
    this.configurarDatosTablaMedidoresDeSocio();
    this.configuracionPeriodo();

    this.configuracionFormularioRegistroLecturaPeriodo();
    this.configurarDatosTablaPeriodoAnioLectura();
  }

  // SOCIO
  private configurarDatosComponenteBuscador(){
    this.tituloItems = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Telefono'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','telefono'];
  }

  public socioSeleccionadoEvent(socio:SocioVO){
    this.socioSeleccionado = socio;
    
    this.medidorSeleccionado = this.aux;
    this.periodoAnioConCostoAguaConLecturaSeleccionado = this.aux;
    
    this.listaMedidoresDeSocio = [];
    this.ListaPeriodoAnioConCostoAguaConLectura = [];

    this.tablaMedidoresSocioComponent.limpiarListaItems();
    this.tablaPeriodosComponent.limpiarListaItems();   
    this.getMedidorDeSocioService(socio.idSocio);
  }

  public nuevaBusquedaBuscadorSocio(busqueda:string){
    let aux:any = null;
    this.socioSeleccionado = aux;
    this.medidorSeleccionado = aux;
    this.periodoAnioConCostoAguaConLecturaSeleccionado = aux;

    this.tablaMedidoresSocioComponent.limpiarListaItems();
    this.tablaPeriodosComponent.limpiarListaItems();
    this.tablaPeriodosRegistroLecturaComponent?.limpiarListaItems();
    this.formularioGestion.get('gestion')?.setValue('0');
    this.formularioGestion.get('gestion')?.disable();

    this.limparInformeLecturaSocioMedidor();
  }

  // medidores de socio
  private configurarDatosTablaMedidoresDeSocio(){
    this.tituloItemsMedidor = ['#','Código','L. Inicial','L. Actual','Fecha asignacion','Direccion','Prop. Coope.','Cod. Cat','Tanque','Vol. Act. Tanque M3'];
    this.campoItemsMedidor = ['codigo','lecturaInicial','lecturaActual','fechaAsignacionSM','direccionSM','propiedadCooperativa','codigoCategoria','nombreTanque','volumenActualTanqueAguaM3'];
    this.idItemMedidor = 'idSocioMedidor';
  }

  public medidorSeleccionadoEvent(medidor:MedidorAsignadoVO){
    if(medidor.idMedidor != this.medidorSeleccionado?.idMedidor){
      let aux:any = null;
      this.periodoAnioConCostoAguaConLecturaSeleccionado = aux;
      this.tablaPeriodosComponent?.limpiarListaItems();
      this.tablaPeriodosRegistroLecturaComponent?.limpiarListaItems();
      this.formularioGestion?.get('gestion')?.setValue('0');
    }

    this.medidorSeleccionado = medidor
    this.banderaHabilitarGestion = true;

    if(this.socioSeleccionado!=undefined && this.medidorSeleccionado!=undefined){
      this.formularioGestion.get('gestion')?.enable();
    }

    this.limparInformeLecturaSocioMedidor();
  }

  // periodos
  private configuracionPeriodo() : void{
    this.formularioGestion = this.fb.group({
      gestion : [{value: '0', disabled:true},[Validators.required],[]],
    });

    this.formularioGestion.get('gestion')?.disable;
    this.getGestionesService();
  }

  private configurarDatosTablaPeriodoAnioLectura(){
    this.tituloItemsPeriodoAnioConCostoAguaConLectura = ['#','Mes','Fecha Reg.','Costo M3','Vol. Min.','Costo Min.','Lec. Ant.','Lec. Mes','Consumo M3','Costo Tot.','Consumo Min.','Lecturado'];
    this.campoItemsPeriodoAnioConCostoAguaConLectura  = ['nombreMes','fechaRegistro','costoM3','volumenMinimoM3','costoMinimoM3','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','costoPeriodo','consumoMinimo','lecturado'];
    this.idItemPeriodoAnioConCostoAguaConLectura      = 'idPeriodoCostoAgua';      
  }

  public getPeriodosDeAnioConCostoAguaConLectura(){
    if(this.formularioGestion.invalid){
      return;
    }

    let aux:any = undefined;
    this.periodoAnioConCostoAguaConLecturaSeleccionado = aux;
    
    let idAnio : number = this.formularioGestion.get('gestion')?.value;
    let idCategoria : number = this.medidorSeleccionado.idCategoria;
    let idSocioMedidor : number = this. medidorSeleccionado.idSocioMedidor;
   
    this.getPeriodoAnioConCostoAguaConLecturaService(idAnio,idCategoria,idSocioMedidor);
    this.getInformeLecturaSocioMedidor(idSocioMedidor,idAnio,idCategoria);    
  }

  public periodoSeleccionadoEvent(periodo:PeriodoAnioConCostoAguaConLecturaVO){
    this.periodoAnioConCostoAguaConLecturaSeleccionado = periodo;
    this.cancelarRegistroLecturaPeriodo();
  }

  public getPeriodoSeleccionado(){
    return this.periodoAnioConCostoAguaConLecturaSeleccionado.nombreMes + " - " + this.periodoAnioConCostoAguaConLecturaSeleccionado.anio;
  }

  // Periodo lectura
  private configuracionFormularioRegistroLecturaPeriodo() : void{
    let responsable:string = this.generalService.getNombreCompletoUsuarioSession();
    this.formularioRegistroPeriodoLectura = this.fb.group({
      lecturaPeriodo       : ['',[Validators.required],[]],
      fechaRegistroLectura : [{value : this.generalService.getFechaActualFormateado(),disabled:false},[Validators.required],[]],
      usuarioResponsable   : [{value : responsable, disabled:true},[Validators.required],[]],
    });

    this.formularioGestion.get('gestion')?.disable;
    this.getGestionesService();
  }

  public seleccionarPeriodoRegistroLecturaEvent(periodo:PeriodoAnioConCostoAguaConLecturaVO){
    this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado = periodo;
  }

  public editarPeriodoRegistroLecturaEvent(periodo:PeriodoAnioConCostoAguaConLecturaVO){
    if(!periodo.lecturado){
      this.generalService.mensajeAlerta("No puede actualizar un periodo no lecturado");
      this.periodoAnioConCostoAguaConLecturaSeleccionado = this.aux;
      this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar = this.aux;
      this.cancelarRegistroLecturaPeriodo();
      return;
    }

    this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.setValue(periodo.lecturaActualPeriodo);
    this.formularioRegistroPeriodoLectura.get('fechaRegistroLectura')?.setValue(periodo.fechaRegistro);
    this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar = periodo;
    this.periodoAnioConCostoAguaConLecturaSeleccionado = periodo;
  }

  public async eliminarPeriodoRegistroLecturaEvent(periodo:PeriodoAnioConCostoAguaConLecturaVO){
    let bandera:BanderaResponseVO = await this.esUltimaLecturaPeriodo(this.medidorSeleccionado.idSocioMedidor,periodo.idLecturaPeriodo);
    if(bandera.bandera){
      let perido:string = `${this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado.anio } - ${this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado.nombreMes}`;
      let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR LECTURA PERIODO',`¿Está seguro de eliminar la lectura del periodo ${perido}?`,'Sí, estoy seguro','No, Cancelar');
      if(!afirmativo)
        return;      
        
      this.deleteLecturaPeriodo(this.medidorSeleccionado.idSocioMedidor,periodo.idLecturaPeriodo,periodo.lecturaAnteriorPeriodo);
    }else{
      this.generalService.mensajeAlerta("No se puede eliminar esta lectura, ya que no es el ultimo registro");
    }
  }

  public async guardarRegistroLecturaPeriodo(){
    if(this.validarRegistroDeLectura())
      return;
    
    let lecturaPeriodoNuevo:LecturaPeriodoDTO = {
      idPeriodoCostoAgua : this.periodoAnioConCostoAguaConLecturaSeleccionado.idPeriodoCostoAgua,
      idSocioMedidor     : this.medidorSeleccionado.idSocioMedidor,
      idMedidor          : this.medidorSeleccionado.idMedidor,
      lecturaPeriodo     : this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.value,
      fechaRegistro      : this.formularioRegistroPeriodoLectura.get('fechaRegistroLectura')?.value,
    }

    let lecturaValida:boolean = await this.validatorLecturaService.validarLecturaCrearMedidor(lecturaPeriodoNuevo,this.medidorSeleccionado.lecturaInicial);
    if(!lecturaValida)
      return;
    
    let nuevoVolumen:number = lecturaPeriodoNuevo.lecturaPeriodo - this.medidorSeleccionado.lecturaActual;
    nuevoVolumen = Math.round(nuevoVolumen*100)/100;
    if(this.medidorSeleccionado.volumenActualTanqueAguaM3 < nuevoVolumen ){
      let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR LECTURA PERIODO',`La nueva lectura ${lecturaPeriodoNuevo.lecturaPeriodo}, genera un volumen mayor ${nuevoVolumen} M3 al volumen restante del tanque de agua ${this.medidorSeleccionado.volumenActualTanqueAguaM3} M3, ¿Esta seguro de registrar la nueva lectura?`,'Sí, estoy seguro','No, Cancelar');
      if(!afirmativo){ 
        return; 
      }   
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR LECTURA PERIODO',`¿Está seguro de registrar la lectura ${lecturaPeriodoNuevo.lecturaPeriodo}? en el periodo ${this.getPeriodoSeleccionado()}, para el socio ${this.socioSeleccionado.codigo}`,'Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }   

    let idTanqueAgua:number = this.medidorSeleccionado.idTanqueAgua;
    
    this.createLecturaPeriodoService(idTanqueAgua,lecturaPeriodoNuevo);
  }
  
  public async actualizarRegistroLecturaPeriodo(){
    if(this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado==undefined || this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado==null){
      this.generalService.mensajeFormularioInvalido("Seleccione un medidor para editar");
      return;
    }

    if(this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.invalid){
      this.formularioRegistroPeriodoLectura.markAllAsTouched();
      this.generalService.mensajeFormularioInvalido("Lectura periodo no valido");
      return;
    }

    let id:number = this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado.idLecturaPeriodo;
    let lecturaPeriodoUpdate:LecturaPeriodoDTO = {
      idPeriodoCostoAgua     : this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionado.idPeriodoCostoAgua,
      idSocioMedidor         : this.medidorSeleccionado.idSocioMedidor,
      idMedidor              : this.medidorSeleccionado.idMedidor,
      lecturaPeriodo         : this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.value,
      fechaRegistro          : this.formularioRegistroPeriodoLectura.get('fechaRegistroLectura')?.value,
    }
         
    let lecturaValida:boolean = await this.validatorLecturaService.validarLecturaActualizarMedidor(lecturaPeriodoUpdate,id);
    if(!lecturaValida)
      return;

    let periodo:string = `${this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar.nombreMes} - ${this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar.anio}`;
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR LECTURA PERIODO',`¿Está seguro de actualizar la lectura ${lecturaPeriodoUpdate.lecturaPeriodo}? en el periodo ${periodo}, para el socio ${this.socioSeleccionado.codigo}`,'Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let idTanqueAgua:number = this.medidorSeleccionado.idTanqueAgua;
    this.updateLecturaPeriodoService(id,idTanqueAgua,lecturaPeriodoUpdate);
  }

  public cancelarRegistroLecturaPeriodo(){
    this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.reset();
    let aux:any = undefined;
    this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar = aux;
  }

  private validarRegistroDeLectura():boolean{
    let  invalido : boolean = false;
    if(!this.socioSeleccionado || !this.medidorSeleccionado || !this.periodoAnioConCostoAguaConLecturaSeleccionado){
      if(!this.socioSeleccionado)
        this.generalService.mensajeAlerta("Seleccione un socio");

      if(!this.medidorSeleccionado)
        this.generalService.mensajeAlerta("Seleccione un medidor");

      if(!this.periodoAnioConCostoAguaConLecturaSeleccionado)
        this.generalService.mensajeAlerta("Seleccione un periodo");
      return true;
    }

    if(this.periodoAnioConCostoAguaConLecturaSeleccionado.lecturado && !invalido){
      this.generalService.mensajeAlerta(`El periodo ${this.getPeriodoSeleccionado()}, ya fue lecturado`);      
      invalido = true;
    }

    if(this.formularioRegistroPeriodoLectura.invalid && !invalido){
      this.generalService.mensajeAlerta("Ingrese la lectura del periodo seleccionado");
      this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.markAllAsTouched();
      invalido = true;
    }

    return invalido;
  }

  public campoLecturaValido(nombreCampo:string):boolean{
    return  this.formularioRegistroPeriodoLectura.controls[nombreCampo].valid;
  }

  public campoLecturaInvalido(nombreCampo:string):boolean{
    return  this.formularioRegistroPeriodoLectura.controls[nombreCampo].touched &&
            this.formularioRegistroPeriodoLectura.controls[nombreCampo].invalid;
  } 

  private getInformeLecturaSocioMedidor(idSocioMedidor:number,idAnio:number,idCategoria:number){
    this.getLecturaPeriodoMinMaxConMesService(idSocioMedidor,idAnio,idCategoria);
    this.getSumaLecturaPeriodoCostoAguaService(idSocioMedidor,idAnio,idCategoria);
  }

  private limparInformeLecturaSocioMedidor(){
    let aux:any = undefined;
    this.lecturaPeridoMinMax = [];
    this.volumenCostoGestion = aux;
  }

  public getLecturaMesIncial(){
    if(this.lecturaPeridoMinMax.length == 0){ return '';}

    return this.lecturaPeridoMinMax[0]?.lecturaAnteriorPeriodo + " - " + this.lecturaPeridoMinMax[0]?.nombreMes;
  }

  public getLecturaMesFinal(){
    if(this.lecturaPeridoMinMax.length == 0){return '';}

    return this.lecturaPeridoMinMax[1]?.lecturaActualPeriodo + " - " + this.lecturaPeridoMinMax[1]?.nombreMes;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }
  
  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getMedidorDeSocioService(idSocio:number):void{
    this.socioMedidorService.getMedidorerPorIdDeSocio(idSocio).subscribe(
      (resp:MedidorAsignadoVO[])=>{        
        if(resp && resp.length>0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          this.listaMedidoresDeSocio = resp;
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.listaMedidoresDeSocio = this.aux;
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  private getGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaItemsGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getPeriodosDeAnioConCostoAguaAsignadosService(idAnio:number,idCategoria:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnioConCostoAguaAsignados(idAnio,idCategoria).subscribe(
      (resp)=>{
        this.listaItemsPeriodo = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodoAnioConCostoAguaConLecturaService(idAnio : number,idCategoria : number,idSocioMedidor : number){
    this.consumoAguaSocioService.getPeriodoAnioConCostoAguaConLectura(idAnio,idCategoria,idSocioMedidor).subscribe(
      (resp)=>{
        this.ListaPeriodoAnioConCostoAguaConLectura = resp;
        if(this.ListaPeriodoAnioConCostoAguaConLectura.length==0){
          this.generalService.mensajeAlerta("Revise que para la categoria del medidor y la gestión seleccionada, se haya establecido el costo de agua por M3","Alerta :");
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createLecturaPeriodoService(idTanqueAgua:number, lecturaPeriodoDTO : LecturaPeriodoDTO) {
    this.consumoAguaSocioService.createLecturaPeriodo(idTanqueAgua,lecturaPeriodoDTO).subscribe(
      (resp)=>{
        this.getPeriodosDeAnioConCostoAguaConLectura();
        this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.reset();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
        this.tablaPeriodosComponent.limpiarListaItems();
        this.getMedidorParaActualizarLecturaPeriodoService(this.medidorSeleccionado.idMedidor);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateLecturaPeriodoService(id:number,idTanqueAgua:number,lecturaPeriodoDTO : LecturaPeriodoDTO) {
    this.consumoAguaSocioService.updateLecturaPeriodo(id,idTanqueAgua,lecturaPeriodoDTO).subscribe(
      (resp)=>{
        this.getPeriodosDeAnioConCostoAguaConLectura();
        this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.reset();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_update);
        this.getMedidorParaActualizarLecturaPeriodoService(this.medidorSeleccionado.idMedidor);
        this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar = this.aux;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }



  public deleteLecturaPeriodo(idSocioMedidor:number,idlecturaPeriodo:number,lecturaAnterior:number) {
    this.consumoAguaSocioService.deleteLecturaPeriodo(idSocioMedidor,idlecturaPeriodo,lecturaAnterior).subscribe(
      (resp)=>{
        this.getPeriodosDeAnioConCostoAguaConLectura();
        //this.formularioRegistroPeriodoLectura.get('lecturaPeriodo')?.reset();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_delete);
        this.getMedidorParaActualizarLecturaPeriodoService(this.medidorSeleccionado.idMedidor);
        this.periodoAnioConCostoAguaConLecturaRegistroLecturaSeleccionadoEditar = this.aux;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public esUltimaLecturaPeriodo(idSocioMedidor:number,idlecturaPeriodo:number) : Promise<BanderaResponseVO>{
    return this.consumoAguaSocioService.esUltimaLecturaPeriodo(idSocioMedidor,idlecturaPeriodo).toPromise()
    .then(resp=>{
      return resp;
    })
    .catch(err=>{
      return err;
    });
  }


  private getMedidorParaActualizarLecturaPeriodoService(idMedidor : number){
    this.medidorService.getMedidor(idMedidor).subscribe(
      (resp)=>{
        let medidorVO:MedidorVO = resp;
        this.medidorSeleccionado.lecturaActual = medidorVO.lecturaActual;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getLecturaPeriodoMinMaxConMesService(idSocioMedidor : number, idGestion : number, idCategoria: number) {    
    this.consumoAguaSocioService.getLecturaPeriodoMinMaxConMes(idSocioMedidor, idGestion , idCategoria).subscribe(
      (resp)=>{
        this.lecturaPeridoMinMax = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getSumaLecturaPeriodoCostoAguaService(idSocioMedidor : number, idGestion : number, idCategoria: number) {    
    this.consumoAguaSocioService.getSumaLecturaPeriodoCostoAgua(idSocioMedidor, idGestion , idCategoria).subscribe(
      (resp)=>{
        this.volumenCostoGestion = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getListaLecturaPeriodoByIdSocioMedidor(idSocioMedidor : number) : Promise<LecturaPeriodoVO[]> {
    return this.consumoAguaSocioService.getListaLecturaPeriodoByIdSocioMedidor(idSocioMedidor).toPromise()
      .then(resp=>{
        return resp;
      })
      .catch(err=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

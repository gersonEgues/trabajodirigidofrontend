import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReportesService } from 'src/app/socio-medidor/services/reportes.service';
import { SocioDataVO } from 'src/app/socio-medidor/models/socioDataVO';
import { MedidorDataVO } from 'src/app/socio-medidor/models/medidorDataVO';
import { LecturaMedidorVO } from 'src/app/socio-medidor/models/lecturaMedidorVO';
import { LecturaMedidorSocioDataTratadoVO } from 'src/app/socio-medidor/models/lecturaMedidorSocioDataTratadoVO';
import { CountItemVO } from '../../../../shared/models/countItemVO';
import { PageEventDTO } from 'src/app/shared/models/pageEventDTO';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteSocioDataVO } from 'src/app/socio-medidor/models/reporteSocioDataVO';

@Component({
  selector: 'app-busqueda-por-gestion',
  templateUrl: './busqueda-por-gestion.component.html',
  styleUrls: ['./busqueda-por-gestion.component.css']
})
export class BusquedaPorGestionComponent implements OnInit {
  @ViewChild("paginator") paginator: MatPaginator;


  aux:any = undefined;
  
  formularioBusqueda!  : FormGroup;
  listaGestiones!      : AnioVO[];

  sumaRows:number = 0;
  reporteSocioDataVO                          : ReporteSocioDataVO;
  sociosDataList                              : SocioDataVO[];
  countItemVO                                 : CountItemVO;
  listaTituloLecturaMedidorSocioDataTratadoVO : string[];
  listaSubCamposSocio                         : string[];
  listaSubCamposMedidor                       : string[];
  listaSubCamposLecturaMedidor                : string[];
  listaItemsLecturaMedidorSocioDataTratadoVO  : LecturaMedidorSocioDataTratadoVO[] = [];
  
  lengthList      : number = 0;
  pageSize        : number = 10;
  pageSizeOptions : number[] = [10,20,50,100]
  page            : PageEventDTO;

  numberSocio      : number = 1;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reportesService                  : ReportesService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.getListaGestionesService();
    this.buildCabezeraDeTabla();
    this.getCountSociosReporteLecturaPorGestionService();

    this.page = {
      pageIndex         : 0,
      pageSize          : 10,
      length            : this.lengthList,
      previousPageIndex : -1
    };
  }

  private construirFormulario() : void{
    this.formularioBusqueda = this.fb.group({
      gestion : ['0',[Validators.required],[]],
    });
  }

  public campoGestionValido(campo:string){
    return  this.formularioBusqueda.get(campo).valid &&
            this.formularioBusqueda.get(campo).value!='0';
  }

  public campoGestionInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  private reiniciarPaginator(){
    this.pageSize        = 10;
    this.page = {
      length : this.lengthList,
      pageIndex : 0,
      pageSize : 10,
      previousPageIndex : -1
    };
    
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;

    this.actualizarNumeracion();
  }

  private actualizarNumeracion(){
    this.reporteSocioDataVO = this.aux;
    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
    if(this.page.pageIndex==0)
      this.numberSocio = 1;
    else
      this.numberSocio = (this.page.pageIndex * this.page.pageSize)+1;
  }

  public buscarLecturaPorGestion(){
    if(this.formularioBusqueda.get('gestion').value=='0'){
      return;
    }

    let idAnio : number = this.formularioBusqueda.get('gestion').value;
    this.reiniciarPaginator();
    this.getReporteLecturaPorGestionService(this.page,idAnio);
  }

  private buildCabezeraDeTabla(){
    // titulo items
    this.listaTituloLecturaMedidorSocioDataTratadoVO = ['#','Cod. Socio','Nombre Socio',"# Med.",'Cod. Medidor','Lect. Inicial','Lect. Final','Prop. Coop.','Nombre Cat.','Vol. Cons. M3','# Lect.','Nro. Mes','Mes','Fecha. Reg.','Cons. Min.','Costo M3','Vol. Min.','Costo Min.','Lec. Incial','Lect. Final','Consumo M3','Costo Periodo'];
    
    this.listaSubCamposSocio          = ['codigoSocio','nombreCompletoSocio'];
    this.listaSubCamposMedidor        = ['codigoMedidor','lecturaInicialMedidor','lecturaActualMedidor','propiedadCooperativaMedidor','nombreCategoriaMedidor','volumenConsumidoMedidor','countItemsLecturaPeriodo'];
    this.listaSubCamposLecturaMedidor = ['nroLecturaMedidor','nombreLecturaMedidor','fechaRegistroLecturaMedidor','consumoMinimo','costoM3LecturaMedidor','volumenMinimoM3LecturaMedidor','costoMinimoM3LecturaMedidor','lecturaAnteriorPeriodoLecturaMedidor','lecturaActualPeriodoLecturaMedidor','consumoVolumenM3LecturaMedidor','costoPeriodoLecturaMedidor'];
  }

  private tratarListaLecturaSocioData(){
    let indexItemSocio   : number = 0;
    let indexItemMedidor : number = 0;   
    
    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
    for (let i = 0; i < this.sociosDataList.length; i++) {
      let socio                  : SocioDataVO = this.sociosDataList[i];
      let listadDeMedidoresSocio : MedidorDataVO[] = socio.listaDeMedidores;
        
      indexItemSocio = 0;
      if(listadDeMedidoresSocio.length>0){
        for (let j = 0; j < listadDeMedidoresSocio.length; j++) {
          let medidor:MedidorDataVO = listadDeMedidoresSocio[j];
          let listaLecturaMedidor:LecturaMedidorVO[] = medidor.listaLecturaSocioMedidor;

          indexItemMedidor = 0;
          if(listaLecturaMedidor.length>0){
            for (let k = 0; k < listaLecturaMedidor.length; k++) {
              let lecturaMedidor:LecturaMedidorVO = listaLecturaMedidor[k];
              
              let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
                indexItemSocio : indexItemSocio,
                indexItemMedidor : indexItemMedidor, 
                numberSocio      : this.numberSocio,
                seleccionado     : false,
                
                // socio
                idSocio                    : socio.id,
                codigoSocio                : socio.codigo,
                nombreCompletoSocio        : socio.nombre + ' ' + socio.apellidoPaterno + ' ' + socio.apellidoMaterno,
                countItemsSocio            : socio.countItems,
                countItemsMedidoresDeSocio : socio.listaDeMedidores.length,

                // medidor
                idMedidor                   : medidor.id,
                idSocioMedidor              : medidor.idSocioMedidor,
                codigoMedidor               : medidor.codigo,
                lecturaInicialMedidor       : medidor.lecturaInicial,
                lecturaActualMedidor        : medidor.lecturaActual,
                propiedadCooperativaMedidor : (medidor.propiedadCooperativa)?'SI':'NO',
                idCategoriaMedidor          : medidor.idCategoria,
                nombreCategoriaMedidor      : medidor.nombreCategoria,
                countItemsLecturaPeriodo    : medidor.countItems,
                volumenConsumidoMedidor     : medidor.volumenConsumido,
                //lectura periodo
                nroLecturaMedidor                    : lecturaMedidor.nro,
                nombreLecturaMedidor                 : lecturaMedidor.nombre,
                costoM3LecturaMedidor                : lecturaMedidor.costoM3,
                volumenMinimoM3LecturaMedidor        : lecturaMedidor.volumenMinimoM3,
                costoMinimoM3LecturaMedidor          : lecturaMedidor.costoMinimoM3,
                lecturaAnteriorPeriodoLecturaMedidor : lecturaMedidor.lecturaAnteriorPeriodo,
                lecturaActualPeriodoLecturaMedidor   : lecturaMedidor.lecturaActualPeriodo,
                consumoVolumenM3LecturaMedidor       : lecturaMedidor.consumoVolumenM3,
                costoPeriodoLecturaMedidor           : lecturaMedidor.costoPeriodo,
                fechaRegistroLecturaMedidor          : lecturaMedidor.fechaRegistro,
                consumoMinimo                        : (lecturaMedidor.consumoMinimo==true)?'SI':'NO',
              }           
              this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
              indexItemSocio += 1;
              indexItemMedidor += 1;
            }
          }else{
            let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
              indexItemSocio   : indexItemSocio,
              indexItemMedidor : indexItemMedidor, 
              numberSocio      : this.numberSocio,
              seleccionado     : false,
              
              // socio
              idSocio                    : socio.id,
              codigoSocio                : socio.codigo,
              nombreCompletoSocio        : socio.nombre + ' ' + socio.apellidoPaterno + ' ' + socio.apellidoMaterno,
              countItemsSocio            : socio.countItems,
              countItemsMedidoresDeSocio : socio.listaDeMedidores.length,

              // medidor
              idMedidor                   : medidor.id,
              idSocioMedidor              : medidor.idSocioMedidor,
              codigoMedidor               : medidor.codigo,
              lecturaInicialMedidor       : medidor.lecturaInicial,
              lecturaActualMedidor        : medidor.lecturaActual,
              propiedadCooperativaMedidor : (medidor.propiedadCooperativa)?'SI':'NO',
              idCategoriaMedidor          : medidor.idCategoria,
              nombreCategoriaMedidor      : medidor.nombreCategoria,
              countItemsLecturaPeriodo    : medidor.countItems,
              volumenConsumidoMedidor     : medidor.volumenConsumido,
              //lectura periodo
              nroLecturaMedidor                    : null,
              nombreLecturaMedidor                 : null,
              costoM3LecturaMedidor                : null,
              volumenMinimoM3LecturaMedidor        : null,
              costoMinimoM3LecturaMedidor          : null,
              lecturaAnteriorPeriodoLecturaMedidor : null,
              lecturaActualPeriodoLecturaMedidor   : null,
              consumoVolumenM3LecturaMedidor       : null,
              costoPeriodoLecturaMedidor           : null,
              fechaRegistroLecturaMedidor          : null,
              consumoMinimo                        : null,
            }         
            this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
            indexItemSocio += 1;
            indexItemMedidor += 1;
          }           
        }
        this.numberSocio += 1;
      }else{
        indexItemMedidor = 0;
        let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
          indexItemSocio   : indexItemSocio,
          indexItemMedidor : indexItemMedidor, 
          numberSocio      : this.numberSocio,
          seleccionado     : false,
          
          // socio
          idSocio                    : socio.id,
          codigoSocio                : socio.codigo,
          nombreCompletoSocio        : socio.nombre + ' ' + socio.apellidoPaterno + ' ' + socio.apellidoMaterno,
          countItemsSocio            : socio.countItems,
          countItemsMedidoresDeSocio : socio.listaDeMedidores.length,

          // medidor
          idMedidor                   : null,
          idSocioMedidor              : null,
          codigoMedidor               : null,
          lecturaInicialMedidor       : null,
          lecturaActualMedidor        : null,
          propiedadCooperativaMedidor : null,
          idCategoriaMedidor          : null,
          nombreCategoriaMedidor      : null,
          countItemsLecturaPeriodo    : 0,
          volumenConsumidoMedidor     : null,
          //lectura periodo
          nroLecturaMedidor                    : null,
          nombreLecturaMedidor                 : null,
          costoM3LecturaMedidor                : null,
          volumenMinimoM3LecturaMedidor        : null,
          costoMinimoM3LecturaMedidor          : null,
          lecturaAnteriorPeriodoLecturaMedidor : null,
          lecturaActualPeriodoLecturaMedidor   : null,
          consumoVolumenM3LecturaMedidor       : null,
          costoPeriodoLecturaMedidor           : null,
          fechaRegistroLecturaMedidor          : null,
          consumoMinimo                        : null,
        }
        this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
        this.numberSocio += 1;
      }
    }    
  }

  public seleccionarItem(lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO){
    this.listaItemsLecturaMedidorSocioDataTratadoVO.forEach(lectura => {
      if(lectura.idSocio == lecturaMedidorSocioDataTratadoVO.idSocio)
        lectura.seleccionado = true;
      else
        lectura.seleccionado = false;
    });
  }

  public cambioSizePaginaOrPagina(pageEvent:any){    
    this.page = {
      pageIndex : pageEvent.pageIndex,
      pageSize  : pageEvent.pageSize,
      length    : pageEvent.length,
      previousPageIndex : pageEvent.previousPageIndex
    }    

    let idGestion:number = Number(this.formularioBusqueda.get('gestion').value);
    
    this.actualizarNumeracion();
    this.getReporteLecturaPorGestionService(this.page,idGestion);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableBusquedaPorGestion')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de lectura medidor por gestión');
  }

  // -------------------- 
  // | Consumo API-REST |
  // --------------------
  public getListaGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountSociosReporteLecturaPorGestionService(){
    this.reportesService.getCountSociosReporteLecturaPorGestion().subscribe(
      (resp)=>{
        this.countItemVO = resp;
        this.lengthList = this.countItemVO.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorGestionService(page:PageEventDTO,idAnio:number){
    this.reportesService.getReporteLecturaPorGestion(page,idAnio).subscribe(
      (resp)=>{    
        if(resp){
          this.reporteSocioDataVO = resp;
          this.sociosDataList = this.reporteSocioDataVO.listaDeSocios;
          this.tratarListaLecturaSocioData();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.sociosDataList = this.aux;
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }  
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

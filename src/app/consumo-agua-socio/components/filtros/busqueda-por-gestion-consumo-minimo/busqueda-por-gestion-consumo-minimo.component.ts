import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { ReportesService } from 'src/app/socio-medidor/services/reportes.service';
import { SocioDataVO } from 'src/app/socio-medidor/models/socioDataVO';
import { MedidorDataVO } from 'src/app/socio-medidor/models/medidorDataVO';
import { LecturaMedidorVO } from 'src/app/socio-medidor/models/lecturaMedidorVO';
import { LecturaMedidorSocioDataTratadoVO } from 'src/app/socio-medidor/models/lecturaMedidorSocioDataTratadoVO';
import { CountItemVO } from '../../../../shared/models/countItemVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteSocioDataVO } from 'src/app/socio-medidor/models/reporteSocioDataVO';

@Component({
  selector: 'app-busqueda-por-gestion-consumo-minimo',
  templateUrl: './busqueda-por-gestion-consumo-minimo.component.html',
  styleUrls: ['./busqueda-por-gestion-consumo-minimo.component.css']
})
export class BusquedaPorGestionConsumoMinimoComponent implements OnInit {
  aux:any = undefined;
  
  formularioBusqueda!  : FormGroup;
  listaGestiones!      : AnioVO[];

  sumaRows:number = 0;
  reporteSocioDataVO                          : ReporteSocioDataVO;
  sociosDataList                              : SocioDataVO[];
  countItemVO                                 : CountItemVO;
  listaTituloLecturaMedidorSocioDataTratadoVO : string[];
  listaSubCamposSocio                         : string[];
  listaSubCamposMedidor                       : string[];
  listaSubCamposLecturaMedidor                : string[];
  listaItemsLecturaMedidorSocioDataTratadoVO  : LecturaMedidorSocioDataTratadoVO[] = [];
  
  numberSocio      : number = 1;
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reportesService                  : ReportesService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.getListaGestionesService();
    this.buildCabezeraDeTabla();
  }

  private construirFormulario() : void{
    this.formularioBusqueda = this.fb.group({
      gestion : ['0',[Validators.required],[]],
    });
  }

  public campoGestionValido(campo:string){
    return  this.formularioBusqueda.get(campo).valid &&
            this.formularioBusqueda.get(campo).value!='0';
  }

  public campoGestionInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  public buscarLecturaPorGestion(){
    if(this.formularioBusqueda.get('gestion').value=='0'){
      return;
    }

    let idAnio : number = this.formularioBusqueda.get('gestion').value;
    this.reporteSocioDataVO = this.aux;
    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
    this.numberSocio = 1;
    this.getReporteLecturaPorGestionConsumoMinimoService(idAnio);
  }

  private buildCabezeraDeTabla(){
    // titulo items
    this.listaTituloLecturaMedidorSocioDataTratadoVO = ['#','Cod. Socio','Nombre Socio',"# Med.",'Cod. Medidor','Lect. Inicial','Lect. Final','Prop. Coop.','Nombre Cat.','Vol. Cons. M3','# Lect.','Nro. Mes','Mes','Fecha. Reg.','Cons. Min.','Costo M3','Vol. Min.','Costo Min.','Lec. Incial','Lect. Final','Cons. M3','Costo'];
    
    this.listaSubCamposSocio          = ['codigoSocio','nombreCompletoSocio'];
    this.listaSubCamposMedidor        = ['codigoMedidor','lecturaInicialMedidor','lecturaActualMedidor','propiedadCooperativaMedidor','nombreCategoriaMedidor','volumenConsumidoMedidor','countItemsLecturaPeriodo'];
    this.listaSubCamposLecturaMedidor = ['nroLecturaMedidor','nombreLecturaMedidor','fechaRegistroLecturaMedidor','consumoMinimo','costoM3LecturaMedidor','volumenMinimoM3LecturaMedidor','costoMinimoM3LecturaMedidor','lecturaAnteriorPeriodoLecturaMedidor','lecturaActualPeriodoLecturaMedidor','consumoVolumenM3LecturaMedidor','costoPeriodoLecturaMedidor'];
  }

  private tratarListaLecturaSocioData(){
    let indexItemSocio   : number = 0;
    let indexItemMedidor : number = 0;   
    
    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
    for (let i = 0; i < this.sociosDataList.length; i++) {
      let socio                  : SocioDataVO = this.sociosDataList[i];
      let listadDeMedidoresSocio : MedidorDataVO[] = socio.listaDeMedidores;
        
      indexItemSocio = 0;
     
        for (let j = 0; j < listadDeMedidoresSocio.length; j++) {
          let medidor:MedidorDataVO = listadDeMedidoresSocio[j];
          let listaLecturaMedidor:LecturaMedidorVO[] = medidor.listaLecturaSocioMedidor;

          indexItemMedidor = 0;
          
            for (let k = 0; k < listaLecturaMedidor.length; k++) {
              let lecturaMedidor:LecturaMedidorVO = listaLecturaMedidor[k];
              
              let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
                indexItemSocio : indexItemSocio,
                indexItemMedidor : indexItemMedidor, 
                numberSocio      : this.numberSocio,
                seleccionado     : false,
                
                // socio
                idSocio                    : socio.id,
                codigoSocio                : socio.codigo,
                nombreCompletoSocio        : socio.nombre + ' ' + socio.apellidoPaterno + ' ' + socio.apellidoMaterno,
                countItemsSocio            : socio.countItems,
                countItemsMedidoresDeSocio : socio.listaDeMedidores.length,

                // medidor
                idMedidor                   : medidor.id,
                idSocioMedidor              : medidor.idSocioMedidor,
                codigoMedidor               : medidor.codigo,
                lecturaInicialMedidor       : medidor.lecturaInicial,
                lecturaActualMedidor        : medidor.lecturaActual,
                propiedadCooperativaMedidor : (medidor.propiedadCooperativa)?'SI':'NO',
                idCategoriaMedidor          : medidor.idCategoria,
                nombreCategoriaMedidor      : medidor.nombreCategoria,
                countItemsLecturaPeriodo    : medidor.countItems,
                volumenConsumidoMedidor     : medidor.volumenConsumido,
                //lectura periodo
                nroLecturaMedidor                    : lecturaMedidor.nro,
                nombreLecturaMedidor                 : lecturaMedidor.nombre,
                costoM3LecturaMedidor                : lecturaMedidor.costoM3,
                volumenMinimoM3LecturaMedidor        : lecturaMedidor.volumenMinimoM3,
                costoMinimoM3LecturaMedidor          : lecturaMedidor.costoMinimoM3,
                lecturaAnteriorPeriodoLecturaMedidor : lecturaMedidor.lecturaAnteriorPeriodo,
                lecturaActualPeriodoLecturaMedidor   : lecturaMedidor.lecturaActualPeriodo,
                consumoVolumenM3LecturaMedidor       : lecturaMedidor.consumoVolumenM3,
                costoPeriodoLecturaMedidor           : lecturaMedidor.costoPeriodo,
                fechaRegistroLecturaMedidor          : lecturaMedidor.fechaRegistro,
                consumoMinimo                        : (lecturaMedidor.consumoMinimo)?'SI':'NO',
              }           
              this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
              indexItemSocio += 1;
              indexItemMedidor += 1;
            }
                 
        }
        this.numberSocio += 1;
      
    }
  }

  public seleccionarItem(lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO){
    this.listaItemsLecturaMedidorSocioDataTratadoVO.forEach(lectura => {
      if(lectura.idSocio == lecturaMedidorSocioDataTratadoVO.idSocio)
        lectura.seleccionado = true;
      else
        lectura.seleccionado = false
    });
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableBusquedaPorGestionMinimo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de lectura por conusmo minimo');
  }

  // -------------------- 
  // | Consumo API-REST |
  // --------------------
  public getListaGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorGestionConsumoMinimoService(idAnio:number){
    this.reportesService.getReporteLecturaPorGestionConsumoMinimo(idAnio).subscribe(
      (resp)=>{
        if(resp){
          this.reporteSocioDataVO = resp;
          this.sociosDataList = this.reporteSocioDataVO.listaDeSocios;
          this.tratarListaLecturaSocioData();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.sociosDataList = this.aux;
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}


import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReportesService } from 'src/app/socio-medidor/services/reportes.service';
import { SocioDataVO } from 'src/app/socio-medidor/models/socioDataVO';
import { MedidorDataVO } from 'src/app/socio-medidor/models/medidorDataVO';
import { LecturaMedidorVO } from 'src/app/socio-medidor/models/lecturaMedidorVO';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteMedidorDataVO } from 'src/app/socio-medidor/models/reporteMedidorDataVO';

@Component({
  selector: 'app-busqueda-por-codigo-de-medidor',
  templateUrl: './busqueda-por-codigo-de-medidor.component.html',
  styleUrls: ['./busqueda-por-codigo-de-medidor.component.css']
})
export class BusquedaPorCodigoDeMedidorComponent implements OnInit {
  aux:any = undefined;
  
  formularioBusqueda!  : FormGroup;
  listaGestiones!      : AnioVO[];
  
  reporteMedidorDataVO : ReporteMedidorDataVO;
  medidorDataList      : MedidorDataVO[];

  tituloItemsSocioMedidor : string[] = [];
  campoItemsSocioMedidor  : string[] = [];
  idItemSocioMedidor!     : string;
  listaItemsSocioMedidor! : any[];

  tituloItemsLecturaMedidor : string[] = [];
  campoItemsLecturaMedidor  : string[] = [];
  idItemLecturaMedidor!     : string;
  listaItemsLecturaMedidor! : LecturaMedidorVO[];

  @ViewChild('tableBusquedaLecturaSocioPorCodigoMedidor') tableBusquedaLecturaSocioPorCodigoMedidor! : TablaComponent;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reportesService                  : ReportesService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.getListaGestionesService();
    this.buildCabezeraDeTabla();
  }

  private construirFormulario() : void{
    this.formularioBusqueda = this.fb.group({
      gestion     : ['0',[Validators.required],[]],
      codigoMedidor : ['',[Validators.required],[]],
    });
  }

  public campoValido(campo:string){
    return this.formularioBusqueda.get(campo).valid;
  }

  public campoInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  public campoGestionValido(campo:string){
    return  this.formularioBusqueda.get(campo).valid &&
            this.formularioBusqueda.get(campo).value!='0';
  }

  public campoGestionInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  public buscarLecturaPorCodigoDeMedidor(){
    if(this.formularioBusqueda.get('gestion').value=='0' || this.formularioBusqueda.get('codigoMedidor').value==''){
      this.limpiarTablas();
      return;
    }

    let codigoMedidor : string = this.formularioBusqueda.get('codigoMedidor').value;
    let idAnio        : number = this.formularioBusqueda.get('gestion').value;
    this.limpiarTablas();
    this.getReporteLecturaPorCodigoDeMedidorService(codigoMedidor,idAnio);
  }

  private buildCabezeraDeTabla(){
    //tabla de datos de soci y medidor
    this.tituloItemsSocioMedidor = ['Nro.','Cod. Socio','Nombre Socio','Cod. Medidor','Lect. Inicial','Lect. Final','Prop. Coop.','Nombre Cat.','Vol. Consumido'];
    this.campoItemsSocioMedidor = ['codigoSocio','nombreSocio','codigoMedidor','lecturaInicio','lecturaFinal','propiedadCoop','nombreCategoria','volumenConsumido'];
    this.idItemSocioMedidor = "idSocioMedidor";

    // tabla de lectura medidor
    this.tituloItemsLecturaMedidor = ['#','Cod. Socio','Nombre Socio','# Lec','Nro. Mes','Mes','Fecha. Reg.','Cons. Min.','Costo M3','Vol. Min.','Costo Min.','Lec. Incial','Lect. Final','Cons. M3','Costo Periodo'];
    this.campoItemsLecturaMedidor = ['nro','nombre','fechaRegistro','consumoMinimo','costoM3','volumenMinimoM3','costoMinimoM3','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','costoPeriodo'];
    this.idItemLecturaMedidor = "nro";
  }

  private tratarListaLecturaSocioData(){
    this.listaItemsSocioMedidor = [];
    this.listaItemsLecturaMedidor = [];


    for (let i = 0; i < this.medidorDataList.length; i++) {      
      let medidorData:MedidorDataVO = this.medidorDataList[i];
      let socio : SocioDataVO = medidorData.socio; 
      this.listaItemsSocioMedidor.push({
        codigoSocio      : socio.codigo,
        nombreSocio      : socio.nombre + ' ' + socio.apellidoPaterno + ' ' + socio.apellidoMaterno,
        codigoMedidor    : medidorData.codigo,
        idSocioMedidor   : medidorData.idSocioMedidor,
        lecturaInicio    : medidorData.lecturaInicial,
        lecturaFinal     : medidorData.lecturaActual,
        propiedadCoop    : medidorData.propiedadCooperativa,
        nombreCategoria  : medidorData.nombreCategoria,
        volumenConsumido : medidorData.volumenConsumido,
      });
      
      let lecturaSocio:LecturaMedidorVO[] = medidorData.listaLecturaSocioMedidor;
      for (let j = 0; j < lecturaSocio.length; j++) {
        lecturaSocio[j].index = j;
        lecturaSocio[j].countItems = medidorData.countItems;
        lecturaSocio[j].codigoSocio = socio.codigo;
        lecturaSocio[j].nombreCompletoSocio = socio.nombre + ' '+ socio.apellidoPaterno + ' ' +  socio.apellidoMaterno;
        lecturaSocio[j].consumoMinimo = lecturaSocio[j].consumoMinimo?'SI':'NO';
        this.listaItemsLecturaMedidor.push(lecturaSocio[j]);        
      }
    }
  }

  public seleccionarItem(lecturaMedidorSocioDataTratadoVO:LecturaMedidorVO){
    this.listaItemsLecturaMedidor.forEach(lectura => {
      if(lectura.codigoSocio == lecturaMedidorSocioDataTratadoVO.codigoSocio){
        lectura.seleccionado = true;
      }else{
        lectura.seleccionado = false;
      }
    });
  }

  public limpiarTablas(){
    this.reporteMedidorDataVO = this.aux;
    this.listaItemsSocioMedidor = [];
    this.listaItemsLecturaMedidor = [];
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableBusquedaLecturaSocioPorCodigoMedidor')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de lectura me medidor de los socios por código');
  }

  // -------------------- 
  // | Consumo API-REST |
  // --------------------
  public getListaGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorCodigoDeMedidorService(codigoMedidor:string,idAnio:number){
    this.reportesService.getReporteLecturaPorCodigoDeMedidor(codigoMedidor,idAnio).subscribe(
      (resp)=>{        
        if(resp){
          this.reporteMedidorDataVO = resp;
          this.medidorDataList = this.reporteMedidorDataVO.listaMedidoresDeSocio;
          this.tratarListaLecturaSocioData();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.medidorDataList = this.aux;
          this.limpiarTablas();
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

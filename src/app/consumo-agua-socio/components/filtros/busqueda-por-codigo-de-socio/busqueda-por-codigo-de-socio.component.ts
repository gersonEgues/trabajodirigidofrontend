import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReportesService } from 'src/app/socio-medidor/services/reportes.service';
import { SocioDataVO } from 'src/app/socio-medidor/models/socioDataVO';
import { MedidorDataVO } from 'src/app/socio-medidor/models/medidorDataVO';
import { LecturaMedidorVO } from 'src/app/socio-medidor/models/lecturaMedidorVO';
import { LecturaMedidorSocioDataTratadoVO } from 'src/app/socio-medidor/models/lecturaMedidorSocioDataTratadoVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteSocioDataVO } from 'src/app/socio-medidor/models/reporteSocioDataVO';

@Component({
  selector: 'app-busqueda-por-codigo-de-socio',
  templateUrl: './busqueda-por-codigo-de-socio.component.html',
  styleUrls: ['./busqueda-por-codigo-de-socio.component.css']
})
export class BusquedaPorCodigoDeSocioComponent implements OnInit {
  aux:any = undefined;
  
  formularioBusqueda!  : FormGroup;
  listaGestiones!      : AnioVO[];

  sumaRows:number = 0;
  reporteSocioDataVO                                  : ReporteSocioDataVO;
  socioData                                           : SocioDataVO;
  listaTituloLecturaMedidorSocioDataTratadoVO         : string[];
  listaCamposItemsLecturaMedidorSocioDataTratadoVO    : string[];
  listaCamposSubItemsLecturaMedidorSocioDataTratadoVO : string[];
  listaItemsLecturaMedidorSocioDataTratadoVO          : LecturaMedidorSocioDataTratadoVO[] = [];
  

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reportesService                  : ReportesService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.getListaGestionesService();
    this.buildCabezeraDeTabla();
  }

  private construirFormulario() : void{
    this.formularioBusqueda = this.fb.group({
      gestion     : ['0',[Validators.required],[]],
      codigoSocio : ['',[Validators.required],[]],
    });
  }

  public campoValido(campo:string){
    return this.formularioBusqueda.get(campo).valid;
  }

  public campoInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  public campoGestionValido(campo:string){
    return  this.formularioBusqueda.get(campo).valid &&
            this.formularioBusqueda.get(campo).value!='0';
  }

  public campoGestionInvalido(campo:string){
    return  this.formularioBusqueda.get(campo).invalid &&
            this.formularioBusqueda.get(campo).touched;
  }

  public buscarLecturaPorCodigoDeSocio(){
    if(this.formularioBusqueda.get('gestion').value=='0' || this.formularioBusqueda.get('codigoSocio').value==''){
      this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
      return;
    }

    let codigoSocio : string = this.formularioBusqueda.get('codigoSocio').value;
    let idAnio      : number = this.formularioBusqueda.get('gestion').value;
    this.reporteSocioDataVO = this.aux;
    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];

    this.getReporteLecturaPorCodigoDeSocioService(codigoSocio,idAnio);
  }  

  public cancelarBusquedaLecturaPorCodigoSocio(){
    this.limpiarFormularioAnio();
  }

  private limpiarFormularioAnio(){
    this.aux = undefined;
    this.formularioBusqueda.reset();
  }


  private buildCabezeraDeTabla(){
    // titulo items
    this.listaTituloLecturaMedidorSocioDataTratadoVO = ['#','Cod. Socio','Nombre Socio',"# Med.",'Cod. Medidor','Lect. Inicial','Lect. Final','Prop. Coop.','Nombre Cat.','Vol. Cons. M3','# Lect.','Nro. Mes','Mes','Fecha. Reg.','Cons. Min.','Costo M3','Vol. Min.','Costo Min.','Lec. Incial','Lect. Final','Consumo M3','Costo'];
    
    this.listaCamposItemsLecturaMedidorSocioDataTratadoVO = ['codigoMedidor','lecturaInicialMedidor','lecturaActualMedidor','propiedadCooperativaMedidor','nombreCategoriaMedidor','volumenConsumidoMedidor','countItemsLecturaPeriodo'];
    this.listaCamposSubItemsLecturaMedidorSocioDataTratadoVO = ['nroLecturaMedidor','nombreLecturaMedidor','fechaRegistroLecturaMedidor','consumoMinimo','costoM3LecturaMedidor','volumenMinimoM3LecturaMedidor','costoMinimoM3LecturaMedidor','lecturaAnteriorPeriodoLecturaMedidor','lecturaActualPeriodoLecturaMedidor','consumoVolumenM3LecturaMedidor','costoPeriodoLecturaMedidor'];
  }

  private tratarListaLecturaSocioData(){
    
    // items
    let indexItemMedidor : number = 0;
    this.sumaRows = 0;
    
    for (let i = 0; i < this.socioData.listaDeMedidores.length; i++) {
      let medidorDataVO : MedidorDataVO = this.socioData.listaDeMedidores[i];
      if(medidorDataVO.listaLecturaSocioMedidor.length>0){
        for (let j = 0; j < medidorDataVO.listaLecturaSocioMedidor.length; j++) {
          this.sumaRows+=1; 
        }
      }else{
        this.sumaRows+=1;
      }
    }

    this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
    for (let i = 0; i < this.socioData.listaDeMedidores.length; i++) {
      let medidorDataVO : MedidorDataVO = this.socioData.listaDeMedidores[i];

      if(medidorDataVO.listaLecturaSocioMedidor.length>0){

        for (let j = 0; j < medidorDataVO.listaLecturaSocioMedidor.length; j++) {
          let lecturaMedidorVO : LecturaMedidorVO = medidorDataVO.listaLecturaSocioMedidor[j];
  
          let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
            indexItemMedidor : indexItemMedidor, 
            seleccionado : false,
            
            // medidor
            idMedidor                   : medidorDataVO.id,
            idSocioMedidor              : medidorDataVO.idSocioMedidor,
            codigoMedidor               : medidorDataVO.codigo,
            lecturaInicialMedidor       : medidorDataVO.lecturaInicial,
            lecturaActualMedidor        : medidorDataVO.lecturaActual,
            propiedadCooperativaMedidor : (medidorDataVO.propiedadCooperativa)?'SI':'NO',
            idCategoriaMedidor          : medidorDataVO.idCategoria,
            nombreCategoriaMedidor      : medidorDataVO.nombreCategoria,
            countItemsLecturaPeriodo    : medidorDataVO.countItems,
            volumenConsumidoMedidor     : medidorDataVO.volumenConsumido,
            //lectura periodo
            nroLecturaMedidor                    : lecturaMedidorVO.nro,
            nombreLecturaMedidor                 : lecturaMedidorVO.nombre,
            costoM3LecturaMedidor                : lecturaMedidorVO.costoM3,
            volumenMinimoM3LecturaMedidor        : lecturaMedidorVO.volumenMinimoM3,
            costoMinimoM3LecturaMedidor          : lecturaMedidorVO.costoMinimoM3,
            lecturaAnteriorPeriodoLecturaMedidor : lecturaMedidorVO.lecturaAnteriorPeriodo,
            lecturaActualPeriodoLecturaMedidor   : lecturaMedidorVO.lecturaActualPeriodo,
            consumoVolumenM3LecturaMedidor       : lecturaMedidorVO.consumoVolumenM3,
            costoPeriodoLecturaMedidor           : lecturaMedidorVO.costoPeriodo,
            fechaRegistroLecturaMedidor          : lecturaMedidorVO.fechaRegistro,
            consumoMinimo                        : (lecturaMedidorVO.consumoMinimo)?'SI':'NO',
          }
  
          this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
          indexItemMedidor +=1;
        }   
        indexItemMedidor = 0;
      }else{
        indexItemMedidor = 0;
        let lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO = {
          indexItemMedidor : indexItemMedidor, 
          seleccionado : false,
          
          // medidor
          idMedidor                   : medidorDataVO.id,
          idSocioMedidor              : medidorDataVO.idSocioMedidor,
          codigoMedidor               : medidorDataVO.codigo,
          lecturaInicialMedidor       : medidorDataVO.lecturaInicial,
          lecturaActualMedidor        : medidorDataVO.lecturaActual,
          propiedadCooperativaMedidor : (medidorDataVO.propiedadCooperativa)?'SI':'NO',
          idCategoriaMedidor          : medidorDataVO.idCategoria,
          nombreCategoriaMedidor      : medidorDataVO.nombreCategoria,
          countItemsLecturaPeriodo    : medidorDataVO.countItems,
          volumenConsumidoMedidor     : 0,
          
          //lectura periodo
          nroLecturaMedidor                    : null,
          nombreLecturaMedidor                 : null,
          costoM3LecturaMedidor                : null,
          volumenMinimoM3LecturaMedidor        : null,
          costoMinimoM3LecturaMedidor          : null,
          lecturaAnteriorPeriodoLecturaMedidor : null,
          lecturaActualPeriodoLecturaMedidor   : null,
          consumoVolumenM3LecturaMedidor       : null,
          costoPeriodoLecturaMedidor           : null,
          fechaRegistroLecturaMedidor          : null,
          consumoMinimo                        : null,
        }
        this.listaItemsLecturaMedidorSocioDataTratadoVO.push(lecturaMedidorSocioDataTratadoVO);
      }
    }
  }

  public seleccionarItem(lecturaMedidorSocioDataTratadoVO:LecturaMedidorSocioDataTratadoVO){
    this.listaItemsLecturaMedidorSocioDataTratadoVO.forEach(lectura => {
      lectura.seleccionado = false;
    });
    lecturaMedidorSocioDataTratadoVO.seleccionado = true;
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableBusquedaLecturaSocioPorCodigo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de lectura me medidor de los socios por código');
  }

  //-------------------
  // Consumo API-REST |
  //-------------------
  public getListaGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorCodigoDeSocioService(codigoSocio:string,idAnio:number){
    this.reportesService.getReporteLecturaPorCodigoDeSocio(codigoSocio,idAnio).subscribe(
      (resp)=>{
        if(resp){
          this.reporteSocioDataVO = resp;
          this.socioData = this.reporteSocioDataVO.listaDeSocios[0];
          this.tratarListaLecturaSocioData();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.socioData = this.aux;
          this.listaItemsLecturaMedidorSocioDataTratadoVO = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AnioVO } from '../../../configuraciones/models/anioVO';
import { LecturaMedidorPorUsuarioService } from '../../services/lectura-medidor-por-usuario.service';
import { DatosSocioMedidorVO } from '../../models/datosSocioMedidorVO';
import { LecturaSocioMedidorValidator } from '../../utils/lectura-meidor-socio.validator';
import { PeriodoActivoVO } from '../../models/periodoActivoVO';
import { DatosLecturaPeriodoSocioMedidorVO } from '../../models/datosLecturaPeriodoSocioMedidorVO';
import { ConsumoAguaSocioService } from '../../services/consumo-agua-socio.service';
import { LecturaPeriodoDTO } from '../../models/lecturaPeriodoDTO';
import { PeriodoCostoAguaVO } from '../../../configuraciones/models/periodoCostoAguaVO';
import { ValidatorLecturaService } from '../../services/validator-lectura.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-lectura-medidor-socio',
  templateUrl: './lectura-medidor-socio.component.html',
  styleUrls: ['./lectura-medidor-socio.component.css']
})
export class LecturaMedidorSocioComponent implements OnInit {
  aux:any = undefined;

  listaGestiones! : AnioVO[];
  periodoActivo!  : PeriodoActivoVO;

  tituloLecturaPeriodo : string[] = [];
  campoLecturaPeriodo  : string[] = [];
  idLecturaPeriodo!    : string;
  listaLecturaPeriodo! : any[];
  datosSocioMedidor!   : DatosSocioMedidorVO;

  formularioLecturaPeriodo!  : FormGroup;

  // medidores de socio
  tituloLecturaPeriodoSocioMedidor      : string[] = [];
  campoLecturaPeriodoSocioMedidor       : string[] = [];
  idItemLecturaPeriodoSocioMedidor      : string = '';
  listaDatosLecturaPeriodoSocioMedidor! : DatosLecturaPeriodoSocioMedidorVO[];

  periodoCostoAguaData                  : PeriodoCostoAguaVO  = this.aux;
  datosLecturaPeriodoSocioMedidorEditar : DatosLecturaPeriodoSocioMedidorVO = this.aux;

  // pdf
  srcBase64 : any = this.aux;
  file      : File = this.aux;
  zoomValue : number = 1;
  fileName  : string = '';

  constructor(private fb                              : FormBuilder,
              private generalService                  : GeneralService,
              private dialog                          : MatDialog,
              private lecturaMedidorPorUsuarioService : LecturaMedidorPorUsuarioService,
              private consumoAguaSocioService         : ConsumoAguaSocioService,
              private validatorLecturaService         : ValidatorLecturaService,
              private router                          : Router) { }

  ngOnInit(): void {
    this.constriurFormularioLecturaPeriodo();
    this.constriurTablaLecturaPeriodo();
    this.getPeriodoActivoService();   
    this.configurarDatosComponenteBuscador();
  }

  private constriurFormularioLecturaPeriodo() : void{
    this.formularioLecturaPeriodo = this.fb.group({
      periodo       : ['0',[Validators.required],[]],
      codigoMedidor : ['',[Validators.required],[LecturaSocioMedidorValidator.codigoMediorValido(this.lecturaMedidorPorUsuarioService)]],
      lectura       : ['',[Validators.required,Validators.min(0)],[]],
      imagen        : ['',[],[]],
    });
    this.formularioLecturaPeriodo.get('imagen')?.disable();
  }

  private constriurTablaLecturaPeriodo() : void{
    this.tituloLecturaPeriodo  = ['#','Gestión'];;
    this.campoLecturaPeriodo   = ['anio'];
    this.idLecturaPeriodo!     = '';
  }

  public formularioLecturaPeriodoSelectValido(campo:string) : boolean{
    return this.formularioLecturaPeriodo.controls[campo].valid && this.formularioLecturaPeriodo.controls[campo].value!='0';
  }

  public formularioLecturaPeriodoSelectInvalido(campo:string) : boolean{   
    return (this.formularioLecturaPeriodo.controls[campo].invalid && 
           this.formularioLecturaPeriodo.controls[campo].touched) || 
           (this.formularioLecturaPeriodo.controls[campo].touched && 
            this.formularioLecturaPeriodo.controls[campo].value=='0');
  }

  public formularioLecturaPeriodoValido(campo:string) : boolean{
    return this.formularioLecturaPeriodo.controls[campo].valid
  }

  public formularioLecturaPeriodoInvalido(campo:string) : boolean{   
    return this.formularioLecturaPeriodo.controls[campo].invalid && 
           this.formularioLecturaPeriodo.controls[campo].touched;
  }

  public getFechaActual():string{
    return this.generalService.getFechaActualFormateadoDiaMesAnio();
  }

  public buscarDatosSocioMedidorService(){
    let codigoMedidor:string =  this.formularioLecturaPeriodo.get('codigoMedidor')?.value;   

    if(codigoMedidor){     
      this.getDatosSocioMedidorByCodigoMedidorService(codigoMedidor);
    }else{
      this.listaDatosLecturaPeriodoSocioMedidor = [];
      this.periodoCostoAguaData = this.aux;
      this.datosSocioMedidor = this.aux;
      this.formularioLecturaPeriodo.get('imagen')?.disable();
      this.formularioLecturaPeriodo.get('imagen')?.reset();  
      this.srcBase64=this.aux;
    }
  }

  public async guardarLecturaPeriodo(){
    if(this.formularioLecturaPeriodo.invalid || this.formularioLecturaPeriodo.get('gestion')?.value=='0' || this.formularioLecturaPeriodo.get('periodo')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioLecturaPeriodo.markAllAsTouched();
      return;
    }
   
    if(this.periodoCostoAguaData==this.aux){
      this.generalService.mensajeAlerta(`No existe un registro de costo de agua para el periodo : ${this.periodoActivo?.nombre} - ${this.periodoActivo?.anio}, para la categoría : ${this.datosSocioMedidor.codigoCategoria} - ${this.datosSocioMedidor.nombreCategoria}` ); 
      return;
    }

    let lecturaPeriodoNuevo : LecturaPeriodoDTO = {
      idPeriodoCostoAgua    : this.periodoCostoAguaData.idPeriodoCostoAgua,
      idSocioMedidor        : this.datosSocioMedidor.idSocioMedidor,
      idMedidor             : this.datosSocioMedidor.idMedidor,
      lecturaPeriodo        : Number(this.formularioLecturaPeriodo.get('lectura')?.value),
      fechaRegistro         : this.getFechaActual(),
    }

    let lecturaValida:boolean = await this.validatorLecturaService.validarLecturaCrearMedidor(lecturaPeriodoNuevo,this.datosSocioMedidor.lecturaInicial);
    if(!lecturaValida)
      return;
  
    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR LECTURA DEL MEDIDOR','¿Está seguro de registrar la lectura del medidor del socio?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo)
      return;

    let idTanqueAgua:number = this.datosSocioMedidor.idTanqueAgua;
    this.createLecturaPeriodoService(idTanqueAgua,lecturaPeriodoNuevo);   
  }
  
  public async actualizarLecturaPeriodo(){
    if(this.formularioLecturaPeriodo.invalid || this.formularioLecturaPeriodo.get('gestion')?.value=='0' || this.formularioLecturaPeriodo.get('periodo')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioLecturaPeriodo.markAllAsTouched();
      return;
    }

    let id:number = this.datosLecturaPeriodoSocioMedidorEditar.idLecturaPeriodo;
    let lecturaPeriodoUpdate:LecturaPeriodoDTO = {     
      idPeriodoCostoAgua     : this.datosLecturaPeriodoSocioMedidorEditar.idPeriodoCostoAgua,
      idSocioMedidor         : this.datosLecturaPeriodoSocioMedidorEditar.idsocioMedidor,
      idMedidor              : this.datosLecturaPeriodoSocioMedidorEditar.idMedidor,
      lecturaPeriodo         : this.formularioLecturaPeriodo.get('lectura')?.value,
      fechaRegistro          : this.getFechaActual(),
    }

    let lecturaValida:boolean = await this.validatorLecturaService.validarLecturaActualizarMedidor(lecturaPeriodoUpdate,id);
    if(!lecturaValida)
      return;
    
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR LECTURA DEL MEDIDOR','¿Está seguro de actualizar la lectura del medidor del socio?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo) 
      return;

    let idTanqueAgua:number = this.datosSocioMedidor.idTanqueAgua;
    this.updateLecturaPeriodoService(id,idTanqueAgua,lecturaPeriodoUpdate);
  }

  public cancelarLecturaPeriodo(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.srcBase64=this.aux;
    this.periodoCostoAguaData = this.aux;
    this.datosLecturaPeriodoSocioMedidorEditar = this.aux;
    this.datosSocioMedidor = this.aux;

    this.formularioLecturaPeriodo.get('codigoMedidor')?.setValue('');
    //this.formularioLecturaPeriodo.get('periodo')?.setValue('0');
    this.formularioLecturaPeriodo.get('periodo')?.enable();
    this.formularioLecturaPeriodo.get('codigoMedidor')?.enable();
    this.formularioLecturaPeriodo.reset();
    this.formularioLecturaPeriodo.get('periodo')?.setValue('0');
  }

  // imagen de la lectura del medidor
  public changeFile(event:any){   
    this.file = event.target.files[0];

    if(this.file){
      let name:string = this.file.name;  
      let extencion:string = String(name.split('.').pop());
      if(extencion!="jpeg" && extencion!="jpg" && extencion!="png" ){
        this.generalService.mensajeAlerta("El archivo tiene que ser una imgen");
        this.srcBase64 = this.aux;
        this.formularioLecturaPeriodo.get('imagen')?.reset();
        return;
      }

      if((this.file.size/1000000)>10){
        this.generalService.mensajeAlerta("El archivo exede los 10 MB");
        this.srcBase64 = this.aux;
        this.formularioLecturaPeriodo.get('imagen')?.reset();
        return;
      }

      this.getBase64(this.file);
    }
  }

  private getBase64(file:File){
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.srcBase64 = reader.result;
    };
  }

  private extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const reader = new FileReader();
      reader.readAsDataURL($event);

      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({            
          base: null
        });
      };
      return reader;
    } catch (err) {
      this.generalService.mensajeError("Fail to cenvert imagen to base 64");
      return null;
    }
  });


  // tabla datos lectura medidor socio
  private configurarDatosComponenteBuscador(){
    this.tituloLecturaPeriodoSocioMedidor  = ['#','Año','Mes','Codigo categoría','Nombre categoría','Lectura anterior','Lectura actual','Consumo M3','Costo','Fecha registro','Lecturado'];
    this.campoLecturaPeriodoSocioMedidor   = ['anio','nombre','codigoCategoria','nombreCategoria','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','costoPeriodo','fechaRegistro','lecturado'];
    this.idItemLecturaPeriodoSocioMedidor  = 'idPeriodoCostoAgua';
  }

  public editarLecturaMedidor(datosLecturaPeriodoSocioMedidor:DatosLecturaPeriodoSocioMedidorVO){
    if(!datosLecturaPeriodoSocioMedidor.activo){
      this.generalService.mensajeAlerta('El periodo que selecciono no esta activo para ser lecturado');
      return;
    }

    if(!datosLecturaPeriodoSocioMedidor.lecturado){
      this.generalService.mensajeAlerta("No se puede actualizar un periodo que no fue lecturado");
      return;
    }

    this.datosLecturaPeriodoSocioMedidorEditar = datosLecturaPeriodoSocioMedidor;
    this.formularioLecturaPeriodo.get('periodo')?.disable();
    this.formularioLecturaPeriodo.get('codigoMedidor')?.disable();
    this.formularioLecturaPeriodo.get('lectura')?.setValue(this.datosLecturaPeriodoSocioMedidorEditar.lecturaActualPeriodo);
    this.formularioLecturaPeriodo.get('imagen')?.reset();
    this.srcBase64 = this.aux;
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getPeriodoActivoService(){
    this.lecturaMedidorPorUsuarioService.getPeriodoActivo().subscribe(
      (resp)=>{
        this.formularioLecturaPeriodo.get('periodo')?.setValue('0');
        this.periodoActivo = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getDatosSocioMedidorByCodigoMedidorService(codigoMedidor:string){
    this.lecturaMedidorPorUsuarioService.getDatosSocioMedidorByCodigoMedidor(codigoMedidor).subscribe(
      (resp)=>{
        this.datosSocioMedidor = resp;     
        
        if(this.datosSocioMedidor){
          let idPeriodo:number = Number(this.formularioLecturaPeriodo.get('periodo')?.value);         

          if(idPeriodo!=0){ 
            this.formularioLecturaPeriodo.get('imagen')?.enable();            

            this.getPeriodoCostoAguaLecturaMedidorService(idPeriodo,resp.idCategoria)
            this.getDatosLecturaPeriodoSocioMedidorService(resp.idSocioMedidor,this.periodoActivo.idAnio,resp.idCategoria);
          }
        }else{
          this.listaDatosLecturaPeriodoSocioMedidor = [];
          this.datosSocioMedidor = this.aux;
          this.formularioLecturaPeriodo.get('imagen')?.disable();  
          this.formularioLecturaPeriodo.get('imagen')?.reset();  
          this.srcBase64=this.aux;
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getDatosLecturaPeriodoSocioMedidorService(idSocioMedidor:number,idAnio:number,idCategoria:number){
    this.lecturaMedidorPorUsuarioService.getDatosLecturaPeriodoSocioMedidor(idSocioMedidor,idAnio,idCategoria).subscribe(
      (resp)=>{
        this.listaDatosLecturaPeriodoSocioMedidor = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodoCostoAguaLecturaMedidorService(idPeriodo:number, idCategoria:number){
    this.lecturaMedidorPorUsuarioService.getPeriodoCostoAguaLecturaMedidor(idPeriodo,idCategoria).subscribe(
      (resp)=>{
        this.periodoCostoAguaData = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // consumo agua socio service
  public createLecturaPeriodoService(idTanqueAgua:number,lecturaPeriodoDTO : LecturaPeriodoDTO) {
    this.consumoAguaSocioService.createLecturaPeriodo(idTanqueAgua,lecturaPeriodoDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se guardo el registro de lectura del medidor");
        this.buscarDatosSocioMedidorService();
    
        this.formularioLecturaPeriodo.get('periodo')?.enable();
        this.formularioLecturaPeriodo.get('codigoMedidor')?.enable();
        this.formularioLecturaPeriodo.get('lectura')?.reset();
        this.formularioLecturaPeriodo.get('imagen')?.reset();
        this.srcBase64=this.aux;
        this.datosLecturaPeriodoSocioMedidorEditar = this.aux;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateLecturaPeriodoService(id:number,idTanqueAgua:number,lecturaPeriodoDTO : LecturaPeriodoDTO){
    this.consumoAguaSocioService.updateLecturaPeriodo(id,idTanqueAgua,lecturaPeriodoDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo el registro de lectura del medidor");
        this.buscarDatosSocioMedidorService();
       
        this.formularioLecturaPeriodo.get('periodo')?.enable();
        this.formularioLecturaPeriodo.get('codigoMedidor')?.enable();
        this.formularioLecturaPeriodo.get('lectura')?.reset();
        this.formularioLecturaPeriodo.get('imagen')?.reset();
        this.srcBase64=this.aux;
        this.datosLecturaPeriodoSocioMedidorEditar = this.aux;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // imagen
  public updateNombreReciboDeLecturaDeMedidor(idLecturaPeriodo:number, nombreImagen:string){
    this.lecturaMedidorPorUsuarioService.updateNombreReciboDeLecturaDeMedidor(idLecturaPeriodo,nombreImagen).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  public uploadReciboDeLecturaDeMedidor(fd:FormData){
    this.lecturaMedidorPorUsuarioService.uploadReciboDeLecturaDeMedidor(fd).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { Injectable } from '@angular/core';

import { Socio } from 'src/app/socios/models/dto-vo/socio-dto';

@Injectable({
  providedIn: 'root'
})
export class HistorialConsumoAguaSocioService {

  private listaSocios:Socio[] = [
    {
      id              : 1,
      codigo          : 12,
      nombres         : 'juan',
      apellidoPaterno : 'perez',
      apellidoMaterno : 'loza',
      telefonos       : [
        {
          id     : 1,
          numero : 65465
        },
        {
          id     : 2,
          numero : 46466
        }
      ],
      medidor         : [
        {
          id         : 1,
          codigo     : 5465,
          registrado : true,
          registroConsumo : {
            volumen         : 5,
            costoM3         : 10,
            costoTotal      : 50,
            lecturaAnterior : 132,
            lecturaActual   : 135,
          }
        },
        {
          id         : 2,
          codigo     : 87988,
          registrado : false,
          registroConsumo : {
            volumen         : NaN,
            costoM3         : NaN,
            costoTotal      : NaN,
            lecturaAnterior : NaN,
            lecturaActual   : NaN,
          }
        }
      ]
    },
    {
      id              : 2,
      codigo          : 798,
      nombres         : 'pedro',
      apellidoPaterno : 'lopez',
      apellidoMaterno : 'lora',
      telefonos       : [
        {
          id     : 1,
          numero : 789796
        },
      ],
      medidor         : [
        {
          id              : 3,
          codigo          : 455,
          registrado      : false,
          registroConsumo : {
            volumen         : NaN,
            costoM3         : NaN,
            costoTotal      : NaN,
            lecturaAnterior : NaN,
            lecturaActual   : NaN,
          }
        },
        {
          id         : 4,
          codigo     : 456,
          registrado : true,
          registroConsumo : {
            volumen         : 6,
            costoM3         : 10,
            costoTotal      : 60,
            lecturaAnterior : 456,
            lecturaActual   : 789,
          }
        },
        {
          id         : 5,
          codigo     : 455,
          registrado : true,
          registroConsumo : {
            volumen         : 9,
            costoM3         : 10,
            costoTotal      : 90,
            lecturaAnterior : 465,
            lecturaActual   : 789,
          }
        }
      ]
    },
    {
      id              : 3,
      codigo          : 645,
      nombres         : 'martha',
      apellidoPaterno : 'lopez',
      apellidoMaterno : 'rocha',
      telefonos       : [
        {
          id     : 1,
          numero : 65465
        },
      ],
      medidor         : [
        {
          id         : 6,
          codigo     : 5465,
          registrado : true,
          registroConsumo : {
            volumen         : 1,
            costoM3         : 10,
            costoTotal      : 10,
            lecturaAnterior : 132,
            lecturaActual   : 133,
          }
        }
      ]
    },
    {
      id              : 4,
      codigo          : 323,
      nombres         : 'tyer',
      apellidoPaterno : 'opey',
      apellidoMaterno : 'rye',
      telefonos       : [
        {
          id     : 1,
          numero : 213213
        },
      ],
      medidor         : [
        {
          id         : 8,
          codigo     : 34534,
          registrado : true,
          registroConsumo : {
            volumen         : 1,
            costoM3         : 10,
            costoTotal      : 10,
            lecturaAnterior : 132,
            lecturaActual   : 133,
          }
        },
        {
          id         : 9,
          codigo     : 567,
          registrado : true,
          registroConsumo : {
            volumen         : 1,
            costoM3         : 10,
            costoTotal      : 10,
            lecturaAnterior : 132,
            lecturaActual   : 133,
          }
        }
      ]
    },
    {
      id              : 5,
      codigo          : 645,
      nombres         : 'rob',
      apellidoPaterno : 'rty',
      apellidoMaterno : 'voms',
      telefonos       : [
        {
          id     : 1,
          numero : 65465
        },
      ],
      medidor         : [
        {
          id         : 10,
          codigo     : 5465,
          registrado : true,
          registroConsumo : {
            volumen         : 1,
            costoM3         : 10,
            costoTotal      : 10,
            lecturaAnterior : 132,
            lecturaActual   : 133,
          }
        },
        {
          id         : 11,
          codigo     : 565,
          registrado : false,
          registroConsumo : {
            volumen         : NaN,
            costoM3         : NaN,
            costoTotal      : NaN,
            lecturaAnterior : NaN,
            lecturaActual   : NaN,
          }
        }
      ]
    }
  ]
  constructor() { }

  
  /**
   * tipo : 1 .- todos
   *        2 .- registrados
   *        3 .- no registrados 
   */
  public getHistorialConsumoAguaSocio(gestion:number, mes:number, tipo:number) : Socio[] {
    return this.listaSocios;
  }

 
}

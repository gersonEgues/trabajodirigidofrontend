import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DatosSocioMedidorVO } from '../models/datosSocioMedidorVO';
import { PeriodoActivoVO } from '../models/periodoActivoVO';
import { DatosLecturaPeriodoSocioMedidorVO } from '../models/datosLecturaPeriodoSocioMedidorVO';
import { PeriodoCostoAguaVO } from '../models/PeriodoCostoAguaVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
@Injectable({
  providedIn: 'root'
})
export class LecturaMedidorPorUsuarioService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/lectura-medidor-por-usuario",http);
  }

  public getPeriodoActivo():Observable<PeriodoActivoVO>{    
    return this.http.get<PeriodoActivoVO>(`${this.url}/periodo`).pipe(
      map(response => {
        return response as PeriodoActivoVO;
      })
    );
  }

  public getDatosSocioMedidorByCodigoMedidor(codigoMedidor:string):Observable<DatosSocioMedidorVO>{    
    return this.http.get<DatosSocioMedidorVO>(`${this.url}/datos-socio-medidor/${codigoMedidor}`).pipe(
      map(response => {
        return response as DatosSocioMedidorVO;
      })
    );
  }

  public getDatosLecturaPeriodoSocioMedidor(idSocioMedidor:number,idAnio:number,idCategoria:number):Observable<DatosLecturaPeriodoSocioMedidorVO[]>{    
    return this.http.get<DatosLecturaPeriodoSocioMedidorVO[]>(`${this.url}/lectura-socio-medidor/${idSocioMedidor}/${idAnio}/${idCategoria}`).pipe(
      map(response => {
        return response as DatosLecturaPeriodoSocioMedidorVO[];
      })
    );
  }

  public getPeriodoCostoAguaLecturaMedidor(idPeriodo:number, idCategoria:number):Observable<PeriodoCostoAguaVO>{    
    return this.http.get<PeriodoCostoAguaVO>(`${this.url}/periodo-costo-agua/${idPeriodo}/${idCategoria}`).pipe(
      map(response => {
        return response as PeriodoCostoAguaVO;
      })
    );
  }

  //----------------
  // documento pdf |
  //----------------
  public uploadReciboDeLecturaDeMedidor(fd:FormData): Observable<any> { 
    return this.http.post<any>(`${this.url}/recibo-pdf`,fd).pipe(
      map(response => {
        return response as any;
      })
    );
  } 

  public updateNombreReciboDeLecturaDeMedidor(idLecturaPeriodo:number, nombreImagen:string): Observable<BanderaResponseVO> { 
    return this.http.put<any>(`${this.url}/recibo-pdf/${idLecturaPeriodo}/${nombreImagen}`,null).pipe(
      map(response => {
        return response as any;
      })
    );
  } 
  
  public descargarReciboSocioLecturaMedidor(idLeturaPeriodo:number): Observable<any> { 
    return this.http.get(`${this.url}/recibo-pdf/${idLeturaPeriodo}`,{responseType: 'blob'});
  }
}

import { Injectable } from '@angular/core';
import { LecturaPeriodoVO } from '../models/lecturaPeriodoVO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConsumoAguaSocioService } from './consumo-agua-socio.service';
import { LecturaPeriodoDTO } from '../models/lecturaPeriodoDTO';

@Injectable({
  providedIn: 'root'
})
export class ValidatorLecturaService {

  constructor(private generalService          : GeneralService,
              private consumoAguaSocioService : ConsumoAguaSocioService,) { }


  public async validarLecturaCrearMedidor(lecturaPeriodoNuevo:LecturaPeriodoDTO, lecturaInicialSocioMedidor:number):Promise<boolean>{
    let bandera:boolean = true;
    let listaLecturaPeriodo:LecturaPeriodoVO[] = await this.getListaLecturaPeriodoByIdSocioMedidor(lecturaPeriodoNuevo.idSocioMedidor);
    let length:number = listaLecturaPeriodo.length;

    if(length==0){
      if(!(lecturaPeriodoNuevo.lecturaPeriodo >= lecturaInicialSocioMedidor)){
        this.generalService.mensajeAlerta(`la nueva lectura no puede ser menor que la lectura inicial del medidor`);
        bandera = false;
      }
    }else if(length==1) {
      let lecturaPeriodoAnterior:LecturaPeriodoVO = listaLecturaPeriodo[0];
      if(!(lecturaPeriodoNuevo.lecturaPeriodo >= lecturaPeriodoAnterior.lecturaActualPeriodo)){
        this.generalService.mensajeAlerta(`la lectura actual no puede ser menor que la lectura anterior`);
        bandera = false;
      }
    }else if(length>1) {
      let lecturaPeriodoAnterior:LecturaPeriodoVO = listaLecturaPeriodo[listaLecturaPeriodo.length-1];      
      if(!(lecturaPeriodoNuevo.lecturaPeriodo >= lecturaPeriodoAnterior.lecturaActualPeriodo)){
        this.generalService.mensajeAlerta(`la lectura actual no puede ser menor que la lectura anterior`);
        bandera = false;
      }
    }

    return bandera;
  }

  private getListaLecturaPeriodoByIdSocioMedidor(idSocioMedidor : number) : Promise<LecturaPeriodoVO[]> {
    return this.consumoAguaSocioService.getListaLecturaPeriodoByIdSocioMedidor(idSocioMedidor).toPromise()
      .then(resp=>{
        return resp;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
        return err;
      });
  }

  public async validarLecturaActualizarMedidor(lecturaPeriodoUpdate:LecturaPeriodoDTO, idLecturaPeriodo:number ):Promise<boolean>{
    let bandera:boolean = true;
    let listaLecturaPeriodo:LecturaPeriodoVO[] = await this.getListaLecturaPeriodoByIdSocioMedidor(lecturaPeriodoUpdate.idSocioMedidor);
    let i:number = this.getIndexLecturaPeriodo(listaLecturaPeriodo,idLecturaPeriodo);

    if(listaLecturaPeriodo[i].idLecturaPeriodo != idLecturaPeriodo ){
      this.generalService.mensajeError("No coinsiden los id's a actualizar, contacte con el adminsitrador");
      bandera = false;
    }

    if(i==0){            
      let lecturaPeriodoAnterior:LecturaPeriodoVO = listaLecturaPeriodo[i];

      if(!(lecturaPeriodoUpdate.lecturaPeriodo >= lecturaPeriodoAnterior.lecturaAnteriorPeriodo)){
        this.generalService.mensajeAlerta(`la lectura actual no puede ser menor que la lectura anterior`);
        bandera = false;
      }
    }else if(i>0){
      let lecturaPeriodoAnterior:LecturaPeriodoVO = listaLecturaPeriodo[i-1];

      if(!(lecturaPeriodoUpdate.lecturaPeriodo >= lecturaPeriodoAnterior.lecturaActualPeriodo)){
        this.generalService.mensajeAlerta(`la lectura actual no puede ser menor que la lectura anterior`);
        bandera = false;
      }
    }
    return bandera;
  }

  private getIndexLecturaPeriodo(listaLectuarPeriodo:LecturaPeriodoVO[],idLP:number):number{
    let i:number = 0;
    let encontrado:boolean = false;

    while(i<listaLectuarPeriodo.length && !encontrado){
      if(listaLectuarPeriodo[i].idLecturaPeriodo == idLP ){
        encontrado = true;
      }else{
        i++;
      }
    }
    return i;
  }
}

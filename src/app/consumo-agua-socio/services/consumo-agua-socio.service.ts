import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PeriodoAnioConCostoAguaConLecturaVO } from '../models/PeriodoAnioConCostoAguaConLecturaVO';
import { LecturaPeriodoVO } from '../models/lecturaPeriodoVO';
import { LecturaPeriodoDTO } from '../models/lecturaPeriodoDTO';
import { LecturaPeridoMinMax } from '../models/lecturaPeridoMinMaxVO';
import { VolumenCostoGestion } from '../models/volumenCostoGestionVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';


@Injectable({
  providedIn: 'root'
})
export class ConsumoAguaSocioService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/consumo-agua-socio",http);
  }

  public getListaLecturaPeriodoByIdSocioMedidor(idSocioMedidor : number) : Observable<LecturaPeriodoVO[]>{    
    return this.http.get<LecturaPeriodoVO[]>(`${this.url}/lectura-periodo/lista/${idSocioMedidor}`).pipe(
      map(response => {
        return response as LecturaPeriodoVO[];
      })
    );
  }
  
  public getPeriodoAnioConCostoAguaConLectura(idAnio         : number,
                                              idCategoria    : number,
                                              idSocioMedidor : number) : Observable<PeriodoAnioConCostoAguaConLecturaVO[]>{    
    return this.http.get<PeriodoAnioConCostoAguaConLecturaVO[]>(`${this.url}/lectura-periodo/${idAnio}/${idCategoria}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as PeriodoAnioConCostoAguaConLecturaVO[];
      })
    );
  }

  public createLecturaPeriodo(idTanqueAgua:number,lecturaPeriodoDTO : LecturaPeriodoDTO) : Observable<LecturaPeriodoVO>{
    return this.http.post<LecturaPeriodoVO>(`${this.url}/lectura-periodo/${idTanqueAgua}`,lecturaPeriodoDTO).pipe(
      map(response => {
        return response as LecturaPeriodoVO;
      })
    );
  }

  public updateLecturaPeriodo(id:number,idTanqueAgua:number,lecturaPeriodoDTO : LecturaPeriodoDTO) : Observable<LecturaPeriodoVO>{
    return this.http.put<LecturaPeriodoVO>(`${this.url}/lectura-periodo/${id}/${idTanqueAgua}`,lecturaPeriodoDTO).pipe(
      map(response => {
        return response as LecturaPeriodoVO;
      })
    );
  }

  public deleteLecturaPeriodo(idSocioMedidor:number,idlecturaPeriodo:number,lecturaAnterior:number) : Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/delete-lectura-periodo/${idSocioMedidor}/${idlecturaPeriodo}/${lecturaAnterior}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public esUltimaLecturaPeriodo(idSocioMedidor:number,idlecturaPeriodo:number) : Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/delete-lectura-periodo/${idSocioMedidor}/${idlecturaPeriodo}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  //---------------------------------------

  public registrarPeriodoAnioConCostoAguaConLectura(idAnio : number,idCategoria : number,idSocioMedidor : number) : Observable<PeriodoAnioConCostoAguaConLecturaVO[]>{    
    return this.http.get<PeriodoAnioConCostoAguaConLecturaVO[]>(`${this.url}/lectura-periodo/${idAnio}/${idCategoria}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as PeriodoAnioConCostoAguaConLecturaVO[];
      })
    );
  }

  public actualizarPeriodoAnioConCostoAguaConLectura(idAnio : number,idCategoria : number,idSocioMedidor : number) : Observable<PeriodoAnioConCostoAguaConLecturaVO[]>{    
    return this.http.get<PeriodoAnioConCostoAguaConLecturaVO[]>(`${this.url}/lectura-periodo/${idAnio}/${idCategoria}/${idSocioMedidor}`).pipe(
      map(response => {
        return response as PeriodoAnioConCostoAguaConLecturaVO[];
      })
    );
  }

  //-----------------------------------------
  //-- informe

  public getLecturaPeriodoMinMaxConMes(idSocioMedidor : number, idGestion : number, idCategoria: number) : Observable<LecturaPeridoMinMax[]>{    
    return this.http.get<LecturaPeridoMinMax[]>(`${this.url}/informe/lectura-gestion/${idSocioMedidor}/${idGestion}/${idCategoria}`).pipe(
      map(response => {
        return response as LecturaPeridoMinMax[];
      })
    );
  }

  public getSumaLecturaPeriodoCostoAgua(idSocioMedidor : number, idGestion : number, idCategoria: number) : Observable<VolumenCostoGestion>{    
    return this.http.get<VolumenCostoGestion>(`${this.url}/informe/suma-consumo-gestion/${idSocioMedidor}/${idGestion}/${idCategoria}`).pipe(
      map(response => {
        return response as VolumenCostoGestion;
      })
    );
  }
}

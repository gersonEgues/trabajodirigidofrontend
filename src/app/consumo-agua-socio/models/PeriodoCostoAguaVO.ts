export interface PeriodoCostoAguaVO {
  idPeriodoCostoAgua : number,
  idPeriodo          : number,
  idCategoria        : number,
  costoM3            : number,
  volumenMinimoM3    : number,
  costoMinimoM3      : number,
  observacion        : string,
}
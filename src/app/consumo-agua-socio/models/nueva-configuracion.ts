export interface NuevaConfiguracion{
  fechaRegistro       : string,
  nombreConfiguracion : string,
  periodoInicio       : string,
  periodoFin?         : string,
  activo?             : boolean,
  itemsExtra  : ItemsExtra[]
}

interface ItemsExtra{
  nombreItem  : string,
  costoItem   : number,
}

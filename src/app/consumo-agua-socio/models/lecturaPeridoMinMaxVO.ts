export interface LecturaPeridoMinMax{
  idLecturaPeriodo       : number,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  consumoVolumenM3       : number,
  costoPeriodo           : number,
  nombreMes              : string,
  lecturado              : boolean,
  fechaRegistro          : string,
}
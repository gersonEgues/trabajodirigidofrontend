export interface LecturaPeriodoDTO{
  idPeriodoCostoAgua : number,
  idSocioMedidor     : number,
  idMedidor          : number,
  lecturaPeriodo     : number,
  fechaRegistro      : string,
}
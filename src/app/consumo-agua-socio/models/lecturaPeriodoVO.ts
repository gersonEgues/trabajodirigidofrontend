export interface LecturaPeriodoVO{
  idLecturaPeriodo       : number,
  idPeriodoCostoAgua     : number,
  idSocioMedidor         : number,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  consumoVolumenM3       : number,
  costoPeriodo           : number,
  lecturado              : boolean,
  fechaRegistro          : string,
}

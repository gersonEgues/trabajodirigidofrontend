export interface HistorialMedidor {
  id                : number,
  gestion           : number,
  mes               : string,     
  lecturaAnterior   : number,
  lecturaActual     : number,
  registro          : string,
  cubos             : number,
  precioM3          : number,
  precioConsumo     : number,          
}
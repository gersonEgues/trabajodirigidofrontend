export interface RegistroConsumoAguaSocio{
  idResponsable   : number,
  idSocio         : number,
  idMedidor       : number,
  gestionLectura  : number,
  mesLectura      : number,
  fechaRegistro   : Date,
  volumen         : number,
  costoM3         : number,
  costoTotal      : number,
  lecturaAnterior : number,
  lecturaActual   : number,
}
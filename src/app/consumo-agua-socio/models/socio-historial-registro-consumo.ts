export interface SocioHRC{
  idSocio           : number,
  codigoSocio       : number,
  nombreApellido    : string,
  telefonos         : string,
  idMedidor         : number,
  codigoMedidor     : number,
  medidorRegistrado : string,
  volumen?          : number,
  costoM3?          : number,
  costoTotal?       : number,
  lecturaAnterior?  : number,
  lecturaActual?    : number, 
}

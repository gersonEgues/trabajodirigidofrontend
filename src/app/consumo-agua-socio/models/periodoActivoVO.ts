export interface PeriodoActivoVO {
  idPeriodo : number,
  idMes     : number,
  idAnio    : number,
  anio      : number,
  nombre    : string,
  activo    : boolean,
}
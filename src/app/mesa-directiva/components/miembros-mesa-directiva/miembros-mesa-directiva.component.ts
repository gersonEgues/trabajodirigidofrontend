import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { SocioService } from 'src/app/socios/services/socio.service';
import { MesaDirectivaService } from '../../services/mesa-directiva.service';
import { MiembroMesaDirectivaService } from '../../services/miembro-mesa-directiva.service';
import { MesaDirectivaVO } from '../../models/MesaDirectivaVO';
import { MiembroMesaDirectivaDTO } from '../../models/miembroMesaDirectivaDTO';
import { MiembroMesaDirectivaVO } from '../../models/miembroMesaDirectivaVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { UsuarioService } from 'src/app/usuarios/services/usuario.service';
import { UsuarioVO } from 'src/app/usuarios/models/usuario-vo';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
@Component({
  selector: 'app-miembros-mesa-directiva',
  templateUrl: './miembros-mesa-directiva.component.html',
  styleUrls: ['./miembros-mesa-directiva.component.css']
})
export class MiembrosMesaDirectivaComponent implements OnInit {
  aux : any = undefined;

  // Tabla
  tituloItemsMiembroMesaDirectiva  : string[] = [];
  campoItemsMiembroMesaDirectiva   : string[] = [];
  idItemMiembroMesaDirectiva       : string = 'id';
  listaUsuarios                    : UsuarioVO[] = [];
  listaUsuariosMesaDirectiva                 : UsuarioVO[] = [];

  formularioMesaDirectiva!              : FormGroup;
  formularioListaMiembrosMesaDirectiva! : FormGroup;

  
  listaMesaDirectiva!             : MesaDirectivaVO[];
  listaMesaDirectivaFiltrado!     : MesaDirectivaVO[];

  banderaEstadoMesaDirectiva    : boolean = true;    
  estadoUsuarioMesaDirectiva    : boolean = true;
  
  @ViewChild('tablaMiembrosMesaDirectiva') tablaMiembrosMesaDirectiva! : TablaComponent;

  constructor(private fb                          : FormBuilder,
              private generalService              : GeneralService,
              private mesaDirectivaService        : MesaDirectivaService,
              private miembroMesaDirectivaService : MiembroMesaDirectivaService,
              private usarioService               : UsuarioService,
              private dialog                      : MatDialog,
              private router                      : Router) { }

  ngOnInit(): void {
    this.construirFormularioCreate();
    this.construirFormularioListar();
    this.setConfiguracionTabla();
    this.getListaMesaDirectivaService(true);
    this.getListaUsuarios();
    this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
  }

  private construirFormularioCreate(){
    this.formularioMesaDirectiva = this.fb.group({
      nombreMesaDirectiva   : ['-1',[Validators.required],[]],
      miembrosMesaDirectiva : this.fb.array([],[Validators.required],[]),
    });
    this.aniadirNuevoMiembroMesaDirectivaFormulario();
  }

  private construirFormularioListar(){
    this.formularioListaMiembrosMesaDirectiva = this.fb.group({
      activo          : ['1',[Validators.required],[]],
      mesaDirectiva   : ['-1',[Validators.required],[]],
    });
  }

  public aniadirNuevoMiembroMesaDirectivaFormulario():void{
    this.miembrosMesaDirectiva.push(this.nuevoMiembroMesaDirectivaFormulario());
  }

  public  get miembrosMesaDirectiva() : FormArray {
    return this.formularioMesaDirectiva.get('miembrosMesaDirectiva') as FormArray;
  }

  public nuevoMiembroMesaDirectivaFormulario():FormGroup{
    return this.fb.group({
      nombreSocio : ['-1', [Validators.required],[]],
    });
  }

  public eliminarMiembroMesaDirectivaFormulario(index:number):void{
    this.miembrosMesaDirectiva.removeAt(index);
  }

  private setConfiguracionTabla(){
    this.tituloItemsMiembroMesaDirectiva = ['#','Apellido paterno','Apellido materno','Nombre','Email', 'Roles'];
    this.campoItemsMiembroMesaDirectiva = ['apellidoPaterno','apellidoMaterno','nombre','email','rol'];
  }

  public async crearMesaDirectiva(){
    if(this.formularioMesaDirectiva.invalid || this.mesaDirectivaInvalido()){
      this.formularioMesaDirectiva.markAllAsTouched();
      return;
    }

    let idMesaDirectiva:number = Number(this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.value);
    let idUsuarios:number[] = this.getListaMiembrosMesaDirectivaDTO();   

    let repetido:boolean = this.miembrosRepetidos(idUsuarios);

    if(repetido){
      this.generalService.mensajeError("No puede seleccionar a un mismo usuario mas de una vez");
      return;
    }

    let miembroMesaDirectiva:any = this.getListaMiembrosMesaDirectivaDTO

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE MIEMBROS PARA MESA DIRECTIVA','¿Está seguro de registrar la mesa directiva con sus respectivos miembros?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let miembrosMesaDirectiva:MiembroMesaDirectivaDTO = {
      idMesaDirectiva : idMesaDirectiva,
      idUsuarios : idUsuarios
    }

    this.asignarUsuarioAMesadirectivaService(miembrosMesaDirectiva)
  }
  
  private miembrosRepetidos(idUsuarios:number[]):boolean{    
    let repetido:boolean = false;
    let set:Set<Number> = new Set();
    let i:number = 0;
    while(i<idUsuarios.length && !repetido){
      if(set.has(idUsuarios[i]))
        repetido = true;
      else
        set.add(idUsuarios[i]);
      i++;
    }

    return repetido;
  }
 

  public cancelarRegistroMesaDirectiva(){
    this.limpiarFormulario();
  }

  public limpiarFormulario(){
    this.formularioMesaDirectiva.reset();
    this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.setValue('-1');
    this.formularioMesaDirectiva.get('gestion')?.setValue('-1');

    this.miembrosMesaDirectiva.at(0).get('nombreSocio')?.setValue('-1');
    this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.enable();


    while(this.miembrosMesaDirectiva.length>1){
      this.miembrosMesaDirectiva.removeAt(1);
    }
  }

  public formularioMesaDirectivaValido(campo:string) : boolean{
    return  this.formularioMesaDirectiva.controls[campo].valid &&
            this.formularioMesaDirectiva.controls[campo].value!='-1';
  }

  public formularioMesaDirectivaInvalido(campo:string) : boolean{   
    return  (this.formularioMesaDirectiva.controls[campo].invalid && 
             this.formularioMesaDirectiva.controls[campo].touched) || 
            (this.formularioMesaDirectiva.controls[campo].value=='-1' && 
             this.formularioMesaDirectiva.controls[campo].touched);
  }

  public formularioSocioMesaDirectivaValido(index:number,campo:string) : boolean{
    return  (this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].valid && 
            (this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].value!='-1';
  }

  public formularioSocioMesaDirectivaInvalido(index:number,campo:string) : boolean{   
    return  ((this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].invalid && 
             (this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].touched) ||  
            ( (this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].value == '-1' &&
              (this.miembrosMesaDirectiva.at(index) as FormGroup).controls[campo].touched);
  }

  public changeEstadoMesaDirectiva(){
    let estado:number = Number(this.formularioListaMiembrosMesaDirectiva.get('activo')?.value);
    this.banderaEstadoMesaDirectiva = (estado==1)?true:false;
    this.tablaMiembrosMesaDirectiva.cambioEstadoEliminar();
    
    this.listaMesaDirectivaFiltrado = [];
    this.listaUsuariosMesaDirectiva = [];
    this.formularioListaMiembrosMesaDirectiva.get('mesaDirectiva')?.setValue('-1');

    this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
  }

  public changeMesaDirectiva(){
    let idMesaDirectiva:number = Number(this.formularioListaMiembrosMesaDirectiva.get('mesaDirectiva')?.value);
    this.getListaUsuariosDeMesaDirectivaService(this.estadoUsuarioMesaDirectiva,idMesaDirectiva);
  }

  private mesaDirectivaInvalido(){
    return this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.value=='-1' || this.miembrosMesaDirectiva.at(0).get('nombreSocio')?.value=='-1';
  }

  private getListaMiembrosMesaDirectivaDTO():number[]{
    let idUsuarios:number[] = [];
    this.miembrosMesaDirectiva.controls.forEach(miembroMD => {
      idUsuarios.push(Number(miembroMD.get('nombreSocio')?.value));
    });
    return idUsuarios;
  }

  public async removeMiembroDeMesaDirectiva(usuario:UsuarioVO){
    let nombreCompleto:string = usuario.nombre + ' ' + usuario.apellidoPaterno + ' ' + usuario.apellidoMaterno;

    let idMesaDirectiva:number = this.formularioListaMiembrosMesaDirectiva.get('mesaDirectiva').value;

    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR MIEMBRO DE MESA DIRECTIVA',`¿Está seguro de eliminar al usuario ${nombreCompleto} de la mesa directiva?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.removeUsuarioDeMesadirectivaService(idMesaDirectiva,usuario.id);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaMiembrosMesaDirectiva')
    this.generalService.genearteArchivoXLSX(tableElemnt,'miembros mesa directiva');
  }

  // ---------------------- 
  // -- Consumo API-REST --
  // ----------------------
  public getListaMesaDirectivaService(estado:boolean){
    this.mesaDirectivaService.getListaMesaDirectivaFiltrado(estado).subscribe(
      (resp)=>{
        this.listaMesaDirectiva = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaMesaDirectivaFiltradoService(estado:boolean){
    this.mesaDirectivaService.getListaMesaDirectivaFiltrado(estado).subscribe(
      (resp)=>{
        this.listaMesaDirectivaFiltrado = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createMiembroMesaDirectivaService(miembroMesaDirectiva:MiembroMesaDirectivaDTO):Promise<MiembroMesaDirectivaVO>{
    return this.miembroMesaDirectivaService.createMiembrosMesaDirectiva(miembroMesaDirectiva).toPromise()
      .then((data)=>{
        this.generalService.mensajeCorrecto("Se creo  correctamente al miembro de la mesa directiva");
        return data;
      })
      .catch((err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }

  public habilitarInhabilitarMiembroMesaDirectivaService(idMesaDirectiva:number,idMiembroMesaDirectiva:number,estado:boolean,nombreCompleto:String):Promise<MesaDirectivaVO>{
    return this.miembroMesaDirectivaService.habilitarInhabilitarMiembrosMesaDirectiva(idMiembroMesaDirectiva).toPromise()
      .then((data)=>{
        if(estado)
          this.generalService.mensajeCorrecto(`Se dio de baja a ${nombreCompleto} de la mesa directiva`);
        else
          this.generalService.mensajeCorrecto(`Se dio de alta a ${nombreCompleto} a la mesa directiva`);

        this.getListaUsuariosDeMesaDirectivaService(this.estadoUsuarioMesaDirectiva,idMesaDirectiva);
        return data;
      })
      .catch((err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }
  
  public getListaUsuarios(){
    this.usarioService.getListaUsuarios().subscribe(
      (resp)=>{
        this.listaUsuarios = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public asignarUsuarioAMesadirectivaService(miembrosMesaDirectiva:MiembroMesaDirectivaDTO){
    this.usarioService.asignarUsuarioAMesadirectiva(miembrosMesaDirectiva).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se asigno correctamente los usuarios a la mesa directiva");
        this.limpiarFormulario();
        
        let idMesaDirectiva:number = Number(this.formularioListaMiembrosMesaDirectiva.get('mesaDirectiva')?.value)
        if(idMesaDirectiva!=-1)
          this.getListaUsuariosDeMesaDirectivaService(this.estadoUsuarioMesaDirectiva,idMesaDirectiva);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }


  public removeUsuarioDeMesadirectivaService(idMesaDirectiva:number,idUsuario:number){
    this.usarioService.removeUsuarioDeMesadirectiva(idMesaDirectiva,idUsuario).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se elimino correctamente al usuarios de la mesa directiva");
        this.limpiarFormulario();
        
        let idMesaDirectiva:number = Number(this.formularioListaMiembrosMesaDirectiva.get('mesaDirectiva')?.value)
        if(idMesaDirectiva!=-1)
          this.getListaUsuariosDeMesaDirectivaService(this.estadoUsuarioMesaDirectiva,idMesaDirectiva);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }


  public getListaUsuariosDeMesaDirectivaService(estado:boolean,idMesaDirectiva:number) {
    this.usarioService.getListaUsuariosDeMesaDirectiva(estado,idMesaDirectiva).subscribe(
      (resp)=>{
        this.listaUsuariosMesaDirectiva = resp;
        if(this.listaUsuariosMesaDirectiva.length>0){
          let nombreRol : string = '';
          this.listaUsuariosMesaDirectiva.forEach(usuario => {
            nombreRol = '';
            usuario.roles.forEach(rol => {
              nombreRol += rol.nombre  + ' - ';
            });
            nombreRol = nombreRol.slice(0,nombreRol.length-3);
            usuario.rol = nombreRol;
          });      
          this.limpiarFormulario();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }      
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

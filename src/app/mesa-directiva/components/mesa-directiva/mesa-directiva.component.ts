import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { AnioVO } from '../../../configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { MesaDirectivaService } from '../../services/mesa-directiva.service';
import { MesaDirectivaDTO } from '../../models/MesaDirectivaDTO';
import { MesaDirectivaVO } from '../../models/MesaDirectivaVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-mesa-directiva',
  templateUrl: './mesa-directiva.component.html',
  styleUrls: ['./mesa-directiva.component.css']
})
export class MesaDirectivaComponent implements OnInit {
  aux : any = undefined;

  // Tabla
  tituloItemsMesaDirectiva  : string[] = [];
  campoItemsMesaDirectiva   : string[] = [];
  idItemMesaDirectiva!      : string;
  listaItemsMesaDirectiva!  : MesaDirectivaVO[];


  formularioMesaDirectiva!      : FormGroup;
  formularioListaMesaDirectiva! : FormGroup;

  listaGestiones!                 : AnioVO[];

  banderaEstadoMesaDirectiva : boolean = true;

  mesaDirectivaEditar : MesaDirectivaVO = this.aux;

  @ViewChild('tablaMiembrosMesaDirectiva') tablaMiembrosMesaDirectiva! : TablaComponent;


  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private mesaDirectivaService             : MesaDirectivaService,
              private dialog                           : MatDialog,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormularioCreate();
    this.construirFormularioListar();
    this.getListaGestionesService();
    this.setConfiguracionTabla();
    this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
  }

  private construirFormularioCreate(){
    this.formularioMesaDirectiva = this.fb.group({
      nombreMesaDirectiva   : ['',[Validators.required],[]],
      gestion               : ['-1',[Validators.required],[]],
      fechaCreacion         : [{value:'', disabled:true},[Validators.required],[]],
    });

    this.formularioMesaDirectiva.get('fechaCreacion')?.setValue(this.generalService.getFechaActualFormateado());
  }

  private construirFormularioListar(){
    this.formularioListaMesaDirectiva = this.fb.group({
      activo          : ['1',[Validators.required],[]],
    });
  }

  public formularioMesaDirectivaValido(campo:string) : boolean{
    return  this.formularioMesaDirectiva.controls[campo].valid &&
            this.formularioMesaDirectiva.controls[campo].value!='-1';
  }

  public formularioMesaDirectivaInvalido(campo:string) : boolean{   
    return  ( this.formularioMesaDirectiva.controls[campo].invalid && 
              this.formularioMesaDirectiva.controls[campo].touched ) || 
            ( this.formularioMesaDirectiva.controls[campo].value=='-1' && 
              this.formularioMesaDirectiva.controls[campo].touched);
  }

  private setConfiguracionTabla(){
    this.tituloItemsMesaDirectiva = ['#','Nombre','Fecha Creación'];
    this.campoItemsMesaDirectiva = ['nombre','fechaCreacion'];
    this.idItemMesaDirectiva = "id";
  }

  public async crearMesaDirectiva(){
    if(this.formularioMesaDirectiva.invalid  ||  this.formularioMesaDirectiva.get('gestion')?.value=='-1'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioMesaDirectiva.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR MESA DIRECTIVA',`¿Está seguro crear esta mesa directiva?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
     
    let mesaDirectivaDTO : MesaDirectivaDTO = this.getMesaDirectivaDTO();
    this.createMesaDirectivaService(mesaDirectivaDTO);
  }

  public async actualizarMesaDirectiva(){
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR MESA DIRECTIVA',`¿Está seguro actualizar esta mesa directiva?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let mesaDirectivaVO:MesaDirectivaVO = this.getMesaDirectivaVO();
    this.updateMesaDirectivaService(mesaDirectivaVO);
  }

  public cancelarRegistroMesaDirectiva(){
    this.limpiarFormularioMesaDirectiva();
  }

  public changeEstadoMesaDirectiva(){
    let estado:number = Number(this.formularioListaMesaDirectiva.get('activo')?.value)
    this.banderaEstadoMesaDirectiva = (estado==1)?true:false;   
    if(this.isAdmin() || this.isPresidente()){
      this.tablaMiembrosMesaDirectiva.cambioEstadoEliminar();
    }
    this.tablaMiembrosMesaDirectiva.cambioEstadoEditar();
    this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
  }

  public changeMesaDirectiva(){
    
  }

  public updateMesaDirectivaEvent(mesaDirectiva:MesaDirectivaVO){
    this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.setValue(mesaDirectiva.nombre);
    this.formularioMesaDirectiva.get('gestion')?.setValue(mesaDirectiva.idAnio);
    this.formularioMesaDirectiva.get('fechaCreacion')?.setValue(mesaDirectiva.fechaCreacion);

    this.mesaDirectivaEditar = mesaDirectiva;
  }

  public async  eliminarMesaDirectivaEvent(mesaDierctiva:MesaDirectivaVO){
    let afirmativo:Boolean = await this.getConfirmacion('DAR DE BAJA A MESA DIRECTIVA',`¿Está seguro dar de baja a la mesa directiva seleccionada?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
    
    this.habilitarInhabilitarMesaDirectiva(mesaDierctiva.id);
  }
  
  public  limpiarFormularioMesaDirectiva(){
    this.formularioMesaDirectiva.reset();
    this.formularioMesaDirectiva.get('gestion')?.setValue('-1');
    this.formularioMesaDirectiva.get('fechaCreacion')?.setValue(this.generalService.getFechaActualFormateado());
    this.mesaDirectivaEditar = this.aux;
  }

  private getMesaDirectivaDTO():MesaDirectivaDTO{
    let mesaDirectivaDTO:MesaDirectivaDTO = {
      idAnio : Number(this.formularioMesaDirectiva.get('gestion')?.value),
      nombre : this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.value,
      fechaCreacion : this.formularioMesaDirectiva.get('fechaCreacion')?.value,
    }
    return mesaDirectivaDTO;
  } 

  private getMesaDirectivaVO():MesaDirectivaVO{
    let mesaDirectivaVO:MesaDirectivaVO = {
      id            : this.mesaDirectivaEditar.id,
      idAnio        : Number(this.formularioMesaDirectiva.get('gestion')?.value),
      nombre        : this.formularioMesaDirectiva.get('nombreMesaDirectiva')?.value,
      fechaCreacion : this.formularioMesaDirectiva.get('fechaCreacion')?.value,
      activo        : true,
    }
    return mesaDirectivaVO;
  } 

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public getListaMesaDirectivaFiltradoService(estado:boolean){
    this.mesaDirectivaService.getListaMesaDirectivaFiltrado(estado).subscribe(
      (resp)=>{
        this.listaItemsMesaDirectiva = resp;
        if(this.listaItemsMesaDirectiva.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createMesaDirectivaService(mesaDirectiva:MesaDirectivaDTO){
    this.mesaDirectivaService.createMesaDirectiva(mesaDirectiva).subscribe(
      (resp)=>{
        this.limpiarFormularioMesaDirectiva();

        if(this.banderaEstadoMesaDirectiva!=this.aux)
          this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateMesaDirectivaService(mesaDirectiva:MesaDirectivaVO){
    this.mesaDirectivaService.updateMesaDirectiva(mesaDirectiva).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo de manera corecta la mesa directiva");
        this.limpiarFormularioMesaDirectiva();
        if(this.banderaEstadoMesaDirectiva!=this.aux)
          this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaGestionesService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public habilitarInhabilitarMesaDirectiva(idMesaDirectiva:number){
    this.mesaDirectivaService.habilitarInhabilitarMesaDirectiva(idMesaDirectiva).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se elimino de manera correcta a la mesa directiva seleccionada`);
        if(this.banderaEstadoMesaDirectiva!=this.aux)
          this.getListaMesaDirectivaFiltradoService(this.banderaEstadoMesaDirectiva);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
  
}

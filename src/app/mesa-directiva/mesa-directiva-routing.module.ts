import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MiembrosMesaDirectivaComponent } from './components/miembros-mesa-directiva/miembros-mesa-directiva.component';
import { MesaDirectivaComponent } from './components/mesa-directiva/mesa-directiva.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'mesa-directiva',
        component: MesaDirectivaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'miembro-mesa-directiva',
        component: MiembrosMesaDirectivaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      {
        path  : '**', 
        redirectTo : 'mesa-directiva' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MesaDirectivaRoutingModule { }

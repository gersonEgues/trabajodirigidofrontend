import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MesaDirectivaRoutingModule } from './mesa-directiva-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MiembrosMesaDirectivaComponent } from './components/miembros-mesa-directiva/miembros-mesa-directiva.component';
import { MesaDirectivaComponent } from './components/mesa-directiva/mesa-directiva.component';

@NgModule({
  declarations: [
    MiembrosMesaDirectivaComponent,
    MesaDirectivaComponent
  ],
  imports: [
    CommonModule,
    MesaDirectivaRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MesaDirectivaModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MiembroMesaDirectivaData } from '../models/miembroMesaDirectivaData';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { MiembroMesaDirectivaVO } from '../models/miembroMesaDirectivaVO';
import { MiembroMesaDirectivaDTO } from '../models/miembroMesaDirectivaDTO';

@Injectable({
  providedIn: 'root'
})
export class MiembroMesaDirectivaService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/miembro-mesa-directiva",http);
  }

  public getListaMiembrosMesaDirectivaData(idMesaDirectiva:number,estado:boolean=true) : Observable<MiembroMesaDirectivaData[]> {
    return this.http.get<MiembroMesaDirectivaData[]>(`${this.url}/lista-data/${idMesaDirectiva}/${estado}`).pipe(
      map(response => {
        return response as MiembroMesaDirectivaData[];
      })
    );
  }

  public createMiembrosMesaDirectiva(miembroMesaDirectiva:MiembroMesaDirectivaDTO) : Observable<MiembroMesaDirectivaVO> {
    return this.http.post<MiembroMesaDirectivaVO>(`${this.url}`,miembroMesaDirectiva).pipe(
      map(response => {
        return response as MiembroMesaDirectivaVO;
      })
    );
  }

  public updateMiembrosMesaDirectiva(miembroMesaDirectiva:MiembroMesaDirectivaVO) : Observable<MiembroMesaDirectivaVO> {
    return this.http.put<MiembroMesaDirectivaVO>(`${this.url}`,miembroMesaDirectiva).pipe(
      map(response => {
        return response as MiembroMesaDirectivaVO;
      })
    );
  }

  public habilitarInhabilitarMiembrosMesaDirectiva(idMiembroMesaDirectiva:number) : Observable<BanderaResponseVO> {
    return this.http.put<BanderaResponseVO>(`${this.url}/cambio-estado/${idMiembroMesaDirectiva}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { MesaDirectivaDTO } from '../models/MesaDirectivaDTO';
import { MesaDirectivaVO } from '../models/MesaDirectivaVO';

@Injectable({
  providedIn: 'root'
})
export class MesaDirectivaService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/mesa-directiva",http);
  }

  public getMesaDirectiva(idMesaDirectiva:number) : Observable<MesaDirectivaVO> {
    return this.http.get<MesaDirectivaVO>(`${this.url}/${idMesaDirectiva}`).pipe(
      map(response => {
        return response as MesaDirectivaVO;
      })
    );
  }

  public getListaMesaDirectivaTodos() : Observable<MesaDirectivaVO[]> {
    return this.http.get<MesaDirectivaVO[]>(`${this.url}/lista`).pipe(
      map(response => {
        return response as MesaDirectivaVO[];
      })
    );
  } 

  public getListaMesaDirectivaFiltrado(estado:boolean) : Observable<MesaDirectivaVO[]> {
    return this.http.get<MesaDirectivaVO[]>(`${this.url}/lista/${estado}`).pipe(
      map(response => {
        return response as MesaDirectivaVO[];
      })
    );
  }

  public createMesaDirectiva(mesaDirectiva:MesaDirectivaDTO):Observable<MesaDirectivaVO>{
    return this.http.post<MesaDirectivaVO>(`${this.url}`,mesaDirectiva).pipe(
      map(response => {
        return response as MesaDirectivaVO;
      })
    );
  }

  public updateMesaDirectiva(mesaDirectiva:MesaDirectivaVO):Observable<MesaDirectivaVO>{
    return this.http.put<MesaDirectivaVO>(`${this.url}`,mesaDirectiva).pipe(
      map(response => {
        return response as MesaDirectivaVO;
      })
    );
  }

  public habilitarInhabilitarMesaDirectiva(idMesaDirectiva:number):Observable<MesaDirectivaVO>{
    return this.http.put<MesaDirectivaVO>(`${this.url}/habilitar-inhabilitar/${idMesaDirectiva}`,null).pipe(
      map(response => {
        return response as MesaDirectivaVO;
      })
    );
  }
}

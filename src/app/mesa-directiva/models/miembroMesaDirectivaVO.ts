import { MiembroMesaDirectivaDTO } from './miembroMesaDirectivaDTO';
export interface MiembroMesaDirectivaVO extends MiembroMesaDirectivaDTO{
  id     : number,
  activo : boolean;
}
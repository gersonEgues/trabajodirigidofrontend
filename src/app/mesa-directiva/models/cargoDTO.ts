export interface CargoDTO{
  nombre      : string,
  descripcion : string,
  jerarquia   : string,
}
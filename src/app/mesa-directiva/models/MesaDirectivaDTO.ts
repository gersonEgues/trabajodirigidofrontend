export interface MesaDirectivaDTO{
  idAnio        : number,
  nombre        : string,
  fechaCreacion : string,
}
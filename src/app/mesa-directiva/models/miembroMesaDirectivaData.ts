export interface MiembroMesaDirectivaData {
  id                  : number,
  idMesaDirectiva     : number,
  idSocio             : number,
  idCargo             : number,
  miembroActivo       : boolean,
  nombreMesaDirectiva : string,
  jerarquia           : number,
  nombreCargo         : string,
  codigoSocio         : string,
  nombreSocio         : string,
  apellidoPaterno     : string,
  apellidoMaterno     : string,    
}
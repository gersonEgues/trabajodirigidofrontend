import { MesaDirectivaDTO } from "./MesaDirectivaDTO";

export interface MesaDirectivaVO extends MesaDirectivaDTO{
  id     : number,
  activo : boolean,
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TanqueAguaVO } from '../models/tanqueAguaVO';
import { TanqueAguaDTO } from '../models/tanqueAguaDTO';

@Injectable({
  providedIn: 'root'
})
export class RegistroTanqueAguaService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/tanque-agua",http);
  }

  public getTanqueAgua(idTanqueAgua : number) : Observable<TanqueAguaVO>{    
    return this.http.get<TanqueAguaVO>(`${this.url}/${idTanqueAgua}`).pipe(
      map(response => {
        return response as TanqueAguaVO;
      })
    );
  }

  public getListaTanqueAgua(bandera:boolean) : Observable<TanqueAguaVO[]>{    
    return this.http.get<TanqueAguaVO[]>(`${this.url}/lista/${bandera}`).pipe(
      map(response => {
        return response as TanqueAguaVO[];
      })
    );
  }

  public createTanqueAgua(tanqueAgua:TanqueAguaDTO) : Observable<TanqueAguaVO>{   
    return this.http.post<TanqueAguaVO>(`${this.url}`,tanqueAgua).pipe(
      map(response => {
        return response as TanqueAguaVO;
      })
    );
  }

  public updateTanqueAgua(tanqueAgua:TanqueAguaVO) : Observable<TanqueAguaVO>{    
    return this.http.put<TanqueAguaVO>(`${this.url}`,tanqueAgua).pipe(
      map(response => {
        return response as TanqueAguaVO;
      })
    );
  }

  public habilitarInhabilitarTanqueAgua(idTanqueAgua:number) : Observable<TanqueAguaVO>{    
    return this.http.delete<TanqueAguaVO>(`${this.url}/habilitar-inhabilitar/${idTanqueAgua}`).pipe(
      map(response => {
        return response as TanqueAguaVO;
      })
    );
  }
}

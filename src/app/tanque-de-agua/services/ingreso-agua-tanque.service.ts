import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IngresoTanqueAguaDTO } from '../models/ingresoTanqueAguaDTO';
import { IngresoTanqueAguaVO } from '../models/ingresoTanqueAguaVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { CountItemVO } from '../../shared/models/countItemVO';
import { PageEventDTO } from '../../shared/models/pageEventDTO';

@Injectable({
  providedIn: 'root'
})
export class IngresoAguaTanqueService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/ingreso-agua-tanque",http);
  }
  
  public getCountListaIngresoAguaTanque(idTanqueAgua:number):Observable<CountItemVO>{
    return this.http.get<CountItemVO>(`${this.url}/count/${idTanqueAgua}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getListaIngresoAguaTanque(idTanqueAgua:number,pageEventDTO:PageEventDTO) : Observable<IngresoTanqueAguaVO[]>{    
    return this.http.get<IngresoTanqueAguaVO[]>(`${this.url}/lista/${idTanqueAgua}/${pageEventDTO.pageIndex}/${pageEventDTO.pageSize}`).pipe(
      map(response => {
        return response as IngresoTanqueAguaVO[];
      })
    );
  }
  
  public createIngresoAguaTanque(ingresoAguaTanqueCreate:IngresoTanqueAguaDTO) : Observable<IngresoTanqueAguaVO>{   
    return this.http.post<IngresoTanqueAguaVO>(`${this.url}`,ingresoAguaTanqueCreate).pipe(
      map(response => {
        return response as IngresoTanqueAguaVO;
      })
    );
  }
  
  public updateIngresoAguaTanque(id:number,ingresoAguaTanqueUpdate:IngresoTanqueAguaDTO) : Observable<IngresoTanqueAguaVO>{   
    return this.http.put<IngresoTanqueAguaVO>(`${this.url}/${id}`,ingresoAguaTanqueUpdate).pipe(
      map(response => {
        return response as IngresoTanqueAguaVO;
      })
    );
  }
  
  public deleteIngresoAguaTanque(idIngresoAguaTanque:number) : Observable<BanderaResponseVO>{   
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idIngresoAguaTanque}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

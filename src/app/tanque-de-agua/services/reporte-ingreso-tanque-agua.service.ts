import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { ReporteIngresoTanqueAgua } from '../models/reporte-ingreso-tanque-agua-vo';
import { ReporteMensualIngresoTanqueAguaVO } from '../models/reporteMensualIngresoTanqueAguaVO ';

@Injectable({
  providedIn: 'root'
})
export class ReporteIngresoTanqueAguaService extends RootServiceService{
  constructor(private http: HttpClient) { 
    super("api/reporte-ingreso-tanque-agua",http);
  }

  public getReporteIngresoAguaRangoFecha(idTanque:number,rangoFechaDTO:RangoFechaDTO):Observable<ReporteIngresoTanqueAgua>{
    return this.http.post<ReporteIngresoTanqueAgua>(`${this.url}/reporte-rango-fecha/${idTanque}`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteIngresoTanqueAgua;
      })
    );
  }

  public getReporteIngresoAguaPorGestion(idTanque:number,gestion:number):Observable<ReporteIngresoTanqueAgua>{
    return this.http.get<ReporteIngresoTanqueAgua>(`${this.url}/reporte-gestion/${idTanque}/${gestion}`).pipe(
      map(response => {
        return response as ReporteIngresoTanqueAgua;
      })
    );
  }

  public getReporteMensualIngresoTanqueAguaEnRangoFecha(idTanque:number,rangoFechaDTO:RangoFechaDTO):Observable<ReporteMensualIngresoTanqueAguaVO>{
    return this.http.post<ReporteMensualIngresoTanqueAguaVO>(`${this.url}/reporte-mensual-rango-fecha/${idTanque}`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteMensualIngresoTanqueAguaVO;
      })
    );
  }

  public getReporteMensualIngresoTanqueAguaPorGestion(idTanque:number,gestion:number):Observable<ReporteMensualIngresoTanqueAguaVO>{
    return this.http.get<ReporteMensualIngresoTanqueAguaVO>(`${this.url}/reporte-mensual-gestion/${idTanque}/${gestion}`).pipe(
      map(response => {
        return response as ReporteMensualIngresoTanqueAguaVO;
      })
    );
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistroTanqueComponent } from './components/registro-tanque/registro-tanque.component';
import { RegistroIngresoAguaTanqueComponent } from './components/registro-ingreso-agua-tanque/registro-ingreso-agua-tanque.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ReporteDeIngresoDeAguaAlTanqueDeAguaComponent } from './components/reporte/reporte-de-ingreso-de-agua-al-tanque-de-agua/reporte-de-ingreso-de-agua-al-tanque-de-agua.component';
import { EstadoActualTanqueAguaComponent } from './components/reporte/estado-actual-tanque-agua/estado-actual-tanque-agua.component';
import { ROL } from '../shared/enums-mensajes/roles';
import { ReporteIngresoAguaRangoFechaComponent } from './components/reporte/reporte-ingreso-agua-rango-fecha/reporte-ingreso-agua-rango-fecha.component';
import { ReporteIngresoAguaGestionComponent } from './components/reporte/reporte-ingreso-agua-gestion/reporte-ingreso-agua-gestion.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'registro-tanque',
        component: RegistroTanqueComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : 'registro-ingreso-agua-tanque',
        component: RegistroIngresoAguaTanqueComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : 'estado-actual',
        component: EstadoActualTanqueAguaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : 'ingreso-tanque-agua',
        component: ReporteDeIngresoDeAguaAlTanqueDeAguaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : 'reporte-ingreso-agua-rango-fecha',
        component: ReporteIngresoAguaRangoFechaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : 'reporte-ingreso-agua-gestion',
        component: ReporteIngresoAguaGestionComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ]
      },
      { 
        path  : '**',
        redirectTo : 'estado-actual'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TanqueDeAguaRoutingModule { }

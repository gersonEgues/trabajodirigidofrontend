import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { RegistroTanqueAguaService } from '../../services/registro-tanque-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { TanqueAguaVO } from '../../models/tanqueAguaVO';
import { TanqueAguaDTO } from '../../models/tanqueAguaDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-registro-tanque',
  templateUrl: './registro-tanque.component.html',
  styleUrls: ['./registro-tanque.component.css']
})
export class RegistroTanqueComponent implements OnInit {
  itemsTitulo : string[] = [];
  campos      : string[] = [];

  banderaTanqueAgua             : boolean = true;  
  tanqueAguaEditarSeleccionado! : TanqueAguaVO;

  conversionM3ToLts : boolean = false;
  conversionLtsToM3 : boolean = false;  

  listaTanqueAgua  : TanqueAguaVO[] = [];
  nuevoTanqueForm! : FormGroup; 

  @ViewChild('tablaRegistroTanqueAgua') tablaRegistroTanqueAgua! : TablaComponent;
  
  constructor(private fb                        : FormBuilder, 
              private dialog                    : MatDialog,
              private generalService            : GeneralService,
              private registroTanqueAguaService : RegistroTanqueAguaService,
              private router                    : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.construirItemsDeTabla();
    this.getListaTanqueAguaService(this.banderaTanqueAgua);
  }

  private construirFormulario(){
    this.nuevoTanqueForm = this.fb.group({     
      codigo        : ['',[Validators.required],[]],
      nombre        : ['',[Validators.required],[]],
      ubicacion     : ['',[Validators.required],[]],
      capacidadM3   : ['',[Validators.required],[]],
      capacidadLts  : ['',[Validators.required],[]],
      fechaRegistro : [{value:this.generalService.getFechaActualFormateado(), disabled:true },[Validators.required],[]],
    });
  }

  private construirItemsDeTabla(){
    this.itemsTitulo = ['#','Código','Nombre','Ubicación','Cap. M3','Cap. Lts','Fec. Reg.'];
    this.campos = ['codigo','nombre','ubicacion','capacidadM3','capacidadLts','fechaRegistro'];
  }

  public campoValido(nombreCampo:string):boolean{
    return  this.nuevoTanqueForm.controls[nombreCampo].valid;
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  this.nuevoTanqueForm.controls[nombreCampo].touched &&
            this.nuevoTanqueForm.controls[nombreCampo].invalid;
  } 

  public changeCapacidadM3(capM3:number){
    let capLts:number = Number(this.nuevoTanqueForm.get('capacidadLts')?.value);
    let conversionCapLts:number = capM3*1000;
    
    if(capLts != conversionCapLts){
      this.nuevoTanqueForm.get('capacidadLts')?.setValue( conversionCapLts );
    }
  }

  public changeCapacidadLts(capLts:number){
    let capM3:number = Number(this.nuevoTanqueForm.get('capacidadM3')?.value);
    let conversionCapM3:number = capLts/1000;
    if(capM3 != conversionCapM3){
      this.nuevoTanqueForm.get('capacidadM3')?.setValue(conversionCapM3);
    }
  }

  public resetFormulario(){
    this.nuevoTanqueForm.reset();
    this.nuevoTanqueForm.get('fechaRegistro')?.setValue('');
  }


  public async addNuevoTanque(){
    if(this.nuevoTanqueForm.invalid){
      this.nuevoTanqueForm.markAllAsTouched();
      this.generalService.mensajeAlerta('Llene todos los campos');
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVO TANQUE DE AGUA','¿Está seguro de crear un nuevo tanque de agua?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let nuevoTanque : TanqueAguaDTO = this.getTanqueDTO();
    
    this.createTanqueAguaService(nuevoTanque);    
  }

  public async editarTanqueAgua() {
    if(this.nuevoTanqueForm.invalid){
      this.nuevoTanqueForm.markAllAsTouched();
      this.generalService.mensajeAlerta('Llene todos los campos');
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR TANQUE DE AGUA','¿Está seguro de actualizar el tanque de agua seleccionado?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let updateTanque : TanqueAguaVO = this.getTanqueVO(this.tanqueAguaEditarSeleccionado);
    this.updateTanqueAguaService(updateTanque);
  }

  public cancelarNuevoTanque(){
    this.limpiarFormulario();
  }

  public limpiarFormulario(){
    this.resetFormulario();
    this.nuevoTanqueForm.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());
    let aux:any = undefined;
    this.tanqueAguaEditarSeleccionado = aux;
  }

  private getTanqueDTO():TanqueAguaDTO{
    let tanqueAguaDTO : TanqueAguaDTO = {
      codigo        : this.nuevoTanqueForm.get('codigo')?.value,
      nombre        : this.nuevoTanqueForm.get('nombre')?.value,
      ubicacion     : this.nuevoTanqueForm.get('ubicacion')?.value,
      capacidadM3   : this.nuevoTanqueForm.get('capacidadM3')?.value,
      capacidadLts  : this.nuevoTanqueForm.get('capacidadLts')?.value,
      fechaRegistro : this.nuevoTanqueForm.get('fechaRegistro')?.value,  
    }
    return tanqueAguaDTO;
  }

  private getTanqueVO(tanqueAgua:TanqueAguaVO):TanqueAguaVO{
    let tanqueAguaVO : TanqueAguaVO = {
      idTanqueAgua  : tanqueAgua.idTanqueAgua,
      codigo        : this.nuevoTanqueForm.get('codigo')?.value,
      nombre        : this.nuevoTanqueForm.get('nombre')?.value,
      ubicacion     : this.nuevoTanqueForm.get('ubicacion')?.value,
      capacidadM3   : this.nuevoTanqueForm.get('capacidadM3')?.value,
      capacidadLts  : this.nuevoTanqueForm.get('capacidadLts')?.value,
      fechaRegistro : this.nuevoTanqueForm.get('fechaRegistro')?.value,
      activo        : tanqueAgua.activo,  
    }
    return tanqueAguaVO;
  }

  public cambioEstadoBandera(){
    this.tablaRegistroTanqueAgua.deseleccionarItem();
    if(this.isPresidente()||this.isAdmin()){
      this.tablaRegistroTanqueAgua.cambioEstadoHabilitar();
      this.tablaRegistroTanqueAgua.cambioEstadoEliminar();
    }
    this.tablaRegistroTanqueAgua.cambioEstadoEditar();
    this.getListaTanqueAguaService(this.banderaTanqueAgua);
  }

  // tabla
  public editarItemSeleccionadoEvent(tanqueAgua:TanqueAguaVO){
    this.tanqueAguaEditarSeleccionado = tanqueAgua;
    this.nuevoTanqueForm.get('codigo')?.setValue(tanqueAgua.codigo);
    this.nuevoTanqueForm.get('nombre')?.setValue(tanqueAgua.nombre);
    this.nuevoTanqueForm.get('ubicacion')?.setValue(tanqueAgua.ubicacion);
    this.nuevoTanqueForm.get('capacidadM3')?.setValue(tanqueAgua.capacidadM3);
    this.nuevoTanqueForm.get('capacidadLts')?.setValue(tanqueAgua.capacidadLts);
    this.nuevoTanqueForm.get('fechaRegistro')?.setValue(tanqueAgua.fechaRegistro);
  }

  public async habilitarInhabilitarTanqueAgua(tanque:TanqueAguaVO){
    this.limpiarFormulario();

    let afirmativo:Boolean = await this.getConfirmacion(`${(tanque.activo)?'DESHABILITAR':'HABILITAR'} TANQUE DE AGUA`,`¿Está seguro de ${(tanque.activo)?'deshabilitar':'habilitar'} el tanque de agua seleccionado?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.habilitarInhabilitarTanqueAguaService(tanque);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaRegistroTanqueAgua')
    this.generalService.genearteArchivoXLSX(tableElemnt,'registro de tanque de agua');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public getListaTanqueAguaService(bandera:boolean){
    this.registroTanqueAguaService.getListaTanqueAgua(bandera).subscribe(
      (resp)=>{
        this.listaTanqueAgua=[];
        this.listaTanqueAgua = resp;
        if(this.listaTanqueAgua.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public createTanqueAguaService(tanqueAguaDTO:TanqueAguaDTO){
    this.registroTanqueAguaService.createTanqueAgua(tanqueAguaDTO).subscribe(
      (resp)=>{
        this.listaTanqueAgua=[];
        this.limpiarFormulario();
        this.generalService.mensajeCorrecto(`Se creo el tanque de agua ${tanqueAguaDTO.codigo}-${tanqueAguaDTO.nombre}, de manera correcta`);
        this.getListaTanqueAguaService(this.banderaTanqueAgua);
        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public updateTanqueAguaService(tanqueAguaVO:TanqueAguaVO){
    this.registroTanqueAguaService.updateTanqueAgua(tanqueAguaVO).subscribe(
      (resp)=>{
        this.listaTanqueAgua=[];
        this.limpiarFormulario();
        this.generalService.mensajeCorrecto(`Se actualizo el tanque de agua ${tanqueAguaVO.codigo}-${tanqueAguaVO.nombre}, de manera correcta`);
        this.getListaTanqueAguaService(this.banderaTanqueAgua);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public habilitarInhabilitarTanqueAguaService(tanqueAguaVO:TanqueAguaVO){
    this.registroTanqueAguaService.habilitarInhabilitarTanqueAgua(tanqueAguaVO.idTanqueAgua).subscribe(
      (resp)=>{
        this.listaTanqueAgua=[];
        this.limpiarFormulario();
        this.generalService.mensajeCorrecto(`Se ${(tanqueAguaVO.activo)?'habilito':'deshabilito'} el tanque de agua ${tanqueAguaVO.codigo}-${tanqueAguaVO.nombre}, de manera correcta`);
        this.getListaTanqueAguaService(this.banderaTanqueAgua);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 


  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

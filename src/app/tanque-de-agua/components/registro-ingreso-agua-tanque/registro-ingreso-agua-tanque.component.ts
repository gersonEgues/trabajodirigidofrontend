import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RegistroTanqueAguaService } from '../../services/registro-tanque-agua.service';
import { TanqueAguaVO } from '../../models/tanqueAguaVO';
import { IngresoTanqueAguaDTO } from '../../models/ingresoTanqueAguaDTO';
import { IngresoTanqueAguaVO } from '../../models/ingresoTanqueAguaVO';
import { IngresoAguaTanqueService } from '../../services/ingreso-agua-tanque.service';
import { CountItemVO } from '../../../shared/models/countItemVO';
import { PageEventDTO } from '../../../shared/models/pageEventDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-registro-ingreso-agua-tanque',
  templateUrl: './registro-ingreso-agua-tanque.component.html',
  styleUrls: ['./registro-ingreso-agua-tanque.component.css']
})
export class RegistroIngresoAguaTanqueComponent implements OnInit {
  aux:any = null;

  formularioTanqueAgua!                : FormGroup; 
  formularioTanqueAguaVisualizacion!   : FormGroup; 
  listaTanqueDeAguaRegistro!           : TanqueAguaVO[];
  listaTanqueDeAguaVisualizacion!      : TanqueAguaVO[];
  idTanqueAguaSeleccionada!            : number;
  pageDTO!                             : PageEventDTO;
  ingresoTanqueAguaSeleccionadoUpdate! : IngresoTanqueAguaVO;

  // tabla
  tituloItems                  : string[] = [];
  campoItems                   : string[] = [];
  idItemIngresoTanqueAgua      : string = 'idIngresoAguaTanque';
  listaIngresoTanqueAgua       : IngresoTanqueAguaVO[]=[];
  countItemsIngresoTanqueAgua! : number;

  constructor(private fb                        : FormBuilder,
              private dialog                    : MatDialog,
              private generalService            : GeneralService,
              private registroTanqueAguaService : RegistroTanqueAguaService,
              private ingresoAguaTanqueService  : IngresoAguaTanqueService,
              private router                    : Router) { }

  ngOnInit(): void {
    this.pageDTO = {
      length: -1,
      pageIndex : 0,
      pageSize : 10,
      previousPageIndex: -1,
    }

    this.getListaTanqueAguaService();
    this.configuracionesTablaPaginator();
    this.construirFormulario();
  }

  private construirFormulario(){
    this.formularioTanqueAgua = this.fb.group({     
      fechaRegistro : [{value:this.generalService.getFechaActualFormateado(), disabled:false },[Validators.required],[]],
      tanqueAgua    : ['0',[Validators.required],[]],
      volumenM3     : ['',[Validators.required],[]],
      volumenLts    : ['',[Validators.required],[]],
      costoTotal    : ['',[Validators.required],[]],
    });

    this.formularioTanqueAguaVisualizacion = this.fb.group({     
      tanqueAgua      : ['0',[Validators.required],[]],
    });
  }

  public campoValido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].valid;
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].touched &&
            this.formularioTanqueAgua.controls[nombreCampo].invalid;
  } 

  public campoValidoTanqueAgua():boolean{
    return  this.formularioTanqueAgua.get('tanqueAgua').valid &&
            this.formularioTanqueAgua.get('tanqueAgua').value!='0';
  }

  public campoInvalidoTanqueAgua():boolean{
    return  this.formularioTanqueAgua.get('tanqueAgua').touched &&
            this.formularioTanqueAgua.get('tanqueAgua').invalid;
  } 

  public changeCapacidadM3(){
    let volumenM3 : number = this.formularioTanqueAgua.get('volumenM3')?.value;
    let volumenLts: number = volumenM3*1000;
    
    this.formularioTanqueAgua.get('volumenLts')?.setValue(volumenLts);
  }

  public changeCapacidadLts(){
    let volumenLts : number = this.formularioTanqueAgua.get('volumenLts')?.value;
    let volumenM3: number = volumenLts/1000;

    this.formularioTanqueAgua.get('volumenM3')?.setValue(volumenM3);
  }

  public async createIngresoAguaTanque(){
    if(this.formularioTanqueAgua.invalid || this.formularioTanqueAgua.get('tanqueAgua').value=='0'){
      this.generalService.mensajeAlerta("Llene todos los campos");
      this.formularioTanqueAgua.markAllAsTouched();
      return;
    }

    let idTanqueAgua : number = Number(this.formularioTanqueAgua.get('tanqueAgua')?.value);
    let volumenM3    : number = Number(this.formularioTanqueAgua.get('volumenM3')?.value);
    let tanqueAux    : TanqueAguaVO = this.aux;
    this.listaTanqueDeAguaRegistro.forEach(tanque => {
      if(tanque.idTanqueAgua==idTanqueAgua){
        tanqueAux = tanque;
      }
    });
    
    if(tanqueAux.capacidadM3<volumenM3){
      this.generalService.mensajeAlerta(`No se puede ingresar un volumen mayor al de la capacidad del tanque de agua el cual es: ${tanqueAux.capacidadM3} M3`);
      return;
    }    
    
    let fechaIngreso  : string = this.formularioTanqueAgua.get('fechaIngreso')?.value;
    let fechaRegistro : string = this.formularioTanqueAgua.get('fechaRegistro')?.value;

    if(this.fechaInvalida(fechaIngreso,fechaRegistro)){
      this.generalService.mensajeAlerta("La fecha de ingreso no puede ser mayor a la fecha de registro")
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE INGRESO DE AGUA AL TANQUE',`¿Está seguro de registrar ${volumenM3} M3 de agua al tanque de agua ${tanqueAux.nombre}?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let ingresoTanqueAgua:IngresoTanqueAguaDTO = this.getIngresoTanqueAguaCreateDTO();
    this.createIngresoAguaTanqueService(ingresoTanqueAgua);
  }

  public async updateIngresoAguaTanque(){
    if(this.formularioTanqueAgua.invalid){
      this.generalService.mensajeAlerta("Llene todos los campos");
      this.formularioTanqueAgua.markAllAsTouched();
      return;
    }  

    let fechaIngreso  : string = this.formularioTanqueAgua.get('fechaIngreso')?.value;
    let fechaRegistro : string = this.formularioTanqueAgua.get('fechaRegistro')?.value;

    if(this.fechaInvalida(fechaIngreso,fechaRegistro)){
      this.generalService.mensajeAlerta("La fecha de ingreso no puede ser mayor a la fecha de registro")
      return;
    }


    let id:number = this.ingresoTanqueAguaSeleccionadoUpdate.idIngresoAguaTanque;
    let ingresoTanqueAgua:IngresoTanqueAguaDTO = this.getIngresoTanqueAguaUpdateDTO();  
    let tanqueAgua:TanqueAguaVO = this.getTanqueAgua(ingresoTanqueAgua.idTanqueAgua);
    
    if(ingresoTanqueAgua.volumenM3>tanqueAgua.capacidadM3){
      this.generalService.mensajeAlerta(`No se puede ingresar un volumen mayor al de la capacidad del tanque de agua el cual es: ${tanqueAgua.capacidadM3} M3`);
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR INGRESO DE AGUA AL TANQUE','¿Está seguro de actualizar el ingreso de agua al tanque?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
    this.updateIngresoAguaTanqueService(id,ingresoTanqueAgua);
  }

  public async deleteIngresoAguaTanque(idIngresoAguaTanque:number){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR INGRESO DE AGUA AL TANQUE','¿Está seguro de eliminar este ingreso de agua al tanque?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.deleteIngresoAguaTanqueService(idIngresoAguaTanque);
  }

  public cancelarNuevoRegistroAgua(){
    this.limpiarFormularioRegistro();
  }

  private getIngresoTanqueAguaCreateDTO():IngresoTanqueAguaDTO{
    let ingresoTanqueAgua:IngresoTanqueAguaDTO = {
      idTanqueAgua  : this.formularioTanqueAgua.get('tanqueAgua')?.value,
      fechaRegistro : this.formularioTanqueAgua.get('fechaRegistro')?.value,
      volumenM3     : this.formularioTanqueAgua.get('volumenM3')?.value,
      volumenLts    : this.formularioTanqueAgua.get('volumenLts')?.value,
      costoTotal    : this.formularioTanqueAgua.get('costoTotal')?.value,
    }
    return ingresoTanqueAgua;
  }

  private getIngresoTanqueAguaUpdateDTO():IngresoTanqueAguaDTO{
    let ingresoTanqueAgua:IngresoTanqueAguaDTO = {
      idTanqueAgua        : this.formularioTanqueAgua.get('tanqueAgua')?.value,
      fechaRegistro       : this.formularioTanqueAgua.get('fechaRegistro')?.value,
      volumenM3           : this.formularioTanqueAgua.get('volumenM3')?.value,
      volumenLts          : this.formularioTanqueAgua.get('volumenLts')?.value,
      costoTotal          : this.formularioTanqueAgua.get('costoTotal')?.value,
    }
    return ingresoTanqueAgua;
  }

  private fechaInvalida(fechaIngreso:string, fechaRegistro:string){
    return fechaRegistro < fechaIngreso;
  }

  private getTanqueAgua(id:number):TanqueAguaVO{
    let tanqueAgua:TanqueAguaVO = this.aux;
    let i:number = 0;
    let encontrado:boolean = false;

    while(i<this.listaTanqueDeAguaRegistro.length && !encontrado){
      if(this.listaTanqueDeAguaRegistro[i].idTanqueAgua == id){
        encontrado = true;
        tanqueAgua = this.listaTanqueDeAguaRegistro[i];
      }
      i++;
    }
    return tanqueAgua;
  }

  // tabla paginator
  public configuracionesTablaPaginator(){
    this.tituloItems  = ['#','Volumen M3','Volumen Lts','Costo','Fecha Registro'];
    this.campoItems   = ['volumenM3','volumenLts','costoTotal','fechaRegistro'];
    this.idItemIngresoTanqueAgua = "idIngresoAguaTanque";
  }

  public async getListaIngresoTanqueAgua(){
    this.idTanqueAguaSeleccionada = this.formularioTanqueAguaVisualizacion.get('tanqueAgua')?.value;
    await this.getCountListaIngresoAguaTanqueService(this.idTanqueAguaSeleccionada);
    this.pageDTO.length = this.countItemsIngresoTanqueAgua;
    this.getListaIngresoAguaTanqueService(this.idTanqueAguaSeleccionada,this.pageDTO);
  }

  public nuevaPaginaOCambioTamanioPaginaEvent(pageEventDTO:PageEventDTO){
    this.pageDTO = pageEventDTO;
    if(!this.idTanqueAguaSeleccionada){
      this.generalService.mensajeAlerta("Seleccione un tanque de agua");
      return;
    }
    this.getListaIngresoAguaTanqueService(this.idTanqueAguaSeleccionada,this.pageDTO);
  }

  public updateItemIngresoAguaTanqueEvent(ingresoTanqueAgua:IngresoTanqueAguaVO){
    this.ingresoTanqueAguaSeleccionadoUpdate = ingresoTanqueAgua;
    this.formularioTanqueAgua.get('tanqueAgua')?.setValue(ingresoTanqueAgua.idtanqueAgua);
    this.formularioTanqueAgua.get('fechaRegistro')?.setValue(ingresoTanqueAgua.fechaRegistro);
    this.formularioTanqueAgua.get('volumenM3')?.setValue(ingresoTanqueAgua.volumenM3);
    this.formularioTanqueAgua.get('volumenLts')?.setValue(ingresoTanqueAgua.volumenLts);
    this.formularioTanqueAgua.get('costoTotal')?.setValue(ingresoTanqueAgua.costoTotal);
  }

  public deleteItemIngresoAguaTanqueEvent(ingresoTanqueAgua:IngresoTanqueAguaVO){
    this.limpiarFormularioRegistro();
    this.deleteIngresoAguaTanque(ingresoTanqueAgua.idIngresoAguaTanque);
  }

  private limpiarFormularioRegistro(){
    this.formularioTanqueAgua.reset();
    this.formularioTanqueAgua.get('tanqueAgua')?.setValue('0');
    this.formularioTanqueAgua.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());
    this.formularioTanqueAgua.get('fechaIngreso')?.setValue(this.generalService.getFechaActualFormateado());
    
    let aux:any = undefined;
    this.ingresoTanqueAguaSeleccionadoUpdate = aux;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaIngresoDeAguaATanque')
    this.generalService.genearteArchivoXLSX(tableElemnt,'registro de ingreso de agua al tanque');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getListaTanqueAguaService(){
    this.registroTanqueAguaService.getListaTanqueAgua(true).subscribe(
      (resp)=>{
        this.listaTanqueDeAguaRegistro = resp;
        this.listaTanqueDeAguaVisualizacion = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }  

  private getCountListaIngresoAguaTanqueService(idTanqueAgua:number):Promise<CountItemVO>{
    return this.ingresoAguaTanqueService.getCountListaIngresoAguaTanque(idTanqueAgua).toPromise()
      .then(resp=>{     
        this.countItemsIngresoTanqueAgua = resp.countItem;   
        return resp;
      })
      .catch(err=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }  

  private async getListaIngresoAguaTanqueService(idTanqueAgua:number,pageEventDTO:PageEventDTO){
    this.ingresoAguaTanqueService.getListaIngresoAguaTanque(idTanqueAgua,pageEventDTO).subscribe(
      (resp)=>{
        this.listaIngresoTanqueAgua = resp;
        if(this.listaIngresoTanqueAgua.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }  

  private createIngresoAguaTanqueService(ingresoTanqueAgua:IngresoTanqueAguaDTO){
    this.ingresoAguaTanqueService.createIngresoAguaTanque(ingresoTanqueAgua).subscribe(
      (resp)=>{         
        this.generalService.mensajeCorrecto("Se registro de manera correcta");
        this.limpiarFormularioRegistro();
        if(this.idTanqueAguaSeleccionada){
          this.getListaIngresoTanqueAgua();
        }
      },
      (err)=>{       
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }  

  private updateIngresoAguaTanqueService(id:number,ingresoTanqueAgua:IngresoTanqueAguaDTO){
    this.ingresoAguaTanqueService.updateIngresoAguaTanque(id,ingresoTanqueAgua).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo de manera correcta");
        this.limpiarFormularioRegistro();
        if(this.idTanqueAguaSeleccionada){
          this.getListaIngresoTanqueAgua();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }  

  private deleteIngresoAguaTanqueService(idIngresoAguaTanque:number){
    this.ingresoAguaTanqueService.deleteIngresoAguaTanque(idIngresoAguaTanque).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se elimino de manera correcta");
        if(this.idTanqueAguaSeleccionada){
          this.getListaIngresoTanqueAgua();
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  Confirm Dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

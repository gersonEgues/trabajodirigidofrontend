import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { TanqueAguaVO } from 'src/app/tanque-de-agua/models/tanqueAguaVO';
import { RegistroTanqueAguaService } from 'src/app/tanque-de-agua/services/registro-tanque-agua.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-estado-actual-tanque-agua',
  templateUrl: './estado-actual-tanque-agua.component.html',
  styleUrls: ['./estado-actual-tanque-agua.component.css']
})
export class EstadoActualTanqueAguaComponent implements OnInit {
  formularioEstadoTanqueAgua! : FormGroup;
  listaTanquesDeAgua          : TanqueAguaVO[]=[];
  tanqueAguaSeleccionado!     : TanqueAguaVO;
  //estadoTanqueAgua!           : EstadoTanqueAguaVO;

  constructor(private fb                        : FormBuilder,
              private generalService            : GeneralService,
              private registroTanqueAguaService : RegistroTanqueAguaService,
              private router                    : Router) { }

  ngOnInit(): void {
    this.construirformularioEstadoTanqueAgua();
    this.getListaTanqueAguaService();
  }

  private construirformularioEstadoTanqueAgua(){
    this.formularioEstadoTanqueAgua = this.fb.group({     
      tanqueAgua : ['0',[Validators.required],[]]
    });
  }

  public changeTanqueAgua(){
    let idTanqueAgua:number = this.formularioEstadoTanqueAgua.get('tanqueAgua')?.value;
    this.tanqueAguaSeleccionado = this.buscarTanqueAgua(idTanqueAgua);
  }

  private buscarTanqueAgua(idTanqueAgua:number):TanqueAguaVO{
    let tanqueAgua!:TanqueAguaVO;
    let i:number = 0;
    let encontrado:boolean = false;
    while(!encontrado && i<this.listaTanquesDeAgua.length){
      if(this.listaTanquesDeAgua[i].idTanqueAgua == idTanqueAgua){
        encontrado = true;
        tanqueAgua = this.listaTanquesDeAgua[i];
      }
      i+=1;
    }
    return tanqueAgua;
  }

  // consumo API-REST
  private getListaTanqueAguaService(){
    this.registroTanqueAguaService.getListaTanqueAgua(true).subscribe(
      (resp)=>{
        this.listaTanquesDeAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

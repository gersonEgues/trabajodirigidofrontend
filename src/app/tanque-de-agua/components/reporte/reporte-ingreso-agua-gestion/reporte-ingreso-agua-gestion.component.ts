import { Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { IngresoTanqueAguaVO } from 'src/app/tanque-de-agua/models/ingresoTanqueAguaVO';
import { TanqueAguaVO } from 'src/app/tanque-de-agua/models/tanqueAguaVO';
import { RegistroTanqueAguaService } from 'src/app/tanque-de-agua/services/registro-tanque-agua.service';
import { ReporteIngresoTanqueAguaService } from 'src/app/tanque-de-agua/services/reporte-ingreso-tanque-agua.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteIngresoTanqueAgua } from 'src/app/tanque-de-agua/models/reporte-ingreso-tanque-agua-vo';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { ReporteMesVO } from 'src/app/tanque-de-agua/models/reporteMesVO';
import { ReporteMensualIngresoTanqueAguaVO } from 'src/app/tanque-de-agua/models/reporteMensualIngresoTanqueAguaVO ';
@Component({
  selector: 'app-reporte-ingreso-agua-gestion',
  templateUrl: './reporte-ingreso-agua-gestion.component.html',
  styleUrls: ['./reporte-ingreso-agua-gestion.component.css']
})
export class ReporteIngresoAguaGestionComponent implements OnInit {
  aux:any = undefined;

  formularioTanqueAgua! : FormGroup;
  listaTanquesDeAgua    : TanqueAguaVO[]=[];
  listaGestiones        : AnioVO[] = [];   
  
  // tabla
  tituloItems         : string[] = [];
  campoItems          : string[] = [];
  idItem              : string   = '';

  reporteIngresoTanqueAgua : ReporteIngresoTanqueAgua = this.aux;
  listaIngresoAgua         : IngresoTanqueAguaVO[] = [];
  rangoFechaDTO!           : RangoFechaDTO;

  // tabla mensual
  tituloItemsMes         : string[] = [];
  campoItemsMes          : string[] = [];
  idItemMes              : string   = '';

  reporteMensual       : ReporteMensualIngresoTanqueAguaVO = this.aux;
  reporteMensualList   : ReporteMesVO[] = [];

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private registroTanqueAguaService        : RegistroTanqueAguaService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reporteIngresoTanqueAguaService  : ReporteIngresoTanqueAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.getGestiones();
    this.configurarTablaIngresoTanqueAgua();
    this.construirformularioEstadoTanqueAgua();
    this.getListaTanqueAguaService();  
  }

  private construirformularioEstadoTanqueAgua(){
    this.formularioTanqueAgua = this.fb.group({     
      tanqueAgua : ['0',[Validators.required],[]],
      gestion    : ['0',[Validators.required],[]],
    });
  }

  private configurarTablaIngresoTanqueAgua(){
    this.tituloItemsMes = ['#','Mes','Volumen M3','Volumen Lts.','Costo total'];
    this.campoItemsMes  = ['mes','volumenM3','volumenLts','costoTotal'];
    this.idItemMes      = 'nro';

    this.tituloItems = ['#','Gestión','Mes','Fecha de registro','Volumen M3','Volumen Lts.','Costo'];
    this.campoItems  = ['gestion','mes','fechaRegistro','volumenM3','volumenLts','costoTotal'];
    this.idItem      = 'idIngresoAguaTanque';
  }
  
  public changeGestionReporte(){
    let idTanqueAgua:number = Number(this.formularioTanqueAgua.get("tanqueAgua")?.value);
    let idGestion:number = Number(this.formularioTanqueAgua.get("gestion")?.value);
    let gestion:number = this.aux;

    let encontrado:boolean=false;
    let i:number = 0;
    while(i<this.listaGestiones.length && !encontrado){
      if(this.listaGestiones[i].idAnio == idGestion){
        gestion = this.listaGestiones[i].anio;
      }
      i++;
    }

    if(gestion==this.aux)
      return;
    
    if(idGestion!=0&&idTanqueAgua!=0){
      this.getReporteIngresoAguaPorGestionService(idTanqueAgua,gestion);
      this.getReporteMensualIngresoTanqueAguaPorGestionService(idTanqueAgua,gestion);
    }
  }

  public campoValido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].valid &&
            this.formularioTanqueAgua.controls[nombreCampo].value != '0'; 
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].touched &&
            this.formularioTanqueAgua.controls[nombreCampo].invalid;
  }

  public seleccionarItem(item:IngresoTanqueAguaVO){
    this.listaIngresoAgua.forEach(element => {
      if(element.idIngresoAguaTanque == item.idIngresoAguaTanque )
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public seleccionarItemMes(item:ReporteMesVO){
    this.reporteMensualList.forEach(element => {
      if(element.nro == item.nro )
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public genearteArchivoXLSX(idTabla:string):void{
    let tableElemnt:HTMLElement = document.getElementById(idTabla)
    this.generalService.genearteArchivoXLSX(tableElemnt,'reporte de ingreos de agua al tanque - rango fecha');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getListaTanqueAguaService(){
    this.registroTanqueAguaService.getListaTanqueAgua(true).subscribe(
      (resp)=>{
        this.listaTanquesDeAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getReporteIngresoAguaPorGestionService(idTanque:number,gestion:number){
    this.reporteIngresoTanqueAguaService.getReporteIngresoAguaPorGestion(idTanque,gestion).subscribe(
      (resp)=>{        
        if(resp!=this.aux){          
          this.reporteIngresoTanqueAgua = resp;
          this.listaIngresoAgua = this.reporteIngresoTanqueAgua.ingresoTanqueAguaList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteIngresoTanqueAgua = this.aux;
          this.listaIngresoAgua = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteMensualIngresoTanqueAguaPorGestionService(idTanque:number,gestion:number){
    this.reporteIngresoTanqueAguaService.getReporteMensualIngresoTanqueAguaPorGestion(idTanque,gestion).subscribe(
      (resp)=>{        
        if(resp!=this.aux && resp.listaMes.length>0){           
          this.reporteMensual = resp;
          this.reporteMensualList = this.reporteMensual.listaMes;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteMensual = this.aux;
          this.reporteMensualList = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}
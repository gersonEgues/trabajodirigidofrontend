import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RangoFechaComponent } from 'src/app/shared/components/rango-fecha/rango-fecha.component';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { IngresoTanqueAguaVO } from 'src/app/tanque-de-agua/models/ingresoTanqueAguaVO';
import { TanqueAguaVO } from 'src/app/tanque-de-agua/models/tanqueAguaVO';
import { RegistroTanqueAguaService } from 'src/app/tanque-de-agua/services/registro-tanque-agua.service';
import { ReporteIngresoTanqueAguaService } from 'src/app/tanque-de-agua/services/reporte-ingreso-tanque-agua.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ReporteIngresoTanqueAgua } from 'src/app/tanque-de-agua/models/reporte-ingreso-tanque-agua-vo';
import { ReporteMesVO } from 'src/app/tanque-de-agua/models/reporteMesVO';
import { ReporteMensualIngresoTanqueAguaVO } from 'src/app/tanque-de-agua/models/reporteMensualIngresoTanqueAguaVO ';
@Component({
  selector: 'app-reporte-ingreso-agua-rango-fecha',
  templateUrl: './reporte-ingreso-agua-rango-fecha.component.html',
  styleUrls: ['./reporte-ingreso-agua-rango-fecha.component.css']
})
export class ReporteIngresoAguaRangoFechaComponent implements OnInit {
  @Input('titulo') titulo : string = "REPORTE DE EGRESOS ECONOMICOS POR COMPRA DE AGUA PARA EL TANQUE";

  aux:any = undefined;
  formularioTanqueAgua! : FormGroup;
  listaTanquesDeAgua    : TanqueAguaVO[]=[];
  
  // tabla
  tituloItems         : string[] = [];
  campoItems          : string[] = [];
  idItem              : string   = '';
  
  reporteIngresoTanqueAgua : ReporteIngresoTanqueAgua = this.aux;
  listaIngresoAgua         : IngresoTanqueAguaVO[] = [];
  rangoFechaDTO!           : RangoFechaDTO;

  // tabla mensual
  tituloItemsMes         : string[] = [];
  campoItemsMes          : string[] = [];
  idItemMes              : string   = '';
  
  reporteMensual       : ReporteMensualIngresoTanqueAguaVO = this.aux;
  reporteMensualList   : ReporteMesVO[] = [];

  @ViewChild('rangoFechaComponent') rangoFechaComponent!       : RangoFechaComponent;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private registroTanqueAguaService        : RegistroTanqueAguaService,
              private reporteIngresoTanqueAguaService  : ReporteIngresoTanqueAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configurarTablaIngresoTanqueAgua();
    this.construirformularioEstadoTanqueAgua();
    this.getListaTanqueAguaService();  
  }

  private construirformularioEstadoTanqueAgua(){
    this.formularioTanqueAgua = this.fb.group({     
      tanqueAgua    : ['0',[Validators.required],[]],
    });
  }

  private configurarTablaIngresoTanqueAgua(){
    this.tituloItemsMes = ['#','Mes','Volumen M3','Volumen Lts.','Costo total'];
    this.campoItemsMes  = ['mes','volumenM3','volumenLts','costoTotal'];
    this.idItemMes      = 'nro';

    this.tituloItems = ['#','Gestión','Mes','Fecha de registro','Volumen M3','Volumen Lts.','Costo'];
    this.campoItems  = ['gestion','mes','fechaRegistro','volumenM3','volumenLts','costoTotal'];
    this.idItem      = 'idIngresoAguaTanque';
  }

  public getReporteIngresoDeAguaATanqueEnRangoFecha(rangoFechaDTO:RangoFechaDTO){
    this.rangoFechaDTO = rangoFechaDTO;
    this.getReporteEnRangoFecha();
  }

  public getReporteEnRangoFecha(){
    if(this.rangoFechaDTO==this.aux)
      this.rangoFechaDTO = this.rangoFechaComponent?.getRangoFecha();

    let idTanqueAgua:number = Number(this.formularioTanqueAgua.get("tanqueAgua")?.value);
    
    if(this.rangoFechaDTO.rangoValido && idTanqueAgua != 0){
      this.getReporteIngresoAguaRangoFechaService(idTanqueAgua,this.rangoFechaDTO);
      this.getReporteMensualIngresoTanqueAguaRangoFechaService(idTanqueAgua,this.rangoFechaDTO);
    }
  }
  
  public campoValido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].valid &&
            this.formularioTanqueAgua.controls[nombreCampo].value != '0'; 
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  this.formularioTanqueAgua.controls[nombreCampo].touched &&
            this.formularioTanqueAgua.controls[nombreCampo].invalid;
  }

  public seleccionarItem(item:IngresoTanqueAguaVO){
    this.listaIngresoAgua.forEach(element => {
      if(element.idIngresoAguaTanque == item.idIngresoAguaTanque )
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public seleccionarItemMes(item:ReporteMesVO){
    this.reporteMensualList.forEach(element => {
      if(element.nro == item.nro )
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public genearteArchivoXLSX(idTable:string):void{
    let tableElemnt:HTMLElement = document.getElementById(idTable);
    this.generalService.genearteArchivoXLSX(tableElemnt,'reporte de ingreos de agua al tanque - rango fecha');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getListaTanqueAguaService(){
    this.registroTanqueAguaService.getListaTanqueAgua(true).subscribe(
      (resp)=>{
        this.listaTanquesDeAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteIngresoAguaRangoFechaService(idTanque:number,rangoFechaDTO:RangoFechaDTO){
    this.reporteIngresoTanqueAguaService.getReporteIngresoAguaRangoFecha(idTanque,rangoFechaDTO).subscribe(
      (resp)=>{       
        if(resp!=this.aux){          
          this.reporteIngresoTanqueAgua = resp;
          this.listaIngresoAgua = this.reporteIngresoTanqueAgua.ingresoTanqueAguaList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteIngresoTanqueAgua = this.aux;
          this.listaIngresoAgua = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteMensualIngresoTanqueAguaRangoFechaService(idTanque:number,rangoFechaDTO:RangoFechaDTO){
    this.reporteIngresoTanqueAguaService.getReporteMensualIngresoTanqueAguaEnRangoFecha(idTanque,rangoFechaDTO).subscribe(
      (resp)=>{        
        if(resp!=this.aux && resp.listaMes.length>0){          
          this.reporteMensual = resp;
          this.reporteMensualList = this.reporteMensual.listaMes;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteMensual = this.aux;
          this.reporteMensualList = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{        
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { TanqueDeAguaRoutingModule } from './tanque-de-agua-routing.module';
import { RegistroTanqueComponent } from './components/registro-tanque/registro-tanque.component';
import { SharedModule } from '../shared/shared.module';
import { RegistroIngresoAguaTanqueComponent } from './components/registro-ingreso-agua-tanque/registro-ingreso-agua-tanque.component';
import { EstadoActualTanqueAguaComponent } from './components/reporte/estado-actual-tanque-agua/estado-actual-tanque-agua.component';
import { ReporteDeIngresoDeAguaAlTanqueDeAguaComponent } from './components/reporte/reporte-de-ingreso-de-agua-al-tanque-de-agua/reporte-de-ingreso-de-agua-al-tanque-de-agua.component';
import { ReporteIngresoAguaRangoFechaComponent } from './components/reporte/reporte-ingreso-agua-rango-fecha/reporte-ingreso-agua-rango-fecha.component';
import { ReporteIngresoAguaGestionComponent } from './components/reporte/reporte-ingreso-agua-gestion/reporte-ingreso-agua-gestion.component';


@NgModule({
  declarations: [        
    RegistroTanqueComponent,
    RegistroIngresoAguaTanqueComponent,
    EstadoActualTanqueAguaComponent,
    ReporteDeIngresoDeAguaAlTanqueDeAguaComponent,
    ReporteIngresoAguaRangoFechaComponent,
    ReporteIngresoAguaGestionComponent,
  ],
  imports: [
    CommonModule,
    TanqueDeAguaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ]
})
export class TanqueDeAguaModule { }

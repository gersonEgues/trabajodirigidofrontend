export interface RangoFechaEgresoTanqueAguaDTO{
  idTanqueAgua : number,
  idCategoria  : number,
  fechaInicio  : string,
  fechaFin     : string,
  page         : number,
  size         : number,
}

import { IngresoTanqueAguaVO } from "./ingresoTanqueAguaVO";

export interface ReporteMesVO {
  nro           : number,
  mes           : string,
  volumenM3     : number,
  volumenLts    : number,
  costoTotal    : number,
  seleccionado? : boolean,
} 
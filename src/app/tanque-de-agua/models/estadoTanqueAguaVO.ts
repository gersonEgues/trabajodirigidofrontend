export interface EstadoTanqueAguaVO{
  idestadoActualTanqueAgua : number,
  idtanqueAgua             : number,
  volumenM3                : number,
  volumenLts               : number,
  fechaRegistro            : string,
}
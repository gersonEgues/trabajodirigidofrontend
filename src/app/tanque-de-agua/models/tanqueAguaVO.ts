import { TanqueAguaDTO } from './tanqueAguaDTO';

export  interface TanqueAguaVO extends TanqueAguaDTO{
  idTanqueAgua      : number,
  activo            : boolean,
  volumemActualM3?  : number;
  volumemActualLts? : number;
  costoTotal?       : number;
}
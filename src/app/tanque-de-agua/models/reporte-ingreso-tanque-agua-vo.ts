import { IngresoTanqueAguaVO } from "./ingresoTanqueAguaVO";

export interface ReporteIngresoTanqueAgua {
  totalVolumenM3        : number,
  totalVolumenLts       : number,
  sumaCostoTotal        : number,
  countRows             : number,
  ingresoTanqueAguaList : IngresoTanqueAguaVO[],
} 
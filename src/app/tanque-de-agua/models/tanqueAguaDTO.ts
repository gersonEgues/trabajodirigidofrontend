export  interface TanqueAguaDTO{
  codigo        : string,
  nombre        : string,
  ubicacion     : string,
  capacidadM3   : number,
  capacidadLts  : number,
  fechaRegistro : string,
}
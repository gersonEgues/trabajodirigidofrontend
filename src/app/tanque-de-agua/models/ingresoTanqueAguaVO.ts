export interface IngresoTanqueAguaVO {
  idIngresoAguaTanque : number,
  idtanqueAgua        : number,
  fechaRegistro       : string,
  volumenM3           : number,
  volumenLts          : number,
  costoTotal          : number,
  gestion             : number,
  mes                 : string,

  seleccionado?       : boolean,
}
import { ReporteMesVO } from "./reporteMesVO";

export interface ReporteMensualIngresoTanqueAguaVO {
  totalVolumenM3  : number,
  totalVolumenLts : number,
  sumaCostoTotal  : number,
  countRows       : number,
  listaMes        : ReporteMesVO[],
} 
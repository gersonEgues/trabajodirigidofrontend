export interface IngresoTanqueAguaDTO {  
  idTanqueAgua  : number,
  fechaRegistro : string,
  volumenM3     : number,
  volumenLts    : number,
  costoTotal    : number,
}

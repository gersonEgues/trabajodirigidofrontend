import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { BusquedaUsuarioComponent } from './components/busqueda-usuario/busqueda-usuario.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { RolesComponent } from './components/roles/roles.component';


@NgModule({
  declarations: [
    CrearUsuarioComponent,
    BusquedaUsuarioComponent,
    RolesComponent
  ],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    HttpClientModule
  ]
})
export class UsuariosModule { }

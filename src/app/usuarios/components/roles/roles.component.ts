import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RolService } from '../../services/rol.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { RolVO } from '../../models/rol-vo';
import { RolDTO } from '../../models/rol-dto';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  aux:any = undefined;

  formularioRol!  : FormGroup;
  rolSeleccionado : RolVO = this.aux;
  banderaUpdate   : boolean = false;

  tituloItemsRol : string[] = [];
  campoItemsRol  : string[] = [];
  idItemRol!     : string;
  listaItemsRol! : RolVO[];

  constructor(private fb             : FormBuilder,
              private generalService : GeneralService,
              private rolService     : RolService,
              private dialog         : MatDialog,
              private router         : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.construirTabla();
  }

  public construirFormulario(){
    this.formularioRol = this.fb.group({
      rol : ['',[Validators.required],[]],
    });    
  }

  private construirTabla(){
    this.tituloItemsRol = ['#','Nombre de rol'];
    this.campoItemsRol = ['nombre'];
    this.idItemRol  = 'id'; 
    
    this.getListaRolesService();
  }

  public formularioRolInvalido(){
    return this.formularioRol.controls['rol']?.invalid && 
           this.formularioRol.controls['rol']?.touched;
  }

  public formularioRolValido(){
    return this.formularioRol.controls['rol']?.valid;
  }

  public async crearRol(){
    if(this.formularioRol.invalid){
      this.formularioRol.markAllAsTouched();
      this.generalService.mensajeAlerta("El campo rol esta vacio");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVO ROL','¿Está seguro de crear este nuevo rol?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      this.limpiarFormularioRol();
      return; 
    }

    let rolDTO : RolDTO = {
      nombre : this.formularioRol.get('rol')?.value
    }

    this.createRolService(rolDTO);
  }

  public async actualizarRol(){
    if(this.formularioRol.invalid){
      this.formularioRol.markAllAsTouched();
      this.generalService.mensajeAlerta("El campo gestión esta vacio");
      return;
    }

    let id = this.rolSeleccionado.id;
    let rol:RolDTO = {
      nombre : this.formularioRol.get('rol')?.value,
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR ROL',`¿Está seguro de actualizar el rol ${this.rolSeleccionado.nombre} por ${rol.nombre}?`,'Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      this.limpiarFormularioRol();
      return; 
    }

    this.updateRolService(id,rol);
  }

  public cancelarCrearRol(){
    this.limpiarFormularioRol();
  }

  private limpiarFormularioRol(){
    let aux:any = undefined;
    this.banderaUpdate = false;
    this.rolSeleccionado = aux;
    this.formularioRol.reset();
  }

  public actualizarRolEvent(rol:RolVO){
    this.banderaUpdate = true;
    this.rolSeleccionado = rol;
    this.formularioRol.get('rol')?.setValue(rol.nombre);    
  }

  public async eliminarRolEvent(rol:RolVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR ROL','¿Está seguro de eliminar este rol?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      this.limpiarFormularioRol();
      return; 
    }

    this.deleteRolService(rol);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaRoles')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de roles');
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  public getListaRolesService() {
    this.rolService.getListaRoles().subscribe(
      (resp)=>{
        this.listaItemsRol = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createRolService(rol:RolDTO){
    this.rolService.createRol(rol).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se creo con exito el rol ${rol.nombre}`);
        this.getListaRolesService();
        this.limpiarFormularioRol();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateRolService(id:number,rol:RolDTO){
    this.rolService.updateRol(id,rol).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se actualizo correctamete el rol ${rol.nombre}`);
        this.getListaRolesService();
        this.limpiarFormularioRol();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteRolService(rol:RolVO){
    this.rolService.deleteRol(rol.id).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se elimino con exito el rol " + rol.nombre);
        this.getListaRolesService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }

}

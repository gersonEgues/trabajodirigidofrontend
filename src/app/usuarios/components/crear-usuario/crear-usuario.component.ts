import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/login/services/user.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { UsuarioService } from '../../services/usuario.service';
import { RolService } from '../../services/rol.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioVO } from '../../models/usuario-vo';
import { validarQueSeanIguales } from '../../validators/misma-contrasenia.validator';
import { RolVO } from '../../models/rol-vo';
import { PageDTO } from '../../../shared/models/pageDTO';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { UsuarioDTO } from '../../models/usuario-dto';
import { PageEventDTO } from 'src/app/shared/models/pageEventDTO';
import { InputFormComponent } from 'src/app/shared/components/input-form/input-form.component';
import { BanderaResponseVO } from '../../../shared/models/banderaResponseVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {
  aux        : any = undefined;
  listaRoles : RolVO[] = [];

  formularioUsuario!  : FormGroup;
  usuarioSeleccionado : UsuarioVO = this.aux;

  banderaUpdate     : boolean = false;
  estadoUsuario     : boolean = true;

  // tabla eventos
  tituloItemsUsuario            : string[] = [];
  campoItemsUsuario             : string[] = [];
  idUsuario                     : string = 'id';
  listaUsuarios                 : UsuarioVO[] = [];
  countItemsUsuario             : number;
  pageDTO!                      : PageDTO;

  @ViewChild('tablaComponent') tablaComponent!                      : TablePaginatorComponent;
  @ViewChild('inputFormComponentNombre') inputFormComponentNombre!  : InputFormComponent;

  constructor(private fb             : FormBuilder,
              private generalService : GeneralService,
              private usarioService  : UsuarioService,
              private userService    : UserService,
              private rolService     : RolService,
              private dialog         : MatDialog,
              private router         : Router,) { }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
    };

    this.construirTabla();
    this.construirFormulario();
    this.getCountListaUsuario(this.estadoUsuario);
    this.getListaRolesService();
    this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
  }

  public construirFormulario(){
    this.formularioUsuario = this.fb.group({
      nombre          : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      apellidoPaterno : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      apellidoMaterno : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      email           : ['',[Validators.required],[]],
      password1       : ['',[Validators.required],[]],
      password2       : ['',[Validators.required],[]],
    },
    {
      validators: validarQueSeanIguales,
    }
    );
  }

  public construirTabla(){
    this.tituloItemsUsuario = ['#','Nombre','Apellido paterno','Apellido materno','Email', 'Roles'];
    this.campoItemsUsuario = ['nombre','apellidoPaterno','apellidoMaterno','email','rol'];
  }

  public formularioUsuarioValido(campo:string) : boolean{
    return this.formularioUsuario.controls[campo]?.valid;
  }

  public formularioUsuarioInvalido(campo:string) : boolean{   
    return this.formularioUsuario.controls[campo]?.invalid && 
           this.formularioUsuario.controls[campo]?.touched;
  }

  public passwordValido(campo:string) : boolean{
    return this.formularioUsuario.controls[campo]?.valid;
  }

  public passwordInvalido(campo:string) : boolean{   
    return  (this.formularioUsuario.controls[campo]?.invalid && 
            this.formularioUsuario.controls[campo]?.touched)||
            this.formularioUsuario?.hasError('noSonIguales');
  }

  public async crearUsuario(){
    if(this.formularioUsuario.invalid){
      this.formularioUsuario.markAllAsTouched();
      this.generalService.mensajeAlerta("Llene todos los campos");
      return;
    }

    if(!this.rolSeleccionado()){
      this.generalService.mensajeAlerta("Seleccione al menos un rol para el nuevo usuario");
      return;
    }

    let existeUsuario:BanderaResponseVO = await this.existeUsuarioService(this.formularioUsuario.get('email').value);
    if(existeUsuario.bandera){
      this.generalService.mensajeAlerta("Este email ya fue registrado, registre otro");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVO USUARIO','¿Está seguro de crear al nuevo usuario?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      this.limpiarFormulario();
      return; 
    }

    let rolesUsuario:RolVO[] = this.getRolesSeleccionados();
    let usuario:UsuarioDTO = {
      nombre          : this.getCapitalize(this.formularioUsuario.get('nombre').value),
      apellidoPaterno : this.getCapitalize(this.formularioUsuario.get('apellidoPaterno').value),
      apellidoMaterno : this.getCapitalize(this.formularioUsuario.get('apellidoMaterno').value),
      email           : this.formularioUsuario.get('email').value,
      contrasenia1    : this.formularioUsuario.get('password1').value,
      contrasenia2    : this.formularioUsuario.get('password2').value,
      roles           : rolesUsuario
    }
    this.createUsuarioService(usuario);
  }

  private getCapitalize(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  public checkRol(rol:RolVO){
    let element = <HTMLInputElement>document.getElementById(`rol${rol.id}`); 
    let checked:boolean = element.checked;

    let i:number=0;   
    let encontrado:boolean = false;

    while(!encontrado && i<this.listaRoles.length){
      if(this.listaRoles[i].id == rol.id){
        this.listaRoles[i].check = checked;
        encontrado = true;
      }
      i++;
    } 
  }

  private rolSeleccionado():boolean{
    let i:number=0;   
    let encontrado:boolean = false;

    while(!encontrado && i<this.listaRoles.length){
      if(this.listaRoles[i].check == true)
        encontrado = true;
      i++;
    } 
    return encontrado;
  }

  private getRolesSeleccionados():RolVO[]{
    let rolesSeleccionados:RolVO[] = [];
    this.listaRoles.forEach(element => {
      if(element.check)
        rolesSeleccionados.push(element);
    });
    return rolesSeleccionados;
  }

  public cancelarCrearUsuario(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.usuarioSeleccionado = this.aux;
    this.banderaUpdate = false;
    this.formularioUsuario.reset();
    this.listaRoles.forEach(element => {
      element.check = false;
    });
  }

  public async actualizarUsuario(){
    if(this.formularioUsuario.invalid){
      this.formularioUsuario.markAllAsTouched();
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      return;
    }

    if(!this.rolSeleccionado()){
      this.generalService.mensajeAlerta("Seleccione al menos un rol para el nuevo usuario");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR NUEVO USUARIO','¿Está seguro de actualizar al nuevo usuario?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      this.limpiarFormulario();
      return; 
    }

    let rolesUsuario:RolVO[] = this.getRolesSeleccionados();
    let usuario:UsuarioDTO = {
      nombre          : this.formularioUsuario.get('nombre').value,
      apellidoPaterno : this.formularioUsuario.get('apellidoPaterno').value,
      apellidoMaterno : this.formularioUsuario.get('apellidoMaterno').value,
      email           : this.formularioUsuario.get('email').value,
      contrasenia1    : this.formularioUsuario.get('password1').value,
      contrasenia2    : this.formularioUsuario.get('password2').value,
      roles           : rolesUsuario
    }

    this.updateUsuarioService(this.usuarioSeleccionado.id,usuario);
  }

  public cambioEstadoUsuario(){
    this.estadoUsuario = !this.estadoUsuario;
    this.tablaComponent.deseleccionarItems();
    
    if(this.isAdmin() || this.isPresidente()){
      this.tablaComponent.cambioEstadoEditar();
      this.tablaComponent.cambioEstadoEliminar();
      this.tablaComponent.cambioEstadoVer();
    }
    
    this.inputFormComponentNombre.limpiarTexto();
    this.getCountListaUsuario(this.estadoUsuario);
    this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
  }

  public updateUsuarioEvent(usuario:UsuarioVO){
    this.limpiarFormulario();
    this.banderaUpdate = true;
    this.usuarioSeleccionado = usuario;

    this.formularioUsuario.get('nombre').setValue(usuario.nombre);
    this.formularioUsuario.get('apellidoPaterno').setValue(usuario.apellidoPaterno);
    this.formularioUsuario.get('apellidoMaterno').setValue(usuario.apellidoMaterno);
    this.formularioUsuario.get('email').setValue(usuario.email);
    this.formularioUsuario.get('password1').setValue('');
    this.formularioUsuario.get('password2').setValue('');


    usuario.roles.forEach(rol => {
      this.listaRoles.forEach(rolLista => {
        if(rol.id == rolLista.id){
          rolLista.check = true;
        }
      });
    });
  }

  public async cambioEstadoUsuarioEvent(usuario:UsuarioVO,estado:boolean){
    let nombreCompleto : string = usuario.nombre + ' ' + usuario.apellidoPaterno + ' ' + usuario.apellidoMaterno;
    
    let afirmativo:Boolean = false;
    if(estado)
      afirmativo = await this.getConfirmacion('DAR DE ALTA A USUARIO',`¿Está seguro de dar de alta al usuario ${nombreCompleto}?`,'Sí, estoy seguro','No, Cancelar');
    else
      afirmativo = await this.getConfirmacion('DAR DE BAJA A USUARIO',`¿Está seguro de dar de baja al usuario ${nombreCompleto}?`,'Sí, estoy seguro','No, Cancelar');

    if(!afirmativo){ 
      this.limpiarFormulario();
      return; 
    }

    this.cambioEstadoUsuarioService(usuario.id,estado);
  }

  public cambioDePagina(page:PageEventDTO){
    let pageDto:PageDTO = {
      page : page.pageIndex,
      size : page.pageSize
    }
    this.limpiarFormulario();
    this.getListaUsuarioPorEstado(this.estadoUsuario,pageDto);
  } 

  public buscarUsuarioPorNombre(nombre:string):void{
    if(nombre == "" || nombre.trim() == ""){ 
      this.tablaComponent.deseleccionarItems();
      return;
    }

    this.tablaComponent.buscarItem(nombre,"nombre");
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    this.tablaComponent.genearteArchivoXLSX("lista de usuarios");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------  
  public getListaRolesService() {
    this.rolService.getListaRoles().subscribe(
      (resp)=>{
        this.listaRoles = resp;
        this.listaRoles.forEach(element => {
          element.check = false;
        });
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountListaUsuario(estado:boolean){
    return this.usarioService.getCountListaUsuario(estado).subscribe(
      (resp)=>{
        this.countItemsUsuario = resp.countItem;       
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  public createUsuarioService(usuario:UsuarioDTO){
    let nombreCompleto:string = usuario.nombre + ' ' + usuario.apellidoPaterno + ' ' + usuario.apellidoMaterno;
    this.usarioService.createUsuario(usuario).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`El usuario ${nombreCompleto} fue creado corectamente`);
        this.limpiarFormulario();
        this.getCountListaUsuario(this.estadoUsuario);
        this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);

      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateUsuarioService(id:number,usuario:UsuarioDTO){
    let nombreCompleto:string = usuario.nombre + ' ' + usuario.apellidoPaterno + ' ' + usuario.apellidoMaterno;
    this.usarioService.updateUsuario(id,usuario).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`El usuario ${nombreCompleto} fue creado corectamente`);
        this.limpiarFormulario();
        this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cambioEstadoUsuarioService(id:number,estado:boolean) {
    this.usarioService.cambioEstadoUsuario(id,estado).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se cambio el estado del usuario correctamente`);
        this.getCountListaUsuario(this.estadoUsuario);
        this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaUsuarioPorEstado(estado:boolean,pageDTO:PageDTO){
    return this.usarioService.getListaUsuarioPorEstado(estado,pageDTO).subscribe(
      (resp)=>{
        if(resp){
          this.listaUsuarios = resp;    
          if(this.listaUsuarios.length>0){
            let nombreRol : string = '';
            this.listaUsuarios.forEach(usuario => {
              nombreRol = '';
              usuario.roles.forEach(rol => {
                nombreRol += rol.nombre  + ' - ';
              });
              nombreRol = nombreRol.slice(0,nombreRol.length-3);
              usuario.rol = nombreRol;
            });
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          }               
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }          
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }


  private existeUsuarioService(mail:string):Promise<BanderaResponseVO>{
    return this.userService.existeUsuarioTodos(mail).toPromise()
      .then((data:BanderaResponseVO) => {
        return data;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
        return err;
      });
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

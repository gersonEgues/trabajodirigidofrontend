import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { UsuarioService } from '../../services/usuario.service';
import { MatDialog } from '@angular/material/dialog';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { PageEventDTO } from 'src/app/shared/models/pageEventDTO';
import { UsuarioVO } from '../../models/usuario-vo';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { InputFormComponent } from 'src/app/shared/components/input-form/input-form.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-busqueda-usuario',
  templateUrl: './busqueda-usuario.component.html',
  styleUrls: ['./busqueda-usuario.component.css']
})
export class BusquedaUsuarioComponent implements OnInit {
  estadoUsuario:boolean = true;

   // tabla eventos
  tituloItemsUsuario            : string[] = [];
  campoItemsUsuario             : string[] = [];
  idAporteEventual              : string = 'id';
  listaUsuarios                 : UsuarioVO[] = [];
  countItemsUsuario             : number;
  pageDTO!                      : PageDTO;

  @ViewChild('tablaBusquedaUsuarios') tablaBusquedaUsuarios!             : TablePaginatorComponent;
  @ViewChild('InputFormComponentEmail') InputFormComponentEmail!         : InputFormComponent;
  @ViewChild('InputFormComponentNombre') InputFormComponentNombre!       : InputFormComponent;
  @ViewChild('InputFormComponentApellido') InputFormComponentApellido!   : InputFormComponent;
   
  constructor(private generalService : GeneralService,
              private usarioService  : UsuarioService,
              private dialog         : MatDialog,
              private router         : Router) { }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
    };

    this.construirTabla();
    this.getCountListaUsuario(this.estadoUsuario);
    this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
  }

  public construirTabla(){
    this.tituloItemsUsuario = ['#','Nombre','Apellido paterno','Apellido materno','Email', 'Roles'];
    this.campoItemsUsuario = ['nombre','apellidoPaterno','apellidoMaterno','email','rol'];
  }

  public cambioEstadoUsuario(){
    this.estadoUsuario = !this.estadoUsuario;
    this.tablaBusquedaUsuarios.deseleccionarItems();  
    this.getCountListaUsuario(this.estadoUsuario);
    this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
  }

  public buscarUsuarioPorEmail(email:string){
    if(email.trim()==""){
      this.tablaBusquedaUsuarios.deseleccionarItems();
    }else{
      this.InputFormComponentNombre.limpiarTexto();
      this.InputFormComponentApellido.limpiarTexto();
      this.tablaBusquedaUsuarios.buscarItem(email,'email');
    }
  }

  public buscarUsuarioPorNombre(nombre:string){
    if(nombre.trim()==""){
      this.tablaBusquedaUsuarios.deseleccionarItems();
    }else{
      this.InputFormComponentApellido.limpiarTexto();
      this.InputFormComponentEmail.limpiarTexto();
      this.tablaBusquedaUsuarios.buscarItem(nombre,'nombre');
    }
  }

  public buscarUsuarioPorApellido(apellido:string){
    if(apellido.trim()==""){
      this.tablaBusquedaUsuarios.deseleccionarItems();
    }else{
      this.InputFormComponentEmail.limpiarTexto();
      this.InputFormComponentNombre.limpiarTexto();
      this.tablaBusquedaUsuarios.buscarItem(apellido,'apellidoPaterno');
    }
  }

  public cambioDePagina(page:PageEventDTO){
    this.pageDTO = {
      page : page.pageIndex,
      size : page.pageSize
    }
    this.getListaUsuarioPorEstado(this.estadoUsuario,this.pageDTO);
  } 

  public genearteArchivoXLSX():void{
    this.tablaBusquedaUsuarios.genearteArchivoXLSX("busqueda de usuarios");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------  
  public getCountListaUsuario(estado:boolean){
    return this.usarioService.getCountListaUsuario(estado).subscribe(
      (resp)=>{
        this.countItemsUsuario = resp.countItem;       
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  public getListaUsuarioPorEstado(estado:boolean,pageDTO:PageDTO){
    return this.usarioService.getListaUsuarioPorEstado(estado,pageDTO).subscribe(
      (resp)=>{
        if(resp){
          this.listaUsuarios = resp;          
          if(this.listaUsuarios.length>0){
            let nombreRol : string = '';
            this.listaUsuarios.forEach(usuario => {
                nombreRol = '';
                usuario.roles.forEach(rol => {
                  nombreRol += rol.nombre  + ' - ';
                });
                nombreRol = nombreRol.slice(0,nombreRol.length-3);
                usuario.rol = nombreRol;
            }); 
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);          
          }                
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);          
        }        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

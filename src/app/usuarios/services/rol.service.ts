import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { RolVO } from '../models/rol-vo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RolDTO } from '../models/rol-dto';

@Injectable({
  providedIn: 'root'
})
export class RolService extends RootServiceService{
  constructor(private http: HttpClient) { 
    super("api/rol-user",http);
  }
  
  public getListaRoles(): Observable<RolVO[]>{
    return this.http.get<RolVO[]>(`${this.url}/lista`).pipe(
      map(resp => {
        return resp as RolVO[];
      })
    );
  }

  public createRol(rol:RolDTO): Observable<RolVO>{
    return this.http.post<RolVO>(this.url,rol).pipe(
      map(resp => {
        return resp as RolVO;
      })
    );
  }

  public updateRol(id:number,rol:RolDTO): Observable<RolVO>{
    return this.http.put<RolVO>(`${this.url}/${id}`,rol).pipe(
      map(resp => {
        return resp as RolVO;
      })
    );
  }

  public deleteRol(id:number): Observable<RolVO>{
    return this.http.delete<RolVO>(`${this.url}/${id}`).pipe(
      map(resp => {
        return resp as RolVO;
      })
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { UsuarioDTO } from '../models/usuario-dto';
import { map } from 'rxjs/operators';
import { CountItemVO } from '../../shared/models/countItemVO';
import { UsuarioVO } from '../models/usuario-vo';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { MiembroMesaDirectivaDTO } from 'src/app/mesa-directiva/models/miembroMesaDirectivaDTO';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends RootServiceService{
  constructor(private http: HttpClient) { 
    super("api/user",http);
  }

  public getCountListaUsuario(estado:boolean): Observable<CountItemVO>{
    return this.http.get<CountItemVO>(`${this.url}/count/${estado}`).pipe(
      map(resp => {
        return resp as CountItemVO;
      })
    );
  }

  public getListaUsuarios(): Observable<UsuarioVO[]>{
    return this.http.get<UsuarioVO[]>(`${this.url}/lista`).pipe(
      map(resp => {
        return resp as UsuarioVO[];
      })
    );
  }

  public getListaUsuarioPorEstado(estado:boolean,pageDTO:PageDTO): Observable<UsuarioVO[]>{
    return this.http.get<UsuarioVO[]>(`${this.url}/lista/${estado}/${pageDTO.page}/${pageDTO.size}`).pipe(
      map(resp => {
        return resp as UsuarioVO[];
      })
    );
  }

  public createUsuario(usuario:UsuarioDTO): Observable<UsuarioVO>{
    return this.http.post<UsuarioVO>(this.url,usuario).pipe(
      map(resp => {
        return resp as UsuarioVO;
      })
    );
  }

  public updateUsuario(id:number,usuario:UsuarioDTO): Observable<UsuarioVO>{
    return this.http.put<UsuarioVO>(`${this.url}/${id}`,usuario).pipe(
      map(resp => {
        return resp as UsuarioVO;
      })
    );
  }

  public cambioEstadoUsuario(id:number,estado:boolean) : Observable<UsuarioVO>{
    return this.http.put<UsuarioVO>(`${this.url}/${id}/${estado}`,null).pipe(
      map(resp => {
        return resp as UsuarioVO;
      })
    );
  }

  public asignarUsuarioAMesadirectiva(miembrosMesaDirectiva:MiembroMesaDirectivaDTO) : Observable<UsuarioVO>{
    return this.http.post<UsuarioVO>(`${this.url}/asignar-usuario-mesa-directiva`,miembrosMesaDirectiva).pipe(
      map(resp => {
        return resp as UsuarioVO;
      })
    );
  }

  public removeUsuarioDeMesadirectiva(idMesaDirectiva:number,idUsuario:number) : Observable<UsuarioVO>{
    return this.http.delete<UsuarioVO>(`${this.url}/remove-usuario-mesa-directiva/${idMesaDirectiva}/${idUsuario}`).pipe(
      map(resp => {
        return resp as UsuarioVO;
      })
    );
  }

  public getListaUsuariosDeMesaDirectiva(estado:boolean,idMesaDirectiva:number) : Observable<UsuarioVO[]>{
    return this.http.get<UsuarioVO[]>(`${this.url}/lista-miembros-mesa-directiva/${estado}/${idMesaDirectiva}`).pipe(
      map(resp => {
        return resp as UsuarioVO[];
      })
    );
  }
}

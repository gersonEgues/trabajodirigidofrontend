import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BusquedaUsuarioComponent } from './components/busqueda-usuario/busqueda-usuario.component';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { RolesComponent } from './components/roles/roles.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'lista-de-usuarios', 
        component: BusquedaUsuarioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [AuthGuard,RolGuard] 
      },
      { 
        path  : 'registro-de-usuarios', 
        component: CrearUsuarioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [AuthGuard,RolGuard]
      },
      { 
        path  : 'roles' , 
        component: RolesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [AuthGuard,RolGuard] 
      },
      { 
        path  : '**', 
        redirectTo : 'lista-de-usuarios' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }

import { RolVO } from './rol-vo';
export interface UsuarioDTO{
  nombre          : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  email           : string,
  contrasenia1    : string,
  contrasenia2    : string,
  roles           : RolVO[]
}

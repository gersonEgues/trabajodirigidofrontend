import { RolVO } from "./rol-vo";

export interface UsuarioVO{
  id              : number,
  nombre          : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  email           : string,
  rol?            : string,
  roles           : RolVO[],
  
}

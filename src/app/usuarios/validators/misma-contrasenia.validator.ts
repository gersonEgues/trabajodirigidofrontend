import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms"

export const validarQueSeanIguales: ValidatorFn = (form:FormGroup): ValidationErrors | null => {
  let p1:string = form.get('password1')?.value;
  let p2:string = form.get('password2')?.value;  
  return p1 === p2 ? null : { noSonIguales: true }
}
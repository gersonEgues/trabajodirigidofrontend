import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RangoPeriodoDTO } from 'src/app/shared/models/rangoPeriodoDTO';
import { ReporteMesCobroExtraVO } from '../models/reporte-mes-cobro-extra-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class ReporteCobroExtraMesService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-cobro-extra/reporte-por-mes",http);
  }

  public getReporteCobroExtraTodosRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<ReporteMesCobroExtraVO>{
    return this.http.put<ReporteMesCobroExtraVO>(`${this.url}/rango-fecha/todas-las-categorias`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteMesCobroExtraVO;
      })
    );
  }

  public getReporteCobroExtraCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO): Observable<ReporteMesCobroExtraVO>{
    return this.http.put<ReporteMesCobroExtraVO>(`${this.url}/rango-fecha/categoria/${idCategoria}`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteMesCobroExtraVO;
      })
    );
  }

  public getReporteCobroExtraTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteMesCobroExtraVO>{
    return this.http.put<ReporteMesCobroExtraVO>(`${this.url}/rango-periodo/todos-los-periodos`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteMesCobroExtraVO;
      })
    );
  }

  public getReporteCobroExtraCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteMesCobroExtraVO>{
    return this.http.put<ReporteMesCobroExtraVO>(`${this.url}/rango-periodo/categoria/${idCategoria}`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteMesCobroExtraVO;
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RangoPeriodoDTO } from 'src/app/shared/models/rangoPeriodoDTO';
import { ReporteResumenCobroExtraVO } from '../models/reporte-resumen-cobro-extra-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class ReporteResumenCobroExtraMesService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-cobro-extra/resumen",http);
  }

  public getResumenCobroExtraTodosRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<ReporteResumenCobroExtraVO>{
    return this.http.put<ReporteResumenCobroExtraVO>(`${this.url}/rango-fecha/todas-las-categorias`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteResumenCobroExtraVO;
      })
    );
  }

  public getResumenCobroExtraCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO): Observable<ReporteResumenCobroExtraVO>{
    return this.http.put<ReporteResumenCobroExtraVO>(`${this.url}/rango-fecha/categoria/${idCategoria}`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteResumenCobroExtraVO;
      })
    );
  }

  public getResumenCobroExtraTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteResumenCobroExtraVO>{
    return this.http.put<ReporteResumenCobroExtraVO>(`${this.url}/rango-periodo/todas-las-categorias`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteResumenCobroExtraVO;
      })
    );
  }

  public getResumenCobroExtraCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteResumenCobroExtraVO>{
    return this.http.put<ReporteResumenCobroExtraVO>(`${this.url}/rango-periodo/categoria/${idCategoria}`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteResumenCobroExtraVO;
      })
    );
  }
}
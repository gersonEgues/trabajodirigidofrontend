import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RangoPeriodoDTO } from 'src/app/shared/models/rangoPeriodoDTO';
import { ReporteLecturaPeridoMesVO } from '../models/reporte-lectura-periodo-mes-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class ReporteIngresoLecturaMedidorMesService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-lectura-mes",http);
  }

  public getReporteLecturaPorMesTodosRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<ReporteLecturaPeridoMesVO>{
    return this.http.put<ReporteLecturaPeridoMesVO>(`${this.url}/rango-fecha/todas-las-categorias`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteLecturaPeridoMesVO;
      })
    );
  }

  public getReporteLecturaPorMesCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO): Observable<ReporteLecturaPeridoMesVO>{
    return this.http.put<ReporteLecturaPeridoMesVO>(`${this.url}/rango-fecha/categoria/${idCategoria}`,rangoFechaDTO).pipe(
      map(response => {
        return response as ReporteLecturaPeridoMesVO;
      })
    );
  }

  public getReporteLecturaPorMesTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteLecturaPeridoMesVO>{
    return this.http.put<ReporteLecturaPeridoMesVO>(`${this.url}/rango-periodo/todas-las-categorias`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteLecturaPeridoMesVO;
      })
    );
  }

  public getReporteLecturaPorMesCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO): Observable<ReporteLecturaPeridoMesVO>{
    return this.http.put<ReporteLecturaPeridoMesVO>(`${this.url}/rango-periodo/categoria/${idCategoria}`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as ReporteLecturaPeridoMesVO;
      })
    );
  }
}

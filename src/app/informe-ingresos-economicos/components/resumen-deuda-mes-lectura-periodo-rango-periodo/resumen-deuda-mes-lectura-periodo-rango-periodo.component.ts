import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { CategoriaVO } from 'src/app/configuraciones/models/categoriaVO';
import { RangoPeriodoDTO } from 'src/app/shared/models/rangoPeriodoDTO';
import { RangoPeriodoComponent } from 'src/app/shared/components/rango-periodo/rango-periodo.component';
import { ReporteCobroExtraMesService } from '../../services/reporte-cobro-extra-mes.service';
import { ReporteResumenCobroExtraMesService } from '../../services/reporte-resumen-cobro-extra-mes.service';
import { ReporteIngresoLecturaMedidorMesService } from '../../services/reporte-ingreso-lectura-medidor-mes.service';
import { MesResumenLecturaPeriodoVO } from '../../models/mes-resumen-lectura-periodovo';
import { MesCobroExtraVOTratado } from '../../models/mes-cobro-extra-vo-tratado';
import { MesCobroExtraVO } from '../../models/mes-cobro-extra-vo';
import { CobroExtraVO } from '../../models/cobro-extra-vo';
import { ReporteLecturaPeridoMesVO } from '../../models/reporte-lectura-periodo-mes-vo';
import { ReporteMesCobroExtraVO } from '../../models/reporte-mes-cobro-extra-vo';

@Component({
  selector: 'app-resumen-deuda-mes-lectura-periodo-rango-periodo',
  templateUrl: './resumen-deuda-mes-lectura-periodo-rango-periodo.component.html',
  styleUrls: ['./resumen-deuda-mes-lectura-periodo-rango-periodo.component.css']
})
export class ResumenDeudaMesLecturaPeriodoRangoPeriodoComponent implements OnInit {

  aux:any = null;
  formularioReporte! : FormGroup;
  listaCategorias : CategoriaVO[] = [];

  // tabla
  tituloItemsLecturaMes         : string[]    = [];
  campoItemsLecturaMes          : string[]    = [];
  idItemLecturaMes              : string      = 'id';

  tituloItemsCobroExtraMes         : string[]    = [];
  campoItemsCobroExtraMes          : string[]    = [];
  idItemCobroExtraMes              : string      = 'id';

  tituloItemsResumenCobroExtraMes         : string[]    = [];
  campoItemsResumenCobroExtraMes          : string[]    = [];
  idItemResumenCobroExtraMes              : string      = 'id';
  
  reporteLecturaMes             : ReporteLecturaPeridoMesVO = this.aux;
  reporteCobroExtraMes          : ReporteMesCobroExtraVO = this.aux;
  mesCobroExtraVOTratadoList    : MesCobroExtraVOTratado[] = this.aux;
  reporteResumenCobroExtraMes   : any = this.aux;
  
  @ViewChild('rangoPeriodoComponent') rangoPeriodoComponent!  : RangoPeriodoComponent;
  rangoPeriodoDTO!        : RangoPeriodoDTO;

  constructor(private fb                                     : FormBuilder,
              private generalService                         : GeneralService,
              private mesAnioCategoriaCostoAguaService       : MesAnioCategoriaCostoAguaService,
              private reporteCobroExtraMesService            : ReporteCobroExtraMesService,
              private reporteResumenCobroExtraMesService     : ReporteResumenCobroExtraMesService,
              private reporteIngresoLecturaMedidorMesService : ReporteIngresoLecturaMedidorMesService,
              private router                                 : Router) { }

  ngOnInit(): void {
    this.getListaCategoriasConfiguracionCostoAguaPeriodoService();
    this.construirFormularioReporte();
    this.configurarTituloTablas();
  }

  private construirFormularioReporte(){
    this.formularioReporte = this.fb.group({
      categoria : ['-1',[Validators.required],[]]
    });
  }

  private configurarTituloTablas(){
    this.tituloItemsLecturaMes = ['#','Mes','Lectura anterior','Lectura actual','Consumo M3','Costo'];
    this.campoItemsLecturaMes  = ['nombreMes','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','costoTotal'];
    this.idItemLecturaMes      = 'nroMes';

    this.tituloItemsCobroExtraMes = ['#','Mes','Cod.','Nombre cobro extra','Costo','Total'];
    this.campoItemsCobroExtraMes  = ['nombreCobroExtra','detalle','costoCobroExtra'];
    this.idItemCobroExtraMes      = 'idmes';

    this.tituloItemsResumenCobroExtraMes = ['#','Código','Nombre cobro extra','Costo'];
    this.campoItemsResumenCobroExtraMes  = ['nombreCobroExtra','detalle','costoCobroExtra'];
    this.idItemResumenCobroExtraMes      = 'idCobroExtra';
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioReporte.get(campo).valid &&
            this.formularioReporte.get(campo).value != '-1';
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioReporte.get(campo).invalid && 
            this.formularioReporte.get(campo).touched;
  }

  public getReporteCategoria(){
    this.rangoPeriodoDTO = this.rangoPeriodoComponent.getRangoPeriodoDTO();
    this.getReporte(this.rangoPeriodoDTO);
  }

  public getReporte(rangoPeriodoDTO:RangoPeriodoDTO){
    this.rangoPeriodoDTO = rangoPeriodoDTO;
    
    if(!rangoPeriodoDTO.todosSeleccionados){ // no todas las opciones estan seleccionadas
      this.limpiarResultadosReporte();
      return;
    }

    if(!rangoPeriodoDTO.rangoValido){
      this.limpiarResultadosReporte();
      this.generalService.mensajeFormularioInvalido('Revise el rango de los periodos, El periodo inicio tiene que ser menor o igual al periodo fin');
      return;
    }

    this.rangoPeriodoDTO.cancelado = false;
    let idCategoria:number = Number(this.formularioReporte.get('categoria').value);
    if(idCategoria==0){
      this.getReporteLecturaPorMesTodosRangoPeriodo(this.rangoPeriodoDTO);
      this.getReporteCobroExtraTodosRangoPeriodo(this.rangoPeriodoDTO);
      this.getResumenCobroExtraTodosRangoPeriodo(this.rangoPeriodoDTO);
    }else{
      this.getReporteLecturaPorMesCategoriaRangoPeriodo(idCategoria,this.rangoPeriodoDTO);
      this.getReporteCobroExtraCategoriaRangoPeriodo(idCategoria,this.rangoPeriodoDTO);
      this.getResumenCobroExtraCategoriaRangoPeriodo(idCategoria,this.rangoPeriodoDTO);
    }
  }

  private limpiarResultadosReporte(){
    this.reporteLecturaMes = this.aux;
    this.reporteCobroExtraMes = this.aux;
    this.mesCobroExtraVOTratadoList = [];
    this.reporteResumenCobroExtraMes = this.aux;
  }

  private tratarCobroExtra(){
    this.mesCobroExtraVOTratadoList = [];

    let mesCobroExtraList:MesCobroExtraVO[] = this.reporteCobroExtraMes.mesCobroExtraList;
    for (let i = 0; i < mesCobroExtraList.length; i++) {
      let mesCobroExtra : MesCobroExtraVO = mesCobroExtraList[i];
      let cobroExtraList:CobroExtraVO[] = mesCobroExtra.cobroExtraPeriodoDataList;
      for (let j = 0; j < cobroExtraList.length; j++) {
        let cobroextra:CobroExtraVO = cobroExtraList[j];
        let mesCobroExtraVOTratado:MesCobroExtraVOTratado = {
          indexGlobalMes  : j,
          indexMes        : i+1,

          // mes
          idmes            : mesCobroExtra.idmes,
          nroMes           : mesCobroExtra.nroMes,
          nombreMes        : mesCobroExtra.nombreMes,
          nroRows          : mesCobroExtra.nroRows,
          sumaTotal        : mesCobroExtra.sumaTotal,

          // cobro extra
          idCobroExtra     : cobroextra.idCobroExtra,
          nombreCobroExtra : cobroextra.nombreCobroExtra,
          detalle          : cobroextra.detalle,
          costoCobroExtra  : cobroextra.costoCobroExtra, 

          seleccionado : false
        }
        this.mesCobroExtraVOTratadoList.push(mesCobroExtraVOTratado);     
      }    
    }
  }

  public seleccionarLecturaMes(lecturaMes:MesResumenLecturaPeriodoVO){
    this.reporteLecturaMes.resumenLecturaPeriodoList.forEach(item => {
      if(item.nroMes == lecturaMes.nroMes){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public seleccionarCobroExtraMes(lecturaMes:MesCobroExtraVOTratado){
    this.mesCobroExtraVOTratadoList.forEach(item => {
      if(item.nroMes == lecturaMes.nroMes){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public seleccionarResumenCobroExtraMes(cobroExtra:CobroExtraVO){
    this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.forEach(item => {
      if(item.idCobroExtra == cobroExtra.idCobroExtra){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public genearteArchivoLecturaPeriodoXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaDeudaLecturaMesRangoPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista ingreso lectura mes - rango fecha');
  }

  public genearteArchivoCobroExtraMesXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaDeudaCobroExtraMesRangoPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista ingreso cobros extra por mes - rango fecha');
  }

  public genearteArchivoResumenCobroExtraMesXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaDeudaResumenCobroExtraMesRangoPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista resumen ingresos cobros extra por mes - rango fecha');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getListaCategoriasConfiguracionCostoAguaPeriodoService(){
    this.mesAnioCategoriaCostoAguaService.getCategorias().subscribe(
      (resp)=>{
        this.listaCategorias = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // reporte lectura mes
  public getReporteLecturaPorMesTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteIngresoLecturaMedidorMesService.getReporteLecturaPorMesTodosRangoPeriodo(rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteLecturaMes = resp;
          if(this.reporteLecturaMes.resumenLecturaPeriodoList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorMesCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteIngresoLecturaMedidorMesService.getReporteLecturaPorMesCategoriaRangoPeriodo(idCategoria,rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteLecturaMes = resp;
          if(this.reporteLecturaMes.resumenLecturaPeriodoList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // cobro extra mes
  public getReporteCobroExtraTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteCobroExtraMesService.getReporteCobroExtraTodosRangoPeriodo(rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteCobroExtraMes = resp;
          if(this.reporteCobroExtraMes.mesCobroExtraList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
            this.tratarCobroExtra();
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();  
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteCobroExtraCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteCobroExtraMesService.getReporteCobroExtraCategoriaRangoPeriodo(idCategoria,rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteCobroExtraMes = resp;
          if(this.reporteCobroExtraMes.mesCobroExtraList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
            this.tratarCobroExtra();
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();  
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // resumen cobro extra
  public getResumenCobroExtraTodosRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteResumenCobroExtraMesService.getResumenCobroExtraTodosRangoPeriodo(rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteResumenCobroExtraMes = resp;
          if(this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getResumenCobroExtraCategoriaRangoPeriodo(idCategoria:number,rangoPeriodoDTO:RangoPeriodoDTO){
    return this.reporteResumenCobroExtraMesService.getResumenCobroExtraCategoriaRangoPeriodo(idCategoria,rangoPeriodoDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteResumenCobroExtraMes = resp;
          if(this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();  
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}
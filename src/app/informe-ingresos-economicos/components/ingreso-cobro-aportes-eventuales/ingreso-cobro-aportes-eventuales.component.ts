import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { AporteEventualVO } from 'src/app/aportes-eventuales/models/aporte-eventual-vo';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { SocioAporteEventualService } from 'src/app/aportes-eventuales/services/socio-aporte-eventual.service';
import { ReporteAporteEventualVO } from 'src/app/aportes-eventuales/models/reporte-aporte-eventual-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { ReporteAporteEventualVOTratado } from 'src/app/aportes-eventuales/models/reporte-aporte-eventual-vo-tratado';
import { SocioAporteEventualDataVO } from 'src/app/aportes-eventuales/models/socio-aporte-eventual-data-vo';

@Component({
  selector: 'app-ingreso-cobro-aportes-eventuales',
  templateUrl: './ingreso-cobro-aportes-eventuales.component.html',
  styleUrls: ['./ingreso-cobro-aportes-eventuales.component.css']
})
export class IngresoCobroAportesEventualesComponent implements OnInit {
  aux:any = null;
  
  // tabla resultados aporte eventual
  tituloItems : string[] = [];
  campoItems  : string[] = [];
  idItem           : string;

  reporteAporteEventualVO:ReporteAporteEventualVO = this.aux;
  reporteAporteEventualListTratado:ReporteAporteEventualVOTratado[];
  
  constructor(private generalService                : GeneralService,
              private socioAporteEventualService    : SocioAporteEventualService,
              private router                        : Router) { }

  ngOnInit(): void {
    this.construirTituloItemsReporteTabla();
  }

  private construirTituloItemsReporteTabla(){ 
    this.tituloItems = ['#','Nombre aporte eventual','Fecha registro','Monto aporte','Código socio','Nombre','Ape. Paterno','Ape. Materno','Cód. Medidor','Dirección','Fecha cobro','Nro. de recibo','Total'];
    this.campoItems   = ['codigoSocio','nombreSocio','apellidoPaterno','apellidoMaterno','codigoMedidor','direccion','fechaCancelado','numeroRecibo'];
    this.idItem       = '';
  }

  
  public getReporte(rangoFecha:RangoFechaDTO){
    if(rangoFecha.rangoValido)
      this.getReporteIngresoRangoFecha(rangoFecha);
  }


  public tratarReporte(){
    let aporteEventualList:AporteEventualVO[] = this.reporteAporteEventualVO.aporteEventualList;    

    this.reporteAporteEventualListTratado = [];
    for (let i = 0; i < aporteEventualList.length; i++) {
      let aporteEventualVO:AporteEventualVO = aporteEventualList[i];
      let socioAporteEventualDataList:SocioAporteEventualDataVO[] = aporteEventualVO.socioAporteEventualDataList;
      for (let j = 0; j < socioAporteEventualDataList.length; j++) {
        let socioAporteEventualDataVO:SocioAporteEventualDataVO = socioAporteEventualDataList[j];
        
        let reporteAporteEventualVOTratado:ReporteAporteEventualVOTratado = {
          indexGlobal : i+1,
          indexItem   : j,
        
          nombre           : aporteEventualVO.nombre,
          fechaRegistro    : aporteEventualVO.fechaRegistro,
          montoAporte      : aporteEventualVO.montoAporte,
          countRows        : aporteEventualVO.countRows,
          sumaTotal        : aporteEventualVO.sumaTotal,
        
          // socios aportes eventuales data
          idSocioMedidor    : socioAporteEventualDataVO.idSocioMedidor,
          codigoSocio       : socioAporteEventualDataVO.codigoSocio,
          nombreSocio       : socioAporteEventualDataVO.nombreSocio,
          apellidoPaterno   : socioAporteEventualDataVO.apellidoPaterno,
          apellidoMaterno   : socioAporteEventualDataVO.apellidoMaterno,
          codigoMedidor     : socioAporteEventualDataVO.codigoMedidor,
          direccion         : socioAporteEventualDataVO.direccion,
          fechaCancelado    : socioAporteEventualDataVO.fechaCancelado,
          numeroRecibo      : socioAporteEventualDataVO.numeroRecibo,
        
          // aux
          seleccionado   : false,
        }
        this.reporteAporteEventualListTratado.push(reporteAporteEventualVOTratado);
      }
    }
  }

  private limpiarResultados(){
    this.reporteAporteEventualListTratado = [];
    this.reporteAporteEventualVO = this.aux;
  }

  public seleccionarItem(aporteEventual:ReporteAporteEventualVOTratado){
    this.reporteAporteEventualListTratado.forEach((item:ReporteAporteEventualVOTratado )=> {
      if(item.idSocioMedidor == aporteEventual.idSocioMedidor)
        item.seleccionado = true;
      else
        item.seleccionado = false;
    });
  }

  public genearteArchivoXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tableResultadosAportesEventuales');
    this.generalService.genearteArchivoXLSX(tableElemnt,"lista de aportes eventuale y socios que si cancelarón");
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  public getReporteIngresoRangoFecha(rangoFechaDTO:RangoFechaDTO){
    this.socioAporteEventualService.getReporteIngresoRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{
        this.reporteAporteEventualVO = resp;

        if(this.reporteAporteEventualVO.aporteEventualList.length>0){          
          this.tratarReporte();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultados();
        }
      },
      (err)=>{
        this.limpiarResultados();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }    
      }
    );
  }
}

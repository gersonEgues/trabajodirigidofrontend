import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CobroExtraPeriodoData } from 'src/app/cuenta-socio/models/cobroExtraPeriodoData';
import { HistorialLecturaPeriodoData } from 'src/app/informe-ingresos-economicos/models/historial-lectura-periodo-data';
import { LecturaPeriodoData } from 'src/app/cuenta-socio/models/lecturaPeriodoData';
import { SocioData } from 'src/app/cuenta-socio/models/socioData';
import { DatosLecturaPeriodoService } from 'src/app/cuenta-socio/services/datos-lectura-periodo.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { HistorialLecturaPeriodoDataTratado } from '../../models/historial-lectura-periodo-data-tratado';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
@Component({
  selector: 'app-ingreso-cobro-lectura-periodo',
  templateUrl: './ingreso-cobro-lectura-periodo.component.html',
  styleUrls: ['./ingreso-cobro-lectura-periodo.component.css']
})
export class IngresoCobroLecturaPeriodoComponent implements OnInit {
  aux                   : any = null;

  // tabla resultados lectura
  listaTituloItemsResultados : string[] = [];
  listaCampoSocios           : string[] = [];
  listaCampoLecturaPeriodo   : string[] = [];

  
  banderaLecturaCorrecta                  : boolean = false;
  historialLecturaPeriodoData             : HistorialLecturaPeriodoData;
  listaHistorialLecturaPeriodoDataTratado : HistorialLecturaPeriodoDataTratado[];

  constructor(private fb                         : FormBuilder,
              private generalService             : GeneralService,
              private datosLecturaPeriodoService : DatosLecturaPeriodoService,
              private router                     : Router) { }

  ngOnInit(): void {
    this.construirTituloItemsReporteTabla();
  }

  private construirTituloItemsReporteTabla(){
    // tabla resultados lectura
    this.listaTituloItemsResultados = ['#','Cod. Socio','Nombre Socio','Cod. Medidor','Dirección','Gestión','Nro. Mes','Mes','Fecha cobro','Lec. Ant.','Lect. Act.','Cons. M3','Cons. Min.','Costo Bs.','Cobro extra','Costo Bs.','Suma cobro extra','Total Bs.'];
    this.listaCampoSocios           = ['indexSocio','codigoSocio','nombreCompleto','codigoMedidor','direccion'];
    this.listaCampoLecturaPeriodo   = ['gestion','nroMes','mes','fechaCancelado','lecturaAnterior','lecturaActual','consumoM3','consumoMinimo','costoLecturaPeriodo'];

  }

  public getReporte(rangoFecha:RangoFechaDTO){
    if(rangoFecha.rangoValido){
      rangoFecha.cancelado = true;
      this.getHistorialLecturaPeriodoCobradosEnRangoFecha(rangoFecha);
    }else{
      this.limpiarResultados();
    }
  }

  public limpiarResultados(){
    this.banderaLecturaCorrecta = false;
    this.historialLecturaPeriodoData = this.aux;
    this.listaHistorialLecturaPeriodoDataTratado = [];
  }

  private tratarListaIngresosCobroExtra(){
    let listaUsuarios:SocioData[] = this.historialLecturaPeriodoData.socioDataList;
    let indexGlobalSocio:number = 0;
    let indexGlobalLecturaPeriodo:number = 0;
    
    this.listaHistorialLecturaPeriodoDataTratado = [];
    for (let indexSocio = 0; indexSocio < listaUsuarios.length; indexSocio++) {
      indexGlobalSocio = 0;
      let socioData:SocioData = listaUsuarios[indexSocio];
      let listaLecturaPeriodo:LecturaPeriodoData[] = socioData.lecturaPeriodoDataList;       
      
      for (let indexLecturaPeriodo = 0; indexLecturaPeriodo < listaLecturaPeriodo.length; indexLecturaPeriodo++) {        
        indexGlobalLecturaPeriodo = 0;
        let lecturaPeriodo:LecturaPeriodoData = listaLecturaPeriodo[indexLecturaPeriodo];
        let listaCobroExtraPeriodo:CobroExtraPeriodoData[] = lecturaPeriodo.cobroExtraPeriodoDataList;

        if(listaCobroExtraPeriodo.length>0){
          for (let indexCobroExtra = 0; indexCobroExtra < listaCobroExtraPeriodo.length; indexCobroExtra++) {
            let cobroExtraPeriodo:CobroExtraPeriodoData = listaCobroExtraPeriodo[indexCobroExtra];  

            let historialLecturaPeriodoDataTratado:HistorialLecturaPeriodoDataTratado = {
              idSocioMedidor : socioData.idSocioMedidor,
              seleccionado   : false,

              // socio
              indexGlobalSocio              : indexGlobalSocio,
              indexSocio                    : indexSocio+1,
              nombreCompleto                : socioData.nombreSocio + ' ' + socioData.apellidoPaterno + ' ' + socioData.apellidoMaterno,
              codigoSocio                   : socioData.codigoSocio,
              codigoMedidor                 : socioData.codigoMedidor,
              direccion                     : socioData.direccion,
              nroRowsLecturaPeriodoDataList : socioData.nroRowsLecturaPeriodoDataList,
            
              // lectura periodo
              indexGlobalLecturaPeriodo   : indexGlobalLecturaPeriodo,
              indexLecturaPeriodo         : indexLecturaPeriodo,
              gestion                     : lecturaPeriodo.anio,
              nroMes                      : lecturaPeriodo.nroMes,
              mes                         : lecturaPeriodo.nombreMes,
              fechaCancelado              : lecturaPeriodo.fechaCancelado,
              lecturaAnterior             : lecturaPeriodo.lecturaAnteriorPeriodo,
              lecturaActual               : lecturaPeriodo.lecturaActualPeriodo,
              consumoM3                   : lecturaPeriodo.consumoVolumenM3,
              consumoMinimo               : lecturaPeriodo.consumoMinimo,
              costoLecturaPeriodo         : lecturaPeriodo.costoPeriodo,
              costoTotalLecturaPeriodo    : lecturaPeriodo.sumaCostoPeriodo,
              nroRowsCobroPeriodoDataList : lecturaPeriodo.nroRowsCobroPeriodoDataList,
            
              // cobro extra socio
              indexCobroExtraSocio : indexCobroExtra,
              nombreCobroExtra     : cobroExtraPeriodo.nombreCobroExtra,
              sumaCostoCobroExtra  : lecturaPeriodo.sumaCostoCobroExtra,
              costoCobroExtra      : cobroExtraPeriodo.costoCobroExtra,
              countSocios          : cobroExtraPeriodo.countSocios,
            }
            this.listaHistorialLecturaPeriodoDataTratado.push(historialLecturaPeriodoDataTratado);
            indexGlobalLecturaPeriodo += 1;     
            indexGlobalSocio += 1;  
          }      
        }else{
          let historialLecturaPeriodoDataTratado:HistorialLecturaPeriodoDataTratado = {
            idSocioMedidor : socioData.idSocioMedidor,
            seleccionado   : false,

            // socio
            indexGlobalSocio              : indexGlobalSocio,
            indexSocio                    : indexSocio+1,
            nombreCompleto                : socioData.nombreSocio + ' ' + socioData.apellidoPaterno + ' ' + socioData.apellidoMaterno,
            codigoSocio                   : socioData.codigoSocio,
            codigoMedidor                 : socioData.codigoMedidor,
            direccion                     : socioData.direccion,
            nroRowsLecturaPeriodoDataList : socioData.nroRowsLecturaPeriodoDataList,
            
            // lectura periodo
            indexGlobalLecturaPeriodo   : indexGlobalLecturaPeriodo,
            indexLecturaPeriodo         : indexLecturaPeriodo,
            gestion                     : lecturaPeriodo.anio,
            nroMes                      : lecturaPeriodo.nroMes,
            mes                         : lecturaPeriodo.nombreMes,
            fechaCancelado              : lecturaPeriodo.fechaCancelado,
            lecturaAnterior             : lecturaPeriodo.lecturaAnteriorPeriodo,
            lecturaActual               : lecturaPeriodo.lecturaActualPeriodo,
            consumoM3                   : lecturaPeriodo.consumoVolumenM3,
            consumoMinimo               : lecturaPeriodo.consumoMinimo,
            costoLecturaPeriodo         : lecturaPeriodo.costoPeriodo,
            costoTotalLecturaPeriodo    : lecturaPeriodo.sumaCostoPeriodo,
            nroRowsCobroPeriodoDataList : lecturaPeriodo.nroRowsCobroPeriodoDataList, // 1

            // cobro extra socio
            indexCobroExtraSocio : 0,
            nombreCobroExtra     : '',
            sumaCostoCobroExtra  : lecturaPeriodo.sumaCostoCobroExtra,
            costoCobroExtra      : 0,
            countSocios          : 0,
          }
          this.listaHistorialLecturaPeriodoDataTratado.push(historialLecturaPeriodoDataTratado);
          indexGlobalLecturaPeriodo += 1;     
          indexGlobalSocio += 1;
        }   
        
      }      
    }
  }
  
  public getValue(value:any){
    return (typeof value == 'boolean')? (value==true)?'SI':'NO':value;
  }

  public isTypeOfBoolean(value:any){
    return typeof value == 'boolean';
  }

  public seleccionarItemCobroExtra(cobroExtra:CobroExtraPeriodoData){
    this.historialLecturaPeriodoData.cobroExtraPeriodoDataList.forEach((item:CobroExtraPeriodoData )=> {
      if(cobroExtra.idCobroExtra == item.idCobroExtra)
        item.seleccionado = true;
      else
        item.seleccionado = false;
    });
  }

  public seleccionarItem(socioData:HistorialLecturaPeriodoDataTratado){
    this.listaHistorialLecturaPeriodoDataTratado.forEach((item:HistorialLecturaPeriodoDataTratado )=> {
      if(socioData.idSocioMedidor == item.idSocioMedidor)
        item.seleccionado = true;
      else
        item.seleccionado = false;
    });
  }

  public genearteArchivoCobroExtraXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tableCobroExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'cobros extra por cobro de consumo de agua en rango fecha');
  }

  public genearteArchivoResumenCobroExtraXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tableResultadosGenerales')
    this.generalService.genearteArchivoXLSX(tableElemnt,'Resumen de cobros por consumo de agua en rango fecha');
  }
  
  public genearteArchivoXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tableResultadosIngresoLecturaPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'historial de ingresos por cobro de consumo de agua en rango fecha');
  }

  

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  public getHistorialLecturaPeriodoCobradosEnRangoFecha(rangoFechaDTO:RangoFechaDTO){
    this.datosLecturaPeriodoService.getHistorialLecturaPeriodoCobradosEnRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{        
        this.historialLecturaPeriodoData = resp;            
        if(this.historialLecturaPeriodoData.socioDataList.length>0){
          this.banderaLecturaCorrecta = true;
          this.tratarListaIngresosCobroExtra();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.banderaLecturaCorrecta = false;
          this.limpiarResultados();
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        this.banderaLecturaCorrecta = false;
        this.historialLecturaPeriodoData = this.aux;
        this.listaHistorialLecturaPeriodoDataTratado = [];
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { CategoriaVO } from 'src/app/configuraciones/models/categoriaVO';
import { RangoFechaComponent } from 'src/app/shared/components/rango-fecha/rango-fecha.component';
import { ReporteCobroExtraMesService } from '../../services/reporte-cobro-extra-mes.service';
import { ReporteResumenCobroExtraMesService } from '../../services/reporte-resumen-cobro-extra-mes.service';
import { ReporteIngresoLecturaMedidorMesService } from '../../services/reporte-ingreso-lectura-medidor-mes.service';
import { ReporteLecturaPeridoMesVO } from '../../models/reporte-lectura-periodo-mes-vo';
import { ReporteMesCobroExtraVO } from '../../models/reporte-mes-cobro-extra-vo';
import { ReporteResumenCobroExtraVO } from '../../models/reporte-resumen-cobro-extra-vo';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';
import { MesResumenLecturaPeriodoVO } from '../../models/mes-resumen-lectura-periodovo';
import { MesCobroExtraVOTratado } from '../../models/mes-cobro-extra-vo-tratado';
import { MesCobroExtraVO } from '../../models/mes-cobro-extra-vo';
import { CobroExtraVO } from '../../models/cobro-extra-vo';

@Component({
  selector: 'app-resumen-ingreso-mes-lectura-periodo-rango-fecha',
  templateUrl: './resumen-ingreso-mes-lectura-periodo-rango-fecha.component.html',
  styleUrls: ['./resumen-ingreso-mes-lectura-periodo-rango-fecha.component.css']
})
export class ResumenIngresoMesLecturaPeriodoRangoFechaComponent implements OnInit {
  aux:any = null;
  formularioReporte! : FormGroup;
  listaCategorias : CategoriaVO[] = [];

  // tabla
  tituloItemsLecturaMes         : string[]    = [];
  campoItemsLecturaMes          : string[]    = [];
  idItemLecturaMes              : string      = 'id';

  tituloItemsCobroExtraMes         : string[]    = [];
  campoItemsCobroExtraMes          : string[]    = [];
  idItemCobroExtraMes              : string      = 'id';

  tituloItemsResumenCobroExtraMes         : string[]    = [];
  campoItemsResumenCobroExtraMes          : string[]    = [];
  idItemResumenCobroExtraMes              : string      = 'id';
  
  reporteLecturaMes             : ReporteLecturaPeridoMesVO = this.aux;
  reporteCobroExtraMes          : ReporteMesCobroExtraVO = this.aux;
  mesCobroExtraVOTratadoList    : MesCobroExtraVOTratado[] = this.aux;
  reporteResumenCobroExtraMes   : ReporteResumenCobroExtraVO = this.aux;
  
  
  @ViewChild('rangoFechaComponent') rangoFechaComponent!       : RangoFechaComponent;
  rangoFechaDTO!        : RangoFechaDTO;

  
  constructor(private fb                                     : FormBuilder,
              private generalService                         : GeneralService,
              private mesAnioCategoriaCostoAguaService       : MesAnioCategoriaCostoAguaService,
              private reporteCobroExtraMesService            : ReporteCobroExtraMesService,
              private reporteResumenCobroExtraMesService     : ReporteResumenCobroExtraMesService,
              private reporteIngresoLecturaMedidorMesService : ReporteIngresoLecturaMedidorMesService,
              private router                                 : Router) { }

  ngOnInit(): void {
    this.getListaCategoriasConfiguracionCostoAguaPeriodoService();
    this.construirFormularioReporte();
    this.configurarTituloTablas();
  }

  private construirFormularioReporte(){
    this.formularioReporte = this.fb.group({
      categoria : ['-1',[Validators.required],[]]
    });
  }

  private configurarTituloTablas(){
    this.tituloItemsLecturaMes = ['#','Mes','Lectura anterior','Lectura actual','Consumo M3','Costo'];
    this.campoItemsLecturaMes  = ['nombreMes','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','costoTotal'];
    this.idItemLecturaMes      = 'nroMes';

    this.tituloItemsCobroExtraMes = ['#','Mes','Cod.','Nombre cobro extra','Costo','Total'];
    this.campoItemsCobroExtraMes  = ['nombreCobroExtra','detalle','costoCobroExtra'];
    this.idItemCobroExtraMes      = 'idmes';

    this.tituloItemsResumenCobroExtraMes = ['#','Código','Nombre cobro extra','Costo'];
    this.campoItemsResumenCobroExtraMes  = ['nombreCobroExtra','detalle','costoCobroExtra'];
    this.idItemResumenCobroExtraMes      = 'idCobroExtra';
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioReporte.get(campo).valid &&
             this.formularioReporte.get(campo).value != '-1';
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioReporte.get(campo).invalid && 
            this.formularioReporte.get(campo).touched;
  }

  public getReporteByCategoria(){
    if(this.rangoFechaDTO==this.aux)
      this.rangoFechaDTO = this.rangoFechaComponent?.getRangoFecha();

    this.getReporte(this.rangoFechaDTO);
  }

  public getReporte(rangoFechaDTO:RangoFechaDTO){
    this.rangoFechaDTO = rangoFechaDTO;
    this.limpiarResultadosReporte();
    if(!rangoFechaDTO.rangoValido){
      this.generalService.mensajeFormularioInvalido('Revise el rango de fecha, la fecha inicio tiene que ser menor o igual a la fecha fin');
      return;
    }

    let idCategoria:number = Number(this.formularioReporte.get('categoria').value);
    rangoFechaDTO.cancelado = true;
    if(idCategoria==0){
      this.getReporteLecturaPorMesTodosRangoFecha(this.rangoFechaDTO);
      this.getReporteCobroExtraTodosRangoFecha(this.rangoFechaDTO);
      this.getResumenCobroExtraTodosRangoFecha(this.rangoFechaDTO);
    }else{
      this.getReporteLecturaPorMesCategoriaRangoFecha(idCategoria,this.rangoFechaDTO);
      this.getReporteCobroExtraCategoriaRangoFecha(idCategoria,this.rangoFechaDTO);
      this.getResumenCobroExtraCategoriaRangoFecha(idCategoria,this.rangoFechaDTO)
    }
  }

  public seleccionarLecturaMes(lecturaMes:MesResumenLecturaPeriodoVO){
    this.reporteLecturaMes.resumenLecturaPeriodoList.forEach(item => {
      if(item.nroMes == lecturaMes.nroMes){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public seleccionarCobroExtraMes(lecturaMes:MesCobroExtraVOTratado){
    this.mesCobroExtraVOTratadoList.forEach(item => {
      if(item.nroMes == lecturaMes.nroMes){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public seleccionarResumenCobroExtraMes(cobroExtra:CobroExtraVO){
    this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.forEach(item => {
      if(item.idCobroExtra == cobroExtra.idCobroExtra){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  private limpiarResultadosReporte(){
    this.reporteLecturaMes = this.aux;
    this.reporteCobroExtraMes = this.aux;
    this.mesCobroExtraVOTratadoList = [];
    this.reporteResumenCobroExtraMes = this.aux;
  }

  private tratarCobroExtra(){
    this.mesCobroExtraVOTratadoList = [];

    let mesCobroExtraList:MesCobroExtraVO[] = this.reporteCobroExtraMes.mesCobroExtraList;
    for (let i = 0; i < mesCobroExtraList.length; i++) {
      let mesCobroExtra : MesCobroExtraVO = mesCobroExtraList[i];
      let cobroExtraList:CobroExtraVO[] = mesCobroExtra.cobroExtraPeriodoDataList;
      for (let j = 0; j < cobroExtraList.length; j++) {
        let cobroextra:CobroExtraVO = cobroExtraList[j];
        let mesCobroExtraVOTratado:MesCobroExtraVOTratado = {
          indexGlobalMes  : j,
          indexMes        : i+1,

          // mes
          idmes            : mesCobroExtra.idmes,
          nroMes           : mesCobroExtra.nroMes,
          nombreMes        : mesCobroExtra.nombreMes,
          nroRows          : mesCobroExtra.nroRows,
          sumaTotal        : mesCobroExtra.sumaTotal,

          // cobro extra
          idCobroExtra     : cobroextra.idCobroExtra,
          nombreCobroExtra : cobroextra.nombreCobroExtra,
          detalle          : cobroextra.detalle,
          costoCobroExtra  : cobroextra.costoCobroExtra, 

          seleccionado : false
        }
        this.mesCobroExtraVOTratadoList.push(mesCobroExtraVOTratado);     
      }    
    }
  }

  public genearteArchivoLecturaPeriodoXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaIngresoLecturaMesRangoFecha')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista ingreso lectura mes - rango fecha');
  }

  public genearteArchivoCobroExtraMesXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaIngresoCobroExtraMesRangoFecha')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista ingreso cobros extra por mes - rango fecha');
  }

  public genearteArchivoResumenCobroExtraMesXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tablaIngresoResumenCobroExtraMesRangoFecha')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista resumen ingresos cobros extra por mes - rango fecha');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getListaCategoriasConfiguracionCostoAguaPeriodoService(){
    this.mesAnioCategoriaCostoAguaService.getCategorias().subscribe(
      (resp)=>{
        this.listaCategorias = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // lectura periodo por mes
  public getReporteLecturaPorMesTodosRangoFecha(rangoFechaDTO:RangoFechaDTO){
    return this.reporteIngresoLecturaMedidorMesService.getReporteLecturaPorMesTodosRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{  
        if(resp){
          this.reporteLecturaMes = resp;
          if(this.reporteLecturaMes.resumenLecturaPeriodoList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteLecturaPorMesCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO){
    return this.reporteIngresoLecturaMedidorMesService.getReporteLecturaPorMesCategoriaRangoFecha(idCategoria,rangoFechaDTO).subscribe(
      (resp)=>{                                    
        if(resp){
          this.reporteLecturaMes = resp;
          if(this.reporteLecturaMes.resumenLecturaPeriodoList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // cobro extra mes
  public getReporteCobroExtraTodosRangoFecha(rangoFechaDTO:RangoFechaDTO){
    return this.reporteCobroExtraMesService.getReporteCobroExtraTodosRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteCobroExtraMes = resp;
          if(this.reporteCobroExtraMes.mesCobroExtraList.length>0){
            this.tratarCobroExtra();
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();  
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteCobroExtraCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO){
    return this.reporteCobroExtraMesService.getReporteCobroExtraCategoriaRangoFecha(idCategoria,rangoFechaDTO).subscribe(
      (resp)=>{                            
        if(resp){          
          this.reporteCobroExtraMes = resp;
          if(this.reporteCobroExtraMes.mesCobroExtraList.length>0){
            this.tratarCobroExtra();
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();  
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }


  // resumen cobro extra mes
  public getResumenCobroExtraTodosRangoFecha(rangoFechaDTO:RangoFechaDTO){
    return this.reporteResumenCobroExtraMesService.getResumenCobroExtraTodosRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{                                    
        if(resp){
          this.reporteResumenCobroExtraMes = resp;
          if(this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{           
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{                   
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getResumenCobroExtraCategoriaRangoFecha(idCategoria:number,rangoFechaDTO:RangoFechaDTO){
    return this.reporteResumenCobroExtraMesService.getResumenCobroExtraCategoriaRangoFecha(idCategoria,rangoFechaDTO).subscribe(
      (resp)=>{                            
        if(resp){
          this.reporteResumenCobroExtraMes = resp;
          if(this.reporteResumenCobroExtraMes.cobroExtraPeriodoDataList.length>0){
            this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          }else{
            this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
            this.limpiarResultadosReporte();
          }
        }else{          
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.limpiarResultadosReporte();
        }
      },
      (err)=>{        
        this.limpiarResultadosReporte();  
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { EventoService } from 'src/app/eventos/services/evento.service';
import { EventoVO } from 'src/app/eventos/models/eventoVO';
import { SocioSeguimientoVO } from 'src/app/eventos/models/socioSeguimientoVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { HistorialCobroReunionesDataTratado } from '../models/historial-cobro-reuniones-data-tratado';
import { HistorialCobroReunionesData } from '../models/historial-cobro-reuniones-data';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Component({
  selector: 'app-deudas-por-falta-o-retrazo-a-reunion',
  templateUrl: './deudas-por-falta-o-retrazo-a-reunion.component.html',
  styleUrls: ['./deudas-por-falta-o-retrazo-a-reunion.component.css']
})
export class DeudasPorFaltaORetrazoAReunionComponent implements OnInit {
  aux:any = null;

  // tabla resumen general
  listaTituloItems           : string[] = [];
  listaCampoEvento           : string[] = [];
  listaCampoSocioSeguimiento : string[] = [];
 
  banderaLecturaCorrecta : boolean = false;
  listaHistorialDeudasReunionesDataTratado : HistorialCobroReunionesDataTratado[];
  historialDeudaReunionesData              : HistorialCobroReunionesData;
  
  constructor(private fb             : FormBuilder,
              private generalService : GeneralService,
              private eventoService  : EventoService,
              private router         : Router) { }

  ngOnInit(): void {
    this.construirTituloItemsReporteTabla();
  }

  private construirTituloItemsReporteTabla(){
    // tabla resultados lectura
    this.listaTituloItems = ['#','Reunión','Fecha realización','Estado reunión','Multa retrazo','Multa retrazo final','Multa falta','Hora reunión','Cod. Socio','Nro. Recibo','Nombre socio','Cod. Medidor','Estado Seguimiento','Estado multa','Multa adeudada'];
    this.listaCampoEvento = ['indexEvento','nombre','fechaRealizacion','estadoEvento','multaRetraso','multaRetrasoFinal','multaFalta','horaReunion'];
    this.listaCampoSocioSeguimiento = ['codigoSocio','nroRecibo','nombreCompleto','codigoMedidor','estadoSeguimientoEvento','estadoMulta','multaCancelada'];
  }

  public getReporte(rangoFecha:RangoFechaDTO){
    if(rangoFecha.rangoValido){
      rangoFecha.cancelado = false;
      this.getEventosRealizadosEnRangoFechaConSociosQueCancelaronMultaService(rangoFecha);
    }else{
      this.limpiarResultados();
    }
  }

  private tratarHistorialCobroReunionesData(){
    this.listaHistorialDeudasReunionesDataTratado = [];
    let eventoList : EventoVO[] = this.historialDeudaReunionesData.eventoList;
    let indexGlobalEvento = 0

    for (let indexEvento = 0; indexEvento < eventoList.length; indexEvento++) {
      indexGlobalEvento = 0;    
      let evento:EventoVO = eventoList[indexEvento];
      let listaSocioSeguimiento : SocioSeguimientoVO[] = evento.listaSocioSeguimiento;
      
      for (let indexSocio = 0; indexSocio < listaSocioSeguimiento.length; indexSocio++) {
        let socioSeguimientoVO:SocioSeguimientoVO = listaSocioSeguimiento[indexSocio];
        
        let historialCobroReunionesDataTratado:HistorialCobroReunionesDataTratado = {
          seleccionado : false,

          // evento
          indexGlobalEvento            : indexGlobalEvento,
          indexEvento                  : indexEvento+1,
          idEvento                     : evento.idEvento,
          nombre                       : evento.nombre,
          ubicacion                    : evento.ubicacion,
          fechaRealizacion             : evento.fechaRealizacion,
          estadoEvento                 : evento.estadoEvento,
          multaFalta                   : evento.multaFalta,
          multaRetraso                 : evento.multaRetraso,
          multaRetrasoFinal            : evento.multaRetrasoFinal,
          horaReunion                  : evento.horaReunion,
          horaRetraso                  : evento.horaRetraso,
          nroRowsListaSocioSeguimiento : evento.nroRowsListaSocioSeguimiento,
          
          // seguimiento evento - multa  
          indexSocio              : indexSocio,
          codigoSocio             : socioSeguimientoVO.codigoSocio,
          codigoMedidor           : socioSeguimientoVO.codigoMedidor,
          nroRecibo               : socioSeguimientoVO.nroRecibo,
          nombreCompleto          : socioSeguimientoVO.nombreCompleto,
          estadoSeguimientoEvento : socioSeguimientoVO.estadoSeguimientoEvento,
          horaLlegada             : socioSeguimientoVO.horaLlegada,
          estadoMulta             : socioSeguimientoVO.estadoMulta,
          multaCancelada          : socioSeguimientoVO.multaCancelada,
          fechaRegistro           : socioSeguimientoVO.fechaRegistro,
          cumplioConRetraso       : socioSeguimientoVO.cumplioConRetraso,
          falta                   : socioSeguimientoVO.falta,
        }

        this.listaHistorialDeudasReunionesDataTratado.push(historialCobroReunionesDataTratado);
        indexGlobalEvento += 1;
      }
    }    
  }

  private limpiarResultados(){
    this.banderaLecturaCorrecta = false;
    this.historialDeudaReunionesData = this.aux;
    this.listaHistorialDeudasReunionesDataTratado = [];
  }

  public getValue(value:any){
    return (typeof value == 'boolean')? (value==true)?'SI':'NO':value;
  }

  public isTypeOfBoolean(value:any){
    return typeof value == 'boolean';
  }

  public seleccionarItem(eventoSocio:HistorialCobroReunionesDataTratado){
    this.listaHistorialDeudasReunionesDataTratado.forEach((item:HistorialCobroReunionesDataTratado )=> {
      if(eventoSocio.idEvento == item.idEvento)
        item.seleccionado = true;
      else
        item.seleccionado = false;
    });
  }

  public genearteArchivoXLSX(){
    let tableElemnt:HTMLElement = document.getElementById('tableResultadosDeudaDeSociosAReuniones')
    this.generalService.genearteArchivoXLSX(tableElemnt,'historial de socios con deuda por retrazo o falta a reuniones');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  public getEventosRealizadosEnRangoFechaConSociosQueCancelaronMultaService(rangoFechaDTO:RangoFechaDTO){
    return this.eventoService.getListaDeudaSeguimientoEventoDeEventoEnRangoFecha(rangoFechaDTO).subscribe(
      (resp)=>{
        this.historialDeudaReunionesData = resp;
        if(this.historialDeudaReunionesData != this.aux && this.historialDeudaReunionesData.eventoList.length>0){
          this.banderaLecturaCorrecta = true;
          this.tratarHistorialCobroReunionesData();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results); 
          this.limpiarResultados()
        }        
      },
      (err)=>{
        this.limpiarResultados();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

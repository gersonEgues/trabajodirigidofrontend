import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngresoCobroAportesEventualesComponent } from './components/ingreso-cobro-aportes-eventuales/ingreso-cobro-aportes-eventuales.component';
import { IngresoCobroLecturaPeriodoComponent } from './components/ingreso-cobro-lectura-periodo/ingreso-cobro-lectura-periodo.component';
import { IngresoCobroMultaReunionComponent } from './components/ingreso-cobro-multa-reunion/ingreso-cobro-multa-reunion.component';
import { RolGuard } from '../login/guards/rol.guard';
import { AuthGuard } from '../login/guards/auth.guard';
import { ROL } from '../shared/enums-mensajes/roles';
import { IngresoCobroLecturaPeriodoRangoPeriodoComponent } from './ingreso-cobro-lectura-periodo-rango-periodo/ingreso-cobro-lectura-periodo-rango-periodo.component';
import { DeudaCobroLecturaPeriodoRangoPeriodoComponent } from './deuda-cobro-lectura-periodo-rango-periodo/deuda-cobro-lectura-periodo-rango-periodo.component';
import { DeudasPorFaltaORetrazoAReunionComponent } from './deudas-por-falta-o-retrazo-a-reunion/deudas-por-falta-o-retrazo-a-reunion.component';
import { HistorialDeudaAportesEventualesComponent } from './historial-deuda-aportes-eventuales/historial-deuda-aportes-eventuales.component';
import { ResumenIngresoMesLecturaPeriodoRangoFechaComponent } from './components/resumen-ingreso-mes-lectura-periodo-rango-fecha/resumen-ingreso-mes-lectura-periodo-rango-fecha.component';
import { ResumenDeudaMesLecturaPeriodoRangoPeriodoComponent } from './components/resumen-deuda-mes-lectura-periodo-rango-periodo/resumen-deuda-mes-lectura-periodo-rango-periodo.component';
import { ResumenIngresoMesLecturaPeriodoRangoPeriodoComponent } from './components/resumen-ingreso-mes-lectura-periodo-rango-periodo/resumen-ingreso-mes-lectura-periodo-rango-periodo.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'ingreso-cobro-lectura-periodo', 
        component: IngresoCobroLecturaPeriodoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'ingreso-cobro-lectura-periodo-rango-periodo', 
        component: IngresoCobroLecturaPeriodoRangoPeriodoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'deuda-cobro-lectura-periodo-rango-periodo', 
        component: DeudaCobroLecturaPeriodoRangoPeriodoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-ingreso-filtro-mes-lectura-periodo-rango-mes', 
        component: ResumenIngresoMesLecturaPeriodoRangoFechaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-ingreso-filtro-mes-lectura-periodo-rango-periodo', 
        component: ResumenIngresoMesLecturaPeriodoRangoPeriodoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-deuda-filtro-mes-lectura-periodo-rango-periodo', 
        component: ResumenDeudaMesLecturaPeriodoRangoPeriodoComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'ingreso-cobro-multa-reunion', 
        component: IngresoCobroMultaReunionComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'deudas-por-falta-o-retrazo-a-reunion', 
        component: DeudasPorFaltaORetrazoAReunionComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'ingreso-cobro-aportes-eventuales', 
        component: IngresoCobroAportesEventualesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'deuda-de-socios-por-aportes', 
        component: HistorialDeudaAportesEventualesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'ingreso-cobro-lectura-periodo' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformeIngresosEconomicosRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InformeIngresosEconomicosRoutingModule } from './informe-ingresos-economicos-routing.module';
import { IngresoCobroAportesEventualesComponent } from './components/ingreso-cobro-aportes-eventuales/ingreso-cobro-aportes-eventuales.component';
import { IngresoCobroMultaReunionComponent } from './components/ingreso-cobro-multa-reunion/ingreso-cobro-multa-reunion.component';
import { IngresoCobroLecturaPeriodoComponent } from './components/ingreso-cobro-lectura-periodo/ingreso-cobro-lectura-periodo.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IngresoCobroLecturaPeriodoRangoPeriodoComponent } from './ingreso-cobro-lectura-periodo-rango-periodo/ingreso-cobro-lectura-periodo-rango-periodo.component';
import { DeudaCobroLecturaPeriodoRangoPeriodoComponent } from './deuda-cobro-lectura-periodo-rango-periodo/deuda-cobro-lectura-periodo-rango-periodo.component';
import { DeudasPorFaltaORetrazoAReunionComponent } from './deudas-por-falta-o-retrazo-a-reunion/deudas-por-falta-o-retrazo-a-reunion.component';
import { HistorialDeudaAportesEventualesComponent } from './historial-deuda-aportes-eventuales/historial-deuda-aportes-eventuales.component';
import { ResumenIngresoMesLecturaPeriodoRangoFechaComponent } from './components/resumen-ingreso-mes-lectura-periodo-rango-fecha/resumen-ingreso-mes-lectura-periodo-rango-fecha.component';
import { ResumenIngresoMesLecturaPeriodoRangoPeriodoComponent } from './components/resumen-ingreso-mes-lectura-periodo-rango-periodo/resumen-ingreso-mes-lectura-periodo-rango-periodo.component';
import { ResumenDeudaMesLecturaPeriodoRangoPeriodoComponent } from './components/resumen-deuda-mes-lectura-periodo-rango-periodo/resumen-deuda-mes-lectura-periodo-rango-periodo.component';


@NgModule({
  declarations: [
    IngresoCobroLecturaPeriodoComponent,
    IngresoCobroAportesEventualesComponent,
    IngresoCobroMultaReunionComponent,
    IngresoCobroLecturaPeriodoRangoPeriodoComponent,
    DeudaCobroLecturaPeriodoRangoPeriodoComponent,
    DeudasPorFaltaORetrazoAReunionComponent,
    HistorialDeudaAportesEventualesComponent,
    ResumenIngresoMesLecturaPeriodoRangoFechaComponent,
    ResumenIngresoMesLecturaPeriodoRangoPeriodoComponent,
    ResumenDeudaMesLecturaPeriodoRangoPeriodoComponent
  ],
  imports: [
    CommonModule,
    InformeIngresosEconomicosRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class InformeIngresosEconomicosModule { }

export interface MesCobroExtraVOTratado {
  indexGlobalMes          : number,
  indexMes                 : number,

  // mes
  idmes                     : number,
  nroMes                    : number,
  nombreMes                 : string,
  nroRows                   : number,
  sumaTotal                 : number,

  // cobro extra
  idCobroExtra     : number,
  nombreCobroExtra : string,
  detalle          : string,
  costoCobroExtra  : number, 

  // extra
  seleccionado : boolean,
}
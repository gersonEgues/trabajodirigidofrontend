import { MesCobroExtraVO } from "./mes-cobro-extra-vo";

export interface ReporteMesCobroExtraVO {
  nroRows           : number,
  sumaTotal         : number,
  mesCobroExtraList : MesCobroExtraVO[]
}

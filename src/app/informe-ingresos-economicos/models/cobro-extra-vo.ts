export interface CobroExtraVO {
  idCobroExtra     : number,
  nombreCobroExtra : string,
  detalle          : string,
  costoCobroExtra  : number,

  seleccionado?    : boolean,
}
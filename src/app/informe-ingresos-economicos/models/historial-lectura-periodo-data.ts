import { CobroExtraPeriodoData } from "src/app/cuenta-socio/models/cobroExtraPeriodoData";
import { SocioData } from "../../cuenta-socio/models/socioData";

export interface HistorialLecturaPeriodoData {
  sumaIngresoTotal          : number,
  sumaIngresoLecturaAgua    : number,
  sumaIngresoCobroExtra     : number,
  sumaVolumenM3             : number,
  nroSocioDataList          : number,
  cobroExtraPeriodoDataList : CobroExtraPeriodoData[];
  socioDataList             : SocioData[],
}

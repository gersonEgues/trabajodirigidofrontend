export interface ResumenCobroLecturaPeriodoTratado {
  sumaIngresoLecturaAgua : number,
  sumaIngresoCobroExtra  : number,
  sumaIngresoTotal       : number,
}
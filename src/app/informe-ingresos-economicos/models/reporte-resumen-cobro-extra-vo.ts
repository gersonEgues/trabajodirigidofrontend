import { CobroExtraVO } from "./cobro-extra-vo";

export interface ReporteResumenCobroExtraVO {
  nroRows                   : number,
  sumaTotal                 : number,
  cobroExtraPeriodoDataList : CobroExtraVO[],
}

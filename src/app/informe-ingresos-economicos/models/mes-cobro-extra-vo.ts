import { CobroExtraVO } from "./cobro-extra-vo";

export interface MesCobroExtraVO {
  idmes                     : number,
  nroMes                    : number,
  nombreMes                 : string,
  nroRows                   : number,
  sumaTotal                 : number,
  cobroExtraPeriodoDataList : CobroExtraVO[],
}
export interface HistorialLecturaPeriodoDataTratado{
  idSocioMedidor : number,
  seleccionado   : boolean

  // socio
  indexGlobalSocio              : number,
  indexSocio                    :  number,
  nombreCompleto                : string,
  codigoSocio                   : string
  codigoMedidor                 : string,
  direccion                     : string,
  nroRowsLecturaPeriodoDataList : number,

  // lectura periodo
  indexGlobalLecturaPeriodo   : number,
  indexLecturaPeriodo         : number,
  gestion                     : number,
  nroMes                      : number,
  mes                         : string,
  fechaCancelado              : string,
  lecturaAnterior             : number,
  lecturaActual               : number,
  consumoM3                   : number,
  consumoMinimo               : boolean,
  costoLecturaPeriodo         : number,
  costoTotalLecturaPeriodo    : number,
  nroRowsCobroPeriodoDataList : number,

  // cobro extra socio
  indexCobroExtraSocio : number,
  nombreCobroExtra     : string,
  sumaCostoCobroExtra  : number,
  costoCobroExtra      : number,
  countSocios          : number,
}
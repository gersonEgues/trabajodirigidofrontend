import { EventoVO } from "src/app/eventos/models/eventoVO";

export interface HistorialCobroReunionesDataTratado {
  seleccionado : boolean,

  // evento
  indexGlobalEvento            : number,
  indexEvento                  : number,
  idEvento                     : number,
  nombre                       : string,
  ubicacion                    : string,
  fechaRealizacion             : string,
  estadoEvento                 : string,
  multaFalta                   : number,
  multaRetraso                 : number,
  multaRetrasoFinal            : number,
  horaReunion                  : string,
  horaRetraso                  : string,
  nroRowsListaSocioSeguimiento : number,
  
  // seguimiento evento - multa 
  indexSocio              : number,
  codigoSocio             : string,
  codigoMedidor           : string,
  nroRecibo               : number,
  nombreCompleto          : string,
  estadoSeguimientoEvento : string,
  horaLlegada             : string,
  estadoMulta             : string,
  multaCancelada          : number,
  fechaRegistro           : string,
  cumplioConRetraso       : boolean,
  falta                   : boolean,
}

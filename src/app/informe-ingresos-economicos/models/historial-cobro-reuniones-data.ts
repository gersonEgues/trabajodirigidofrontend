import { EventoVO } from "src/app/eventos/models/eventoVO";

export interface HistorialCobroReunionesData {
  sumaIngresoTotal                  : number,
  sumaIngresoRetrazoAReuniones      : number,
  sumaIngresoRetrazoFinalAReuniones : number,
  sumaIngresoFaltaAReuniones        : number,
  eventoList                        : EventoVO[],
}

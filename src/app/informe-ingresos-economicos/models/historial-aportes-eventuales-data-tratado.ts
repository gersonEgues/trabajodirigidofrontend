export interface HistorialAporteEventualDataTratado {
  indexGlobalAporteEventual : number,
  seleccionado : boolean,
  // aporte evntual
  idAporteEventual          : number,
  indexAporteEventual       : number,
  nombre                             : string,
  fecha                              : string,
  montoAporte                        : number,
  nroRowsSocioAporteEventualDataList : number,

  // aporte evetnual socio
  nombreSocio     : string,
  codigoSocio     : string,
  codigoMedidor   : string,
  montoAportado   : number,
  aporteCancelado : boolean,
  numeroRecibo    : number,
  fechaCancelado  : string,  

  deuda? :  number,
}
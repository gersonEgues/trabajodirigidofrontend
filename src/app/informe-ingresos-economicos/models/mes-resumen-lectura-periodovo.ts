export interface MesResumenLecturaPeriodoVO {
  nroMes                 : number,
  nombreMes              : string,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  consumoVolumenM3       : number,
  costoTotal             : number,

  //
  seleccionado? : boolean,
}
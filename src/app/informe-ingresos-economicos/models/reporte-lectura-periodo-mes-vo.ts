import { MesResumenLecturaPeriodoVO } from "./mes-resumen-lectura-periodovo";

export interface ReporteLecturaPeridoMesVO {
  nroRows                   : number,
  sumaTotal                 : number,
  consumoVolumenM3          : number,
  resumenLecturaPeriodoList : MesResumenLecturaPeriodoVO[]
}

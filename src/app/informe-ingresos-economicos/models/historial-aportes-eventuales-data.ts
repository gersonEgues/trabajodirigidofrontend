import { AporteEventualVO } from "src/app/aportes-eventuales/models/aporte-eventual-vo";
import { NombreAporteEvetual } from "src/app/aportes-eventuales/models/nombreAporteEventual";

export interface HistorialAporteEventualData {
  sumaIngresoTotal       : number,
  sumaAporteEventualList : NombreAporteEvetual[],
  aporteEventualList     : AporteEventualVO[],
}
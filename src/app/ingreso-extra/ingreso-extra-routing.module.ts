import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IngresoExtraComponent } from './components/ingreso-extra/ingreso-extra.component';
import { ReporteIngresoExtraComponent } from './components/reporte-ingreso-extra/reporte-ingreso-extra.component';
import { ROL } from '../shared/enums-mensajes/roles';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ReporteIngresoExtraRangoFechaComponent } from './components/reporte-ingreso-extra-rango-fecha/reporte-ingreso-extra-rango-fecha.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'registro-ingresos-extra', 
        component: IngresoExtraComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-ingresos-extra-rango-fecha', 
        component: ReporteIngresoExtraRangoFechaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-ingresos-extra-by-gestion', 
        component: ReporteIngresoExtraComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'reporte-ingresos-extra' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IngresoExtraRoutingModule { }

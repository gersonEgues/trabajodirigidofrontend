export interface IngresoExtraVO {
  id            : number,
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
  costoUnitario : number,
  unidades      : number,
  costoTotal    : number,

  seleccionado? : boolean,
}
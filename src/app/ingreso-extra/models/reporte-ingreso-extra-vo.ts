import { IngresoExtraVO } from "./ingreso-extra-vo";

export interface ReporteIngresoExtraVO {
  sumaCostoTotal   : number,
  countRows        : number,
  ingresoExtraList : IngresoExtraVO[],  
}
export interface IngresoExtraDTO {
  idGestion     : number,
  nombre        : string,
  fechaRegistro : string,
  costoUnitario : number,
  unidades      : number
}
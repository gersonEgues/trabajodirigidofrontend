import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { ReporteIngresoExtraVO } from '../models/reporte-ingreso-extra-vo';
import { map } from 'rxjs/operators';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class ReporteIngresoExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/reporte-ingreso-extra",http);
  }

  public getReporteIngresoExtraRangoFecha(rangoFecha:RangoFechaDTO) : Observable<ReporteIngresoExtraVO> {
    return this.http.put<any>(`${this.url}/rango-fecha`,rangoFecha).pipe(
      map(response => {
        return response as ReporteIngresoExtraVO;
      })
    );
  }

  public getReporteIngresoByGestion(id:number) : Observable<ReporteIngresoExtraVO> {
    return this.http.put<any>(`${this.url}/gestion/${id}`,null).pipe(
      map(response => {
        return response as ReporteIngresoExtraVO;
      })
    );
  }
}

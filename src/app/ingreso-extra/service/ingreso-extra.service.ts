import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { IngresoExtraVO } from '../models/ingreso-extra-vo';
import { IngresoExtraDTO } from '../models/ingreso-extra-dto';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';

@Injectable({
  providedIn: 'root'
})
export class IngresoExtraService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/ingreso-extra",http);
  }

  public getIngresoExtra(id:number) : Observable<IngresoExtraVO> {
    return this.http.get<any>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as IngresoExtraVO;
      })
    );
  }

  public createIngresoExtra(ingresoExtraDTO:IngresoExtraDTO) : Observable<IngresoExtraVO> {
    return this.http.post<any>(`${this.url}`,ingresoExtraDTO).pipe(
      map(response => {
        return response as IngresoExtraVO;
      })
    );
  }

  public updateIngresoExtra(id:number,ingresoExtraDTO:IngresoExtraDTO) : Observable<IngresoExtraVO> {
    return this.http.put<any>(`${this.url}/${id}`,ingresoExtraDTO).pipe(
      map(response => {
        return response as IngresoExtraVO;
      })
    );
  }

  public deleteIngresoExtra(id:number) : Observable<BanderaResponseVO> {
    return this.http.delete<any>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

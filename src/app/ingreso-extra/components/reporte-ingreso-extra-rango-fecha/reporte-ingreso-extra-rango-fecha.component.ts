import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReporteIngresoExtraVO } from '../../models/reporte-ingreso-extra-vo';
import { IngresoExtraVO } from '../../models/ingreso-extra-vo';
import { ReporteIngresoExtraService } from '../../service/reporte-ingreso-extra.service';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Component({
  selector: 'app-reporte-ingreso-extra-rango-fecha',
  templateUrl: './reporte-ingreso-extra-rango-fecha.component.html',
  styleUrls: ['./reporte-ingreso-extra-rango-fecha.component.css']
})
export class ReporteIngresoExtraRangoFechaComponent implements OnInit {
  aux:any = null;
  listaGestiones     : AnioVO[];
  formularioGestion! : FormGroup;

  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';
  
  reporteIngresoExtraVO : ReporteIngresoExtraVO = this.aux;
  listaIngresoExtraVO   : IngresoExtraVO[] = [];
  rangoFechaDTO!        : RangoFechaDTO;
  

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private reporteIngresoExtraService       : ReporteIngresoExtraService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre ingreso extra','Fecha registro','Costo unitario','Unidades','Costo tot.'];
    this.campoItems  = ['fechaRegistro','costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    this.formularioGestion = this.fb.group({
      gestion : ['0',[Validators.required],[]]
    });
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public seleccionarItem(item:IngresoExtraVO){
    this.listaIngresoExtraVO.forEach(element => {
      if(element.id==item.id)
        element.seleccionado = true;
      else
        element.seleccionado = false;
    });
  }

  public getReporteIngresoExatraEnRangoFecha(rangoFechaDTO:RangoFechaDTO){
    if(!rangoFechaDTO.rangoValido){
      this.reporteIngresoExtraVO = this.aux;
      this.listaIngresoExtraVO = [];
      return;
    }

    this.rangoFechaDTO = rangoFechaDTO;
    this.getReporteIngresoExtraRangoFechaService(rangoFechaDTO);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('idTableReporteIngresoExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de reporte de ingresos extra');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getReporteIngresoExtraRangoFechaService(rangoFecha:RangoFechaDTO) {
    return this.reporteIngresoExtraService.getReporteIngresoExtraRangoFecha(rangoFecha).subscribe(
      (resp)=>{
        if(resp!=this.aux){          
          this.reporteIngresoExtraVO = resp;
          this.listaIngresoExtraVO = this.reporteIngresoExtraVO.ingresoExtraList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteIngresoExtraVO = this.aux;
          this.listaIngresoExtraVO = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

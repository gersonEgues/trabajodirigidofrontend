import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ReporteIngresoExtraService } from '../../service/reporte-ingreso-extra.service';
import { IngresoExtraService } from '../../service/ingreso-extra.service';
import { IngresoExtraDTO } from '../../models/ingreso-extra-dto';
import { ReporteIngresoExtraVO } from '../../models/reporte-ingreso-extra-vo';
import { IngresoExtraVO } from '../../models/ingreso-extra-vo';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-ingreso-extra',
  templateUrl: './ingreso-extra.component.html',
  styleUrls: ['./ingreso-extra.component.css']
})
export class IngresoExtraComponent implements OnInit {
  aux:any = null;
  listaGestiones          : AnioVO[];
  formularioIngresoExtra! : FormGroup;
  formularioGestion!      : FormGroup;
  

  // tabla
  tituloItems         : string[]    = [];
  campoItems          : string[]    = [];
  idItem              : string      = 'id';
  
  reporteIngresoExtraVO : ReporteIngresoExtraVO = this.aux;
  listaIngresoExtraVO   : IngresoExtraVO[] = [];
  ingresoExtraUpdate    : IngresoExtraVO   = this.aux;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private ingresoExtraService              : IngresoExtraService,
              private reporteIngresoExtraService       : ReporteIngresoExtraService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.tituloItems = ['#','Nombre ingreso extra','Fecha registro','Costo unitario','Unidades','Costo tot.'];
    this.campoItems  = ['nombre','fechaRegistro','costoUnitario','unidades','costoTotal'];

    this.construirFormulario();
    this.getGestiones();
  }

  private construirFormulario(){
    this.formularioIngresoExtra = this.fb.group({
      gestion       : ['0',[Validators.required],[]],
      fechaRegistro : [{value: this.generalService.getFechaActualFormateado(), disabled: false},[Validators.required],[]],
      ingresosExtra : this.fb.array([],[Validators.required],[])
    });
    this.aniadirIngresoExtra();

    this.formularioGestion = this.fb.group({
      gestion : ['0',[Validators.required],[]]
    });
  }
  
  public get ingresosExtraList() : FormArray {
    return this.formularioIngresoExtra.get('ingresosExtra') as FormArray;
  }

  public nuevoIngresoExtra():FormGroup{
    return this.fb.group({
      nombre        : ['', [Validators.required],[]],
      costoUnitario : ['', [Validators.required],[]],
      unidades      : ['', [Validators.required],[]],
    });
  }

  public aniadirIngresoExtra():void{
    this.ingresosExtraList.push(this.nuevoIngresoExtra());
  }

  public eliminarIngresoExtra(index:number):void{
    this.ingresosExtraList.removeAt(index);
  }

  public campoValido(campo:string) : boolean{
    return  this.formularioIngresoExtra.get(campo).valid &&
            this.formularioIngresoExtra.get(campo).value!=0;
  }

  public campoInvalido(campo:string) : boolean{   
    return  this.formularioIngresoExtra.get(campo).invalid && 
            this.formularioIngresoExtra.get(campo).touched;
  }

  public ingresoExtraValido(index:number,campo:string) : boolean{
    return (this.ingresosExtraList.at(index) as FormGroup).controls[campo].valid;
  }

  public ingresoExtraInvalido(index:number,campo:string) : boolean{   
    return (this.ingresosExtraList.at(index) as FormGroup).controls[campo].invalid && 
           (this.ingresosExtraList.at(index) as FormGroup).controls[campo].touched;
  }

  public campoValidoGestion(campo:string) : boolean{
    return  this.formularioGestion.get(campo).valid &&
            this.formularioGestion.get(campo).value!=0;
  }

  public campoInvalidoGestion(campo:string) : boolean{   
    return  this.formularioGestion.get(campo).invalid && 
            this.formularioGestion.get(campo).touched;
  }

  public async guardarRegistroIngresoExtra(){
    if(this.formularioInvalido())
      return;

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR EL INGRESO EXTRA','¿Está seguro de registrar el/los ingreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    
    for (let i = 0; i < this.ingresosExtraList.length; i++) {
      let ingresoExtra:AbstractControl = this.ingresosExtraList.at(i);
      
      let item:IngresoExtraDTO = {
        idGestion     : Number(this.formularioIngresoExtra.get('gestion').value),
        fechaRegistro : this.formularioIngresoExtra.get('fechaRegistro').value,
        nombre        : ingresoExtra.get('nombre').value,
        costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
        unidades      : Number(ingresoExtra.get('unidades').value),
      }
      this.createIngresoExtraService(item);
    }
  }

  public async actualizarRegistroIngresoExtra(){
    if(this.formularioInvalido())
      return;

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR EL INGRESO EXTRA','¿Está seguro de actualizar el/los ingreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let ingresoExtra:AbstractControl = this.ingresosExtraList.at(0);
    let item:IngresoExtraDTO = {
      idGestion     : Number(this.formularioIngresoExtra.get('gestion').value),
      fechaRegistro : this.formularioIngresoExtra.get('fechaRegistro').value,
      nombre        : ingresoExtra.get('nombre').value,
      costoUnitario : Number(ingresoExtra.get('costoUnitario').value),
      unidades      : Number(ingresoExtra.get('unidades').value),
    }
    this.updateIngresoExtraService(this.ingresoExtraUpdate.id,item);      
  }

  public cancelarRegistroIngresoExtra(){
    this.limpiarFormulario();
  }

  private formularioInvalido():boolean{
    let invalido:boolean = false;

    if(this.formularioIngresoExtra.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioIngresoExtra.markAllAsTouched();
      return true;
    }

    let idGestion:number = this.formularioIngresoExtra.get('gestion').value;
    let fecha:string = this.formularioIngresoExtra.get('fechaRegistro').value;
    let gestionFecha:number = Number(fecha.split('-')[0]);
    let gestion:number = this.getGestionFromLista(idGestion);

    if(gestionFecha!=gestion){
      this.generalService.mensajeFormularioInvalido('La gestión selecionada con la gestion de la fecha no coincide',INFO_MESSAGE.title_form_invalid);
      return true;
    }
    
    return invalido;
  }

  private getGestionFromLista(idGestion:number):number{
    let gestion:number = this.aux;
    let encontrado:boolean = false;
    let i:number = 0;
    while(!encontrado && i<this.listaGestiones.length){
      if(this.listaGestiones[i].idAnio == idGestion){
        encontrado = true;
        gestion  = this.listaGestiones[i].anio;
      }
      i++;
    }
    return gestion;
  }

  private limpiarFormulario(){
    this.formularioIngresoExtra.reset();
    this.formularioIngresoExtra.get('fechaRegistro').setValue(this.generalService.getFechaActualFormateado());  
    this.formularioIngresoExtra.get('gestion').setValue('0');
    this.ingresosExtraList.clear();
    this.ingresoExtraUpdate = this.aux;
    this.aniadirIngresoExtra();
  }

  public actualizarItemEvent(item:IngresoExtraVO){
    this.formularioIngresoExtra.get('gestion').setValue(item.idGestion);
    this.formularioIngresoExtra.get('fechaRegistro').setValue(item.fechaRegistro);
    this.ingresosExtraList.at(0).get('nombre').setValue(item.nombre);
    this.ingresosExtraList.at(0).get('costoUnitario').setValue(item.costoUnitario);
    this.ingresosExtraList.at(0).get('unidades').setValue(item.unidades);

    this.ingresoExtraUpdate = item;
  }

  public async eliminarItemEvent(item:IngresoExtraVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR EL INGRESO EXTRA','¿Está seguro de eliminar el/los ingreso/s extra ingresado/s?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    this.deleteIngresoExtraService(item.id);
  }

  public changeGestionListaIngresoExtra(){
    let idGestion:number = this.formularioGestion.get('gestion').value;
    this.getReporteIngresoByGestionService(idGestion);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tablaIngresoExtra')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de ingresos extra');
  }

  // ----------------------
  // | Consumo API-REST   |
  // ----------------------
  private getGestiones(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public createIngresoExtraService(ingresoExtraDTO:IngresoExtraDTO){
    return this.ingresoExtraService.createIngresoExtra(ingresoExtraDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteIngresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateIngresoExtraService(id:number,ingresoExtraDTO:IngresoExtraDTO){
    return this.ingresoExtraService.updateIngresoExtra(id,ingresoExtraDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteIngresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteIngresoExtraService(id:number){
    return this.ingresoExtraService.deleteIngresoExtra(id).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.formularioGestion.valid && this.formularioGestion.get('gestion').value!='0'){
          let idGestion:number = this.formularioGestion.get('gestion').value;
          this.getReporteIngresoByGestionService(idGestion);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getReporteIngresoByGestionService(id:number){
    return this.reporteIngresoExtraService.getReporteIngresoByGestion(id).subscribe(
      (resp)=>{
        if(resp!=this.aux){          
          this.reporteIngresoExtraVO = resp;
          this.listaIngresoExtraVO = this.reporteIngresoExtraVO.ingresoExtraList;
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.reporteIngresoExtraVO = this.aux;
          this.listaIngresoExtraVO = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IngresoExtraRoutingModule } from './ingreso-extra-routing.module';
import { IngresoExtraComponent } from './components/ingreso-extra/ingreso-extra.component';
import { ReporteIngresoExtraComponent } from './components/reporte-ingreso-extra/reporte-ingreso-extra.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ReporteIngresoExtraRangoFechaComponent } from './components/reporte-ingreso-extra-rango-fecha/reporte-ingreso-extra-rango-fecha.component';


@NgModule({
  declarations: [
    IngresoExtraComponent,
    ReporteIngresoExtraComponent,
    ReporteIngresoExtraRangoFechaComponent
  ],
  imports: [
    CommonModule,
    IngresoExtraRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class IngresoExtraModule { }

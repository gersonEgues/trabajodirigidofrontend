import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo : 'guest',
    pathMatch:  'full' // la ruta '', si o si tiene que ser solo un espacio vacio (''), ya que esto: /path === ''/path
  },
  {
    path: 'guest',
    loadChildren: () => import('./layout/guest-layout/guest-layout.module').then(m=>m.GuestLayoutModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./layout/user-layout/user-layout.module').then(m=>m.UserLayoutModule),
  },
  {
    path: '**',
    redirectTo: 'guest',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

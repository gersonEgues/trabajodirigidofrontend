import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { RegistroSociosComponent } from './components/registro-socios/registro-socios.component';
import { BusquedaSocioComponent } from './components/busqueda-socio/busqueda-socio.component';
import { ListaDeSociosComponent } from './components/lista-de-socios/lista-de-socios.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'lista-de-socios', 
        component: ListaDeSociosComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [AuthGuard,RolGuard] 
      },
      { 
        path  : 'busqueda-socio', 
        component: BusquedaSocioComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [AuthGuard,RolGuard]
      },
      { 
        path  : 'registro-socio' , 
        component: RegistroSociosComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [AuthGuard,RolGuard] 
      },
      { 
        path  : '**', 
        redirectTo : 'lista-de-socios' 
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ReactiveFormsModule,
  ],
  exports: [
    RouterModule
  ]
})
export class SociosRoutingModule { }

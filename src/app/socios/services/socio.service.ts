import { Injectable } from '@angular/core';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CountItemVO } from 'src/app/shared/models/countItemVO';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { EstadoSocioDTO } from '../models/estadoSocioDTO';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { SocioLite } from '../models/dto-vo/socioLite';
import { SocioMedidorVO } from 'src/app/shared/models/socio-medidor-vo';
import { SocioVO } from 'src/app/shared/models/socio-vo';
import { SocioDTO } from 'src/app/shared/models/socio-dto';

@Injectable({
  providedIn: 'root'
})

export class SocioService extends RootServiceService{
  constructor(private http: HttpClient) { 
    super("api/socio",http);
  }

  public getCountSocios(estadoSocio:boolean) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/count/${estadoSocio}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }
  
  public getListaSociosLite(estadoSocio:boolean) : Observable<SocioLite[]> {
    return this.http.get<SocioLite[]>(`${this.url}/lista-lite/${estadoSocio}`).pipe(
      map(response => {
        return response as SocioLite[];
      })
    );
  }

  public getSociosPaginator(pageDTO:PageDTO, estadoSocio:boolean): Observable<SocioVO[]> {
    let url:string = `${this.url}/pagination/${estadoSocio}?page=${pageDTO.page}&size=${pageDTO.size}`
    if(pageDTO.sort)
      url += `&sort=${pageDTO.sort}`

    return this.http.get<SocioVO[]>(url).pipe(
      map(response => {
        return response as SocioVO[];
      })
    );
  }

  public buscarSocio(codigo:string, nombre:string, apellido:string, estadoSocio:boolean=true): Observable<SocioVO[]> {
    return this.http.get<SocioVO[]>(`${this.url}/search/${estadoSocio}?codigo=${codigo}&nombre=${nombre}&apellido=${apellido}`).pipe(
      map(res => {
        return res as SocioVO[]
      })
    );
  }

  // busca de entro todos los socios, independeintemente del estado del mismo, activo o dado de baja
  public buscarSocioTodos(codigo:string, nombre:string, apellido:string): Observable<SocioVO[]> {
    return this.http.get<SocioVO[]>(`${this.url}/search?codigo=${codigo}&nombre=${nombre}&apellido=${apellido}`).pipe(
      map(res => {
        return res as SocioVO[]
      })
    );
  }

  public buscarSocioMedidor(codigo:string, nombre:string, apellido:string, estadoSocio:boolean=true): Observable<SocioMedidorVO[]> {
    return this.http.get<SocioMedidorVO[]>(`${this.url}/search-socio-medidor/${estadoSocio}?codigo=${codigo}&nombre=${nombre}&apellido=${apellido}`).pipe(
      map(res => {
        return res as SocioMedidorVO[]
      })
    );
  }

  public tieneSocioMedidoresAsignadosActivos(idSocio:number) : Observable<BanderaResponseVO> {
    return this.http.get<BanderaResponseVO>(`${this.url}/tiene-medidor-asignado/${idSocio}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public createSocio(socio:SocioDTO) : Observable<SocioVO>{
    return this.http.post<SocioVO>(`${this.url}/1`,socio).pipe(
      map(resp => {
        return resp as SocioVO;
      })
    );
  }

  public updateSocio(socio:SocioVO) : Observable<SocioVO>{
    return this.http.put<SocioVO>(this.url,socio).pipe(
      map(resp => {
        return resp as SocioVO;
      })
    );
  }

  public updateEstadoSocio(estadoSocioDTO:EstadoSocioDTO):Observable<SocioVO>{
    return this.http.put<SocioVO>(`${this.url}/updateEstado`,estadoSocioDTO).pipe(
      map(resp => {
        return resp as SocioVO;
      })
    );
  }

  public codigoSocioDisponile(codigoSocio:string): Observable<BanderaResponseVO> {
    return this.http.get<BanderaResponseVO>(`${this.url}/codigo-socio-disponile/${codigoSocio}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public codigoSocioDisponileUpdate(codigoSocio:string,codigoSocioExclude:string): Observable<BanderaResponseVO> {
    return this.http.get<BanderaResponseVO>(`${this.url}/codigo-socio-disponile-update/${codigoSocio}/${codigoSocioExclude}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public dniSocioDisponile(dniSocio:string): Observable<BanderaResponseVO> {
    return this.http.get<BanderaResponseVO>(`${this.url}/dni-socio-disponile/${dniSocio}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public dniSocioDisponileUpdate(dniSocio:string,dniSocioExclude:string): Observable<BanderaResponseVO> {
    return this.http.get<BanderaResponseVO>(`${this.url}/dni-socio-disponile-update/${dniSocio}/${dniSocioExclude}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}
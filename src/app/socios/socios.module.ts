import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { BusquedaSocioComponent } from './components/busqueda-socio/busqueda-socio.component';
import { RegistroSociosComponent } from './components/registro-socios/registro-socios.component';
import { SociosRoutingModule } from './socios-routing.module';
import { ListaDeSociosComponent } from './components/lista-de-socios/lista-de-socios.component';
import { HttpClientModule } from '@angular/common/http'; 


@NgModule({
  declarations: [
    RegistroSociosComponent,
    BusquedaSocioComponent,
    ListaDeSociosComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SociosRoutingModule,
    SharedModule,
    FormsModule,
    HttpClientModule
  ],
  exports:[
    BusquedaSocioComponent
  ]
})

export class SociosModule { }
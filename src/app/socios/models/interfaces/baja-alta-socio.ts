export interface BajaAltaSocio{
  idSocio       : number
  idResponsable : number,
  fechaRegistro : Date,
  motivo        : string  
}

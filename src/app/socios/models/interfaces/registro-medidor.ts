import { Medidor } from "../../../shared/models/medidor";

export interface RegistroMedidor{
  idSocio       : number
  idResponsable : number,
  fechaRegistro : Date,
  medidores       : Medidor[]  
}

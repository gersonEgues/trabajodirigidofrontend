export interface LogBajaVO{
  idLogBajaSocio : number,
  idUsuario      : number,
  idSocio        : number,
  fecha          : string,
  motivo         : string,
}
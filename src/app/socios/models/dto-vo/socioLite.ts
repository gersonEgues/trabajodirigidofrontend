export interface SocioLite{
  idSocio        : number,
  codigo         : string,
  nombreCompleto : string,
}
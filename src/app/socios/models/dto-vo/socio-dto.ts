import { Medidor } from '../../../shared/models/medidor';
import { Telefono } from '../interfaces/telefono';

export interface Socio {
  id              : number,
  codigo?         : number,
  nombres         : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  ciSocio?        : string,
  extencionCI?    : string,
  direccionSocio? : string,
  nroCasaSocio?   : string,
  telefonos?      : Telefono[],
  numMedidores?   : number,
  medidor?        : Medidor[]
}
//['codigo','nombres','apellidoPaterno','apellidoMaterno','telefono','medidor']
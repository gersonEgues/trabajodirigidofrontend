import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { GeneralService } from 'src/app/shared/services/general.service';
import { SocioIU } from '../../models/socioIU';
import { SocioService } from '../../services/socio.service';
import { map } from 'rxjs/operators';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { EstadoSocioDTO } from '../../models/estadoSocioDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { SocioVO } from 'src/app/shared/models/socio-vo';
import { SocioDTO } from 'src/app/shared/models/socio-dto';

@Component({
  selector: 'app-registro-socios',
  templateUrl: './registro-socios.component.html',
  styleUrls: ['./registro-socios.component.css']
})
export class RegistroSociosComponent implements OnInit {
  aux:any = null;
  extencionDepartamento! : string[];
  socioForm!             : FormGroup; 
  banderaUpdate          : boolean = false
  idSocioUpdate!         : number; 
  socioSeleccionado!     : SocioVO;              

  // LISTA DE SOCIOS
  tituloItems         : string[]  = [];
  campoItems          : string[]  = [];
  idItem              : string    = 'idSocio';
  countSocios         : number    = 0;
  listaDeSocios       : SocioVO[] = [];
  update              : boolean   = true;
  delete              : boolean   = false;
  pageDTO!            : PageDTO;

  estadoSocio:boolean = true;

  @ViewChild('tablaRegistroSocios') tablaRegistroSocios! : TablePaginatorComponent;

  constructor(private fb              : FormBuilder, 
              private socioService    : SocioService,
              private generalService  : GeneralService,
              private dialog          : MatDialog,
              private router          : Router) {  }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : 'apellido_paterno,apellido_materno,nombre',
    }  
    this.extencionDepartamento = ['LPZ','CO','SCZ','POT','OR','TA','CH','PA','BE']
    this.construirFormulario();
    this.configurarDatosTablaListaSocios();
  }

  private construirFormulario(){
    this.socioForm = this.fb.group({
      codigo          : ['',[Validators.required],[this.codigoSocioDisponile.bind(this)]],
      nombre          : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      apellidoPaterno : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      apellidoMaterno : ['',[Validators.required,Validators.pattern('[a-zA-Z ]*')],[]],
      ci              : ['',[Validators.required,Validators.maxLength(10),Validators.minLength(6)],[this.dniSocioDisponile.bind(this)]],
      extencion       : ['',[Validators.required],[]],
      direccion       : ['',[Validators.required],[]],
      nroCasa         : ['',[Validators.required],[]],
      fechaCreacion   : [{value : this.generalService.getFechaActual(), disabled:true},[Validators.required],[]],
      telefono        : ['',[Validators.required,Validators.max(99999999),Validators.min(9999999)],[]],
    });
  }

  public async createSocio() {
    if(this.formularioInvalido()){  return; }

    let socioIU  : SocioIU  = this.socioForm.getRawValue();
    let socioDTO : SocioDTO = this.getSocioDTO(socioIU);
    let nombreCompleto:string = socioDTO.nombre + ' ' + socioDTO.apellidoPaterno + ' ' + socioDTO.apellidoMaterno;

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE SOCIO',`¿Está seguro de registrar los datos del socio ${nombreCompleto}?`,'Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }
         
    this.createSocioService(socioDTO);
  }

  public async updateSocio(){
    if(this.formularioInvalido()){  return; }

    let nombreCompleto:string = this.socioSeleccionado.nombre + ' ' + this.socioSeleccionado.apellidoPaterno + ' ' + this.socioSeleccionado.apellidoMaterno;
    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR DATOS DEL SOCIO',`¿Esta seguro de actualizar los datos del socio ${nombreCompleto}?`,'Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }
      
    let socioIU : SocioIU = this.socioForm.getRawValue();
    let socioVO : any = this.getSocioDTO(socioIU);
    socioVO.idSocio = this.idSocioUpdate;

    this.updateSocioService(socioVO);
  }


  private formularioInvalido() : boolean{
    let bandera : boolean = false;
    if(this.socioForm.invalid){
      this.socioForm.markAllAsTouched();
      this.generalService.mensajeError('Llene todos los campos de manera correcta','Error');
      bandera = true;
    }
    return bandera;
  }

  public cancelarRegistroSocio(){
    let aux:any =  undefined;
    
    this.banderaUpdate = false;
    this.idSocioUpdate = -1;
    this.socioSeleccionado = aux;
    this.limpiarTodosLosCampos();

    this.socioForm.get('codigo')?.clearAsyncValidators();
    this.socioForm.get('codigo')?.setAsyncValidators(this.codigoSocioDisponile.bind(this));
    this.socioForm.get('ci')?.setAsyncValidators(this.dniSocioDisponile.bind(this));
  }

  private limpiarTodosLosCampos(){
    this.socioForm.reset();
    this.socioForm.get('fechaCreacion')?.setValue(this.generalService.getFechaActual());
  }

  public campoValido(nombreCampo:string):boolean{
    return  this.socioForm.controls[nombreCampo].valid;
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  this.socioForm.controls[nombreCampo].touched &&
            this.socioForm.controls[nombreCampo].invalid;
  }

  private getSocioDTO(socioIU : SocioIU) : SocioDTO{
    let socioDTO : SocioDTO = {
      codigo          : socioIU.codigo,
      nombre          : this.getCapitalize(socioIU.nombre),
      apellidoPaterno : this.getCapitalize(socioIU.apellidoPaterno),
      apellidoMaterno : this.getCapitalize(socioIU.apellidoMaterno),
      ci              : socioIU.ci,
      extencion       : socioIU.extencion,
      direccion       : this.getCapitalize(socioIU.direccion),
      nroCasa         : socioIU.nroCasa,
      fechaCreacion   : socioIU.fechaCreacion,
      telefono        : socioIU.telefono
    }
    return socioDTO;
  }

  private getCapitalize(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  //---------------
  // LISTA SOCIOS :
  //---------------
  private configurarDatosTablaListaSocios() : void{
    this.tituloItems = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Ext.','Dirección','Nro. casa','Telefono'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','extencion','direccion','nroCasa','telefono'];  
    this.getCountSocios(this.estadoSocio);
  }

  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : 'apellido_paterno,apellido_materno,nombre',
    }
    this.getSociosPaginator(this.pageDTO,this.estadoSocio);
  }

  public socioSeleccionadoEvent(socio:any){
    this.socioSeleccionado = socio;
  }

  public updateSocioEvent(socio:SocioVO){
    this.banderaUpdate = true;
    this.idSocioUpdate = socio.idSocio;
    this.socioSeleccionado = socio;
    this.limpiarTodosLosCampos();

    this.socioForm.get('codigo')?.clearAsyncValidators();
    this.socioForm.get('codigo')?.setAsyncValidators(this.codigoSocioDisponileUpdate.bind(this));
    this.socioForm.get('ci')?.setAsyncValidators(this.dniSocioDisponileUpdate.bind(this));


    this.socioForm.get('codigo')?.setValue(socio.codigo);
    this.socioForm.get('nombre')?.setValue(socio.nombre);
    this.socioForm.get('apellidoPaterno')?.setValue(socio.apellidoPaterno);
    this.socioForm.get('apellidoMaterno')?.setValue(socio.apellidoMaterno);
    this.socioForm.get('ci')?.setValue(socio.ci);
    this.socioForm.get('extencion')?.setValue(socio.extencion);
    this.socioForm.get('direccion')?.setValue(socio.direccion);
    this.socioForm.get('nroCasa')?.setValue(socio.nroCasa);
    this.socioForm.get('fechaCreacion')?.setValue(socio.fechaCreacion);
    this.socioForm.get('telefono')?.setValue(socio.telefono);
  }

  public cambioEstadoSocio(){
    if(this.isPresidente() || this.isAdmin() || this.isSecretaria()){
      this.tablaRegistroSocios.cambioEstadoEditar();
      this.tablaRegistroSocios.cambioEstadoEliminar();
      this.tablaRegistroSocios.cambioEstadoVer();
    }

    this.limpiarTodosLosCampos();
    this.tablaRegistroSocios.deseleccionarItem();   
    this.getCountSocios(this.estadoSocio);
  }

  public async darDeBajaSocioEvent(socio:SocioVO){
    this.limpiarTodosLosCampos();
    let afirmativo:Boolean = await this.getConfirmacion('DAR DE BAJA A SOCIO',`¿Está seguro de dar de baja al socio ${socio.nombre} ${socio.apellidoPaterno} ${socio.apellidoMaterno}?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let estadoSocio:EstadoSocioDTO = {
      idSocio : socio.idSocio,
      estado  : false
    }
    this.updateEstadoSocio(estadoSocio);
  }

  public async activarSocioEvent(socio:SocioVO){
    let afirmativo:Boolean = await this.getConfirmacion('ACTIVAR ESTADO DEL SOCIO',`¿Está seguro de activar el estado de socio ${socio.nombre} ${socio.apellidoPaterno} ${socio.apellidoMaterno}?`,'Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
    
    let estadoSocio:EstadoSocioDTO = {
      idSocio : socio.idSocio,
      estado  : true
    }
    this.updateEstadoSocio(estadoSocio);
  }

  public buscarSocioPorNombre(nombre:string):void{
    if(nombre == "" || nombre.trim() == ""){ 
      this.tablaRegistroSocios.deseleccionarItems();
      return;
    }

    this.tablaRegistroSocios.buscarItem(nombre,"nombre");
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public isSecretaria():boolean{
    return this.generalService.isSecretaria();
  }

  public genearteArchivoXLSX():void{
    this.tablaRegistroSocios.genearteArchivoXLSX("Lista de registro de socios");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  private getCountSocios(estadoSocio:boolean) : void{
    this.socioService.getCountSocios(estadoSocio).subscribe(
      (resp)=>{
        if(resp){
          this.countSocios = resp.countItem
          this.getSociosPaginator(this.pageDTO,estadoSocio);        
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getSociosPaginator(pageDTO : PageDTO,estadoSocio:boolean) : void{
    this.socioService.getSociosPaginator(pageDTO,estadoSocio).subscribe(
      (resp:SocioVO[]) => { 
        this.listaDeSocios = resp;
        if(this.listaDeSocios.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createSocioService(socio:SocioDTO){
    this.socioService.createSocio(socio).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Socio creado correctamente","Exito :");
        this.limpiarTodosLosCampos();
        this.getCountSocios(this.estadoSocio);
      }, (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateSocioService(socio:SocioVO){
    this.socioService.updateSocio(socio).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Socio actualizado correctamente","Exito :");
        this.limpiarTodosLosCampos();

        this.banderaUpdate     = false;
        this.idSocioUpdate     = this.aux; 
        this.socioSeleccionado = this.aux; 

        this.getSociosPaginator(this.pageDTO,this.estadoSocio);
      }, (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private updateEstadoSocio(estadoSocio:EstadoSocioDTO){
    this.socioService.updateEstadoSocio(estadoSocio).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo el estado del socio");
        this.getCountSocios(this.estadoSocio);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private codigoSocioDisponile(control: AbstractControl){
    return this.socioService.codigoSocioDisponile(control.value).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }

  private codigoSocioDisponileUpdate(control: AbstractControl){
    return this.socioService.codigoSocioDisponileUpdate(control.value,this.socioSeleccionado.codigo).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }

  private dniSocioDisponile(control: AbstractControl){
    return this.socioService.dniSocioDisponile(control.value).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }
  
  private dniSocioDisponileUpdate(control: AbstractControl){
    return this.socioService.dniSocioDisponileUpdate(control.value,this.socioSeleccionado.ci).pipe(
      map((res:BanderaResponseVO)=>{
        return res.bandera? null : {bandera:true};
      })
    );
  }


  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

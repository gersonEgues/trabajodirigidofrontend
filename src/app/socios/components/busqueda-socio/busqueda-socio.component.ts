import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { SocioService } from '../../services/socio.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { SocioVO } from 'src/app/shared/models/socio-vo';

@Component({
  selector: 'busqueda-socio',
  templateUrl: './busqueda-socio.component.html',
  styleUrls: ['./busqueda-socio.component.css']
})
export class BusquedaSocioComponent implements OnInit {
  opcionEstado        : boolean = true;
  tituloItems         : string[] = [];
  campoItems          : string[] = [];
  idItem              : string = 'idSocio';
  listaDeSocios       : SocioVO[]=[];
  
  constructor(private socioService   :SocioService,
              private generalService : GeneralService,
              private router         : Router) {  }

  ngOnInit(): void {
    this.tituloItems         = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Ext.','Dirección','Nro. casa','Telefono'];
    this.campoItems          = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','extencion','direccion','nroCasa','telefono'];    
  }

  public buscarSocioPorCodigo(codigo:string):void{
    let valNull:any = null;
    if(codigo.trim()==""){
      this.listaDeSocios = [];
    }else{
      this.buscarSocio(codigo,valNull,valNull);
    }
  }

  public buscarSocioPorNombre(nombre:string):void{
    let valNull:any = null;
    if(nombre.trim()==""){
      this.listaDeSocios = [];
    } else {
      this.buscarSocio(valNull,nombre,valNull);
    }
  }
  
  public buscarSocioPorApellido(apellido:string):void{
    let valNull:any = null;
    if(apellido.trim()==""){
      this.listaDeSocios = [];
    }else{
      this.buscarSocio(valNull,valNull,apellido);
    }
  }

  public socioSeleccionado(socio:any){

  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  private buscarSocio(codigo:string, nombre:string, apellido:string) : void {
    this.socioService.buscarSocio(codigo,nombre,apellido).subscribe(
      (resp:SocioVO[])=>{
        this.listaDeSocios = resp;
      },(err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

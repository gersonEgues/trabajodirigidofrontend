import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { TablePaginatorMultipleRowsComponent } from 'src/app/shared/components/table-paginator-multiple-rows/table-paginator-multiple-rows.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { SocioService } from '../../services/socio.service';
import { PageDTO } from '../../../shared/models/pageDTO';
import { InputFormComponent } from 'src/app/shared/components/input-form/input-form.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { SocioVO } from 'src/app/shared/models/socio-vo';

@Component({
  selector: 'app-lista-de-socios',
  templateUrl: './lista-de-socios.component.html',
  styleUrls: ['./lista-de-socios.component.css'],
})
export class ListaDeSociosComponent implements OnInit {
  estadoSocio:boolean = true;

  // tabla
  tituloItems         : string[] = [];
  campoItems          : string[] = [];
  idItem              : string = 'idSocio';
  countSocios         : number = 0;
  listaDeSocios       : SocioVO[]=[];
  pageDTO!            : PageDTO;

  @ViewChild('inputFormComponentCodigo') inputFormComponentCodigo!     : InputFormComponent;
  @ViewChild('inputFormComponentApellido') inputFormComponentApellido! : InputFormComponent;
  @ViewChild('inputFormComponentNombre') inputFormComponentNombre!     : InputFormComponent;
  
  @ViewChild(TablePaginatorComponent) tablePaginatorComponent! : TablePaginatorComponent;
  @ViewChild('tablaListaSocios') tablaListaSocios! : TablePaginatorComponent;

  constructor(private socioService   : SocioService,
              private generalService : GeneralService,
              private router         : Router) { 
  }

  ngOnInit(): void { 
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : 'apellido_paterno,apellido_materno,nombre',
    } 
    this.configurarDatosTablaListaSocios();
  }

  private configurarDatosTablaListaSocios() : void{
    this.tituloItems = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Ext.','Dirección','Nro. casa','Telefono'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','extencion','direccion','nroCasa','telefono'];
    this.getCountSocios(this.estadoSocio);
  }

  public buscarSocioPorCodigo(codigo:string):void{
    if(codigo.trim()==""){ 
      this.tablePaginatorComponent.deseleccionarItems(); 
    }else{
      this.inputFormComponentApellido.limpiarTexto();
      this.inputFormComponentNombre.limpiarTexto();

      this.tablePaginatorComponent.buscarItem(codigo.toLowerCase(),"codigo");
    }
  }
  
  public buscarSocioPorNombre(nombre:string):void{
    if(nombre.trim()==""){ 
      this.tablePaginatorComponent.deseleccionarItems();  
    }else{
      this.inputFormComponentCodigo.limpiarTexto();
      this.inputFormComponentApellido.limpiarTexto();
      this.tablePaginatorComponent.buscarItem(nombre.toLowerCase(),"nombre");
    }
  }

  public buscarSocioPorApellido(apellido:string):void{
    if(apellido.trim()==""){ 
      this.tablePaginatorComponent.deseleccionarItems(); 
    }else{
      this.inputFormComponentCodigo.limpiarTexto();
      this.inputFormComponentNombre.limpiarTexto();
      
      this.tablePaginatorComponent.buscarItem(apellido.toLowerCase(),"apellidoPaterno");
    }    
  }

   // Event Output
   public cambioDePagina(pageEvent:PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : 'apellido_paterno,apellido_materno,nombre',
    }
    this.getSociosPaginator(this.pageDTO,this.estadoSocio);
  }

  public cambioEstadoSocio(){
    this.inputFormComponentCodigo.limpiarTexto();
    this.inputFormComponentApellido.limpiarTexto();
    this.inputFormComponentNombre.limpiarTexto();

    this.getCountSocios(this.estadoSocio);    
  }
  
  public genearteArchivoXLSX():void{
    this.tablaListaSocios.genearteArchivoXLSX("Lista de socios");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------
  private getCountSocios(estado:boolean) : void{
    this.socioService.getCountSocios(this.estadoSocio).subscribe(
      (resp)=>{      
        if(resp){
          this.countSocios = resp.countItem
          this.getSociosPaginator(this.pageDTO,estado);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getSociosPaginator(pageDTO : PageDTO,estado:boolean) : void{
    this.socioService.getSociosPaginator(pageDTO,estado).subscribe(
      (resp:SocioVO[]) => { 
        this.listaDeSocios = resp;
        if(this.listaDeSocios.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

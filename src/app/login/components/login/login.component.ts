import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserData } from '../../models/userData';
import { LoginService } from '../../services/login.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../services/user.service';
import { UserDataRecoveryDTO } from '../../models/userDataRecoveryDTO';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('spinnerElement') spinnerElement     : ElementRef;

  formularioLogin! : FormGroup;

  formuRecuperarContra! : FormGroup;
  formuNuevaContra!     : FormGroup;

  modalReferenceRecContra   : NgbModalRef;
  modalReferenceNuevaContra : NgbModalRef;

  banderaSpinnerCorreoElectronico : boolean = false;
  banderaSpinnerNuevaContrasenia  : boolean = false;

  userEmail : string;

  constructor(private fb             : FormBuilder,
              private router         : Router,
              private loginService   : LoginService,
              private userService    : UserService,
              private generalService : GeneralService,
              private modalService   : NgbModal) { }

  ngOnInit(): void {
    this.buildFormulario();
  }

  private buildFormulario(){
    this.formularioLogin = this.fb.group({
      usuario  : ['', [Validators.required],[]],
      password : ['', [Validators.required],[]],
    });

    this.formuRecuperarContra = this.fb.group({
      email  : ['', [Validators.required],[]]
    });

    this.formuNuevaContra = this.fb.group({
      codigo       : ['', [Validators.required],[]],
      contrasenia1 : ['', [Validators.required,Validators.min(5)],[]],
      contrasenia2 : ['', [Validators.required,Validators.min(5)],[]],
    });
  }

  public campoValido(campo:string):boolean{
    return  this.formularioLogin.get(campo)!.valid;
  }

  public campoInvalido(campo:string):boolean{
    return  this.formularioLogin.get(campo)!.touched && 
            this.formularioLogin.get(campo)!.invalid;
  }

  public loginUser(){
    if(this.formularioLogin.invalid){
      this.formularioLogin.markAllAsTouched();
      this.generalService.mensajeAlerta('Ingrese su usario y contraseña por favor.');
      return;
    }

    let userData : UserData = {
      email    : this.formularioLogin.get('usuario').value,
      password : this.formularioLogin.get('password').value,
    }

    
    this.activarSpinner();
    this.loginUserService(userData);  
  }

  public cancelarLogin(){
    this.formularioLogin.reset();
  }

  private activarSpinner(){
    this.spinnerElement.nativeElement.classList.remove('spinner-off');
    this.spinnerElement.nativeElement.classList.add('spinner-on');
  }

  private desactivarSpinner(){
    this.spinnerElement.nativeElement.classList.remove('spinner-on');
    this.spinnerElement.nativeElement.classList.add('spinner-off');
  }

  public recuperarContrasenia(modal:any){
    this.formuRecuperarContra.reset();
    this.modalReferenceRecContra = this.modalService.open(modal);
  }

  public async enviarCorreoParaRecuperarContrasenia(modal:any){
    if(this.formuRecuperarContra.invalid){
      this.generalService.mensajeAlerta("Ingrese un correo electronico valido");
      return;
    }

    this.userEmail = this.formuRecuperarContra.get('email').value;
    this.activarSpinnerBanderaCorreoElectronico()
    let existeUsuario:BanderaResponseVO = await this.existeUsuarioService(this.userEmail);
    if(!existeUsuario.bandera)
      return;
    
    let codigoEnviado:BanderaResponseVO = await this.solicitarCodigoDeRecupertacionDeCuentaService(this.userEmail);
    if(!codigoEnviado.bandera)
      return;


    this.modalReferenceRecContra.close();
    this.formuNuevaContra.reset();
    this.modalReferenceNuevaContra = this.modalService.open(modal);
  }

  private activarSpinnerBanderaCorreoElectronico(){
    this.banderaSpinnerCorreoElectronico = true;
  }

  private desactivarSpinnerBanderaCorreoElectronico(){
    this.banderaSpinnerCorreoElectronico = false;
  }

  public campoValidoRecContra(campo:string):boolean{
    return  this.formuRecuperarContra.get(campo)!.valid;
  }

  public campoInvalidoRecContra(campo:string):boolean{
    return  this.formuRecuperarContra.get(campo)!.touched && 
            this.formuRecuperarContra.get(campo)!.invalid;
  }

  public async confirmarRecuperarContrasenia(){
    let pass1:string = this.formuNuevaContra.get('contrasenia1').value;
    let pass2:string = this.formuNuevaContra.get('contrasenia2').value;
    
    if(this.formuNuevaContra.invalid || pass1!=pass2){
      if(this.formuNuevaContra.invalid)
        this.generalService.mensajeAlerta("Ingrese todos los campos por favor");
      
      if(pass1!=pass2)
        this.generalService.mensajeAlerta("Las contraseña no coinciden");
      return;
    }
    
    this.activarSpinnerBanderaNuevaContrasenia();
    let userData : UserDataRecoveryDTO = {
      usuario      : this.userEmail,
      codigo       : this.formuNuevaContra.get('codigo').value,
      contrasenia1 : pass1,
      contrasenia2 : pass2,
    }
    
    let cambioContrasenia:BanderaResponseVO = await this.solicitarCambioDeContraseniaService(userData);
    if(!cambioContrasenia.bandera)
      return;
      
    this.modalReferenceNuevaContra.close();
  }

  public campoValidoNuevaContrasenia(campo:string):boolean{
    return  this.formuNuevaContra.get(campo)!.valid;
  }

  public campoInvalidoNuevaContrasenia(campo:string):boolean{
    return  this.formuNuevaContra.get(campo)!.touched && 
            this.formuNuevaContra.get(campo)!.invalid;
  }

  public campoValidoNuevaContrasenia2(campo:string):boolean{
    return  this.formuNuevaContra.get(campo)!.valid && 
            this.formuNuevaContra.get('contrasenia1').value == this.formuNuevaContra.get('contrasenia2').value;
  }

  public campoInvalidoNuevaContrasenia2(campo:string):boolean{
    return  (this.formuNuevaContra.get(campo)!.touched && 
            this.formuNuevaContra.get(campo)!.invalid) ||
            (this.formuNuevaContra.get(campo)!.touched &&
            this.formuNuevaContra.get('contrasenia1').value != this.formuNuevaContra.get('contrasenia2').value);
  }

  private activarSpinnerBanderaNuevaContrasenia(){
    this.banderaSpinnerNuevaContrasenia = true;
  }

  private desactivarSpinnerBanderaNuevaContrasenia(){
    this.banderaSpinnerNuevaContrasenia = false;
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  // mes anio categoria costo agua service
  public loginUserService(userData:UserData){
    this.loginService.loginUser(userData).subscribe(
      (resp)=>{
        this.desactivarSpinner()           
        let body = resp.body;
        let headers = resp.headers;
        let bearToken = headers.get('Authorization');
        let token = bearToken.replace('Bearer ','');        
        localStorage.setItem('token',token); 
        this.generalService.mensajeCorrecto("Datos de usuario correcto!!!")
        this.router.navigate(['/user']);
      },
      (err)=>{             
        this.desactivarSpinner();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
          this.generalService.mensajeError('Error al autenticarse, revise su usario y contraseña por favor.');
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{          
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  
  public existeUsuarioService(mail:string) : Promise<BanderaResponseVO> {
    return this.userService.existeUsuario(mail).toPromise()
      .then((resp:BanderaResponseVO)=>{
        if(resp.bandera)
          this.generalService.mensajeCorrecto("El nombre de usuario ingresado es correcto");
        else{
          this.generalService.mensajeError(resp.mensaje); 
          this.desactivarSpinnerBanderaCorreoElectronico();
        }
        return resp;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
        this.desactivarSpinnerBanderaCorreoElectronico();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }
        return err;
      });
  }
  

  public solicitarCodigoDeRecupertacionDeCuentaService(mail:string) {
    return this.userService.solicitarCodigoDeRecupertacionDeCuenta(mail).toPromise()
      .then((resp:BanderaResponseVO)=>{
        if(resp.bandera)
          this.generalService.mensajeCorrecto("Se envio un codigo a su email, revise por favor.");
        else
          this.generalService.mensajeError(resp.mensaje);
        this.desactivarSpinnerBanderaCorreoElectronico();
        return resp;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
        this.desactivarSpinnerBanderaCorreoElectronico();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }
        return err;
      });
  }

  public solicitarCambioDeContraseniaService(userData:UserDataRecoveryDTO) {
    return this.userService.solicitarCambioDeContrasenia(userData).toPromise()
      .then((resp:BanderaResponseVO)=>{
        if(resp.bandera)
          this.generalService.mensajeCorrecto(resp.mensaje);
        else
          this.generalService.mensajeError(resp.mensaje);

        this.desactivarSpinnerBanderaNuevaContrasenia();
        return resp;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
        this.desactivarSpinnerBanderaNuevaContrasenia();
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }
        return err;
      });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { UserDataRecoveryDTO } from '../models/userDataRecoveryDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService extends RootServiceService {

  constructor(private http: HttpClient) {
    super("api/usuario",http);
  }

  public existeUsuario(mail:string): Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/recuperar-contrasenia/existe-usuario/${mail}`).pipe(
      map((response:BanderaResponseVO) => {             
        return response as BanderaResponseVO;
      })
    );
  }

  public existeUsuarioTodos(mail:string): Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/existe-usuario/${mail}`).pipe(
      map((response:BanderaResponseVO) => {             
        return response as BanderaResponseVO;
      })
    );
  }

  public solicitarCodigoDeRecupertacionDeCuenta(mail:string): Observable<BanderaResponseVO>{
    return this.http.get<BanderaResponseVO>(`${this.url}/recuperar-contrasenia/solicitar-codigo-confirmacion/${mail}`).pipe(
      map((response:BanderaResponseVO) => {             
        return response as BanderaResponseVO;
      })
    );
  }

  public solicitarCambioDeContrasenia(userData:UserDataRecoveryDTO): Observable<BanderaResponseVO>{
    return this.http.post<BanderaResponseVO>(`${this.url}/recuperar-contrasenia/cambio-contrasenia`,userData).pipe(
      map((response:BanderaResponseVO) => {             
        return response as BanderaResponseVO;
      })
    );
  }
}

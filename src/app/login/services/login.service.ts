import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserData } from '../models/userData';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends RootServiceService {
  constructor(private http: HttpClient) {
    super("login",http);
  }

  public loginUser(loginData:UserData): Observable<HttpResponse<any>>{
    return this.http.post<HttpResponse<any>>(`${this.url}`,loginData,{observe:'response'}).pipe(
      map((response:HttpResponse<any>) => {             
        return response as HttpResponse<any>;
      })
    );
  }
}

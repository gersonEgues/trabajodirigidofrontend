import { Rol } from "./rol";

export interface User {
  id              : number,
  email           : string,
  nombre          : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  estado          : boolean,
  roles           : Rol[]
}
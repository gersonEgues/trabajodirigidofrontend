export interface UserDataRecoveryDTO {
  usuario      : string,
  codigo       : string,
  contrasenia1 : string,
  contrasenia2 : string,
}
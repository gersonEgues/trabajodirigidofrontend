export interface Rol {
  id        : number,
  nombre    : string,
  fecha     : string,
  authority : any
}
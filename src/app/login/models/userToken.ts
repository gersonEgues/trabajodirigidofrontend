import { User } from "./user";

export interface UserToken{
  sub  : string,
  exp  : number,
  user : User
}
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GeneralService } from 'src/app/shared/services/general.service';
import {JwtHelperService  } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router         : Router,
              private generalService : GeneralService,
              private jwtHelper      : JwtHelperService){}

  canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let token:string = localStorage.getItem('token');
    let bandera:boolean = false;

    if(token && !this.jwtHelper.isTokenExpired(token)){
      bandera = true;
    }else{
      this.generalService.mensajeError("Ingrese su usuario y contraseña por favor")
      this.router.navigate(['/guest/login']);
    }    
    return bandera;
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagarConsumoAguaComponent } from './components/pagar-consumo-agua/pagar-consumo-agua.component';
import { HistorialPagosConsumoAguaComponent } from './components/historial-pagos-consumo-agua/historial-pagos-consumo-agua.component';
import { PagarFaltaReunionesComponent } from './components/pagar-falta-reuniones/pagar-falta-reuniones.component';
import { HistorialFaltaReunionesComponent } from './components/historial-falta-reuniones/historial-falta-reuniones.component';
import { CobroAportesEventualesComponent } from '../aportes-eventuales/components/cobro-aportes-eventuales/cobro-aportes-eventuales.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ReporteSocioAportesEventualesComponent } from '../aportes-eventuales/components/reporte-socio-aportes-eventuales/reporte-socio-aportes-eventuales.component';
import { ROL } from '../shared/enums-mensajes/roles';
import { PagarConsumoAguaSociosConBajaComponent } from './components/pagar-consumo-agua-socios-con-baja/pagar-consumo-agua-socios-con-baja.component';
import { HistorialPagosConsumoAguaSociosConBajaComponent } from './components/historial-pagos-consumo-agua-socios-con-baja/historial-pagos-consumo-agua-socios-con-baja.component';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'pagar-consumo-agua', 
        component: PagarConsumoAguaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'historial-pago-consumo-agua', 
        component: HistorialPagosConsumoAguaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },


      { 
        path  : 'pagar-consumo-agua-socios-o-medidores-con-baja', 
        component: PagarConsumoAguaSociosConBajaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'historial-pago-consumo-agua-socios-o-medidores-con-baja', 
        component: HistorialPagosConsumoAguaSociosConBajaComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },



      { 
        path  : 'pagar-falta-retraso-reuniones', 
        component: PagarFaltaReunionesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'historial-falta-retraso-reunion', 
        component: HistorialFaltaReunionesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'cobro-aporte-eventual', 
        component: CobroAportesEventualesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'reporte-socio-aporte-eventual', 
        component: ReporteSocioAportesEventualesComponent, 
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
          { nombre : ROL.MIEMBRO_MESA_DIRECTIVA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', redirectTo : 'historial-pago-consumo-agua' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuentaSocioRoutingModule { }

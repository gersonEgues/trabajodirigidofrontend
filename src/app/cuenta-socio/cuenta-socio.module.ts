import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CuentaSocioRoutingModule } from './cuenta-socio-routing.module';
import { PagarConsumoAguaComponent } from './components/pagar-consumo-agua/pagar-consumo-agua.component';
import { HistorialPagosConsumoAguaComponent } from './components/historial-pagos-consumo-agua/historial-pagos-consumo-agua.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { PagarFaltaReunionesComponent } from './components/pagar-falta-reuniones/pagar-falta-reuniones.component';
import { HistorialFaltaReunionesComponent } from './components/historial-falta-reuniones/historial-falta-reuniones.component';
import { PagarConsumoAguaSociosConBajaComponent } from './components/pagar-consumo-agua-socios-con-baja/pagar-consumo-agua-socios-con-baja.component';
import { HistorialPagosConsumoAguaSociosConBajaComponent } from './components/historial-pagos-consumo-agua-socios-con-baja/historial-pagos-consumo-agua-socios-con-baja.component';

@NgModule({
  declarations: [
    PagarConsumoAguaComponent,
    HistorialPagosConsumoAguaComponent,
    PagarFaltaReunionesComponent,
    HistorialFaltaReunionesComponent,
    PagarConsumoAguaSociosConBajaComponent,
    HistorialPagosConsumoAguaSociosConBajaComponent,
  ],
  imports: [
    CommonModule,
    CuentaSocioRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CuentaSocioModule { }

import { CobroExtraPeriodoData } from "./cobroExtraPeriodoData";

export interface LecturaPeriodoTratadoData {
  indexGlobal  : number,
  indexItem    : number,
  seleccionado : boolean,
  checkItem    : boolean,

  // lectura periodo
  idLecturaPeriodo       : number,
  idPeriodo              : number,
  idAnio                 : number,
  idMes                  : number,
  anio                   : number,
  nroMes                 : number,
  nombreMes              : string,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  consumoVolumenM3       : number,
  consumoMinimo          : boolean,
  costoM3                : number,
  costoMinimoM3          : number,
  costoPeriodo           : number,
  cancelado              : boolean,
  sumaCostoPeriodo       : number,
  countItemsCobroExtra   : number,
  fechaCancelado?        : string

  // cobro extra
  idCobroExtra     : number,
  nombreCobroExtra : string,
  costoCobroExtra  : number,

}
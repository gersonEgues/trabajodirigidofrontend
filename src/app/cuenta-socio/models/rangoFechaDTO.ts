export interface RangoFechaDTO{
  fechaInicio : string,
  fechaFin    : string,
  cancelado   : boolean,
}

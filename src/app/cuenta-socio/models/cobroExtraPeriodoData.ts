export interface CobroExtraPeriodoData{
  idCobroExtra        : number,
  nombreCobroExtra    : string,
  detalle             : string,
  costoCobroExtra     : number,
  sumaTotalCobroExtra : number,
  countSocios         : number,
  seleccionado?       : boolean,
}

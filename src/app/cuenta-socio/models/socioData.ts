import { LecturaPeriodoData } from "./lecturaPeriodoData";

export interface SocioData{
  idSocioMedidor          : number,
  idSocio                 : number,
  idCategoria             : number,
  idMedidor               : number,
  codigoSocio             : string,
  nombreSocio             : string,
  apellidoPaterno         : string,
  apellidoMaterno         : string,
  direccion               : string,
  codigoMedidor           : string,
  lecturaInicial          : number,
  lecturaActual           : number,
  codigoCategoria         : string,
  nombreCategoria         : string,
  countItems              : number,
  countLecturaPeriodoData : number,

  //historial
  nroRowsLecturaPeriodoDataList? : number;
  lecturaPeriodoDataList         : LecturaPeriodoData[],
}
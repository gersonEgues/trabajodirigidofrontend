import { CobroExtraPeriodoData } from "./cobroExtraPeriodoData";

export interface LecturaPeriodoData {
  idLecturaPeriodo          : number,
  idPeriodo                 : number,
  idSocioMedidor            : number,
  idAnio                    : number,
  idMes                     : number,
  anio                      : number,
  nroMes                    : number,
  nombreMes                 : string,
  lecturaAnteriorPeriodo    : number,
  lecturaActualPeriodo      : number,
  consumoVolumenM3          : number,
  consumoMinimo             : boolean,
  costoM3                   : number,
  costoMinimoM3             : number,
  costoPeriodo              : number,
  cancelado                 : boolean,
  sumaCostoPeriodo          : number,
  countItemsCobroExtra      : number,
  
  // historial
  fechaCancelado               : string, 
  nroRowsCobroPeriodoDataList? : number,
  sumaCostoCobroExtra          : number,
  cobroExtraPeriodoDataList    : CobroExtraPeriodoData[];
}
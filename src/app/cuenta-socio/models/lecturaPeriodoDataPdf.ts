import { DatosGeneralesComiteVO } from "src/app/shared/models/datosGeneralesComiteVO";
import { CobroExtraPeriodoData } from "./cobroExtraPeriodoData";

export interface LecturaPeriodoDataPdf{
  // socio
  codigoSocio             : string,
  nombreCompletoSocio     : string,
  direccionSocio          : string,
  codigoMedidor           : string,
  codigoCategoria         : string,
  nombreCategoria         : string,
  //lectura periodo
  idLecturaPeriodo       : number,
  anio                   : number,
  nroMes                 : number,
  
  nombreMes              : string,
  lecturaAnteriorPeriodo : number,
  lecturaActualPeriodo   : number,
  
  consumoVolumenM3       : number,
  consumoMinimo          : boolean,
  costoM3                : number,
  costoMinimoM3          : number,
  costoPeriodo           : number,
  sumaCostoPeriodo       : number,
  //cobro extra
  cobroExtraPeriodoDataList : CobroExtraPeriodoData[];

  // formulario
  nroRecibo             : number,
  motivoRecibo          : string,
  fechaActualHibirido   : string,
  fechaActual           : string,
  horaActual            : string,
  montoIngresoNumero     : number,
  montoIngresoLetra      : string,
  observacion           : string,
  nombreEncargadoComite : string,
  rolEncargadoComite    : string,
  datosComiteDeAgua     : DatosGeneralesComiteVO;

}
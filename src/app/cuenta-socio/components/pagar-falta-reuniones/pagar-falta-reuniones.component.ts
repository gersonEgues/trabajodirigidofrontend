import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { BuscadorComponent } from 'src/app/shared/components/buscador/buscador.component';
import { Observable } from 'rxjs';
import { EventoVO } from 'src/app/eventos/models/eventoVO';
import { EventoService } from 'src/app/eventos/services/evento.service';
import { SeguimientoEventoService } from 'src/app/eventos/services/seguimiento-evento.service';
import { SocioSeguimientoVO } from 'src/app/eventos/models/socioSeguimientoVO';
import { EstadoEvento } from 'src/app/eventos/models/estadoEvento';
import { EstadoMultaSeguimientoEvento } from '../../../eventos/models/estadoMultaSeguimientoEvento';
import { EstadoSeguimientoEvento } from '../../../eventos/models/estadoSeguimientoEvento';
import { PdfMultaReunionesService } from '../../services/pdf-multa-reuniones.service';
import { SocioSeguimientoDataPdf } from 'src/app/eventos/models/socioSeguimientoDataPDF';
import { SeguimientoEventoVO } from 'src/app/eventos/models/seguimientoEventoVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-pagar-falta-reuniones',
  templateUrl: './pagar-falta-reuniones.component.html',
  styleUrls: ['./pagar-falta-reuniones.component.css']
})
export class PagarFaltaReunionesComponent implements OnInit {
  aux : any = undefined;

  // Buscador
  listaEventosBusqueda!    : Observable<EventoVO[]>;
  eventoSeleccionado       : EventoVO = this.aux;
  @ViewChild('buscardorComponent') buscardorComponent! : BuscadorComponent;

  //Tabla lista de socios
  formularioSeleccionEvento!   : FormGroup;
  buscadorSocio!               : FormGroup;
  listaTitulosSociosSegEvento! : string[];
  listaCamposSociosSegEvento!  : string[];
  listaSociosEvento!           : any[]; // SocioSeguimientoVO, si pongo su tipado, en html no lo puedo generalizar: item[campo]
    
  nroSociosConRetraso          : number = 0;
  nroSociosConFalta            : number = 0;

  constructor(private fb                       : FormBuilder,
              private generalService           : GeneralService,
              private eventoService            : EventoService,
              private seguimientoEventoService : SeguimientoEventoService,
              private pdfMultaReunionesService : PdfMultaReunionesService,
              private dialog                   : MatDialog,
              private router                   : Router) { }

  ngOnInit(): void {
    this.construirFormularioBuscadorSocio();

    this.construirTablaListaSocios();
    this.construirFormularioSeleccionEvento();
  }

  public construirFormularioBuscadorSocio(){
    this.buscadorSocio = this.fb.group({
      codigo    : ['',[],[]],
      nombres   : ['',[],[]],
    });
  }

  public construirFormularioSeleccionEvento(){
    this.formularioSeleccionEvento = this.fb.group({
      estado    : ['0',[Validators.required],[]],
      evento    : ['0',[Validators.required],[]],
    });
  }

  public construirTablaListaSocios(){
    this.listaTitulosSociosSegEvento = ['#','Cod. Socio','Nombre completo de socio','Cod. Medidor','Direccion','Hora llegada','Estado seg. Evento','Estado multa','Retraso','Retraso final','Falta','Multa Bs.','Seleccionar'];
    this.listaCamposSociosSegEvento = ['codigoSocio','nombreCompleto','codigoMedidor','direccion','horaLlegada','estadoSeguimientoEvento','estadoMulta','cumplioConRetraso','cumplioConRetrasoFinal','falta'];
  }

  public buscarTextoPorCodigo(){
    let textoInput:string = String(this.buscadorSocio.get('codigo')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('nombres')?.setValue('');
    }


    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.codigoSocio.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }
  
  public buscarTextoPorNombre(){
    let textoInput:string = String(this.buscadorSocio.get('nombres')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('codigo')?.setValue('');
    }

    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.nombreCompleto.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }

  private anlaizarListaSociosEvento(listaSociosSegEvento:SocioSeguimientoVO[]){
    this.nroSociosConRetraso  = 0;
    this.nroSociosConFalta    = 0;

    listaSociosSegEvento.forEach(socioSegEvento => {
      if(socioSegEvento.cumplioConRetraso || socioSegEvento.cumplioConRetrasoFinal){
        this.nroSociosConRetraso+=1;
      }else{
        this.nroSociosConFalta+=1;
      }
    });
  }

  public seleccionarSeguimientoEvento(socioSegEvento:SocioSeguimientoVO){
    socioSegEvento.seleccionado = !socioSegEvento.seleccionado;
  }

  public esTipoBoolean(data:any):boolean{
    return typeof data == "boolean";
  }

  public async cobrarMultaSocio(socioSegEvento:SocioSeguimientoVO,evento:any){
    if(evento.target.checked){      
      let motivo:string = (socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.FALTO)?'falta':'retrazo';

      let afirmativo:Boolean = await this.getConfirmacion(`COBRO DE MULTA POR ${motivo.toUpperCase()} A REUNIÓN`,`¿Está seguro de cobrar multa por ${motivo} al socio ${socioSegEvento.nombreCompleto}?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${socioSegEvento.idSeguimientoEvento}`);     
        element.checked=false;
        return;
      }

      this.updateEstadoMultaSeguimientoEvento(socioSegEvento,EstadoMultaSeguimientoEvento.CANCELADA,true);
    }
  }

  public generarReciboMultaReunionSocioPDF(socioSegEvento:SocioSeguimientoVO,nroRecibo:number){
    let montoMultaAux  : number = 0;
    let motivoMultaAux : string = '';

    if(socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.RETRASO){
      montoMultaAux = socioSegEvento.multaRetraso;
      motivoMultaAux = 'RETRASO';
    }else if(socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.RETRASO_FINAL){
      montoMultaAux = socioSegEvento.multaRetrasoFinal;
      motivoMultaAux = 'RETRASO FINAL';
    }else{
      montoMultaAux = socioSegEvento.multaFalta;
      motivoMultaAux = 'FALTA';
    }


    let socioSeguimientoDataPdf:SocioSeguimientoDataPdf = {
      idSeguimientoEvento     : socioSegEvento.idSeguimientoEvento,
      idEvento                : socioSegEvento.idEvento,
      idSocio                 : socioSegEvento.idSocio,
      codigoSocio             : socioSegEvento.codigoSocio,
      nombreCompleto          : socioSegEvento.nombreCompleto,
      direccion               : socioSegEvento.direccion,
      nroCasa                 : socioSegEvento.nroCasa,
      telefonos               : socioSegEvento.telefonos,
      cumplioEnHora           : socioSegEvento.cumplioEnHora,
      cumplioConRetraso       : socioSegEvento.cumplioConRetraso,
      licencia                : socioSegEvento.licencia,
      horaLlegada             : socioSegEvento.horaLlegada,
      estadoSeguimientoEvento : socioSegEvento.estadoSeguimientoEvento,
      estadoMulta             : socioSegEvento.estadoMulta,
      multaCancelada          : socioSegEvento.multaCancelada,
      multaCondonada          : socioSegEvento.multaCondonada,
      seleccionado            : socioSegEvento.seleccionado,
      
      cancelado               : socioSegEvento.cancelado,
      fechaRegistro           : socioSegEvento.fechaRegistro,
      multaFalta              : socioSegEvento.multaFalta,
      multaRetraso            : socioSegEvento.multaRetraso,
      multaRetrasoFinal       : socioSegEvento.multaRetrasoFinal,
      nroRecibo               : nroRecibo,
      nombreDocumento         : socioSegEvento.nombreDocumento,
      nombreReunion           : this.eventoSeleccionado.nombre,

      fechaActualHibirido   : this.generalService.getFechaActualHibrido(),
      fechaActual           : this.generalService.getFechaActual(),
      horaActual            : this.generalService.getHoraActualFormateado(),
      montoMultaNumero      : montoMultaAux,
      montoMultaLetra       : this. generalService.getNumeroLetra(montoMultaAux),
      motivoMulta           : motivoMultaAux,

      observacion           : 'niguna',
      nombreEncargadoComite : this.generalService.getNombreCompletoUsuarioSession(),
      rolEncargadoComite    : this.generalService.getRolCompletoUsuarioSession(),      
      datosComiteDeAgua     : this.generalService.getDatosComiteDeAgua(),
    } 
    this.pdfMultaReunionesService.generarReciboMultaReunionPDF(socioSeguimientoDataPdf);
  }

  // ----- buscador eventos ------
  public buscarEventoBusqueda(texto:string){
    this.busquedaEventoTodosListarFiltradoService(texto,EstadoEvento.REALIZADO);
  }

  public seleccionarEventoBusqueda(evento:EventoVO){
    this.eventoSeleccionado = evento;
    if(this.eventoSeleccionado == this.aux){
      this.listaSociosEvento = [];
    }else{
      this.getListaTodosLosSociosDeEventoPorEstadoMultaService(this.eventoSeleccionado.idEvento); 
    }
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableCobroRetrazoFaltaAreunion')
    this.generalService.genearteArchivoXLSX(tableElemnt,'cobro por retrazo falta a reunion');
  }

  //-------------------
  // Consumo API-REST |
  //-------------------
  // Evento  
  public busquedaEventoTodosListarFiltradoService(texto:string,estadoEvento:string) {
    this.listaEventosBusqueda = this.eventoService.busquedaEventoTodosFiltradoLista(texto,estadoEvento);    
  }

  // Seguimiento evento
  public getListaTodosLosSociosDeEventoPorEstadoMultaService(idEvento:number){
    this.seguimientoEventoService.getListaTodosLosSociosDeEventoPorEstadoMulta(idEvento,EstadoMultaSeguimientoEvento.PENDIENTE).subscribe(
      (resp)=>{              
        this.listaSociosEvento = resp;    
        if(this.listaSociosEvento.length>0){
          this.anlaizarListaSociosEvento(this.listaSociosEvento);
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateEstadoMultaSeguimientoEvento(socioSegEvento:SocioSeguimientoVO,estadoMulta:string,banderaCancelado:boolean ){
    let multa:number = 0;

    if(socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.RETRASO)
      multa = socioSegEvento.multaRetraso;
    else if(socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.RETRASO_FINAL)
      multa = socioSegEvento.multaRetrasoFinal;
    else // ultimo caso = FALTO
      multa = socioSegEvento.multaFalta;

    this.seguimientoEventoService.updateEstadoMultaSeguimientoEvento(socioSegEvento.idSeguimientoEvento,estadoMulta,banderaCancelado,multa).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se registro el pago de la multa con exito!!!");
        let seguimientoEvento:SeguimientoEventoVO = resp;
        this.generarReciboMultaReunionSocioPDF(socioSegEvento,seguimientoEvento.nroRecibo);
        this.getListaTodosLosSociosDeEventoPorEstadoMultaService(this.eventoSeleccionado.idEvento); 
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

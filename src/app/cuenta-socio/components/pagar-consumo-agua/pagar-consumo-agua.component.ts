import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatosLecturaPeriodoService } from '../../services/datos-lectura-periodo.service';
import { SocioData } from '../../models/socioData';
import { LecturaPeriodoData } from '../../models/lecturaPeriodoData';
import { CobroExtraPeriodoData } from '../../models/cobroExtraPeriodoData';
import { LecturaPeriodoTratadoData } from '../../models/lecturaPeriodoTratadoData';
import { PagoLecturaMedidorPeriodoService } from '../../services/pago-lectura-medidor-periodo.service';
import { LecturaPeriodoDataPdf } from '../../models/lecturaPeriodoDataPdf';
import { ReciboLecturaPeriodoSocioPdfService } from '../../services/recibo-lectura-periodo-socio-pdf.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-pagar-consumo-agua',
  templateUrl: './pagar-consumo-agua.component.html',
  styleUrls: ['./pagar-consumo-agua.component.css']
})


export class PagarConsumoAguaComponent implements OnInit {
  aux:any = undefined;
  pagoLecturaPeriodo:boolean = true;  
  formularioCodigoMedidor!   : FormGroup;

  listaTituloItems           : string[] = [];
  listaCampoLecturaPeriodo   : string[] = [];
  listaLecturaPeriodoTratado : LecturaPeriodoTratadoData[];
  socioData                  : SocioData;
  sumatoriaCosto             : number = 0;

  constructor(private fb                                  : FormBuilder,
              private generalService                      : GeneralService,
              private datosLecturaPeriodoService          : DatosLecturaPeriodoService,
              private pagoLecturaMedidorPeriodoService    : PagoLecturaMedidorPeriodoService,
              private reciboLecturaPeriodoSocioPdfService : ReciboLecturaPeriodoSocioPdfService,
              private dialog                              : MatDialog,
              private router                              : Router) { }

  ngOnInit(): void {
    this.construirFormularioSelccionPeriodoCobroExtra();
    this.construirTituloTabla();
  }

  private construirFormularioSelccionPeriodoCobroExtra(){
    this.formularioCodigoMedidor = this.fb.group({
      codigoMedidor : [ , [Validators.required],[]],
    });
  }

  public campoCodigoValido(campo:string) : boolean{
    return this.formularioCodigoMedidor.controls[campo].valid
  }

  public campoCodigoInvalido(campo:string) : boolean{
    return this.formularioCodigoMedidor.controls[campo].invalid && 
           this.formularioCodigoMedidor.controls[campo].touched;
  }

  private construirTituloTabla(){
    this.listaTituloItems = ['#','Gestión','Nro. Mes','Mes','Lect. Anterior','Lect. Actual','Cons. M3','Conusmo Min','Costo M3','Costo Min','Cancelado','Costo Bs.','Cobro extra','Costo Bs.','Total Bs.','Seleccionar'];
    this.listaCampoLecturaPeriodo = ['indexItem','anio','nroMes','nombreMes','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','consumoMinimo','costoM3','costoMinimoM3','cancelado','costoPeriodo'];
  }

  public buscarSocioDataByCodigoMedidor(){
    let codigoMedidor = this.formularioCodigoMedidor.get('codigoMedidor').value;
    if(codigoMedidor=='' || codigoMedidor==this.aux || codigoMedidor==null)
      return;
    this.sumatoriaCosto = 0;
    this.getDataSocioByCodigoMedidorService(codigoMedidor);
  }

  public inputChangeCodigoMedidor(){
    let codigoMedidor = this.formularioCodigoMedidor.get('codigoMedidor').value;
    if(codigoMedidor=='' || codigoMedidor==this.aux || codigoMedidor==null){
      this.limpiarFormulario();
    }
  }

  public tratarLecturaPeriodo(){
    let indexGlobal:number = 0;
    let indexItem:number = 1;
    
    this.listaLecturaPeriodoTratado = [];
    let lecturaPeriodoSocioList = this.socioData.lecturaPeriodoDataList;
    for (let i = 0; i < lecturaPeriodoSocioList.length; i++) {
      let lecturaPeriodo:LecturaPeriodoData  = lecturaPeriodoSocioList[i];
      let listaCobroExtraPeriodo:CobroExtraPeriodoData[] = lecturaPeriodo.cobroExtraPeriodoDataList;

      if(listaCobroExtraPeriodo.length!=0){
        for (let j = 0; j < listaCobroExtraPeriodo.length; j++) {
          let cobroExtraPeriodoData:CobroExtraPeriodoData = listaCobroExtraPeriodo[j];

          let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
            indexGlobal  : indexGlobal,
            indexItem    : indexItem,
            seleccionado : false,
            checkItem    : false,
            // lectura periodo
            idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
            idPeriodo              : lecturaPeriodo.idPeriodo,
            idAnio                 : lecturaPeriodo.idAnio,
            idMes                  : lecturaPeriodo.idMes,
            anio                   : lecturaPeriodo.anio,
            nroMes                 : lecturaPeriodo.nroMes,
            nombreMes              : lecturaPeriodo.nombreMes,
            lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
            lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
            consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
            consumoMinimo          : lecturaPeriodo.consumoMinimo,
            costoM3                : lecturaPeriodo.costoM3,
            costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
            costoPeriodo           : lecturaPeriodo.costoPeriodo,
            cancelado              : lecturaPeriodo.cancelado,
            sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
            countItemsCobroExtra   : lecturaPeriodo.countItemsCobroExtra,
            // cobro extra
            idCobroExtra     : cobroExtraPeriodoData.idCobroExtra,
            nombreCobroExtra : cobroExtraPeriodoData.nombreCobroExtra,
            costoCobroExtra  : cobroExtraPeriodoData.costoCobroExtra,
          } 
          this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
          indexGlobal += 1;
        }
        indexGlobal = 0;
        indexItem += 1;
      }else{
        let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
          indexGlobal  : 0,
          indexItem    : indexItem,     
          seleccionado : false,   
          checkItem    : false,
          // lectura periodo
          idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
          idPeriodo              : lecturaPeriodo.idPeriodo,
          idAnio                 : lecturaPeriodo.idAnio,
          idMes                  : lecturaPeriodo.idMes,
          anio                   : lecturaPeriodo.anio,
          nroMes                 : lecturaPeriodo.nroMes,
          nombreMes              : lecturaPeriodo.nombreMes,
          lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
          lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
          consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
          consumoMinimo          : lecturaPeriodo.consumoMinimo,
          costoM3                : lecturaPeriodo.costoM3,
          costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
          costoPeriodo           : lecturaPeriodo.costoPeriodo,
          cancelado              : lecturaPeriodo.cancelado,
          sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
          countItemsCobroExtra   : 1,
          
          // cobro extra
          idCobroExtra     : this.aux,
          nombreCobroExtra : this.aux,
          costoCobroExtra  : this.aux,
        } 
        this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
        indexGlobal = 0;
        indexItem += 1;
      }  
    }    
  }

  public seleccionarItem(lecturaPeriodo:LecturaPeriodoTratadoData){
    this.deseleccionarItems();
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodoAux => {
      if(lecturaPeriodo.idLecturaPeriodo == lecturaPeriodoAux.idLecturaPeriodo)
        lecturaPeriodoAux.seleccionado = true;
    });
  }

  private deseleccionarItems(){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      lecturaPeriodo.seleccionado = false;
    });
  }

  public checkLecturaPeriodo(event:any,lecturaPeriodo:LecturaPeriodoTratadoData){
    this.checkLecturaPeriodoData(event.target.checked,lecturaPeriodo.idLecturaPeriodo);
    if(event.target.checked){
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto+lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }else{
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto-lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }
  }

  private checkLecturaPeriodoData(bandera:boolean,idLecturaPeriodo:number,){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(idLecturaPeriodo == lecturaPeriodo.idLecturaPeriodo){
        lecturaPeriodo.checkItem = bandera;
      }
    });
  }

  public cancelarRegistroPagoLecturaPeriodo(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioCodigoMedidor.get('codigoMedidor').setValue('');
    this.socioData = this.aux;
    this.listaLecturaPeriodoTratado = [];
  }

  public async registrarCobroLecturaPeriodo(){
    if(this.sumatoriaCosto==0){
      this.generalService.mensajeError("No hay periodos seleccionados, seleccione uno");
      return;
    }

    const seleccionContinuaAsendente:boolean = this.verificarSeleccionContinuaAsendente(this.listaLecturaPeriodoTratado);   
    if(!seleccionContinuaAsendente){
      this.generalService.mensajeError("Seleccione los periodos de manera asendente y continua, empezando del periodo mas antiguo");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR COBRO DE LECTURA PERIODO',`¿Está seguro registrar el cobro de los periodos seleccionado ?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){    
      return;
    }
    
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(lecturaPeriodo.indexGlobal==0 && lecturaPeriodo.checkItem==true){
        this.registroPagoLecturaSocioMedidorService(lecturaPeriodo,this.pagoLecturaPeriodo);
      }
    });
  }

  private verificarSeleccionContinuaAsendente(lecturaPeriodo:LecturaPeriodoTratadoData[]):boolean{
    const indexUltimo = this.getIndexUltimoItemSeleccionado(lecturaPeriodo);
    
    let bandera = true;
    for(let i=indexUltimo; i>=0; i--){
      if(!lecturaPeriodo[i].checkItem==true)
        bandera = false;
    }
    return bandera;
  }

  private getIndexUltimoItemSeleccionado(lecturaPeriodo:LecturaPeriodoTratadoData[]):number{
    let index = lecturaPeriodo.length-1;

    let encontrado:boolean = false;
    while(index>=0 && !encontrado){
      if(lecturaPeriodo[index].checkItem==true)
        encontrado=true;
      else
        index-=1;
    }
    return index;
  }

  

  // generate pdf
  public generateReciboLecturaPeriodoPdf(lecturaPeriodo:LecturaPeriodoTratadoData,nroRecibo:number){
    let lecturaPeriodoDataPdf : LecturaPeriodoDataPdf = this.getLecturaPeriodoDataPdf(lecturaPeriodo,nroRecibo);
    this.reciboLecturaPeriodoSocioPdfService.generarReciboPorCobroLecturaPeriodoDeSocioPDF(lecturaPeriodoDataPdf);
  }

  private getLecturaPeriodoDataPdf(lecturaPeriodo:LecturaPeriodoTratadoData,nroRecibo:number){
    let cobroExtraPeriodoDataList : CobroExtraPeriodoData[] = this.getCobrosExtraFromLecturaPeriodo(lecturaPeriodo.idLecturaPeriodo);
    let lecturaPeriodoDataPdf:LecturaPeriodoDataPdf = {
      // socio
      codigoSocio             : this.socioData.codigoSocio,
      nombreCompletoSocio     : `${this.socioData.nombreSocio} ${this.socioData.apellidoPaterno} ${this.socioData.apellidoMaterno}`,
      direccionSocio          : this.socioData.direccion,
      codigoMedidor           : this.socioData.codigoMedidor,
      codigoCategoria         : this.socioData.codigoCategoria,
      nombreCategoria         : this.socioData.nombreCategoria,
      //lectura periodo
      idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
      anio                   : lecturaPeriodo.anio,
      nroMes                 : lecturaPeriodo.nroMes,
      nombreMes              : lecturaPeriodo.nombreMes,
      lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
      lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
      consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
      consumoMinimo          : lecturaPeriodo.consumoMinimo,
      costoM3                : lecturaPeriodo.costoM3,
      costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
      costoPeriodo           : lecturaPeriodo.costoPeriodo,
      sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
      //cobro extra
      cobroExtraPeriodoDataList : cobroExtraPeriodoDataList,

      // recibo
      nroRecibo             : nroRecibo,
      motivoRecibo          : "cobro por consumo de agua",
      fechaActualHibirido   : this.generalService.getFechaActualHibrido(),
      fechaActual           : this.generalService.getFechaActual(),
      horaActual            : this.generalService.getHoraActualFormateado(),
      montoIngresoNumero    : lecturaPeriodo.sumaCostoPeriodo,
      montoIngresoLetra     : this. generalService.getNumeroLetra(lecturaPeriodo.sumaCostoPeriodo),
      observacion           : "ninguna",
      nombreEncargadoComite : this.generalService.getNombreCompletoUsuarioSession().toUpperCase(),
      rolEncargadoComite    : this.generalService.getRolCompletoUsuarioSession().toUpperCase(),
      datosComiteDeAgua     : this.generalService.getDatosComiteDeAgua(),
    }
    return lecturaPeriodoDataPdf;
  }

  private getCobrosExtraFromLecturaPeriodo(idLecturaPeriodoIn:number) : CobroExtraPeriodoData[] {
    let cobroExtraDataList:CobroExtraPeriodoData[]=[];
    let i:number = 0;
    let encontrado = false;
    let lecturaPeriodoDataList:LecturaPeriodoData[] = this.socioData.lecturaPeriodoDataList;

    while(i<lecturaPeriodoDataList.length && !encontrado) {
      if(lecturaPeriodoDataList[i].idLecturaPeriodo == idLecturaPeriodoIn){
        encontrado = true;
        cobroExtraDataList = lecturaPeriodoDataList[i].cobroExtraPeriodoDataList;
      }
      i++;
    }
    return cobroExtraDataList;
  }

  public getValue(value:any){
    return (typeof value == 'boolean')? (value==true)?'SI':'NO':value;
  }

  public isTypeOfBoolean(value:any){
    return typeof value == 'boolean';
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableCobroLecturaPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'cobro lectura periodo socio');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  // mes anio categoria costo agua service
  public getDataSocioByCodigoMedidorService(codigoMedidor:string){
    this.datosLecturaPeriodoService.getDataSocioByCodigoMedidor(codigoMedidor).subscribe(
      (resp)=>{                
        if(resp!=null){          
          this.socioData = resp;
          this.tratarLecturaPeriodo();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.socioData = this.aux;
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.listaLecturaPeriodoTratado = [];
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public registroPagoLecturaSocioMedidorService(lecturaPeriodo:LecturaPeriodoTratadoData,bandera:boolean){
    this.pagoLecturaMedidorPeriodoService.registroPagoLecturaSocioMedidor(lecturaPeriodo.idLecturaPeriodo,bandera).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se registro con exito el cobro de los periodos seleccionados!!!");
        this.getDataSocioByCodigoMedidorService(this.formularioCodigoMedidor.get('codigoMedidor').value);
        this.sumatoriaCosto = 0;
        this.generateReciboLecturaPeriodoPdf(lecturaPeriodo,resp.nro);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

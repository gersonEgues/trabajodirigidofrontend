import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { SocioVO } from 'src/app/shared/models/socio-vo';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MedidorAsignadoVO } from 'src/app/socio-medidor/models/medidorAsignadoVO';
import { SocioMedidorService } from 'src/app/socio-medidor/services/socio-medidor.service';
import { DatosLecturaPeriodoService } from '../../services/datos-lectura-periodo.service';
import { LecturaPeriodoTratadoData } from '../../models/lecturaPeriodoTratadoData';
import { LecturaPeriodoData } from '../../models/lecturaPeriodoData';
import { CobroExtraPeriodoData } from '../../models/cobroExtraPeriodoData';
import { SocioData } from '../../models/socioData';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PagoLecturaMedidorPeriodoService } from '../../services/pago-lectura-medidor-periodo.service';
import { LecturaPeriodoDataPdf } from '../../models/lecturaPeriodoDataPdf';
import { ReciboLecturaPeriodoSocioPdfService } from '../../services/recibo-lectura-periodo-socio-pdf.service';
import { LecturaMedidorPorUsuarioService } from 'src/app/consumo-agua-socio/services/lectura-medidor-por-usuario.service';

@Component({
  selector: 'app-historial-pagos-consumo-agua-socios-con-baja',
  templateUrl: './historial-pagos-consumo-agua-socios-con-baja.component.html',
  styleUrls: ['./historial-pagos-consumo-agua-socios-con-baja.component.css']
})
export class HistorialPagosConsumoAguaSociosConBajaComponent implements OnInit {
  aux:undefined = null;
  // Buscar Socio
  tituloItems          : string[] = [];
  campoItems           : string[] = [];
  socioSeleccionado!   : SocioVO;

  // medidores de socio
  tituloItemsMedidor     : string[] = [];
  campoItemsMedidor      : string[] = [];
  idItemMedidor          : string = '';
  listaMedidoresDeSocio! : MedidorAsignadoVO[];
  medidorSeleccionado!   : MedidorAsignadoVO;
  
  // lecturas periodo
  listaTituloItems           : string[] = [];
  listaCampoLecturaPeriodo   : string[] = [];
  pagoLecturaPeriodo         : boolean = false;
  estadoLecturaPeriodo       : boolean = true;
  listaLecturaPeriodoTratado : LecturaPeriodoTratadoData[];
  socioData                  : SocioData;
  sumatoriaCosto             : number = 0;

  constructor(private generalService                      : GeneralService,
              private socioMedidorService                 : SocioMedidorService,
              private datosLecturaPeriodoService          : DatosLecturaPeriodoService,
              private lecturaMedidorPorUsuarioService     : LecturaMedidorPorUsuarioService,
              private pagoLecturaMedidorPeriodoService    : PagoLecturaMedidorPeriodoService,
              private reciboLecturaPeriodoSocioPdfService : ReciboLecturaPeriodoSocioPdfService,
              private dialog                              : MatDialog,
              private router                              : Router) { }
              
  ngOnInit(): void {
    this.configurarDatosComponenteBuscador();
    this.configurarDatosTablaMedidoresDeSocio();
    this.construirTituloTablaLecturaPeriodo();
  }

  // BUSQUEDA DE SOCIOS
  private configurarDatosComponenteBuscador(){
    this.tituloItems = ['#','Código','Nombre','Ape. Paterno','Ape. Materno','C.I.','Telefono','Estado'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','ci','telefono','estado'];
  }

  public socioSeleccionadoEvent(socio:SocioVO){
    this.socioSeleccionado = socio;
    this.getMedidoresDeSocioConBajaSocioMedidorService(this.socioSeleccionado.idSocio);
  }

  public nuevaBusquedaBuscadorSocio(busqueda:string){
    this.limpiarResultadoDeSocioEncontrados();
    this.limpiarResultadosMedidoresDeSocio();
    this.limpiarResultadosLecturaPeriodo();
  }

  public limpiarResultadoDeSocioEncontrados(){
    this.socioSeleccionado = this.aux;
  }

  // MEDIDORES DE SOCIO
  private configurarDatosTablaMedidoresDeSocio(){
    this.tituloItemsMedidor = ['#','Código','L. Inicial','L. Actual','Fecha asignacion','Direccion','Prop. Coope.','Cod. Cat','Tanque','Med. Asg. a Socio','Medidor disponible'];
    this.campoItemsMedidor = ['codigo','lecturaInicial','lecturaActual','fechaAsignacionSM','direccionSM','propiedadCooperativa','codigoCategoria','nombreTanque','estadoSocioMedidor','estadoMedidor'];
    this.idItemMedidor = 'idSocioMedidor';
  }

  public medidorSeleccionadoEvent(medidor:MedidorAsignadoVO) {
    this.medidorSeleccionado = medidor;
    this.getLecturaPeroidoDeSocioByIdSocioMedidorSocMedInac(this.medidorSeleccionado.idSocioMedidor,this.estadoLecturaPeriodo);
    this.limpiarResultadosLecturaPeriodo();
  }

  public limpiarResultadosMedidoresDeSocio(){
    this.listaMedidoresDeSocio = this.aux;
    this.medidorSeleccionado = this.aux;
  }

  // LECTURA PERIODO
  private construirTituloTablaLecturaPeriodo(){
    this.listaTituloItems = ['#','Gestión','Nro. Mes','Mes','Lect. Anterior','Lect. Actual','Cons. M3','Conusmo Min','Costo M3','Costo Min','Cancelado','Costo Bs.','Cobro extra','Costo Bs.','Total Bs.','Seleccionar','Opciones'];
    this.listaCampoLecturaPeriodo = ['indexItem','anio','nroMes','nombreMes','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','consumoMinimo','costoM3','costoMinimoM3','cancelado','costoPeriodo'];
  }
  
  public seleccionarItemLecturaPeriodo(lecturaPeriodo:LecturaPeriodoTratadoData){
    this.deseleccionarItemsLecturaPeriodo();
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodoAux => {
      if(lecturaPeriodo.idLecturaPeriodo == lecturaPeriodoAux.idLecturaPeriodo)
        lecturaPeriodoAux.seleccionado = true;
    });
  }

  private deseleccionarItemsLecturaPeriodo(){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      lecturaPeriodo.seleccionado = false;
    });
  }

  public getValueLecturaPeriodo(value:any){
    return (typeof value == 'boolean')? (value==true)?'SI':'NO':value;
  }

  public isTypeOfBooleanLecturaPeriodo(value:any){
    return typeof value == 'boolean';
  }

  public checkLecturaPeriodo(event:any,lecturaPeriodo:LecturaPeriodoTratadoData){
    this.checkLecturaPeriodoData(event.target.checked,lecturaPeriodo.idLecturaPeriodo);
    if(event.target.checked){
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto+lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }else{
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto-lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }
  }

  private checkLecturaPeriodoData(bandera:boolean,idLecturaPeriodo:number,){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(idLecturaPeriodo == lecturaPeriodo.idLecturaPeriodo){
        lecturaPeriodo.checkItem = bandera;
      }
    });
  }

  public cancelarRegistroPagoLecturaPeriodo(){
    this.limpiarResultadoDeSocioEncontrados();
    this.limpiarResultadosMedidoresDeSocio();
    this.limpiarResultadosLecturaPeriodo();
  }

  public async canecelarRegistroCobroLecturaPeriodo(){
    if(this.sumatoriaCosto==0){
      this.generalService.mensajeAlerta("No hay periodos seleccionados, seleccione uno");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REVERTIR COBRO REALIZADO POR CONSUMO DE AGUA',`¿Está seguro revertir el cobro de los periodos seleccionado ?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){    
      return;
    }
        
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(lecturaPeriodo.indexGlobal==0 && lecturaPeriodo.checkItem==true){
        this.revertirRegistroPagoLecturaSocioMedidorService(lecturaPeriodo.idLecturaPeriodo,this.pagoLecturaPeriodo);
      }
    });
  }


  public limpiarResultadosLecturaPeriodo(){
    this.listaLecturaPeriodoTratado = this.aux;
    this.socioData = this.aux;
    this.sumatoriaCosto = 0;
  }

  public tratarLecturaPeriodo(){
    let indexGlobal:number = 0;
    let indexItem:number = 1;
    
    this.listaLecturaPeriodoTratado = [];
    let lecturaPeriodoSocioList = this.socioData.lecturaPeriodoDataList;
    for (let i = 0; i < lecturaPeriodoSocioList.length; i++) {
      let lecturaPeriodo:LecturaPeriodoData  = lecturaPeriodoSocioList[i];
      let listaCobroExtraPeriodo:CobroExtraPeriodoData[] = lecturaPeriodo.cobroExtraPeriodoDataList;

      if(listaCobroExtraPeriodo.length!=0){
        for (let j = 0; j < listaCobroExtraPeriodo.length; j++) {
          let cobroExtraPeriodoData:CobroExtraPeriodoData = listaCobroExtraPeriodo[j];

          let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
            indexGlobal  : indexGlobal,
            indexItem    : indexItem,     
            seleccionado : false, 
            checkItem    : false,  
            // lectura periodo
            idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
            idPeriodo              : lecturaPeriodo.idPeriodo,
            idAnio                 : lecturaPeriodo.idAnio,
            idMes                  : lecturaPeriodo.idMes,
            anio                   : lecturaPeriodo.anio,
            nroMes                 : lecturaPeriodo.nroMes,
            nombreMes              : lecturaPeriodo.nombreMes,
            lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
            lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
            consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
            consumoMinimo          : lecturaPeriodo.consumoMinimo,
            costoM3                : lecturaPeriodo.costoM3,
            costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
            costoPeriodo           : lecturaPeriodo.costoPeriodo,
            cancelado              : lecturaPeriodo.cancelado,
            sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
            countItemsCobroExtra   : lecturaPeriodo.countItemsCobroExtra,
            // cobro extra
            idCobroExtra     : cobroExtraPeriodoData.idCobroExtra,
            nombreCobroExtra : cobroExtraPeriodoData.nombreCobroExtra,
            costoCobroExtra  : cobroExtraPeriodoData.costoCobroExtra,
          } 
          this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
          indexGlobal += 1;
        }
        indexGlobal = 0;
        indexItem += 1;
      }else{
        let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
          indexGlobal  : 0,
          indexItem    : indexItem,     
          seleccionado : false,   
          checkItem    : false,
          // lectura periodo
          idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
          idPeriodo              : lecturaPeriodo.idPeriodo,
          idAnio                 : lecturaPeriodo.idAnio,
          idMes                  : lecturaPeriodo.idMes,
          anio                   : lecturaPeriodo.anio,
          nroMes                 : lecturaPeriodo.nroMes,
          nombreMes              : lecturaPeriodo.nombreMes,
          lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
          lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
          consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
          consumoMinimo          : lecturaPeriodo.consumoMinimo,
          costoM3                : lecturaPeriodo.costoM3,
          costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
          costoPeriodo           : lecturaPeriodo.costoPeriodo,
          cancelado              : lecturaPeriodo.cancelado,
          sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
          countItemsCobroExtra   : 1,
          
          // cobro extra
          idCobroExtra     : this.aux,
          nombreCobroExtra : this.aux,
          costoCobroExtra  : this.aux,
        } 
        this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
        indexGlobal = 0;
        indexItem += 1;
      }      
    }
  }

  // generate pdf
  public generateReciboLecturaPeriodoPdf(lecturaPeriodo:LecturaPeriodoTratadoData,nroRecibo:number){
    let lecturaPeriodoDataPdf : LecturaPeriodoDataPdf = this.getLecturaPeriodoDataPdf(lecturaPeriodo,nroRecibo);
    this.reciboLecturaPeriodoSocioPdfService.generarReciboPorCobroLecturaPeriodoDeSocioPDF(lecturaPeriodoDataPdf);
  }

  private getLecturaPeriodoDataPdf(lecturaPeriodo:LecturaPeriodoTratadoData,nroRecibo:number){
    let cobroExtraPeriodoDataList : CobroExtraPeriodoData[] = this.getCobrosExtraFromLecturaPeriodo(lecturaPeriodo.idLecturaPeriodo);
    let lecturaPeriodoDataPdf:LecturaPeriodoDataPdf = {
      // socio
      codigoSocio             : this.socioData.codigoSocio,
      nombreCompletoSocio     : `${this.socioData.nombreSocio} ${this.socioData.apellidoPaterno} ${this.socioData.apellidoMaterno}`,
      direccionSocio          : this.socioData.direccion,
      codigoMedidor           : this.socioData.codigoMedidor,
      codigoCategoria         : this.socioData.codigoCategoria,
      nombreCategoria         : this.socioData.nombreCategoria,
      //lectura periodo
      idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
      anio                   : lecturaPeriodo.anio,
      nroMes                 : lecturaPeriodo.nroMes,
      nombreMes              : lecturaPeriodo.nombreMes,
      lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
      lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
      consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
      consumoMinimo          : lecturaPeriodo.consumoMinimo,
      costoM3                : lecturaPeriodo.costoM3,
      costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
      costoPeriodo           : lecturaPeriodo.costoPeriodo,
      sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
      //cobro extra
      cobroExtraPeriodoDataList : cobroExtraPeriodoDataList,

      // recibo
      nroRecibo             : nroRecibo,
      motivoRecibo          : "cobro de lectura de medidor",
      fechaActualHibirido   : this.generalService.getFechaActualHibrido(),
      fechaActual           : this.generalService.getFechaActual(),
      horaActual            : this.generalService.getHoraActualFormateado(),
      montoIngresoNumero    : lecturaPeriodo.sumaCostoPeriodo,
      montoIngresoLetra     : this. generalService.getNumeroLetra(lecturaPeriodo.sumaCostoPeriodo),
      observacion           : "ninguna",
      nombreEncargadoComite : this.generalService.getNombreCompletoUsuarioSession().toUpperCase(),
      rolEncargadoComite    : this.generalService.getRolCompletoUsuarioSession().toUpperCase(),
      datosComiteDeAgua     : this.generalService.getDatosComiteDeAgua(),
    }
    return lecturaPeriodoDataPdf;
  }

  private getCobrosExtraFromLecturaPeriodo(idLecturaPeriodoIn:number) : CobroExtraPeriodoData[] {
    let cobroExtraDataList:CobroExtraPeriodoData[]=[];
    let i:number = 0;
    let encontrado = false;
    let lecturaPeriodoDataList:LecturaPeriodoData[] = this.socioData.lecturaPeriodoDataList;

    while(i<lecturaPeriodoDataList.length && !encontrado) {
      if(lecturaPeriodoDataList[i].idLecturaPeriodo == idLecturaPeriodoIn){
        encontrado = true;
        cobroExtraDataList = lecturaPeriodoDataList[i].cobroExtraPeriodoDataList;
      }
      i++;
    }
    return cobroExtraDataList;
  }

  public descargarRecibo(idLecturaPeriodo:number){
    this.descargarReciboSocioLecturaMedidorService(idLecturaPeriodo);
  }


  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableLecturaPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'cobro lectura periodo socio');
  }
  
  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private getMedidoresDeSocioConBajaSocioMedidorService(idSocio:number){
    this.socioMedidorService.getMedidoresDeSocioConBajaSocioMedidor(idSocio).subscribe(
      (resp:MedidorAsignadoVO[])=>{        
        if(resp && resp.length>0){
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
          this.listaMedidoresDeSocio = resp;
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.listaMedidoresDeSocio = this.aux;
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getLecturaPeroidoDeSocioByIdSocioMedidorSocMedInac(idSocioMedidor:number,estadoLecturaPeriodo:boolean) {
    this.datosLecturaPeriodoService.getLecturaPeroidoDeSocioByIdSocioMedidorSocMedInac(idSocioMedidor,estadoLecturaPeriodo).subscribe(
      (resp)=>{
        if(resp!=null){
          this.socioData = resp;          
          this.tratarLecturaPeriodo();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.socioData = this.aux;
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
          this.listaLecturaPeriodoTratado = [];
        }
        
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public revertirRegistroPagoLecturaSocioMedidorService(idlecturaPeriodo:number,pagoLecturaPeriodo:boolean){
    this.pagoLecturaMedidorPeriodoService.registroPagoLecturaSocioMedidor(idlecturaPeriodo,pagoLecturaPeriodo).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se revirtio el registro del cobro de los periodos seleccionados!!!");
        this.getLecturaPeroidoDeSocioByIdSocioMedidorSocMedInac(this.medidorSeleccionado.idSocioMedidor,this.estadoLecturaPeriodo);
        this.sumatoriaCosto = 0;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //----------------
  // documento pdf |
  //----------------
  public descargarReciboSocioLecturaMedidorService(idLeturaPeriodo:number){
    this.lecturaMedidorPorUsuarioService.descargarReciboSocioLecturaMedidor(idLeturaPeriodo).subscribe(
      (resp)=>{
        if(resp){
          this.generalService.mensajeCorrecto("Se descargo correctamente la lectura del periodo");
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url); 
        }
      },
      (err)=>{      
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{          
          this.generalService.mensajeError("El documento no existe");
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}
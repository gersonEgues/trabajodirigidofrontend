import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { BuscadorComponent } from 'src/app/shared/components/buscador/buscador.component';
import { Observable } from 'rxjs';
import { EventoVO } from 'src/app/eventos/models/eventoVO';
import { EventoService } from 'src/app/eventos/services/evento.service';
import { SeguimientoEventoService } from 'src/app/eventos/services/seguimiento-evento.service';
import { SocioSeguimientoVO } from 'src/app/eventos/models/socioSeguimientoVO';
import { EstadoEvento } from 'src/app/eventos/models/estadoEvento';
import { EstadoMultaSeguimientoEvento } from '../../../eventos/models/estadoMultaSeguimientoEvento';
import { EstadoSeguimientoEvento } from '../../../eventos/models/estadoSeguimientoEvento';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-historial-falta-reuniones',
  templateUrl: './historial-falta-reuniones.component.html',
  styleUrls: ['./historial-falta-reuniones.component.css']
})
export class HistorialFaltaReunionesComponent implements OnInit {
  aux : any = undefined;

  // Buscador
  listaEventosBusqueda!    : Observable<EventoVO[]>;
  eventoSeleccionado       : EventoVO = this.aux;
  @ViewChild('buscardorComponent') buscardorComponent! : BuscadorComponent;

  //Tabla lista de socios
  formularioSeleccionEvento!   : FormGroup;
  buscadorSocio!               : FormGroup;
  listaTitulosSociosSegEvento! : string[];
  listaCamposSociosSegEvento!  : string[];
  listaSociosEvento!           : any[]; // SocioSeguimientoVO, si pongo su tipado, en html no lo puedo generalizar: item[campo]
    
  nroSociosConRetraso          : number = 0;
  nroSociosConFalta            : number = 0;

  constructor(private fb                       : FormBuilder,
              private generalService           : GeneralService,
              private eventoService            : EventoService,
              private seguimientoEventoService : SeguimientoEventoService,
              private dialog                   : MatDialog,
              private router                   : Router) { }

  ngOnInit(): void {
    this.construirFormularioBuscadorSocio();

    this.construirTablaListaSocios();
    this.construirFormularioSeleccionEvento();
  }

  public construirFormularioBuscadorSocio(){
    this.buscadorSocio = this.fb.group({
      codigo    : ['',[],[]],
      nombres   : ['',[],[]],
    });
  }

  public construirFormularioSeleccionEvento(){
    this.formularioSeleccionEvento = this.fb.group({
      estado    : ['0',[Validators.required],[]],
      evento    : ['0',[Validators.required],[]],
    });
  }

  public construirTablaListaSocios(){
    if(this.isPresidente() || this.isAdmin())
      this.listaTitulosSociosSegEvento = ['#','Código','Nombre completo de socio','Cod. Medidor','Direccion','Hora llegada','Fecha cobro','Estado seg. evento','Estado multa','Retraso','Retraso final','Falta','Multa Bs.','Cancelado','Revertir','Descargar'];
    else
      this.listaTitulosSociosSegEvento = ['#','Código','Nombre completo de socio','Cod. Medidor','Direccion','Hora llegada','Fecha cobro','Estado seg. evento','Estado multa','Retraso','Retraso final','Falta','Multa Bs.','Cancelado','Descargar'];
    this.listaCamposSociosSegEvento = ['codigoSocio','nombreCompleto','codigoMedidor','direccion','horaLlegada','fechaRegistro','estadoSeguimientoEvento','estadoMulta','cumplioConRetraso','cumplioConRetrasoFinal','falta'];
  }

  public buscarTextoPorCodigo(){
    let textoInput:string = String(this.buscadorSocio.get('codigo')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('nombres')?.setValue('');
    }


    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.codigoSocio.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }
  
  public buscarTextoPorNombre(){
    let textoInput:string = String(this.buscadorSocio.get('nombres')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('codigo')?.setValue('');
    }

    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.nombreCompleto.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }

  private anlaizarListaSociosEvento(listaSociosSegEvento:SocioSeguimientoVO[]){
    this.nroSociosConRetraso  = 0;
    this.nroSociosConFalta    = 0;

    listaSociosSegEvento.forEach(socioSegEvento => {
      if(socioSegEvento.cumplioConRetraso){
        this.nroSociosConRetraso+=1;
      }else{
        this.nroSociosConFalta+=1;
      }
    });
  }

  public seleccionarSeguimientoEvento(socioSegEvento:SocioSeguimientoVO){
    socioSegEvento.seleccionado = !socioSegEvento.seleccionado;
  }

  public esTipoBoolean(data:any):boolean{
    return typeof data == "boolean";
  }

  public async revertirCobroMultaSocio(socioSegEvento:SocioSeguimientoVO,evento:any){
    if(evento.target.checked){
      let motivo:string = (socioSegEvento.estadoSeguimientoEvento == EstadoSeguimientoEvento.FALTO)?'falta':'retrazo';
      let afirmativo:Boolean = await this.getConfirmacion(`REVERTIR COBRO DE MULTA POR ${motivo.toUpperCase()} A REUNIÓN`,`¿Está seguro de revertir el cobro de multa por ${motivo} al socio ${socioSegEvento.nombreCompleto}?`,`Sí, estoy seguro`,`No, Cancelar`);
      if(!afirmativo){    
        let element = <HTMLInputElement>document.getElementById(`check${socioSegEvento.idSeguimientoEvento}`);     
        element.checked=false;
        return;
      }

      let multa:number = 0;
      this.updateEstadoMultaSeguimientoEvento(socioSegEvento.idSeguimientoEvento,EstadoMultaSeguimientoEvento.PENDIENTE,false,multa);
    }
  }

  // ----- buscador eventos ------
  public buscarEventoBusqueda(texto:string){
    this.busquedaEventoTodosListarFiltradoService(texto,EstadoEvento.REALIZADO);
  }

  public seleccionarEventoBusqueda(evento:EventoVO){
    this.eventoSeleccionado = evento;
    if(this.eventoSeleccionado==this.aux){
      this.listaSociosEvento = [];
    }else{
      this.getListaTodosLosSociosDeEventoPorEstadoMultaService(this.eventoSeleccionado.idEvento); 
    }
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public descargarRecibo(idSeguimientoEvento:number){
    this.descargarReciboSocioLecturaMedidorService(idSeguimientoEvento);
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableHistorialCobroLecturaPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de lectura de medidores de los socios por periodo');
  }

  //-------------------
  // Consumo API-REST |
  //-------------------
  // Evento  
  public busquedaEventoTodosListarFiltradoService(texto:string,estadoEvento:string) {
    this.listaEventosBusqueda = this.eventoService.busquedaEventoTodosFiltradoLista(texto,estadoEvento);
  }

  // Seguimiento evento
  public getListaTodosLosSociosDeEventoPorEstadoMultaService(idEvento:number){
    this.seguimientoEventoService.getListaTodosLosSociosDeEventoPorEstadoMulta(idEvento,EstadoMultaSeguimientoEvento.CANCELADA).subscribe(
      (resp)=>{
        this.listaSociosEvento = resp;
        if(this.listaCamposSociosSegEvento.length>0){
          this.anlaizarListaSociosEvento(this.listaSociosEvento);
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateEstadoMultaSeguimientoEvento(idSeguimientoEvento:number,estadoMulta:string,banderaCancelado:boolean,multa:number){
    this.seguimientoEventoService.updateEstadoMultaSeguimientoEvento(idSeguimientoEvento,estadoMulta,banderaCancelado,multa).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se registro el pago de la multa con exito!!!");
        this.getListaTodosLosSociosDeEventoPorEstadoMultaService(this.eventoSeleccionado.idEvento); 
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //----------------
  // documento pdf |
  //----------------
  public descargarReciboSocioLecturaMedidorService(idSeguimientoEvento:number){
    this.seguimientoEventoService.descargarDocumentoReciboMultaReunion(idSeguimientoEvento).subscribe(
      (resp)=>{
        if(resp){
          this.generalService.mensajeCorrecto("Se descargo correctamente el recibo de la multa por la reunion");
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url); 
        }
      },
      (err)=>{      
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

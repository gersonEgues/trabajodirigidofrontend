import { Component, OnInit } from '@angular/core';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatosLecturaPeriodoService } from '../../services/datos-lectura-periodo.service';
import { SocioData } from '../../models/socioData';
import { LecturaPeriodoData } from '../../models/lecturaPeriodoData';
import { CobroExtraPeriodoData } from '../../models/cobroExtraPeriodoData';
import { LecturaPeriodoTratadoData } from '../../models/lecturaPeriodoTratadoData';
import { PagoLecturaMedidorPeriodoService } from '../../services/pago-lectura-medidor-periodo.service';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { LecturaMedidorPorUsuarioService } from 'src/app/consumo-agua-socio/services/lectura-medidor-por-usuario.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-historial-pagos-consumo-agua',
  templateUrl: './historial-pagos-consumo-agua.component.html',
  styleUrls: ['./historial-pagos-consumo-agua.component.css']
})
export class HistorialPagosConsumoAguaComponent implements OnInit {
  aux:any = undefined;
  pagoLecturaPeriodo : boolean  = false;
  listaGestiones     : AnioVO[] = [];

  formularioCodigoMedidor! : FormGroup;

  listaTituloItems           : string[] = [];
  listaCampoLecturaPeriodo   : string[] = [];
  listaLecturaPeriodoTratado : LecturaPeriodoTratadoData[];
  socioData                  : SocioData;
  sumatoriaCosto:number = 0;


  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private datosLecturaPeriodoService       : DatosLecturaPeriodoService,
              private pagoLecturaMedidorPeriodoService : PagoLecturaMedidorPeriodoService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private lecturaMedidorPorUsuarioService  : LecturaMedidorPorUsuarioService,
              private dialog                           : MatDialog,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirFormularioSelccionPeriodoCobroExtra();
    this.construirTituloTabla();
    this.getAniosService();
  }

  private construirFormularioSelccionPeriodoCobroExtra(){
    this.formularioCodigoMedidor = this.fb.group({
      codigoMedidor : [ , [Validators.required],[]],
      gestion    :  [ '0', [Validators.required],[]],
    });
  }

  public campoGestionValido():boolean{
    return  this.formularioCodigoMedidor.get('gestion')!.valid && 
            this.formularioCodigoMedidor.get('gestion')!.value != '0';
  }

  public campoGestionInvalido():boolean{
    return  this.formularioCodigoMedidor.get('gestion')!.touched && 
            this.formularioCodigoMedidor.get('gestion')!.value == '0';
  }

  private construirTituloTabla(){
    if(this.isPresidente() || this.isAdmin())
      this.listaTituloItems = ['#','Gestión','Nro. Mes','Mes','Fecha cobro','Lect. Anterior','Lect. Actual','Cons. M3','Conusmo Min','Costo M3','Costo Min','Cancelado','Costo Bs.','Cobro extra','Costo Bs.','Total Bs.','Seleccionar','Opciones'];
    else
      this.listaTituloItems = ['#','Gestión','Nro. Mes','Mes','Fecha cobro','Lect. Anterior','Lect. Actual','Cons. M3','Conusmo Min','Costo M3','Costo Min','Cancelado','Costo Bs.','Cobro extra','Costo Bs.','Total Bs.','Opciones'];
    this.listaCampoLecturaPeriodo = ['indexItem','anio','nroMes','nombreMes','fechaCancelado','lecturaAnteriorPeriodo','lecturaActualPeriodo','consumoVolumenM3','consumoMinimo','costoM3','costoMinimoM3','cancelado','costoPeriodo'];
  }

  public buscarSocioDataByCodigoMedidor(){
    let codigoMedidor = this.formularioCodigoMedidor.get('codigoMedidor').value;
    let idGestion = this.formularioCodigoMedidor.get('gestion').value;

    if(codigoMedidor=='' || codigoMedidor==this.aux || codigoMedidor==null)
      return;
    if(idGestion=='0' || codigoMedidor==this.aux || codigoMedidor==null)
      return;

    this.sumatoriaCosto = 0;  
    this.getDataSocioByCodigoMedidorAndGestionCanceladoService(codigoMedidor,idGestion);
  }

  public inputChangeCodigoMedidor(){
    let codigoMedidor = this.formularioCodigoMedidor.get('codigoMedidor').value;
    if(codigoMedidor=='' || codigoMedidor==this.aux || codigoMedidor==null){
      this.limpiarFormulario();
    }
  }


  public tratarLecturaPeriodo(){
    let indexGlobal:number = 0;
    let indexItem:number = 1;
    
    this.listaLecturaPeriodoTratado = [];
    let lecturaPeriodoSocioList = this.socioData.lecturaPeriodoDataList;
    for (let i = 0; i < lecturaPeriodoSocioList.length; i++) {
      let lecturaPeriodo:LecturaPeriodoData  = lecturaPeriodoSocioList[i];
      let listaCobroExtraPeriodo:CobroExtraPeriodoData[] = lecturaPeriodo.cobroExtraPeriodoDataList;

      if(listaCobroExtraPeriodo.length!=0){
        for (let j = 0; j < listaCobroExtraPeriodo.length; j++) {
          let cobroExtraPeriodoData:CobroExtraPeriodoData = listaCobroExtraPeriodo[j];

          let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
            indexGlobal  : indexGlobal,
            indexItem    : indexItem,     
            seleccionado : false, 
            checkItem    : false,  

            // lectura periodo
            idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
            idPeriodo              : lecturaPeriodo.idPeriodo,
            idAnio                 : lecturaPeriodo.idAnio,
            idMes                  : lecturaPeriodo.idMes,
            anio                   : lecturaPeriodo.anio,
            nroMes                 : lecturaPeriodo.nroMes,
            nombreMes              : lecturaPeriodo.nombreMes,
            lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
            lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
            consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
            consumoMinimo          : lecturaPeriodo.consumoMinimo,
            costoM3                : lecturaPeriodo.costoM3,
            costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
            costoPeriodo           : lecturaPeriodo.costoPeriodo,
            cancelado              : lecturaPeriodo.cancelado,
            sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
            countItemsCobroExtra   : lecturaPeriodo.countItemsCobroExtra,
            fechaCancelado         : lecturaPeriodo.fechaCancelado,
            
            // cobro extra
            idCobroExtra     : cobroExtraPeriodoData.idCobroExtra,
            nombreCobroExtra : cobroExtraPeriodoData.nombreCobroExtra,
            costoCobroExtra  : cobroExtraPeriodoData.costoCobroExtra,

          } 
          this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
          indexGlobal += 1;
        }
        indexGlobal = 0;
        indexItem += 1;
      }else{
        let lecturaPeriodoTratadoData:LecturaPeriodoTratadoData = {
          indexGlobal  : 0,
          indexItem    : indexItem,     
          seleccionado : false,   
          checkItem    : false,

          // lectura periodo
          idLecturaPeriodo       : lecturaPeriodo.idLecturaPeriodo,
          idPeriodo              : lecturaPeriodo.idPeriodo,
          idAnio                 : lecturaPeriodo.idAnio,
          idMes                  : lecturaPeriodo.idMes,
          anio                   : lecturaPeriodo.anio,
          nroMes                 : lecturaPeriodo.nroMes,
          nombreMes              : lecturaPeriodo.nombreMes,
          lecturaAnteriorPeriodo : lecturaPeriodo.lecturaAnteriorPeriodo,
          lecturaActualPeriodo   : lecturaPeriodo.lecturaActualPeriodo,
          consumoVolumenM3       : lecturaPeriodo.consumoVolumenM3,
          consumoMinimo          : lecturaPeriodo.consumoMinimo,
          costoM3                : lecturaPeriodo.costoM3,
          costoMinimoM3          : lecturaPeriodo.costoMinimoM3,
          costoPeriodo           : lecturaPeriodo.costoPeriodo,
          cancelado              : lecturaPeriodo.cancelado,
          sumaCostoPeriodo       : lecturaPeriodo.sumaCostoPeriodo,
          countItemsCobroExtra   : 1,
          fechaCancelado         : lecturaPeriodo.fechaCancelado,

          // cobro extra
          idCobroExtra     : this.aux,
          nombreCobroExtra : this.aux,
          costoCobroExtra  : this.aux,
        } 
        this.listaLecturaPeriodoTratado.push(lecturaPeriodoTratadoData);
        indexGlobal = 0;
        indexItem += 1;
      }      
    }
  }

  public seleccionarItem(lecturaPeriodo:LecturaPeriodoTratadoData){
    this.deseleccionarItems();
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodoAux => {
      if(lecturaPeriodo.idLecturaPeriodo == lecturaPeriodoAux.idLecturaPeriodo)
        lecturaPeriodoAux.seleccionado = true;
    });
  }

  private deseleccionarItems(){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      lecturaPeriodo.seleccionado = false;
    });
  }

  public checkLecturaPeriodo(event:any,lecturaPeriodo:LecturaPeriodoTratadoData){
    this.checkLecturaPeriodoData(event.target.checked,lecturaPeriodo.idLecturaPeriodo);
    if(event.target.checked){
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto+lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }else{
      this.sumatoriaCosto =  Math.round(((this.sumatoriaCosto-lecturaPeriodo.sumaCostoPeriodo) + Number.EPSILON) * 100) / 100
    }
  }

  private checkLecturaPeriodoData(bandera:boolean,idLecturaPeriodo:number,){
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(idLecturaPeriodo == lecturaPeriodo.idLecturaPeriodo){
        lecturaPeriodo.checkItem = bandera;
      }
    });
  }

  public cancelarRevertirRegistroPagoLecturaPeriodo(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioCodigoMedidor.get('codigoMedidor').setValue('');
    this.socioData = this.aux;
    this.listaLecturaPeriodoTratado = [];
  }

  public async canecelarRegistroCobroLecturaPeriodo(){
    if(this.sumatoriaCosto==0){
      this.generalService.mensajeAlerta("No hay periodos seleccionados, seleccione uno");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REVERTIR COBRO REALIZADO POR CONSUMO DE AGUA',`¿Está seguro revertir el cobro de los periodos seleccionado ?`,`Sí, estoy seguro`,`No, Cancelar`);
    if(!afirmativo){    
      return;
    }
        
    this.listaLecturaPeriodoTratado.forEach(lecturaPeriodo => {
      if(lecturaPeriodo.indexGlobal==0 && lecturaPeriodo.checkItem==true){
        this.revertirRegistroPagoLecturaSocioMedidorService(lecturaPeriodo.idLecturaPeriodo,lecturaPeriodo.idAnio,this.pagoLecturaPeriodo);
      }
    });
  }

  public getValue(value:any){
    return (typeof value == 'boolean')? (value==true)?'SI':'NO':value;
  }

  public isTypeOfBoolean(value:any){
    return typeof value == 'boolean';
  }

  public descargarRecibo(idLecturaPeriodo:number){
    this.descargarReciboSocioLecturaMedidorService(idLecturaPeriodo);
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableCanecelarCobroLecturaPeriodo')
    this.generalService.genearteArchivoXLSX(tableElemnt,'historial cobro lectura periodo socio');
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  public getAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestiones = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getDataSocioByCodigoMedidorAndGestionCanceladoService(codigoMedidor:string,idAnio:number){
    this.datosLecturaPeriodoService.getDataSocioByCodigoMedidorAndGestionCancelado(codigoMedidor,idAnio).subscribe(
      (resp)=>{     
        if(resp!=null){
          this.socioData = resp;
          this.tratarLecturaPeriodo();
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }else{
          this.socioData = this.aux;
          this.listaLecturaPeriodoTratado = [];
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public revertirRegistroPagoLecturaSocioMedidorService(idlecturaPeriodo:number,idGestion:number,bandera:boolean){
    this.pagoLecturaMedidorPeriodoService.registroPagoLecturaSocioMedidor(idlecturaPeriodo,bandera).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se revirtio el registro del cobro de los periodos seleccionados!!!");
        this.getDataSocioByCodigoMedidorAndGestionCanceladoService(this.formularioCodigoMedidor.get('codigoMedidor').value,idGestion);
        this.sumatoriaCosto = 0;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //----------------
  // documento pdf |
  //----------------
  public descargarReciboSocioLecturaMedidorService(idLeturaPeriodo:number){
    this.lecturaMedidorPorUsuarioService.descargarReciboSocioLecturaMedidor(idLeturaPeriodo).subscribe(
      (resp)=>{
        if(resp){
          this.generalService.mensajeCorrecto("Se descargo correctamente la lectura del periodo");
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url); 
        }
      },
      (err)=>{      
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{          
          this.generalService.mensajeError("El documento no existe");
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

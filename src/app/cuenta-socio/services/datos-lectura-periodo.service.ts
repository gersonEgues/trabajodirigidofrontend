import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SocioData } from '../models/socioData';
import { HistorialLecturaPeriodoData } from '../../informe-ingresos-economicos/models/historial-lectura-periodo-data';
import { RangoPeriodoDTO } from 'src/app/shared/models/rangoPeriodoDTO';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class DatosLecturaPeriodoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/datos-cuenta-socio",http);
  }

  public getDataSocioByCodigoMedidor(codigoMedidor:string): Observable<SocioData>{
    return this.http.get<SocioData>(`${this.url}/codigo-medidor/${codigoMedidor}`).pipe(
      map(response => {
        return response as SocioData;
      })
    );
  }

  public getDataSocioByCodigoMedidorAndGestionCancelado(codigoMedidor:string,idAnio:number): Observable<SocioData>{
    return this.http.get<SocioData>(`${this.url}/codigo-medidor-gestion/${codigoMedidor}/${idAnio}`).pipe(
      map(response => {
        return response as SocioData;
      })
    );
  }

  /** lecturas de un socios que si tubbiera asignado un medidor, esat asignacion esta dado de baja */
  public getLecturaPeroidoDeSocioByIdSocioMedidorSocMedInac(idSocioMedidor:number,cancealdo:boolean): Observable<SocioData>{
    return this.http.get<SocioData>(`${this.url}/lectura-medidor/${idSocioMedidor}/${cancealdo}`).pipe(
      map(response => {
        return response as SocioData;
      })
    );
  }

  public getHistorialLecturaPeriodoCobradosEnRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<HistorialLecturaPeriodoData>{
    return this.http.put<HistorialLecturaPeriodoData>(`${this.url}/historial-cobro-lectura-medidor-rango-fecha`,rangoFechaDTO).pipe(
      map(response => {
        return response as HistorialLecturaPeriodoData;
      })
    );
  }

  public getHistorialLecturaPeriodoCobradosEnRangoPeriodo(rangoPeriodoDTO:RangoPeriodoDTO): Observable<HistorialLecturaPeriodoData>{
    return this.http.put<HistorialLecturaPeriodoData>(`${this.url}/historial-cobro-lectura-medidor-rango-periodo`,rangoPeriodoDTO).pipe(
      map(response => {
        return response as HistorialLecturaPeriodoData;
      })
    );
  }
}
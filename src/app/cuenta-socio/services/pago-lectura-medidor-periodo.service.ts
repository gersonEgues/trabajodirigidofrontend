import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NumeracionDTO } from 'src/app/shared/models/numeracionDTO';

@Injectable({
  providedIn: 'root'
})
export class PagoLecturaMedidorPeriodoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/registro-pago-lectura-periodo",http);
  }

  public registroPagoLecturaSocioMedidor(idlecturaPeriodo:number,bandera:boolean): Observable<NumeracionDTO>{
    return this.http.put<NumeracionDTO>(`${this.url}/${idlecturaPeriodo}/${bandera}`,null).pipe(
      map(response => {
        return response as NumeracionDTO;
      })
    );
  }
}
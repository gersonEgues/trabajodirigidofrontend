import { Injectable } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { LecturaPeriodoDataPdf } from '../models/lecturaPeriodoDataPdf';
import { CobroExtraPeriodoData } from '../models/cobroExtraPeriodoData';
import { LecturaMedidorPorUsuarioService } from 'src/app/consumo-agua-socio/services/lectura-medidor-por-usuario.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class ReciboLecturaPeriodoSocioPdfService {

  constructor(private generalService                   : GeneralService,
              private lecturaMedidorPorUsuarioService  : LecturaMedidorPorUsuarioService,
              private router                           : Router) { }

  public async generarReciboPorCobroLecturaPeriodoDeSocioPDF(lecturaPeriodoDataPdf:LecturaPeriodoDataPdf){
    let nombrePDF = `${lecturaPeriodoDataPdf.nombreCompletoSocio.toUpperCase()}-${lecturaPeriodoDataPdf.anio.toString()}-${lecturaPeriodoDataPdf.nroMes}-${lecturaPeriodoDataPdf.nombreMes}.pdf`;
    let fd = new FormData();

    let count           : number = 10+lecturaPeriodoDataPdf.cobroExtraPeriodoDataList.length;
    let porcentageWidth : number = 100/count;

    let widthHederArray   : string[] = await this.getWidthHederArray(lecturaPeriodoDataPdf.cobroExtraPeriodoDataList.length,porcentageWidth);
    let titleTableArray   : any[] = await this.getTitleTableArray(lecturaPeriodoDataPdf.cobroExtraPeriodoDataList);
    let contentTableArray : string[] = this.getDataLecturaPeriodo(lecturaPeriodoDataPdf);

    let tableBody:any[][] = [];
      

    tableBody.push(titleTableArray);
    tableBody.push(contentTableArray);
    
    const pdfDefinitionOpen = pdfMake.createPdf(await this.getPdfDefinition(lecturaPeriodoDataPdf,widthHederArray,tableBody));
    const pdfDefinitionDownload = pdfMake.createPdf(await this.getPdfDefinition(lecturaPeriodoDataPdf,widthHederArray,tableBody));
    const pdfDefinitionUpload = pdfMake.createPdf(await this.getPdfDefinition(lecturaPeriodoDataPdf,widthHederArray,tableBody));
    
    pdfDefinitionOpen.open();
    pdfDefinitionDownload.download(nombrePDF); 

    pdfDefinitionUpload.getBlob((blob) => {      
      fd.append('file',blob,nombrePDF);
      // actualiza en la tabale el nombre del pdf por el cual se lo encontrara
      this.updateNombreReciboDeLecturaDeMedidorService(lecturaPeriodoDataPdf.idLecturaPeriodo,nombrePDF);
      // crea el pdf
      this.uploadReciboDeLecturaDeMedidorService(fd);
    });
  }

  private getWidthHederArray(cobroExtraLength:number,pw:number):string[]{
    // gestion, mes, lect ant, lect act, cons min, cost min, cons, min?, costo cons,  
    let widthHederArray:string[] = [`${pw}%`,`*`,`${pw}%`,`${pw}%`,`${pw}%`,`${pw}%`,`${pw}%`,`${pw}%`,`${pw}%`];
    for (let i = 0; i < cobroExtraLength; i++) {
      widthHederArray.push(`${pw}%`);      
    }
    widthHederArray.push(`${pw}%`);
    return widthHederArray;
  }

  private getTitleTableArray(cobroExtraPeriodoData:CobroExtraPeriodoData[]):any[]{
    let titleTableArray:any[] = [
      { text:'Año', bold:true, fillColor:'#D6EAF8'},
      { text:'Mes', bold:true, fillColor:'#D6EAF8'},
      { text:'L Ant M3', bold:true, fillColor:'#D6EAF8'},
      { text:'L Act M3', bold:true, fillColor:'#D6EAF8'},
      { text:'Cost Min M3', bold:true, fillColor:'#D6EAF8'},
      { text:'Cost M3', bold:true, fillColor:'#D6EAF8'},
      { text:'Cons M3', bold:true, fillColor:'#D6EAF8'},
      { text:'Min', bold:true, fillColor:'#D6EAF8'},
      { text:'Bs', bold:true, fillColor:'#D6EAF8'}
    ];
    for (let i = 0; i < cobroExtraPeriodoData.length; i++) {
      titleTableArray.push({text: `${cobroExtraPeriodoData[i].nombreCobroExtra} Bs` , bold:true, fillColor:'#EAFAF1'});
    }
    titleTableArray.push({text:'Tot. Bs', bold:true, fillColor:'#FEF5E7 '});

    return titleTableArray;
  }

  private getDataLecturaPeriodo(lecturaPeriodoDataPdf:LecturaPeriodoDataPdf):string[]{
    let data:any[] = [
      {text:lecturaPeriodoDataPdf.anio, margin:[0,10]},
      {text:lecturaPeriodoDataPdf.nombreMes, margin:[0,10]},
      {text:lecturaPeriodoDataPdf.lecturaAnteriorPeriodo, margin:[0,10]},
      {text:lecturaPeriodoDataPdf.lecturaActualPeriodo, margin:[0,10]},

      {text:lecturaPeriodoDataPdf.costoMinimoM3, margin:[0,10]},
      {text:lecturaPeriodoDataPdf.costoM3, margin:[0,10]},

      {text:lecturaPeriodoDataPdf.consumoVolumenM3, margin:[0,10]},
      {text:(lecturaPeriodoDataPdf.consumoMinimo)?'Si':'No', margin:[0,10]},
      {text:lecturaPeriodoDataPdf.costoPeriodo, margin:[0,10]},
    ]

    for (let i = 0; i < lecturaPeriodoDataPdf.cobroExtraPeriodoDataList.length; i++) {
      data.push({text:lecturaPeriodoDataPdf.cobroExtraPeriodoDataList[i].costoCobroExtra, margin:[0,10]});      
    }
    data.push({text:lecturaPeriodoDataPdf.sumaCostoPeriodo, margin:[0,10]});
    return data;
  } 

  private async getPdfDefinition(lecturaPeriodoDataPdf:LecturaPeriodoDataPdf,widthHederArray:string[],tableBody:any){
    const pdfDefinition:any = {
      pageSize: 'A5',
      pageOrientation: 'landscape',
      pageMargins: [ 30, 40, 30, 10 ],
      info :{
        title: `Recibo de constancia por ${lecturaPeriodoDataPdf.motivoRecibo.toUpperCase()}`,
        author: 'Asociacion de Agua Potable Cabaña Violeta',
        subject: 'recibo por cobro de lectura medidor',
        keywords: 'aporte eventual',
      },
      content : [
        {
          margin : [0,0,0,10],
          columns : [
            {
              width : '25%',
              margin : 0,
              style : 'small',
              columns : [
                [
                  {
                    text : `${lecturaPeriodoDataPdf.datosComiteDeAgua.nombreComite}`,
                  },
                  {
                    text : `Personería Juridica ${lecturaPeriodoDataPdf.datosComiteDeAgua.personeriaJuridica}. Fundado el ${this.generalService.getFechaHibrido(lecturaPeriodoDataPdf.datosComiteDeAgua.fechaFundacion)}`,
                  },
                  {
                    text : `Telf.: ${lecturaPeriodoDataPdf.datosComiteDeAgua.telefono1} - ${lecturaPeriodoDataPdf.datosComiteDeAgua.telefono2}`,
                  },
                  {
                    text : `${lecturaPeriodoDataPdf.datosComiteDeAgua.nombreDeparatmento} - Bolivia`,
                  }
                ],
              ]
            },
            {
              width : '55%',
              style: 'header',
              margin : 0,
              columns : [
                [
                  {
                    image: await this.generalService.getBase64ImageFromURL('assets/icono.png'),
                    fit: [30, 30],
                    margin : [0,0,0,5]
                  },
                  {
                    text: `RECIBO DE APORTE POR "${lecturaPeriodoDataPdf.motivoRecibo.toUpperCase()}"`,
                  }
                ]
              ]
            },
            {
              width : '20%',
              margin : 0,
              columns : [              
                [
                  {
                    text : `LP. Nro: ${lecturaPeriodoDataPdf.nroRecibo}`,
                    style : 'quote_bold'
                  },
                  {
                    text:`Cod. Medidor: ${lecturaPeriodoDataPdf.codigoMedidor}`,
                    style : 'quote_bold'
                  },
                  {
                    text  : `${lecturaPeriodoDataPdf.datosComiteDeAgua.nombreDeparatmento}, ${lecturaPeriodoDataPdf.fechaActualHibirido}`,
                    style : 'quote'
                  },
                ]                
              ],
            }
          ]
        },
        {
          canvas: 
          [
              {
                  type: 'line',
                  x1: 0, 
                  y1: 10,
                  x2: 535, 
                  y2: 10,
                  lineWidth: 0.7
              },
          ]
        },
        {
          width : '100%',
          margin : [0,7,0,0],
          style : 'subheader',
          columns : [
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/user.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `[${lecturaPeriodoDataPdf.codigoSocio}] ${lecturaPeriodoDataPdf.nombreCompletoSocio.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/ubicacion.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `${lecturaPeriodoDataPdf.direccionSocio.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
          ]
        },
        {
        canvas: 
        [
            {
                type: 'line',
                x1: 0, 
                y1: 10,
                x2: 535, 
                y2: 10,
                lineWidth: 0.7
            },
        ]
        },
        {
          width : '100%',
          margin : [0,10,0,5],
          columns : [
            {
              style: 'quote_bold_center',
              layout: 'headerLineOnly',
              margin : 0,
              table: {
                headerRows: 1,
                widths: widthHederArray,
                body: tableBody
              }              
            }
          ]
        },
        {
          width : '100%',
          margin : [0,0,0,30],
          columns : [
            [
              {                
                style : 'quote_bold',
                columns:[
                  [
                    {
                      text: `Son ${lecturaPeriodoDataPdf.montoIngresoLetra} Bolivianos.`
                    },
                    {
                      text:  `Fecha y hora de impresión ${lecturaPeriodoDataPdf.fechaActual} ${lecturaPeriodoDataPdf.horaActual}`
                    },
                    {
                      text: `Observaciones : ${lecturaPeriodoDataPdf.observacion}`
                    }
                  ]
                ]
              }
            ]
          ]
        },
        {
          width : '100%',
          style : 'medium_bold_center',
          margin : [0,0,0,20],
          columns : [
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Entregué  conforme',
                  },
                  {
                    text : `${lecturaPeriodoDataPdf.nombreCompletoSocio.toUpperCase()}`,
                  },
                  {
                    text : 'Socio consumidor',
                  },
                ]
              ]
            },
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Recibí conforme',
                  },
                  {
                    text : `${lecturaPeriodoDataPdf.nombreEncargadoComite.toUpperCase()}`,
                  },
                  {
                    text : `${lecturaPeriodoDataPdf.rolEncargadoComite}`,
                  },
                ]
              ]
            },
          ]
        },
        {
          text: 'Nota: Este documento no es una factura, es un recibo como constancia del aporte efectuado por parte del socio',
          style : 'small'
        }
      ],
      styles: {
        header: {
          fontSize  : 13,
          alignment : 'center',
          bold      : true,
        },
        subheader: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_left: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_rigth: {
          fontSize: 11,
          alignment : 'right',
          bold: true
        },
        quote: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
        },
        quote_bold: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
          bold : true,
        },
        quote_bold_center_italic: {
          fontSize  : 9,
          alignment : 'center',
          italics: true,
          bold : true,
        },
        quote_bold_center: {
          fontSize  : 9,
          alignment : 'center',
          bold : true,
        },
        medium_bold_center: {
          fontSize: 9,
          alignment : 'center',
          bold : true,
        },
        small: {
          fontSize: 8,
          alignment : 'left',
        }
      }
    }
    return pdfDefinition;
  }

  // Consumo API-REST
  public uploadReciboDeLecturaDeMedidorService(fd:FormData) {
    this.lecturaMedidorPorUsuarioService.uploadReciboDeLecturaDeMedidor(fd).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se genero el recibo de la lectura del medidor");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateNombreReciboDeLecturaDeMedidorService(idLecturaPeriodo:number, nombreImagen:string) {
    this.lecturaMedidorPorUsuarioService.updateNombreReciboDeLecturaDeMedidor(idLecturaPeriodo,nombreImagen).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

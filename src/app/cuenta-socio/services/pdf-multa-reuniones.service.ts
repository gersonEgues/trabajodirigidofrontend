import { Injectable } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general.service';
import { DatosGeneralesComiteVO } from 'src/app/shared/models/datosGeneralesComiteVO';
import { SocioSeguimientoDataPdf } from 'src/app/eventos/models/socioSeguimientoDataPDF';
import { SeguimientoEventoService } from 'src/app/eventos/services/seguimiento-evento.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root'
})
export class PdfMultaReunionesService {

  constructor(private generalService           : GeneralService,
              private seguimientoEventoService : SeguimientoEventoService,
              private router                   : Router) { }

  public async generarReciboMultaReunionPDF(socioSeguimientoDataPdf:SocioSeguimientoDataPdf){
    let nombrePDF = `${socioSeguimientoDataPdf.nombreCompleto.toUpperCase()}-${socioSeguimientoDataPdf.fechaRegistro}-multa por-${socioSeguimientoDataPdf.estadoSeguimientoEvento.toUpperCase()}.pdf`;
    let fd = new FormData();
    
    const pdfDefinitionDownload = pdfMake.createPdf(await this.getPdfDefinition(socioSeguimientoDataPdf.datosComiteDeAgua,socioSeguimientoDataPdf));
    const pdfDefinitionUpload   = pdfMake.createPdf(await this.getPdfDefinition(socioSeguimientoDataPdf.datosComiteDeAgua,socioSeguimientoDataPdf));
    
    pdfDefinitionDownload.download(nombrePDF); 
    pdfDefinitionUpload.getBlob((blob) => {      
      fd.append('file',blob,nombrePDF);
      this.updateNombreDocumentoReciboMultaReunion(socioSeguimientoDataPdf.idSeguimientoEvento,nombrePDF);
      this.createDocumentoReciboMultaReunion(fd);
    });
  }

  private async getPdfDefinition(datosComiteDeAgua:DatosGeneralesComiteVO,socioSeguimientoDataPdf:SocioSeguimientoDataPdf){
    const pdfDefinition:any = {
      pageSize: 'A5',
      pageOrientation: 'landscape',
      pageMargins: [ 30, 40, 30, 10 ],
      info :{
        title: `Recibo de constancia por ${socioSeguimientoDataPdf.nombreCompleto.toUpperCase()}`,
        author: 'Asociacion de Agua Potable Cabaña Violeta',
        subject: 'recibo por multa por falta o retrazo a reunion',
        keywords: 'falto retrazo falta',
      },
      content : [
        {
          margin : [0,0,0,15],
          columns : [
            {
              width : '25%',
              margin : 0,
              style : 'small',
              columns : [
                [
                  {
                    text : `${datosComiteDeAgua.nombreComite}`,
                  },
                  {
                    text : `Personería Juridica ${datosComiteDeAgua.personeriaJuridica}. Fundado el ${this.generalService.getFechaHibrido(datosComiteDeAgua.fechaFundacion)}`,
                  },
                  {
                    text : `Telf.: ${datosComiteDeAgua.telefono1} - ${datosComiteDeAgua.telefono2}`,
                  },
                  {
                    text : `${datosComiteDeAgua.nombreDeparatmento} - Bolivia`,
                  }
                ],
              ]
            },
            {
              width : '55%',
              style: 'header',
              margin : 0,
              columns : [
                [
                  {
                    image: await this.generalService.getBase64ImageFromURL('assets/icono.png'),
                    fit: [30, 30],
                    margin : [0,0,0,5]
                  },
                  {
                    text: `RECIBO DE MULTA POR "${socioSeguimientoDataPdf.motivoMulta}" A EVENTO`,
                  }
                ]
              ]
            },
            {
              width : '20%',
              margin : 0,
              columns : [              
                [
                  {
                    text : `Recibo ME. Nro: ${(socioSeguimientoDataPdf.nroRecibo)?socioSeguimientoDataPdf.nroRecibo:''}`,
                    style : 'subheader_rigth'
                  },
                  {
                    text  : `${datosComiteDeAgua.nombreDeparatmento}, ${this.generalService.getFechaActualHibrido()}`,
                    style : 'quote'
                  }
                ]                
              ],
            }
          ]
        },
        {
          canvas: 
          [
              {
                  type: 'line',
                  x1: 0, 
                  y1: 10,
                  x2: 535, 
                  y2: 10,
                  lineWidth: 0.7
              },
          ]
        },
        {
          width : '100%',
          margin : [0,7,0,0],
          style : 'subheader',
          columns : [
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/user.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `[${socioSeguimientoDataPdf.codigoSocio}] ${socioSeguimientoDataPdf.nombreCompleto.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
            {
              width:'50%',
              columns : [              
                {
                  image: await this.generalService.getBase64ImageFromURL('assets/ubicacion.png'),
                  width: '7%',
                  fit: [13, 13],                  
                },
                {
                  text  : `${socioSeguimientoDataPdf.direccion.toUpperCase()}`,
                  width : '93%',
                }
              ]
            },
          ]
        },
        {
        canvas: 
        [
            {
                type: 'line',
                x1: 0, 
                y1: 10,
                x2: 535, 
                y2: 10,
                lineWidth: 0.7
            },
        ]
        },
        {
          width : '100%',
          margin : [0,20,0,40],
          columns : [
            [
              {
                text : `Por medio de este recibo se da fe que el/la socio ${socioSeguimientoDataPdf.nombreCompleto} cancelo el monto de ${ socioSeguimientoDataPdf.montoMultaNumero } Bs. (${socioSeguimientoDataPdf.montoMultaLetra} Bolivianos), por concepto de ${socioSeguimientoDataPdf.motivoMulta} a la ${socioSeguimientoDataPdf.nombreReunion}, dada en fecha ${socioSeguimientoDataPdf.fechaRegistro}.`
              },
              {
                margin : [0,20,0,0],
                style : 'quote_bold',
                columns:[
                  [
                    {
                      text: `Son ${socioSeguimientoDataPdf.montoMultaLetra} ${socioSeguimientoDataPdf.montoMultaNumero}/100 bolivianos.`
                    },
                    {
                      text:  `Fecha y hora de impresión ${socioSeguimientoDataPdf.fechaActual} ${socioSeguimientoDataPdf.horaActual}`
                    },
                    {
                      text: `Observaciones : ${socioSeguimientoDataPdf.observacion}`
                    }
                  ]
                ]
              }
            ]
          ]
        },
        {
          width : '100%',
          style : 'medium_bold_center',
          margin : [0,0,0,30],
          columns : [
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Entregué  conforme',
                  },
                  {
                    text : `${socioSeguimientoDataPdf.nombreCompleto.toUpperCase()}`,
                  },
                  {
                    text : 'Socio consumidor',
                  },
                ]
              ]
            },
            {
              columns : [
                [
                  {
                    text : '................................................................................',
                  },
                  {
                    text : 'Recibí conforme',
                  },
                  {
                    text : `${socioSeguimientoDataPdf.nombreEncargadoComite.toUpperCase()}`,
                  },
                  {
                    text : `${socioSeguimientoDataPdf.rolEncargadoComite}`,
                  },
                ]
              ]
            },
          ]
        },
        {
          text: 'Nota: Este documento no es una factura, es un recibo como constancia del aporte efectuado por parte del socio',
          style : 'small'
        }
      ],
      styles: {
        header: {
          fontSize  : 13,
          alignment : 'center',
          bold      : true,
        },
        subheader: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_left: {
          fontSize: 11,
          alignment : 'left',
          bold: true
        },
        subheader_rigth: {
          fontSize: 11,
          alignment : 'right',
          bold: true
        },
        quote: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
        },
        quote_bold: {
          fontSize  : 9,
          alignment : 'right',
          italics: true,
          bold : true,
        },
        quote_bold_center: {
          fontSize  : 9,
          alignment : 'center',
          italics: true,
          bold : true,
        },
        medium_bold_center: {
          fontSize: 9,
          alignment : 'center',
          bold : true,
        },
        small: {
          fontSize: 8,
          alignment : 'left',
        }
      }
    }
    return pdfDefinition;
  }

  //-------------------
  // Consumo API-REST |
  //-------------------
  private updateNombreDocumentoReciboMultaReunion(idSeguimientoEvento:number,nombreDocumento:string){
    this.seguimientoEventoService.updateNombreDocumentoReciboMultaReunion(idSeguimientoEvento,nombreDocumento).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  private createDocumentoReciboMultaReunion(fd:FormData){
    this.seguimientoEventoService.createDocumentoReciboMultaReunion(fd).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se genero un recibo correctamente")
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}

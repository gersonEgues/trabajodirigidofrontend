import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EventoVO } from '../models/eventoVO';
import { EventoDTO } from '../models/eventoDTO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';
import { NombreDocumentoActa } from '../models/nombreDocumentoActa';
import { CountItemVO } from '../../shared/models/countItemVO';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { HistorialCobroReunionesData } from 'src/app/informe-ingresos-economicos/models/historial-cobro-reuniones-data';
import { RangoFechaDTO } from 'src/app/shared/models/rangoFechaDTO';

@Injectable({
  providedIn: 'root'
})
export class EventoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/evento",http);
  }

  public getEvento(id:number) : Observable<EventoVO> {
    return this.http.get<EventoVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as EventoVO;
      })
    );
  }

  public getCountListaEventoFiltrado(estadoEvento:string) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/count-lista/${estadoEvento}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getListaEventoFiltrado(estadoEvento:string,page:number,size:number) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/lista/${estadoEvento}/${page}/${size}`).pipe(
      map(response => {
        return response as EventoVO[];
      })
    );
  }

  public getListaEventoSinActa(estadoEvento:string) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/lista-sin-acta/${estadoEvento}`).pipe(
      map(response => {
        return response as EventoVO[];
      })
    );
  }

  public busquedaEventoSinActaFiltradoLista(texto:string,estadoEvento:string) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/busqueda-lista-sin-acta/${texto}/${estadoEvento}`).pipe(
      map(response => {
        return response as EventoVO[];
      })
    );
  }

  public busquedaEventoTodosFiltradoLista(texto:string,estadoEvento:string) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/busqueda-lista-todos/${texto}/${estadoEvento}`).pipe(
      map(response => {
        return response as EventoVO[];
      })
    );
  }

  public getSizeListaEventoConActa(estadoEvento:string) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/size-lista-con-acta/${estadoEvento}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  public getListaEventoConActa(estadoEvento:string,pageDTO:PageDTO) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/lista-con-acta/${estadoEvento}/${pageDTO.page}/${pageDTO.size}`).pipe(
      map(response => {
        return response as EventoVO[];
      })
    );
  }

  public buscarListaEventoFiltrado(texto:string,estadoEvento:string) : Observable<EventoVO[]> {
    return this.http.get<EventoVO[]>(`${this.url}/buscar-evento/${texto}/${estadoEvento}`);
  }

  public createEvento(eventoDTO:EventoDTO) : Observable<EventoVO> {   
    return this.http.post<EventoVO>(`${this.url}`,eventoDTO).pipe(
      map(response => {
        return response as EventoVO;
      })
    );
  }

  public updateEvento(eventoVO:EventoVO) : Observable<EventoVO> {
    return this.http.put<EventoVO>(`${this.url}`,eventoVO).pipe(
      map(response => {
        return response as EventoVO;
      })
    );
  }

  public deleteEvento(id:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public cambioEstadoEvento(idEvento:number,estadoEvento:string) : Observable<EventoVO> {
    return this.http.put<EventoVO>(`${this.url}/cambio-estado/${idEvento}/${estadoEvento}`,null).pipe(
      map(response => {
        return response as EventoVO;
      })
    );
  }

  public updateNombreActa(actaEvento:NombreDocumentoActa):Observable<EventoVO>{
    return this.http.put<EventoVO>(`${this.url}/nombre-doc-acta`,actaEvento).pipe(
      map(response => {
        return response as EventoVO;
      })
    );
  }

  public getDocumentActa(idEvento:number): Observable<any> { 
    return this.http.get(`${this.url}/documento-acta/${idEvento}`,{responseType: 'blob'});
  }

  public uploadDocumentActa(fd:FormData): Observable<any> { 
    return this.http.post<any>(`${this.url}/documento-acta`,fd).pipe(
      map(response => {
        return response as any;
      })
    );
  } 

  public buscarEvento(texto: String): Observable<any> {
    return this.httpClient.get(`${this.url}/lista/PENDIENTE`);
  }

  // ----------------------------------------
  // reporte ingreso por cobro de reuniones |
  //-----------------------------------------
  public getEventosRealizadosEnRangoFechaConSociosQueCancelaronMulta(rangoFechaDTO:RangoFechaDTO): Observable<HistorialCobroReunionesData>{
    return this.http.put<HistorialCobroReunionesData>(`${this.url}/historial-cobro-evento-rango-fecha`,rangoFechaDTO).pipe(
      map(response => {
        return response as HistorialCobroReunionesData;
      })
    );
  }

  public getListaDeudaSeguimientoEventoDeEventoEnRangoFecha(rangoFechaDTO:RangoFechaDTO): Observable<HistorialCobroReunionesData>{
    return this.http.put<HistorialCobroReunionesData>(`${this.url}/historial-deuda-evento-rango-fecha`,rangoFechaDTO).pipe(
      map(response => {
        return response as HistorialCobroReunionesData;
      })
    );
  }
}
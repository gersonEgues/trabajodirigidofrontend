import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TipoEventoVO } from '../models/tipoEventoVO';
import { TipoEventoDTO } from '../models/tipoEventoDTO';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';

@Injectable({
  providedIn: 'root'
})
export class TipoEventoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/tipo-evento",http);
  }

  public getTipoEvento(id:number) : Observable<TipoEventoVO> {
    return this.http.get<TipoEventoVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as TipoEventoVO;
      })
    );
  }

  public getListaTipoEvento() : Observable<TipoEventoVO[]> {
    return this.http.get<TipoEventoVO[]>(`${this.url}/lista`).pipe(
      map(response => {
        return response as TipoEventoVO[];
      })
    );
  }

  public createTipoEvento(tipoEventoDTO:TipoEventoDTO) : Observable<TipoEventoVO> {
    return this.http.post<TipoEventoVO>(`${this.url}`,tipoEventoDTO).pipe(
      map(response => {
        return response as TipoEventoVO;
      })
    );
  }

  public updateTipoEvento(tipoEventoVO:TipoEventoVO):Observable<TipoEventoVO> {
    return this.http.put<TipoEventoVO>(`${this.url}`,tipoEventoVO).pipe(
      map(response => {
        return response as TipoEventoVO;
      })
    );
  }

  public deleteTipoEvento(id:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { SeguimientoEventoTodosLosSociosDTO } from '../models/seguimientoEventoTodosLosSociosDTO';
import { map } from 'rxjs/operators';
import { SeguimientoEventoVO } from '../models/seguimientoEventoVO';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { SocioSeguimientoVO } from '../models/socioSeguimientoVO';
import { SeguimientoEventoCheckDTO } from '../models/seguimientoEventoCheck';
import { CancelarSeguimientoEventoDTO } from '../models/cancelarSeguimientoEventoDTO';
import { CountItemVO } from 'src/app/shared/models/countItemVO';
import { PageDTO } from '../../shared/models/pageDTO';
import { SocioMedidorVO } from 'src/app/shared/models/socio-medidor-vo';


@Injectable({
  providedIn: 'root'
})
export class SeguimientoEventoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/seguimiento-evento",http);
  }

  public getSeguimientoEvento(id:number) : Observable<SeguimientoEventoVO> {
    return this.http.get<SeguimientoEventoVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public getSocioSeguimientoEvento(idSeguimientoEvento:number) : Observable<SocioSeguimientoVO> {
    return this.http.get<SocioSeguimientoVO>(`${this.url}/socio/${idSeguimientoEvento}`).pipe(
      map(response => {
        return response as SocioSeguimientoVO;
      })
    );
  }

  public getListaSeguimientoEvento(idEvento:number) : Observable<SeguimientoEventoVO[]> {
    return this.http.get<SeguimientoEventoVO[]>(`${this.url}/lista/${idEvento}`).pipe(
      map(response => {
        return response as SeguimientoEventoVO[];
      })
    );
  }
  
  public getListaTodosLosSociosDeEvento(idEvento:number) : Observable<SocioSeguimientoVO[]> {
    return this.http.get<SocioSeguimientoVO[]>(`${this.url}/lista-socios-todos/${idEvento}`).pipe(
      map(response => {
        return response as SocioSeguimientoVO[];
      })
    );
  }

  public getListaTodosLosSociosDeEventoPorEstadoMulta(idEvento:number,estadoMulta:string) : Observable<SocioSeguimientoVO[]> {
    return this.http.get<SocioSeguimientoVO[]>(`${this.url}/lista-socios-todos/${idEvento}/${estadoMulta}`).pipe(
      map(response => {
        return response as SocioSeguimientoVO[];
      })
    );
  }


  public getListaSociosSeguimientoDeEvento(idEvento:number,estadoSeguimientoEvento:string) : Observable<SocioSeguimientoVO[]> {
    return this.http.get<SocioSeguimientoVO[]>(`${this.url}/lista-socios-seguimiento/${idEvento}/${estadoSeguimientoEvento}`).pipe(
      map(response => {
        return response as SocioSeguimientoVO[];
      })
    );
  }

  public getListaSociosDeEvento(idEvento:number,estadoSeguimientoEvento:string) : Observable<SocioMedidorVO[]> {
    return this.http.get<SocioMedidorVO[]>(`${this.url}/lista-socios/${idEvento}/${estadoSeguimientoEvento}`).pipe(
      map(response => {
        return response as SocioMedidorVO[];
      })
    );
  }

  public createSeguimientoEvento(idSocioMedidor:number,seguimientoEventoTodosLosSociosDTO:SeguimientoEventoTodosLosSociosDTO) : Observable<SeguimientoEventoVO> {
    return this.http.post<SeguimientoEventoVO>(`${this.url}/${idSocioMedidor}`,seguimientoEventoTodosLosSociosDTO).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public createSeguimientoEventoParaTodosLosSocios(seguimientoEventoTodosSociosDTO:SeguimientoEventoTodosLosSociosDTO) : Observable<BanderaResponseVO> {
    return this.http.post<BanderaResponseVO>(`${this.url}/todos-socios`,seguimientoEventoTodosSociosDTO).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public updateSeguimientoEvento(seguimientoEventoVO:SeguimientoEventoVO) : Observable<SeguimientoEventoVO> {
    return this.http.put<SeguimientoEventoVO>(`${this.url}`,seguimientoEventoVO).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public cambioEstadoSeguimientoEvento(idSeguimientoEvento:number,estadoSeguimientoEvento:string) : Observable<SeguimientoEventoVO> {
    return this.http.put<SeguimientoEventoVO>(`${this.url}/cambio-estado/${idSeguimientoEvento}/${estadoSeguimientoEvento}`,null).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public cambioEstadoSeguimientoTodos(idEvento:number,estadoSegEvento:string) : Observable<BanderaResponseVO> {
    return this.http.put<BanderaResponseVO>(`${this.url}/cambio-estado-todos/${idEvento}/${estadoSegEvento}/${estadoSegEvento}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public updateNumeracionReciboFaltaOretrazo(idEvento:number,estadoMulta:string ) : Observable<BanderaResponseVO> {
    return this.http.put<BanderaResponseVO>(`${this.url}/update-numeracion/${idEvento}/${estadoMulta}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public updateEstadoMultaSeguimientoEvento(idSeguimientoEvento:number,estadoMulta:string,banderaCancelado:boolean,montoCancelado:number) : Observable<SeguimientoEventoVO> {
    return this.http.put<SeguimientoEventoVO>(`${this.url}/update-estado-multa-seg-evento/${idSeguimientoEvento}/${estadoMulta}/${banderaCancelado}/${montoCancelado}`,null).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public deleteTodosLosSeguimientoEventoDeEvento(idEvento:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/${idEvento}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public checkSeguimientoEvento(seguimientoEventoCheck:SeguimientoEventoCheckDTO) : Observable<SocioSeguimientoVO> {
    return this.http.post<SocioSeguimientoVO>(`${this.url}/check-seg-evento`,seguimientoEventoCheck).pipe(
      map(response => {
        return response as SocioSeguimientoVO;
      })
    );
  }

  public cancelarSeguimientoEventoTodos(cancelarSeguimientoEventoDTO:CancelarSeguimientoEventoDTO) : Observable<BanderaResponseVO> {
    return this.http.put<BanderaResponseVO>(`${this.url}/cancelar-seguimiento-evento-todos`,cancelarSeguimientoEventoDTO).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getListaSocioSeguimientoEventoConDocumentoDeLicencia(idEvento:number,pageDTO:PageDTO) : Observable<SocioSeguimientoVO[]> {
    return this.http.get<SocioSeguimientoVO[]>(`${this.url}/lista-seg-evento-doc/${idEvento}/${pageDTO.page}/${pageDTO.size}`).pipe(
      map(response => {
        return response as SocioSeguimientoVO[];
      })
    );
  }

  public getSizeListaSocioSeguimientoEventoConDocumentoDeLicencia(idEvento:number) : Observable<CountItemVO> {
    return this.http.get<CountItemVO>(`${this.url}/size-lista-seg-evento-doc/${idEvento}`).pipe(
      map(response => {
        return response as CountItemVO;
      })
    );
  }

  
  //----------------------------------------
  // documento recibo  licencia para socio |
  //----------------------------------------
  public updateNombreDocumentoLicencia(idSegEvento:number,nombreDoc:string) : Observable<SocioSeguimientoVO> {
    return this.http.put<SocioSeguimientoVO>(`${this.url}/documento-licencia-socio/${idSegEvento}/${nombreDoc}`,null).pipe(
      map(response => {
        return response as SocioSeguimientoVO;
      })
    );
  }

  public uploadDocumentoLicencia(fd:FormData): Observable<any> { 
    return this.http.post<any>(`${this.url}/documento-licencia-socio`,fd).pipe(
      map(response => {
        return response as any;
      })
    );
  } 

  public getDocumentoSeguimientoSociosEvento(idSegEvento:number): Observable<any> { 
    return this.http.get(`${this.url}/documento-licencia-socio/${idSegEvento}`,{responseType: 'blob'});
  }

  //-----------------------------
  // documento recibo por multa |
  //-----------------------------
  public updateNombreDocumentoReciboMultaReunion(idSeguimientoEvento:number,nombreDocumento:string): Observable<SeguimientoEventoVO>{
    return this.http.put<SeguimientoEventoVO>(`${this.url}/documento-recibo-multa-reunion/${idSeguimientoEvento}/${nombreDocumento}`,null).pipe(
      map(response => {
        return response as SeguimientoEventoVO;
      })
    );
  }

  public createDocumentoReciboMultaReunion(fd:FormData): Observable<BanderaResponseVO> { 
    return this.http.post<BanderaResponseVO>(`${this.url}/documento-recibo-multa-reunion`,fd).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  } 
  
  public descargarDocumentoReciboMultaReunion(idSeguimientoEvento:number): Observable<any> { 
    return this.http.get(`${this.url}/documento-recibo-multa-reunion/${idSeguimientoEvento}`,{responseType: 'blob'});
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DeterminacionEventoDTO } from '../models/determinacionEventoDTO';
import { DeterminacionEventoVO } from '../models/determinacionEventoVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';


@Injectable({
  providedIn: 'root'
})
export class DeterminacionesEventoService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/determinacion-evento",http);
  }

  public getDeterminacionEvento(id:number) : Observable<DeterminacionEventoVO> {
    return this.http.get<DeterminacionEventoVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as DeterminacionEventoVO;
      })
    );
  }

  public getListaDeterminacionesDeEvento(idEvento:number) : Observable<DeterminacionEventoVO[]> {
    return this.http.get<DeterminacionEventoVO[]>(`${this.url}/lista/${idEvento}`).pipe(
      map(response => {
        return response as DeterminacionEventoVO[];
      })
    );
  }

  public createDeterminacionEvento(determinacionEventoDTO:DeterminacionEventoDTO) : Observable<DeterminacionEventoVO> {
    return this.http.post<DeterminacionEventoVO>(`${this.url}`,determinacionEventoDTO).pipe(
      map(response => {
        return response as DeterminacionEventoVO;
      })
    );
  }

  public updateDeterminacionEvento(determinacionEventoVO:DeterminacionEventoVO) : Observable<DeterminacionEventoVO> {
    return this.http.put<DeterminacionEventoVO>(`${this.url}`,determinacionEventoVO).pipe(
      map(response => {
        return response as DeterminacionEventoVO;
      })
    );
  }

  public deleteDeterminacionEvento(id:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/${id}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}
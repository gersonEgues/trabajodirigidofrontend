import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TipoEventoService } from '../../services/tipo-evento.service';
import { TipoEventoVO } from '../../models/tipoEventoVO';
import { TipoEventoDTO } from '../../models/tipoEventoDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-tipo-evento',
  templateUrl: './tipo-evento.component.html',
  styleUrls: ['./tipo-evento.component.css']
})
export class TipoEventoComponent implements OnInit {
  aux : any = undefined;

  // Tabla
  tituloItemsCategoria  : string[] = [];
  campoItemsCategoria   : string[] = [];
  idItemCargo!          : string;
  listaItemsTipoEvento! : TipoEventoVO[];

  formularioTipoEvento! : FormGroup;

  tipoEventoEditar : TipoEventoVO = this.aux;

  constructor(private fb                : FormBuilder,
              private generalService    : GeneralService,
              private dialog            : MatDialog,
              private tipoEventoService : TipoEventoService,
              private router            : Router) { }

  ngOnInit(): void {
    this.construirFormulario();
    this.setConfiguracionTabla();
    this.getListaTipoEventoService();    
  }

  private construirFormulario(){
    this.formularioTipoEvento = this.fb.group({
      nombre       : ['',[Validators.required],[]],
      descripcion  : ['',[Validators.required],[]],
    });
  }

  private setConfiguracionTabla(){
    this.tituloItemsCategoria = ['#','Nombre','Descripcion'];
    this.campoItemsCategoria  = ['nombre','descripcion'];
    this.idItemCargo          = "idTipoEvento";
  }

  public tipoEventoValido(campo:string) : boolean{
    return this.formularioTipoEvento.controls[campo].valid
  }

  public tipoEventoInvalido(campo:string) : boolean{   
    return this.formularioTipoEvento.controls[campo].invalid && 
           this.formularioTipoEvento.controls[campo].touched;
  }

  public async createTipoEvento(){
    if(this.formularioTipoEvento.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioTipoEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVO TIPO DE EVENTO','¿Está seguro de crear este nuevo tipo de evento?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let tipoEvento:TipoEventoDTO = this.getTipoEventoDTO();
    this.createTipoEventoService(tipoEvento);
  }

  public async updateTipoEvento(){
    if(this.formularioTipoEvento.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioTipoEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR TIPO DE EVENTO','¿Está seguro de actualizar este tipo de evento?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let  tipoEventoVO:TipoEventoVO = this.getTipoEventoVO();
    this.updateTipoEventoService(tipoEventoVO);
  }

  public updateTipoeEventoEvent(tipoEventoVO:TipoEventoVO){
    this.tipoEventoEditar = tipoEventoVO;
    this.formularioTipoEvento.get('nombre')?.setValue(tipoEventoVO.nombre);
    this.formularioTipoEvento.get('descripcion')?.setValue(tipoEventoVO.descripcion);
  }

  public async deleteTipoEventoEvent(tipoEventoVO:TipoEventoVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR TIPO EVENTO','¿Está seguro de eliminar este tipo de evento?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.deleteTipoEventoService(tipoEventoVO.idTipoEvento);
  }

  public cancelTipoEvento(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioTipoEvento.reset();
    this.tipoEventoEditar = this.aux;
  }

  private getTipoEventoDTO():TipoEventoDTO{
    let tipoEventoDTO:TipoEventoDTO = {
      nombre      : this.formularioTipoEvento.get('nombre')?.value,
      descripcion : this.formularioTipoEvento.get('descripcion')?.value,
    }
    return tipoEventoDTO;
  }

  private getTipoEventoVO():TipoEventoVO{
    let tipoEventoVO:TipoEventoVO = {
      idTipoEvento : this.tipoEventoEditar.idTipoEvento,
      nombre       : this.formularioTipoEvento.get('nombre')?.value,
      descripcion  : this.formularioTipoEvento.get('descripcion')?.value,
    }
    return tipoEventoVO;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('idTablaTipoReunion')
    this.generalService.genearteArchivoXLSX(tableElemnt,'lista de tipos de reunion');
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  public getListaTipoEventoService() {
    this.tipoEventoService.getListaTipoEvento().subscribe(
      (resp)=>{
        this.listaItemsTipoEvento = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createTipoEventoService(tipoEventoDTO:TipoEventoDTO)  {
    this.tipoEventoService.createTipoEvento(tipoEventoDTO).subscribe(
      (resp)=>{
        this.getListaTipoEventoService();
        this.limpiarFormulario();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateTipoEventoService(tipoEventoVO:TipoEventoVO)  {
    this.tipoEventoService.updateTipoEvento(tipoEventoVO).subscribe(
      (resp)=>{
        this.getListaTipoEventoService();
        this.limpiarFormulario();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteTipoEventoService(id:number) {
    this.tipoEventoService.deleteTipoEvento(id).subscribe(
      (resp)=>{
        this.getListaTipoEventoService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

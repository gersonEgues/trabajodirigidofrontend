import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { EventoService } from '../../services/evento.service';
import { MatDialog } from '@angular/material/dialog';
import { EventoVO } from '../../models/eventoVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { EstadoEvento } from '../../models/estadoEvento';
import { NombreDocumentoActa } from '../../models/nombreDocumentoActa';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { PageEvent } from '@angular/material/paginator';
import { BuscadorComponent } from '../../../shared/components/buscador/buscador.component';
import { Observable } from 'rxjs';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-acta-evento',
  templateUrl: './acta-evento.component.html',
  styleUrls: ['./acta-evento.component.css']
})
export class ActaEventoComponent implements OnInit {
  @ViewChild('buscardorComponent') buscardorComponent! : BuscadorComponent;

  aux       : any = undefined;
  srcBase64 : any = this.aux;
  file      : File = this.aux;

  formularioActaEvento! : FormGroup;
  listaEventosSinActa!  : Observable<EventoVO[]>; 
  
  zoomValue : number = 1;
  fileName  : string = '';


  // tabla eventos
  tituloEventosConActa     : string[] = [];
  campoEventosConActa      : string[] = [];
  idEventosConActa         : string = 'idSocio';
  listaEventosConActa!     : EventoVO[];
  countlistaEventosConActa : number = 0;
  pageDTO!                 : PageDTO;

  eventoSeleccionado  : EventoVO = this.aux;
  eventoEditar        : EventoVO = this.aux;
  
  @ViewChild('tablaEventosRealizados') tablaEventosRealizados! : TablePaginatorComponent;

  constructor(private fb             : FormBuilder,
              private generalService : GeneralService,
              private eventoService  : EventoService,
              private dialog         : MatDialog,
              private router         : Router) { }

  ngOnInit() {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : '',
    }

    this.construirFormularioActaEvento();
    this.getSizeListaEventoConActaService(EstadoEvento.REALIZADO);
    this.getListaEventoConActaService(EstadoEvento.REALIZADO,this.pageDTO);
    this.configurarDatosTablaListaEventos();
  }

  private construirFormularioActaEvento(){
    this.formularioActaEvento = this.fb.group({
      evento  : ['0',[Validators.required],[]],
      acta    : ['',[Validators.required],[]],
    });
  }

  public buscarEvento(texto:string){
    this.busquedaEventoSinActaFiltradoService(texto,EstadoEvento.REALIZADO);
  }

  public seleccionarEvento(evento:EventoVO){
    this.eventoSeleccionado = evento;
  }

  public async createActa(){
    if(this.formularioActaEvento.invalid || this.eventoSeleccionado==this.aux){
      if(this.formularioActaEvento.invalid){
        this.formularioActaEvento.markAllAsTouched();
        this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      }
      if(this.eventoSeleccionado==this.aux){
        this.generalService.mensajeError("Seleccione un evento");
      }
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE ACTA DEL EVENTO','¿Está seguro de registrar el acta del evento?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return;
    }

    let nombreFile:string = `acta_de_evento_${this.eventoSeleccionado.nombre}_${this.eventoSeleccionado.fechaRealizacion}.pdf`;

    let actaEvento:NombreDocumentoActa = {
      idEvento : this.eventoSeleccionado.idEvento,
      nombre   : nombreFile,
    }

    let fd = new FormData();
    fd.append('file',this.file,nombreFile);
    
    this.updateActaService(actaEvento, fd);
    this.limpiarFormulario();
  }

  public changeFile(event:any){
    this.file = event.target.files[0];
    
    if(this.file){
      let name:string = this.file.name;  
      let extencion:string = String(name.split('.').pop());
      if(extencion!="pdf"){
        this.generalService.mensajeError("El archivo tiene que ser PDF");
        this.formularioActaEvento.get('acta')?.reset();
        return;
      }

      if((this.file.size/1000000)>10){
        this.generalService.mensajeError("El archivo exede los 10 MB");
        this.formularioActaEvento.get('acta')?.reset();
        return;
      }

      this.getBase64(this.file);
    }
  }

  private getBase64(file:File){
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.srcBase64 = reader.result;
    };
  }

  public async actualizarActa(){
    if(this.formularioActaEvento.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioActaEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR DE ACTA DEL EVENTO','¿Está seguro de actualuizar el acta del evento?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return;
    }

    let nombreFile:string = `acta_de_evento_${this.eventoEditar.nombre}_${this.eventoEditar.fechaRealizacion}.pdf`;
    let actaEvento:NombreDocumentoActa = {
      idEvento : this.eventoEditar.idEvento,
      nombre   : nombreFile,
    }

    let fd = new FormData();
    fd.append('file', this.file, nombreFile);
    
    this.updateActaService(actaEvento, fd);
    this.limpiarFormulario();
  }

  public async cancelarUploadActa(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.buscardorComponent.limpiarFormulario();

    this.formularioActaEvento.get('acta').reset();

    this.file = this.aux;
    this.srcBase64 = this.aux;
    this.eventoSeleccionado = this.aux;

    this.eventoEditar = this.aux;
    
  } 

  public campoEventoValido(campo:string) : boolean{
    return this.formularioActaEvento.controls[campo].valid && this.formularioActaEvento.controls[campo].value!="0";
  }

  public campoEventoInvalido(campo:string) : boolean{   
    return  (this.formularioActaEvento.controls[campo].invalid && this.formularioActaEvento.controls[campo].touched) ||
            (this.formularioActaEvento.controls[campo].touched && this.formularioActaEvento.controls[campo].value=='0');
  }

  public zoomMinus(){
    this.zoomValue -= 0.1;
  }

  public zoomPlus(){
    this.zoomValue = this.zoomValue + 0.1;
  }

  private configurarDatosTablaListaEventos():void{
    this.tituloEventosConActa = ['#','Nombre','Fecha realización','Estado Evento','Obligatorio','Todos los socios','Hora reunion','Nombre acta'];
    this.campoEventosConActa = ['nombre','fechaRealizacion','estadoEvento','obligatorio','todosLosSocios','horaReunion','nombreActa'];
    this.idEventosConActa = 'idEvento';
  }

  public verActaDeEvento(evento:EventoVO){
    //es la primera  vez que visualizo
    if(this.eventoSeleccionado==this.aux){
      this.eventoSeleccionado = evento;
      this.verDocumentActaService(evento.idEvento);
      return;
    }
    
    // estoy visualizando el mismo evento
    if(this.eventoSeleccionado.idEvento == evento.idEvento){
      this.srcBase64 = this.aux;
      this.eventoSeleccionado = this.aux;
      return;
    }
    
    this.verDocumentActaService(evento.idEvento);
    this.eventoSeleccionado = evento;    
  }

  public descargarActaDeEvento(evento:EventoVO){
    this.descargarDocumentActaService(evento.idEvento);
  }

  public updateEvento(evento:EventoVO){
    this.eventoEditar = evento;
    this.srcBase64 = this.aux;
    //this.formularioActaEvento.get('evento')?.setValue(evento.idEvento);
    //this.formularioActaEvento.get('evento')?.disable();
    this.buscardorComponent.setValue(evento.nombre+', '+evento.fechaRealizacion);
    this.buscardorComponent.disable();
  }
  
  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.getSizeListaEventoConActaService(EstadoEvento.REALIZADO);
    this.getListaEventoConActaService(EstadoEvento.REALIZADO,this.pageDTO);
  }

  public genearteArchivoXLSX():void{
    this.tablaEventosRealizados.genearteArchivoXLSX("Eventos realizados");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  // Evento service
  public busquedaEventoSinActaFiltradoService(texto:string,estadoEvento:string){
    this.listaEventosSinActa = this.eventoService.busquedaEventoSinActaFiltradoLista(texto,estadoEvento);
  }

  public getSizeListaEventoConActaService(estadoEvento:string){
    this.eventoService.getSizeListaEventoConActa(estadoEvento).subscribe(
      (resp)=>{
        this.countlistaEventosConActa = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaEventoConActaService(estadoEvento:string,pageDTO:PageDTO){
    this.eventoService.getListaEventoConActa(estadoEvento,pageDTO).subscribe(
      (resp)=>{
        this.listaEventosConActa = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateActaService(actaEvento:NombreDocumentoActa,fd:FormData){
    this.eventoService.updateNombreActa(actaEvento).subscribe(
      (resp)=>{
        this.buscardorComponent.setValue('');
        this.buscardorComponent.enable();
        this.uploadDocumentActaService(fd);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public verDocumentActaService(idEvento:number){
    this.eventoService.getDocumentActa(idEvento).subscribe(
      (resp)=>{
        if(resp)
          this.getBase64(resp);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public descargarDocumentActaService(idEvento:number){
    this.eventoService.getDocumentActa(idEvento).subscribe(
      (resp)=>{
        if(resp){
          const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
          window.open(url);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public uploadDocumentActaService(fd:FormData){
    this.eventoService.uploadDocumentActa(fd).subscribe(
      (resp)=>{
        this.getListaEventoConActaService(EstadoEvento.REALIZADO,this.pageDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { EventoService } from '../../services/evento.service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { SeguimientoEventoService } from '../../services/seguimiento-evento.service';
import { EventoVO } from '../../models/eventoVO';
import { Observable } from 'rxjs';
import { SocioSeguimientoVO } from '../../models/socioSeguimientoVO';
import { EstadoSeguimientoEvento } from '../../models/estadoSeguimientoEvento';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-licencia',
  templateUrl: './licencia.component.html',
  styleUrls: ['./licencia.component.css']
})
export class LicenciaComponent implements OnInit {
  aux:any          = undefined;
  srcBase64:any    = this.aux;
  file: File       = this.aux;
  zoomValue:number = 1;

  formularioBuscadorEvento! : FormGroup;
  listaEventos!:Observable<EventoVO[]>;
  listaSocioSeguimientoEvento:SocioSeguimientoVO[] = [];

  eventoSeleccionado:EventoVO = this.aux;
  socioSeguEventoSeleccionado:SocioSeguimientoVO = this.aux;

  // tabla socio seguimiento eventos
  tituloSocioSegEventoLicencia     : string[] = [];
  campoSocioSegEventoLicencia      : string[] = [];
  idSocioSegEventoLicencia         : string = '';
  countItemsSocioSegEventoLicencia : number = 0;
  pageDTO!                         : PageDTO;
  listaSociosSegConLicencia!       : SocioSeguimientoVO[];


  formularioBuscadorEventoTabla! : FormGroup;
  listaEventosTabla!             : Observable<EventoVO[]>;
  eventoTablaSeleccionado        : EventoVO = this.aux;
  socioSeguimientoUpdate         : SocioSeguimientoVO = this.aux;
  socioSegEventoSeleccionado     : SocioSeguimientoVO = this.aux;

  constructor(private fb                       : FormBuilder,
              private generalService           : GeneralService,
              private eventoService            : EventoService,
              private seguimientoEventoService : SeguimientoEventoService,
              private dialog                   : MatDialog,
              private router                   : Router) { }

  ngOnInit(): void {
    this.pageDTO = {
      page : 0,
      size : 10,
      sort : '',
    }

    this.construirformularioBuscadorEvento();
    this.setConfiguracionTabla();
    this.construirformularioBuscadorEventoTabla();
  }

  public construirformularioBuscadorEvento(){
    this.formularioBuscadorEvento = this.fb.group({
      nombreEvento           : ['',[Validators.required],[]],
      socioSeguimientoEvento : ['0',[Validators.required],[]],
      documento              : ['',[Validators.required],[]],
    });
  }

  public buscarEvento(){
    let texto:string = this.formularioBuscadorEvento.get('nombreEvento')?.value;
    texto=texto?.toLocaleLowerCase();
    this.listaSocioSeguimientoEvento = [];

    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.reset();
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.setValue('0');

    if(texto?.trim()==''){
      this.listaEventos = this.aux;
      return;
    }
    
    if(this.eventoSeleccionado==this.aux){
      this.buscarListaEventoFiltradoService(texto,'REALIZADO');    
    }else{
      this.eventoSeleccionado = this.aux;
    }
  }

  public seleccionarEvento(evento:EventoVO){
    this.formularioBuscadorEvento.get('nombreEvento')?.setValue(evento.nombre + ', ' + evento.fechaRealizacion);
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.reset();
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.setValue('0');
    this.eventoSeleccionado = evento;
    this.listaEventos = this.aux;

    this.getListaSocioSeguimientoEventoService(this.eventoSeleccionado.idEvento,EstadoSeguimientoEvento.LICENCIA);
  }

  public mouseOver(item:any){
    item.hover = true;
  }
  public mouseOut(item:any){
    item.hover = false;
  }

  public campoEventoValido(campo:string) : boolean{
    return this.formularioBuscadorEvento.controls[campo].valid && this.formularioBuscadorEvento.controls[campo].value!="0";
  }

  public campoEventoInvalido(campo:string) : boolean{   
    return  (this.formularioBuscadorEvento.controls[campo].invalid && this.formularioBuscadorEvento.controls[campo].touched) ||
            (this.formularioBuscadorEvento.controls[campo].touched && this.formularioBuscadorEvento.controls[campo].value=='0');
  }

  public changeFile(event:any){
    this.file = event.target.files[0];

    if(this.file){
      let name:string = this.file.name;  
      let extencion:string = String(name.split('.').pop());
      if(extencion!="pdf"){
        this.generalService.mensajeAlerta("El archivo tiene que ser PDF");
        this.formularioBuscadorEvento.get('documento')?.reset();
        return;
      }

      if((this.file.size/1000000)>10){
        this.generalService.mensajeAlerta("El archivo exede los 10 MB");
        this.formularioBuscadorEvento.get('documento')?.reset();
        return;
      }

      this.getBase64(this.file);
    }
  }

  private getBase64(file:File){
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.srcBase64 = reader.result;
    };
  }

  public async uploadLicencia(){
    if(this.formularioBuscadorEvento.invalid || this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioBuscadorEvento.markAllAsTouched();
      return;
    }

    if(this.socioSeguimientoUpdate!=this.aux){ // update
      let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR DOCUMENTO DE RESPALDO DE LICENCIA','¿Está seguro de actualizar este documento de respaldo para licencia del socio?','Sí, estoy seguro','No, Cancelar');
      if(!afirmativo){ 
        return;
      }
    }else{
      let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DOCUMENTO DE RESPALDO DE LICENCIA','¿Está seguro de registrar este documento de resapldo para licencia del socio?','Sí, estoy seguro','No, Cancelar');
      if(!afirmativo){ 
        return;
      }
    }
    

    let idSocioSegEvento:number = Number(this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.value);
    let nombreDocumento:string = this.file.name;
    let array:string[] = nombreDocumento.split('.');
    let extensionDocumento:string = array[array.length-1];
    let nombre:string = this.getNombreSocio(idSocioSegEvento);
    nombreDocumento = `${nombre} - LICENCIA - ${this.generalService.getFechaActualFormateado()}.${extensionDocumento}`;  

    let fd = new FormData();
    fd.append('file',this.file,nombreDocumento);

    this.updateNombreDocumentoLicenciaService(idSocioSegEvento,nombreDocumento,fd);

    this.getSociosDeEventoSiEstaSeleccionado();
    this.limpiarFormulario();
  }

  private getNombreSocio(idSocioSegEvento:number):string{
    let i:number = 0;
    let encotrado:boolean = false;
    let nombre:string = '';
    while(!encotrado && i<this.listaSocioSeguimientoEvento.length){
      if(this.listaSocioSeguimientoEvento[i].idSeguimientoEvento == idSocioSegEvento){
        nombre = this.listaSocioSeguimientoEvento[i].nombreCompleto;
        encotrado = true;
      }
      i++;
    }
    return nombre;
  }

  public cancelarUploadLicencia(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioBuscadorEvento.reset();
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.setValue('0');

    this.file = this.aux;
    this.srcBase64 = this.aux;
    this.eventoSeleccionado = this.aux;

    this.formularioBuscadorEvento.get('nombreEvento')?.enable();
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.enable();
    this.socioSeguimientoUpdate = this.aux;

    this.socioSegEventoSeleccionado = this.aux;
  } 


  // pdf view
  public zoomMinus(){
    this.zoomValue -= 0.1;
  }

  public zoomPlus(){
    this.zoomValue = this.zoomValue + 0.1;
  }

  // tabla socio seguimiento evento
  private setConfiguracionTabla(){
    this.tituloSocioSegEventoLicencia = ['#','codigoSocio','Nombre completo','En hora','Con retraso','Con licencia','Estado actual','Nombre documento'];
    this.campoSocioSegEventoLicencia = ['codigoSocio','nombreCompleto','cumplioEnHora','cumplioConRetraso','licencia','estadoSeguimientoEvento','nombreDocumento'];
    this.idSocioSegEventoLicencia = "idSeguimientoEvento";
  }

  public construirformularioBuscadorEventoTabla(){
    this.formularioBuscadorEventoTabla = this.fb.group({
      nombreEvento           : ['',[Validators.required],[]],
    });
  }

  public buscarEventoParaTabla(){
    let texto:string = this.formularioBuscadorEventoTabla.get('nombreEvento')?.value;
    texto=texto?.toLocaleLowerCase();
    this.listaSociosSegConLicencia = [];
    this.eventoTablaSeleccionado = this.aux;

    this.limpiarFormulario();

    if(texto?.trim()==''){
      this.listaEventosTabla = this.aux;
      return;
    }
    
    if(this.eventoTablaSeleccionado==this.aux){
      this.buscarListaEventoFiltradoPararTablaService(texto,'REALIZADO');    
    }else{
      this.eventoTablaSeleccionado = this.aux;
    } 
  }

  public seleccionarEventoParaTabla(evento:EventoVO){
    this.formularioBuscadorEventoTabla.get('nombreEvento')?.setValue(evento.nombre + ', ' + evento.fechaRealizacion);
    this.eventoTablaSeleccionado = evento;
    this.listaEventosTabla = this.aux;

    this.getSizeListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento);
    this.getListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento,this.pageDTO);
  }

  private getSociosDeEventoSiEstaSeleccionado(){
    if(this.eventoSeleccionado.idEvento == this.eventoTablaSeleccionado.idEvento &&  this.eventoTablaSeleccionado != null ){
      this.getSizeListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento);
      this.getListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento,this.pageDTO);
    }
  }

  public verActaDeEvento(socioSeguimientoVO:SocioSeguimientoVO){
    if(socioSeguimientoVO.nombreDocumento==this.aux){
      this.generalService.mensajeAlerta('Este socio no tiene ningun respaldo para su licencia')
      return;
    }

    // es la primera vez que visualiso
    if(this.socioSegEventoSeleccionado==this.aux){
      this.socioSegEventoSeleccionado = socioSeguimientoVO;
      this.getDocumentoSeguimientoSociosEventoService(socioSeguimientoVO.idSeguimientoEvento,0);
      return;
    }

    // volver a visualizar el mismo documento
    if(this.socioSegEventoSeleccionado.idSeguimientoEvento == socioSeguimientoVO.idSeguimientoEvento){
      this.srcBase64 = this.aux;
      this.socioSegEventoSeleccionado = this.aux;
      return;
    }
  
    this.getDocumentoSeguimientoSociosEventoService(socioSeguimientoVO.idSeguimientoEvento,0);
    this.socioSegEventoSeleccionado = socioSeguimientoVO;    
  }

  public descargarActaDeEvento(socioSeguimientoVO:SocioSeguimientoVO){
    if(socioSeguimientoVO.nombreDocumento==this.aux){
      this.generalService.mensajeAlerta('Este socio no tiene ningun respaldo pata su licencia')
      return;
    }
    
    this.getDocumentoSeguimientoSociosEventoService(socioSeguimientoVO.idSeguimientoEvento,1);
  }

  public updateEvento(socioSeguimientoVO:SocioSeguimientoVO){
    if(socioSeguimientoVO.nombreCompleto==this.aux){
      this.generalService.mensajeAlerta('Este socio no tiene ningun respaldo pata su licencia')
      return;
    }

    this.socioSeguimientoUpdate = socioSeguimientoVO;
    let nombreEvento:string = this.formularioBuscadorEventoTabla.get('nombreEvento')?.value;
    
    this.formularioBuscadorEvento.get('nombreEvento')?.setValue(nombreEvento);
    this.formularioBuscadorEvento.get('nombreEvento')?.disable();
    
    this.listaSocioSeguimientoEvento.push(socioSeguimientoVO);
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.setValue(socioSeguimientoVO.idSeguimientoEvento);
    this.formularioBuscadorEvento.get('socioSeguimientoEvento')?.disable();
    
    

  }

  public cambioDePagina(pageEvent : PageEvent){
    this.pageDTO = {
      page : pageEvent.pageIndex,
      size : pageEvent.pageSize,
      sort : '',
    }
    this.getSizeListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento);
    this.getListaSocioSeguimientoEventoConDocumentoDeLicenciaService(this.eventoTablaSeleccionado.idEvento,this.pageDTO);
  }

  //-------------------
  // Consumo API-REST |
  //-------------------
  public buscarListaEventoFiltradoService(texto:string,estadoEvento:string){
    this.listaEventos = this.eventoService.buscarListaEventoFiltrado(texto,estadoEvento);
  }

  public buscarListaEventoFiltradoPararTablaService(texto:string,estadoEvento:string){
    this.listaEventosTabla = this.eventoService.buscarListaEventoFiltrado(texto,estadoEvento);
  }
  
  public getListaSocioSeguimientoEventoService(idEvento:number,estadoSocioSegEvento:string){
    this.seguimientoEventoService.getListaSociosSeguimientoDeEvento(idEvento,estadoSocioSegEvento).subscribe(
      (resp)=>{
        this.listaSocioSeguimientoEvento = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaSocioSeguimientoEventoConDocumentoDeLicenciaService(idEvento:number,pageDTO:PageDTO){
    this.seguimientoEventoService.getListaSocioSeguimientoEventoConDocumentoDeLicencia(idEvento,pageDTO).subscribe(
      (resp)=>{
        this.listaSociosSegConLicencia = resp;
        if(this.listaSociosSegConLicencia.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getSizeListaSocioSeguimientoEventoConDocumentoDeLicenciaService(idEvento:number){
    this.seguimientoEventoService.getSizeListaSocioSeguimientoEventoConDocumentoDeLicencia(idEvento).subscribe(
      (resp)=>{
        this.countItemsSocioSegEventoLicencia = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }


  //----------------
  // documento pdf |
  //----------------
  public updateNombreDocumentoLicenciaService(idSegEvento:number,nombreDocumento:string,fd:FormData){
    this.seguimientoEventoService.updateNombreDocumentoLicencia(idSegEvento,nombreDocumento).subscribe(
      (resp)=>{
        this.uploadDocumentoLicenciaService(fd);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public uploadDocumentoLicenciaService(fd:FormData){
    this.seguimientoEventoService.uploadDocumentoLicencia(fd).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se actualizo el documento de respaldo para la licencia");
      },
      (err)=>{                
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getDocumentoSeguimientoSociosEventoService(idSocioSegEvento:number,tipoAccion:number){
    this.seguimientoEventoService.getDocumentoSeguimientoSociosEvento(idSocioSegEvento).subscribe(
      (resp)=>{
        if(resp){
          if(tipoAccion==0){ // visualizar
            this.getBase64(resp);
          }else if(tipoAccion==1){// descargar
            const url = window.URL.createObjectURL(new Blob([resp],{type:'application/pdf'}));
            window.open(url);
          }
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  //------------------
  //  confirm dialog |
  //------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

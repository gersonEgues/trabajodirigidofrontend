import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { EventoService } from '../../services/evento.service';
import { EstadoEvento } from '../../models/estadoEvento';
import { EventoVO } from '../../models/eventoVO';
import { SeguimientoEventoService } from '../../services/seguimiento-evento.service';
import { SocioSeguimientoVO } from '../../models/socioSeguimientoVO';
import { SeguimientoEventoCheckDTO } from '../../models/seguimientoEventoCheck';
import { EstadoSeguimientoEvento } from '../../models/estadoSeguimientoEvento';
import { EstadoMultaSeguimientoEvento } from '../../models/estadoMultaSeguimientoEvento';
import { SeguimientoEventoVO } from '../../models/seguimientoEventoVO';
import { CancelarSeguimientoEventoDTO } from '../../models/cancelarSeguimientoEventoDTO';
import { BuscadorComponent } from 'src/app/shared/components/buscador/buscador.component';
import { Observable } from 'rxjs';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-seguimiento-evento',
  templateUrl: './seguimiento-evento.component.html',
  styleUrls: ['./seguimiento-evento.component.css']
})
export class SeguimientoEventoComponent implements OnInit {
  aux : any = undefined;

  // Buscador
  listaEventosBusqueda!    : Observable<EventoVO[]>;
  estadoEventoSeleccionado : string = this.aux;
  eventoSeleccionado       : EventoVO = this.aux;
  @ViewChild('buscardorComponent') buscardorComponent! : BuscadorComponent;

  listaEventosPendiente!       : EventoVO[];
  listaEventos!                : EventoVO[];
  now!                         : Date;
  formularioSeguimientoEvento! : FormGroup;

  countItemsEventos            : number;
  pageEventoDTO!               : PageDTO;

  //Tabla lista de socios
  formularioSeleccionEvento!   : FormGroup;
  buscadorSocio!               : FormGroup;
  listaTitulosSociosSegEvento! : string[];
  listaCamposSociosSegEvento!  : string[];
  listaSociosEvento!           : any[]; // SocioSeguimientoVO, si pongo su tipado, en html no lo puedo generalizar: item[campo]
    
  nroSociosEnHora              : number = 0;
  nroSociosConRetraso          : number = 0;
  nroSociosConRetrasoFinal     : number = 0;
  nroSociosConLicencia         : number = 0;
  nroSociosConFalta            : number = 0;
  nroSociosTotal               : number = 0;
  contadorSocios               : number = 0;

  constructor(private fb                       : FormBuilder,
              private generalService           : GeneralService,
              private eventoService            : EventoService,
              private seguimientoEventoService : SeguimientoEventoService,
              private dialog                   : MatDialog,
              private router                   : Router) { }

  ngOnInit(): void {
    this.now = new Date();
    setInterval(() => {
      this.now = new Date();
    }, 1000);

    this.pageEventoDTO = {
      page : 0,
      size : 100,
    };

    this.construirFormularioSeguimientoEvento();    
    this.construirFormularioBuscadorSocio();
    this.getListaEventoPendienteFiltrado(EstadoEvento.PENDIENTE,this.pageEventoDTO);

    this.construirTablaListaSocios();
    this.construirFormularioSeleccionEvento();
  }

  public construirFormularioSeguimientoEvento(){
    this.formularioSeguimientoEvento = this.fb.group({
      evento : ['0',[Validators.required],[]],
      fecha  : [{value:this.generalService.getFechaActualFormateado(), disabled:false},[Validators.required],[]],
    });
  }

  public campoSegEventoValido(campo:string) : boolean{
    return this.formularioSeguimientoEvento.controls[campo].valid && this.formularioSeguimientoEvento.controls[campo].value!="0";
  }

  public campoSegEventoInvalido(campo:string) : boolean{   
    return  (this.formularioSeguimientoEvento.controls[campo].invalid && this.formularioSeguimientoEvento.controls[campo].touched) ||
            (this.formularioSeguimientoEvento.controls[campo].touched && this.formularioSeguimientoEvento.controls[campo].value=='0');
  }
  
  public async realizarEvento(){
    if(this.formularioSeguimientoEvento.invalid || this.formularioSeguimientoEvento.get('evento')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioSeguimientoEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REALIZAR EVENTO','¿Está seguro de llevar a cabo el evento?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return;
    }

    let idEvento:number = Number(this.formularioSeguimientoEvento.get('evento')?.value);
    this.cambioEstadoEventoService(idEvento,EstadoEvento.REALIZANDO);
  }

  public cancelar(){
    this.limpiarFormulario();
  }

  public async cancelarRealizacionEvento_pendiente(){
    if(this.formularioSeguimientoEvento.invalid || this.formularioSeguimientoEvento.get('evento')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioSeguimientoEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CANCELAR EVENTO','¿Está seguro de cancelar la realización del evento?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return;
    }

    let cancelarSeguimientoEventoDTO:CancelarSeguimientoEventoDTO = {
      idEvento                : Number(this.formularioSeguimientoEvento.get('evento')?.value),
      estadoMulta             : EstadoMultaSeguimientoEvento.NO_APLICA,
      estadoSeguimientoEvento : EstadoSeguimientoEvento.CANCELADO,
    }

    this.cambioEstadoEventoService(cancelarSeguimientoEventoDTO.idEvento,EstadoEvento.CANCELADO);
    this.cancelarSeguimientoEventoTodosService(cancelarSeguimientoEventoDTO);
  }

  private limpiarFormulario(){
    this.formularioSeguimientoEvento.reset();
    this.formularioSeguimientoEvento.get('evento')?.setValue('0');
    this.formularioSeguimientoEvento.get('fecha')?.setValue(this.generalService.getFechaActualFormateado());
  }

  //Tabla de soscios
  public construirFormularioBuscadorSocio(){
    this.buscadorSocio = this.fb.group({
      codigo    : ['',[],[]],
      nombres   : ['',[],[]],
    });
  }

  public construirFormularioSeleccionEvento(){
    this.formularioSeleccionEvento = this.fb.group({
      estado    : ['0',[Validators.required],[]],
      evento    : ['0',[Validators.required],[]],
    });
  }

  public construirTablaListaSocios(){
    this.listaTitulosSociosSegEvento = ['#','Cod. Socio','Nombre completo de socio','Cod. Medidor','Direccion','Hora llegada','Estado seg. evento','Estado multa','En hora','Retraso','Retraso final','Falta','Licencia'];
    this.listaCamposSociosSegEvento = ['codigoSocio','nombreCompleto','codigoMedidor','direccion','horaLlegada','estadoSeguimientoEvento','estadoMulta','cumplioEnHora','cumplioConRetraso','cumplioConRetrasoFinal','falta','licencia'];
  }

  public limpiarFormularioSocioSeguimientoEvento(){
    this.formularioSeleccionEvento.get('estado')?.setValue('0');
    this.buscardorComponent.limpiarFormulario();
    this.listaEventos = [];
    this.listaSociosEvento = [];
    this.estadoEventoSeleccionado = this.aux;

    this.nroSociosEnHora          = 0;
    this.nroSociosConRetraso      = 0;
    this.nroSociosConRetrasoFinal = 0;
    this.nroSociosConLicencia     = 0;
    this.nroSociosConFalta        = 0;
    this.nroSociosTotal           = 0;
    this.contadorSocios           = 0;
  }

  public buscarTextoPorCodigo(){
    let textoInput:string = String(this.buscadorSocio.get('codigo')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('nombres')?.setValue('');
    }


    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.codigoSocio.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }
  
  public buscarTextoPorNombre(){
    let textoInput:string = String(this.buscadorSocio.get('nombres')?.value);
    if(textoInput.trim()!=''){
      this.buscadorSocio.get('codigo')?.setValue('');
    }

    if(textoInput.trim()==''){
      this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
        socioSegEvento.seleccionado = false;
      });  
      return;
    }

    textoInput = textoInput.toLocaleLowerCase();
    this.listaSociosEvento.forEach((socioSegEvento:SocioSeguimientoVO) => {
      (socioSegEvento.nombreCompleto.toLocaleLowerCase().includes(textoInput))?socioSegEvento.seleccionado=true:socioSegEvento.seleccionado=false;
    });
  }

  public chancheEstadoEvento(evento:any){
    this.estadoEventoSeleccionado = evento.target.value;
    this.listaSociosEvento = [];

    this.nroSociosEnHora          = 0;
    this.nroSociosConRetraso      = 0;
    this.nroSociosConRetrasoFinal = 0;
    this.nroSociosConLicencia     = 0;
    this.nroSociosConFalta        = 0;
    this.nroSociosTotal           = 0;
    this.contadorSocios           = 0;
    
    this.buscardorComponent.limpiarFormulario();
  }
  
  public esTipoBoolean(data:any):boolean{
    return typeof data == "boolean";
  }

  public seleccionarSeguimientoEvento(socioSegEvento:SocioSeguimientoVO){
    socioSegEvento.seleccionado = !socioSegEvento.seleccionado;
  }

  public checkCampoSegEventoSocio(index:number,socioSegEvento:SocioSeguimientoVO,campo:string,event:any){  
    let hora:number = this.now.getHours();
    let horaRegistro:string = this.now.toLocaleTimeString();
    if(hora<10){
      horaRegistro = '0'+horaRegistro;
    }

    let estadoSeguimientoEvento : string = '';
    let estMulta                : string = '';
    let cmpEnHora               : boolean = false;
    let cmpConRetrazo           : boolean = false;
    let cmpConRetrazoFinal      : boolean = false;
    let lic                     : boolean = false;
    let falta                   : boolean = false;

    socioSegEvento.listado = true;
    if(campo=='cumplioEnHora' && event.target.checked){
      estadoSeguimientoEvento = EstadoSeguimientoEvento.CUMPLIO;
      estMulta = EstadoMultaSeguimientoEvento.NO_APLICA;
      cmpEnHora = true;
    }else if(campo=='cumplioConRetraso' && event.target.checked){
      estadoSeguimientoEvento = EstadoSeguimientoEvento.RETRASO;
      estMulta = EstadoMultaSeguimientoEvento.PENDIENTE;
      cmpConRetrazo = true;
    }else if(campo=='cumplioConRetrasoFinal' && event.target.checked){
      estadoSeguimientoEvento = EstadoSeguimientoEvento.RETRASO_FINAL;
      estMulta = EstadoMultaSeguimientoEvento.PENDIENTE;
      cmpConRetrazoFinal = true;
    }else if(campo=='licencia' && event.target.checked){
      estadoSeguimientoEvento = EstadoSeguimientoEvento.LICENCIA;
      estMulta = EstadoMultaSeguimientoEvento.NO_APLICA;
      lic = true;
    }else if(campo=='falta' && event.target.checked){
      estadoSeguimientoEvento = EstadoSeguimientoEvento.FALTO;
      estMulta = EstadoMultaSeguimientoEvento.PENDIENTE;
      falta = true;
    }else{
      socioSegEvento.listado = false;
      estadoSeguimientoEvento = EstadoSeguimientoEvento.PENDIENTE;
      estMulta = EstadoMultaSeguimientoEvento.PENDIENTE;
    }

    let seguimientoEventoCheck:SeguimientoEventoCheckDTO = {
      idSeguimientoEvento    : socioSegEvento.idSeguimientoEvento,
      cumplioEnHora          : cmpEnHora,
      cumplioConRetraso      : cmpConRetrazo,
      cumplioConRetrasoFinal : cmpConRetrazoFinal,
      licencia               : lic,
      falta                  : falta,
      horaLlegada            : horaRegistro,
      estadoMulta            : estMulta,
      estadoSegEvento        : estadoSeguimientoEvento,
    }
    this.checkSeguimientoEventoService(index,seguimientoEventoCheck);
  }



  public async finalizarEvento(){
    let afirmativo:Boolean = await this.getConfirmacion('FINALIZAR EVENTO','¿Está seguro de finalizar el evento?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo)
      return;
    
    this.cambioEstadoEventoSocioSeguimientoEventoService(this.eventoSeleccionado.idEvento,EstadoEvento.REALIZADO);
  }

  public async cancelarRealizacionEvento_realizando_realizado(){
    let afirmativo:Boolean = await this.getConfirmacion('CANCELAR EVENTO','¿Está seguro de cancelar el evento en curso?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return;
    }

    //idEvento                : this.idEventoSelecionado,
    let cancelarSeguimientoEventoDTO:CancelarSeguimientoEventoDTO = {
      idEvento                : this.eventoSeleccionado.idEvento,
      estadoMulta             : EstadoMultaSeguimientoEvento.NO_APLICA,
      estadoSeguimientoEvento : EstadoSeguimientoEvento.CANCELADO,
    }
    
    this.cambioEstadoEventoSocioSeguimientoEventoService(cancelarSeguimientoEventoDTO.idEvento,EstadoEvento.CANCELADO);
    this.cancelarSeguimientoEventoTodosService(cancelarSeguimientoEventoDTO);

  }

  private anlaizarListaSociosEvento(listaSociosSegEvento:SocioSeguimientoVO[]){
    this.nroSociosEnHora          = 0;
    this.nroSociosConRetraso      = 0;
    this.nroSociosConRetrasoFinal = 0;
    this.nroSociosConFalta        = 0;
    this.nroSociosConLicencia     = 0;
    this.nroSociosTotal           = 0;
    this.contadorSocios           = 0;

    listaSociosSegEvento.forEach(socioSegEvento => {
      this.nroSociosTotal += 1;
      socioSegEvento.listado = true;
      if(socioSegEvento.cumplioEnHora){
        this.nroSociosEnHora += 1;
        this.contadorSocios += 1;        
      }else if(socioSegEvento.cumplioConRetraso){
        this.nroSociosConRetraso += 1;
        this.contadorSocios += 1;
      }else if(socioSegEvento.cumplioConRetrasoFinal){
        this.nroSociosConRetrasoFinal += 1;
        this.contadorSocios += 1;
      }else if(socioSegEvento.falta){
        this.nroSociosConFalta += 1;
        this.contadorSocios += 1;
      }else if(socioSegEvento.licencia){
        this.nroSociosConLicencia += 1;
        this.contadorSocios += 1;
      }else{
        socioSegEvento.listado=false;
      }
    });
  }

  // ----- buscador eventos ------
  public buscarEventoBusqueda(texto:string){
    if(this.estadoEventoSeleccionado!=this.aux)
      this.busquedaEventoTodosListarFiltradoService(texto,this.estadoEventoSeleccionado);
  }

  public seleccionarEventoBusqueda(evento:EventoVO){
    this.listaSociosEvento = [];
    this.eventoSeleccionado = evento;
    if(this.eventoSeleccionado!=this.aux)
      this.getListaTodosLosSociosDeEventoService(this.eventoSeleccionado.idEvento); 
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  public isMiembroMesaDirectiva():boolean{
    return this.generalService.isMiembroMesaDirectiva();
  }

  public genearteArchivoXLSX():void{
    let tableElemnt:HTMLElement = document.getElementById('tableSeguimientoEvento')
    this.generalService.genearteArchivoXLSX(tableElemnt,'tabla seguimiento evento');
  }  
  
  //-------------------
  // Consumo API-REST |
  //-------------------
  // Evento
  public getListaEventoPendienteFiltrado(estadoEvento:string,pageDTO:PageDTO){
    this.eventoService.getListaEventoFiltrado(estadoEvento,pageDTO.page,pageDTO.size).subscribe(
      (resp)=>{
        this.listaEventosPendiente = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public busquedaEventoTodosListarFiltradoService(texto:string,estadoEvento:string) {
    this.listaEventosBusqueda = this.eventoService.busquedaEventoTodosFiltradoLista(texto,estadoEvento);
  }

  public cambioEstadoEventoService(idEvento:number,estadoEvento:string){
    this.eventoService.cambioEstadoEvento(idEvento,estadoEvento).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
        this.getListaEventoPendienteFiltrado(EstadoEvento.PENDIENTE,this.pageEventoDTO);
        this.limpiarFormulario();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cambioEstadoEventoSocioSeguimientoEventoService(idEvento:number,estadoEvento:string){
    this.eventoService.cambioEstadoEvento(idEvento,estadoEvento).subscribe(
      (resp)=>{
        this.limpiarFormularioSocioSeguimientoEvento();
        this.generalService.mensajeCorrecto(INFO_MESSAGE.success_save);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // Seguimiento evento
  public getListaTodosLosSociosDeEventoService(idEvento:number){
    this.seguimientoEventoService.getListaTodosLosSociosDeEvento(idEvento).subscribe(
      (resp)=>{
        this.listaSociosEvento = resp;
        this.anlaizarListaSociosEvento(this.listaSociosEvento);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public checkSeguimientoEventoService(index:number,seguimientoEventoCheck:SeguimientoEventoCheckDTO){
    this.seguimientoEventoService.checkSeguimientoEvento(seguimientoEventoCheck).subscribe(
      (resp)=>{
        let socioSeguimientoVO:SocioSeguimientoVO = resp;
        socioSeguimientoVO.seleccionado=true;
        this.listaSociosEvento.splice(index,1,socioSeguimientoVO);
        this.anlaizarListaSociosEvento(this.listaSociosEvento);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateSeguimientoEventoService(seguimientoEventoVO:SeguimientoEventoVO){
    this.seguimientoEventoService.updateSeguimientoEvento(seguimientoEventoVO).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateNumeracionReciboFaltaOretrazo(idEvento:number,estadoMulta:string ){
    this.seguimientoEventoService.updateNumeracionReciboFaltaOretrazo(idEvento,estadoMulta).subscribe(
      (resp)=>{
        if(resp.bandera)
          this.generalService.mensajeCorrecto("Numeración de socios que faltaron o se retrazaron actualizado!!!");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cancelarSeguimientoEventoTodosService(cancelarSeguimientoEventoDTO:CancelarSeguimientoEventoDTO){
    this.seguimientoEventoService.cancelarSeguimientoEventoTodos(cancelarSeguimientoEventoDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se llevo a cabo con exito la operación");
        this.getListaEventoPendienteFiltrado(EstadoEvento.PENDIENTE,this.pageEventoDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

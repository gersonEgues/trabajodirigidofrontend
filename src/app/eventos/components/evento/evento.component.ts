import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TipoEventoService } from '../../services/tipo-evento.service';
import { EventoService } from '../../services/evento.service';
import { TipoEventoVO } from '../../models/tipoEventoVO';
import { MesaDirectivaVO } from 'src/app/mesa-directiva/models/MesaDirectivaVO';
import { MesaDirectivaService } from '../../../mesa-directiva/services/mesa-directiva.service';
import { EventoDTO } from '../../models/eventoDTO';
import { EstadoEvento } from '../../models/estadoEvento';
import { EventoVO } from '../../models/eventoVO';
import { SeguimientoEventoService } from '../../services/seguimiento-evento.service';
import { SeguimientoEventoTodosLosSociosDTO } from '../../models/seguimientoEventoTodosLosSociosDTO';
import { EstadoSeguimientoEvento } from '../../models/estadoSeguimientoEvento';
import { EstadoMultaSeguimientoEvento } from '../../models/estadoMultaSeguimientoEvento';
import { SocioSeguimientoVO } from '../../models/socioSeguimientoVO';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { BanderaResponseVO } from 'src/app/shared/models/banderaResponseVO';
import { PageDTO } from 'src/app/shared/models/pageDTO';
import { PageEventDTO } from 'src/app/shared/models/pageEventDTO';
import { TablePaginatorComponent } from 'src/app/shared/components/table-paginator/table-paginator.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';
import { SocioMedidorVO } from 'src/app/shared/models/socio-medidor-vo';
import { SocioVO } from 'src/app/shared/models/socio-vo';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html',
  styleUrls: ['./evento.component.css']
})
export class EventoComponent implements OnInit {
  aux : any = undefined;
  formularioEvento!   : FormGroup;

  listaMesaDirectiva : MesaDirectivaVO[] = [];
  listaTipoEvento    : TipoEventoVO[] = [];
  banderaTodos       : boolean = true;

  // Buscar Socio
  tituloItems          : string[] = [];
  campoItems           : string[] = [];

  // Tabla socios seleccionados para participar en evento
  tituloSocioSeleccionado : string[] = [];
  campoSocioSeleccionado  : string[] = [];
  idSocioSeleccionado!    : string;
  listaSocioSeleccionado  : SocioMedidorVO[] = [];

  // Tabla de eventos
  listatituloEvento       : string[] = [];
  listaCampoEvento        : string[] = [];
  idItemEvento!           : string;
  listaEventos            : EventoVO[] = [];
  formularioEstadoEvento! : FormGroup;
  estadoEvento            : string = this.aux;
  countItemsEventos       : number;
  pageEventoDTO!          : PageDTO;
  

  // Tabla socios de evento
  listaTituloSocioEvento      : string[] = [];
  listaCampoSocioEvento       : string[] = [];
  idSocioEvento!              : string;
  listaSocioSeguimientoEvento : SocioSeguimientoVO[] = [];
  eventoSeleccionado          : EventoVO = this.aux; 
  formularioSociosEvento!     : FormGroup;

  eventoEditar     : EventoVO = this.aux;
  listaEstadoSocio : string[] = [];
  
  @ViewChild('tablaEvento') tablaEvento! : TablePaginatorComponent;
  @ViewChild('tablaSocio') tablaSocio!   : TablaComponent;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesaDirectivaService             : MesaDirectivaService,
              private tipoEventoService                : TipoEventoService,
              private seguimientoEventoService         : SeguimientoEventoService,
              private eventoService                    : EventoService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.pageEventoDTO = {
      page : 0,
      size : 10,
    };

    this.construirFormulario();
    this.configurarDatosComponenteBuscador();
    this.configurarDatosTablaSociosSeleccionados();
    
    this.getMesaDirectivaService();
    this.getTipoEventoService();

    this.configurarFormularioSocioEvento();
    this.configurarDatosTablaEventos();  
    this.configurarTablaSociosDeEvento()  
  }

  // gestion
  // periodo
  private construirFormulario(){
    this.formularioEvento = this.fb.group({
      mesaDirectiva     : ['0',[Validators.required],[]],
      tipoEvento        : ['0',[Validators.required],[]],
      nombre            : ['',[Validators.required],[]],
      ubicacion         : ['',[Validators.required],[]],
      fechaRegistro     : [{value:this.generalService.getFechaActualFormateado(),disabled:false},[Validators.required],[]],
      fechaRealizacion  : [{value:this.generalService.getFechaActualFormateado(),disabled:false},[Validators.required],[]],
      obligatorio       : [{value:'true',disabled:true},[Validators.required],[]],
      todos             : ['true',[Validators.required],[]],
      multaFalta        : ['',[Validators.required,Validators.min(0)],[]],
      multaRetrazo      : ['',[Validators.required,Validators.min(0)],[]],
      multaRetrasoFinal : ['',[Validators.required,Validators.min(0)],[]],
      horaReunion       : [{value:this.generalService.getHoraActualFormateado(),disabled:false},[Validators.required],[]],
      horaRetrazo       : [{value:this.generalService.getHoraActualFormateado(),disabled:false},[Validators.required],[]],
    });
  }

  public campoEventoSelectValido(campo:string) : boolean{
    return this.formularioEvento.controls[campo].valid && this.formularioEvento.controls[campo].value != "0";
  }

  public campoEventoSelectInvalido(campo:string) : boolean{   
    return  (this.formularioEvento.controls[campo].invalid && 
            this.formularioEvento.controls[campo].touched) || 
            (this.formularioEvento.controls[campo].touched && 
            this.formularioEvento.controls[campo].value=="0");
  }


  public campoEventoValido(campo:string) : boolean{
    return this.formularioEvento.controls[campo].valid
  }

  public campoEventoInvalido(campo:string) : boolean{   
    return this.formularioEvento.controls[campo].invalid && 
           this.formularioEvento.controls[campo].touched;
  }

  public async createEvento(){
    if(this.formularioEvento.invalid || this.formularioInvalido()){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioEvento.markAllAsTouched();
      return;
    }

    let fechaRegistro:string = this.formularioEvento.get('fechaRegistro')?.value;
    let fechaRealizacion:string = this.formularioEvento.get('fechaRealizacion')?.value;
    if(fechaRegistro>fechaRealizacion){
      this.generalService.mensajeAlerta("las fechas son incongruentes, la fecha de registro tiene que ser menor o igual a la fecha de realizacion");
      return;
    }

    //No todos los socios seran parte del evento
    if(!this.banderaTodos && this.listaSocioSeleccionado.length==0){
      this.generalService.mensajeAlerta("Tiene que seleccionar por lo menos un socio para esta reunion");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR NUEVO EVENTO','¿Está seguro de registrar este evento?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let evento:EventoDTO = this.getEventoDTO();
  
    this.createEventoService(evento);
  }

  public async updateEvento(){
    if(this.formularioEvento.invalid || this.formularioInvalido()){
      this.generalService.mensajeAlerta("Llene todos los campos");
      this.formularioEvento.markAllAsTouched();
      return;
    }

    let fechaRegistro:string = this.formularioEvento.get('fechaRegistro')?.value;
    let fechaRealizacion:string = this.formularioEvento.get('fechaRealizacion')?.value;
    if(fechaRegistro>fechaRealizacion){
      this.generalService.mensajeAlerta("las fechas son incongruentes, la fecha de registro tiene que ser menor o igual a la fecha de realizacion");
      return;
    }

    //No todos los socios seran parte del evento
    if(!this.banderaTodos && this.listaSocioSeleccionado.length==0){
      this.generalService.mensajeAlerta("Tiene que seleccionar por lo menos un socio para esta reunion");
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR EVENTO','¿Está seguro de actualizar el evento seleccionado?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }
    
    let evento:EventoVO = await this.getEventoVO(this.eventoEditar);
    let eventoUpdate:EventoVO = await this.updateEventoService(evento);
    let bandera : BanderaResponseVO = await this.deleteTodosLosSeguimientoEventoDeEventoService(evento.idEvento);
    
    let seguimientoEventoTodosLosSociosDTO:SeguimientoEventoTodosLosSociosDTO = {
      idEvento : evento.idEvento,
      estadoSeguimientoEvento : EstadoSeguimientoEvento.PENDIENTE,
      estadoMulta : EstadoMultaSeguimientoEvento.NO_APLICA,
    };

    if(eventoUpdate.todosLosSocios){
      this.createSeguimientoEventoParaTodosLosSocios(seguimientoEventoTodosLosSociosDTO);
    }else{
      this.listaSocioSeleccionado.forEach(socioMedidor => {
        this.createSeguimientoEvento(socioMedidor.idSocioMedidor,seguimientoEventoTodosLosSociosDTO);
      });
    }

    this.limpiarFormulario();
  }

  public cancelEvento(){
    this.limpiarFormulario();
  }

  public limpiarFormulario(){
    this.formularioEvento.reset();
    this.formularioEvento.get('mesaDirectiva')?.setValue('0');
    this.formularioEvento.get('tipoEvento')?.setValue('0');
    this.formularioEvento.get('fechaRegistro')?.setValue(this.generalService.getFechaActualFormateado());
    this.formularioEvento.get('fechaRealizacion')?.setValue(this.generalService.getFechaActualFormateado());
    this.formularioEvento.get('horaReunion')?.setValue(this.generalService.getHoraActualFormateado());
    this.formularioEvento.get('horaRetrazo')?.setValue(this.generalService.getHoraActualFormateado());
    this.formularioEvento.get('obligatorio')?.setValue(true);
    this.formularioEvento.get('todos')?.setValue(true);
    this.formularioEvento.get('todos')?.enable();
    this.listaSocioSeleccionado = [];
    this.banderaTodos = true;
    this.eventoEditar = this.aux;
  } 
  
  private getEventoDTO():EventoDTO{
    let evento:EventoDTO = {
      idMesaDirectiva   : this.formularioEvento.get('mesaDirectiva')?.value,
      idTipoEvento      : this.formularioEvento.get('tipoEvento')?.value,
      nombre            : this.formularioEvento.get('nombre')?.value,
      ubicacion         : this.formularioEvento.get('ubicacion')?.value,
      fechaRegistro     : this.formularioEvento.get('fechaRegistro')?.value,
      fechaRealizacion  : this.formularioEvento.get('fechaRealizacion')?.value,
      estadoEvento      : EstadoEvento.PENDIENTE,
      obligatorio       : this.formularioEvento.get('obligatorio')?.value,
      todosLosSocios    : this.formularioEvento.get('todos')?.value,
      multaFalta        : this.formularioEvento.get('multaFalta')?.value,
      multaRetraso      : this.formularioEvento.get('multaRetrazo')?.value,
      multaRetrasoFinal : this.formularioEvento.get('multaRetrasoFinal')?.value,
      horaReunion       : this.formularioEvento.get('horaReunion')?.value,
      horaRetraso       : this.formularioEvento.get('horaRetrazo')?.value,
    }
    return evento;
  } 

  private getEventoVO(eventoEditar:EventoVO):EventoVO{
    let evento:EventoVO = {
      idEvento          : eventoEditar.idEvento,
      idMesaDirectiva   : this.formularioEvento.get('mesaDirectiva')?.value,
      idTipoEvento      : this.formularioEvento.get('tipoEvento')?.value,
      nombre            : this.formularioEvento.get('nombre')?.value,
      ubicacion         : this.formularioEvento.get('ubicacion')?.value,
      fechaRegistro     : this.formularioEvento.get('fechaRegistro')?.value,
      fechaRealizacion  : this.formularioEvento.get('fechaRealizacion')?.value,
      estadoEvento      : eventoEditar.estadoEvento,
      obligatorio       : this.formularioEvento.get('obligatorio')?.value,
      todosLosSocios    : this.formularioEvento.get('todos')?.value,
      multaFalta        : this.formularioEvento.get('multaFalta')?.value,
      multaRetraso      : this.formularioEvento.get('multaRetrazo')?.value,
      multaRetrasoFinal : this.formularioEvento.get('multaRetrasoFinal')?.value,
      horaReunion       : this.formularioEvento.get('horaReunion')?.value,
      horaRetraso       : this.formularioEvento.get('horaRetrazo')?.value,
    }
    return evento;
  } 
  
  private formularioInvalido():boolean{
    return this.formularioEvento.get('mesaDirectiva')?.value == "0" || this.formularioEvento.get('tipoEvento')?.value == "0";
  }

  // eventos table
  public updateTipoeEventoEvent(any:any){
    this.aux = any;
    this.formularioEvento.get('cargo')?.setValue(any.nombre);
    this.formularioEvento.get('descripcion')?.setValue(any.descripcion);
    this.formularioEvento.get('jerarquia')?.setValue(any.jerarquia);
  }

  public async deleteTipoEventoEvent(any:any){
    this.aux  = any;
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR CARGO','¿Está seguro de eliminar este cargo?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    //this.deleteCargoDeMesaDirectivaService(any.id);
  }

  public changeTodos(){
    this.banderaTodos = this.formularioEvento.get('todos')?.value;
    if(this.banderaTodos){
      this.listaSocioSeleccionado = [];
    }
  }

  //Busqueda de socios
  public socioSeleccionadoEvent(socio:SocioMedidorVO){
    if(this.indexOf(this.listaSocioSeleccionado,socio)==-1){
      this.listaSocioSeleccionado.push(socio);
    }
  }

  private indexOf(lista:SocioMedidorVO[],socio:SocioMedidorVO):number{
    let encontrado:boolean = false;
    let i:number = 0;
    let index:number = -1;
    while(!encontrado&&i<lista.length){
      if(lista[i].idSocioMedidor == socio.idSocioMedidor){
        encontrado = true;
        index = i;
      }
      i++;
    }
    return index;
  }

  private configurarDatosComponenteBuscador(){
    this.tituloItems = ['#','Cod. socio','Nombre','Ape. Paterno','Ape. Materno','Cod. medidor','C.I.','Telefono'];
    this.campoItems = ['codigo','nombre','apellidoPaterno','apellidoMaterno','codigoMedidor','ci','telefono'];
  }
  
  // tabla socios seleccionados
  public deleteSocioSeleccionadoEvent(socio:SocioMedidorVO){
    let index:number = this.indexOf(this.listaSocioSeleccionado,socio);
    this.listaSocioSeleccionado.splice(index,1);
  }

  private configurarDatosTablaSociosSeleccionados(){
    this.tituloSocioSeleccionado = ['#','Código','Nombre','Ape. paterno','Ape. materno','Cod. medidor' ];
    this.campoSocioSeleccionado  = ['codigo','nombre','apellidoPaterno','apellidoMaterno','codigoMedidor'];
    this.idSocioSeleccionado     = "idSocio";
  }

  // Tabla eventos
  public configurarDatosTablaEventos(){
    this.formularioEstadoEvento = this.fb.group({
      estadoEvento : ['0',[Validators.required],[]],
    });

    this.listatituloEvento = ["#","Nombre","Ubicación","Fecha registro","Fecha realizacion","Estado evento","Obligatorio","Hora reunion","Hora retraso","Multa ret.","Multa ret. fin.","Multa falta","Todos"];
    this.listaCampoEvento = ["nombre","ubicacion","fechaRegistro","fechaRealizacion","estadoEvento","obligatorio","horaReunion","horaRetraso","multaRetraso","multaRetrasoFinal","multaFalta","todosLosSocios"];
    this.idItemEvento = 'idEvento';
  }

  public cambioEstadoEvento(){
    this.estadoEvento = this.formularioEstadoEvento.get('estadoEvento')?.value;
    this.setListaEstadoSeguimientoEvento();
    this.eventoSeleccionado = this.aux;

    this.getCountListaEventoFiltradoService(this.estadoEvento);
    this.getListaEventoFiltradoService(this.estadoEvento,this.pageEventoDTO);
    this.limpiarFormulario();
    this.tablaSocio.limpiarListaItems();
    this.tablaEvento.deseleccionarItems();

    this.listaSocioSeguimientoEvento = [];
    this.formularioSociosEvento.get('estadoSeguimientoSocioEvento')?.setValue('0')

  }

  public cambioDePaginaEventos(page:PageEventDTO){
    this.pageEventoDTO = {
      page : page.pageIndex,
      size : page.pageSize
    }
    this.getListaEventoFiltradoService(this.estadoEvento,this.pageEventoDTO);
  } 

  public selectEventoEvent(evento:EventoVO){
    this.eventoSeleccionado = evento;

    this.tablaSocio.limpiarListaItems();
    this.listaSocioSeguimientoEvento = [];
    this.formularioSociosEvento.get('estadoSeguimientoSocioEvento')?.setValue('0')
  }

  public async editarEventoEvent(evento:EventoVO){
    this.eventoEditar = evento;
    this.listaSocioSeleccionado = [];
    this.banderaTodos = evento.todosLosSocios;

    this.formularioEvento.get('mesaDirectiva')?.setValue(evento.idMesaDirectiva);
    this.formularioEvento.get('tipoEvento')?.setValue(evento.idTipoEvento);
    this.formularioEvento.get('fechaRegistro')?.setValue(evento.fechaRegistro);
    this.formularioEvento.get('fechaRealizacion')?.setValue(evento.fechaRealizacion);
    this.formularioEvento.get('nombre')?.setValue(evento.nombre);
    this.formularioEvento.get('ubicacion')?.setValue(evento.ubicacion);
    this.formularioEvento.get('multaFalta')?.setValue(evento.multaFalta);
    this.formularioEvento.get('multaRetrazo')?.setValue(evento.multaRetraso),
    this.formularioEvento.get('multaRetrasoFinal')?.setValue(evento.multaRetrasoFinal),
    this.formularioEvento.get('horaReunion')?.setValue(evento.horaReunion);
    this.formularioEvento.get('horaRetrazo')?.setValue(evento.horaRetraso);
    this.formularioEvento.get('obligatorio')?.setValue(evento.obligatorio);
    this.formularioEvento.get('todos')?.setValue(evento.todosLosSocios);
    this.formularioEvento.get('todos')?.disable();

    this.getListaSociosDeEventoService(evento.idEvento,EstadoSeguimientoEvento.PENDIENTE);
  }

  public async cancelarEventoEvent(evento:EventoVO){
    let afirmativo:Boolean = await this.getConfirmacion('CANCELAR EVENTO','¿Está seguro de cancelar la realizacion de este vento?','Sí, estoy seguro','No, cancelar acción');
    if(!afirmativo){ 
      return; 
    }
    
    this.cambioEstadoEventoService(evento.idEvento,EstadoEvento.CANCELADO);
    this.cambioEstadoSeguimientoTodosService(evento.idEvento,EstadoSeguimientoEvento.CANCELADO);
  }

  public getEstadoEvento(){
    return Object.values(EstadoEvento)
  }

  // Tabla de socios de evento
  public configurarFormularioSocioEvento(){
    this.formularioSociosEvento = this.fb.group({
      estadoSeguimientoSocioEvento : ['0',[Validators.required],[]],
    });
  }

  public configurarTablaSociosDeEvento(){
    this.listaTituloSocioEvento = ["#","Cod. socio","Nombre completo",'Cod. medidor',"Direccion","Nro. casa","Telefono"];
    this.listaCampoSocioEvento = ["codigoSocio","nombreCompleto",'codigoMedidor',"direccion","nroCasa","telefono"];
    this.idSocioEvento = 'idSocioMedidor';
  }

  public async darDeBajaASocioDeEvento(socioSeguimientoVO:SocioSeguimientoVO){
    let afirmativo:Boolean = await this.getConfirmacion('INHABILITAR SOCIO DE EVENTO','¿Está seguro de inhabilitar a socio del vento?','Sí, estoy seguro','No, cancelar acción');
    if(!afirmativo){ 
      return; 
    }
    this.cambioEstadoSeguimientoEventoService(socioSeguimientoVO.idSeguimientoEvento,EstadoSeguimientoEvento.CANCELADO);
  }

  public async darDeAltaASocioDeEvento(socioSeguimientoVO:SocioSeguimientoVO){
    let afirmativo:Boolean = await this.getConfirmacion('HABILITAR A SOCIO PARA EVENTO','¿Está seguro de habilitar a socio para el vento?','Sí, estoy seguro','No, cancelar acción');
    if(!afirmativo){ 
      return; 
    }
    this.cambioEstadoSeguimientoEventoService(socioSeguimientoVO.idSeguimientoEvento,EstadoSeguimientoEvento.PENDIENTE);
  }

  private setListaEstadoSeguimientoEvento(){    
      if(this.estadoEvento == EstadoEvento.PENDIENTE){
        this.listaEstadoSocio = [EstadoSeguimientoEvento.PENDIENTE];
      }else if(this.estadoEvento == EstadoEvento.REALIZANDO){
        this.listaEstadoSocio = [EstadoSeguimientoEvento.CUMPLIO,EstadoSeguimientoEvento.RETRASO,EstadoSeguimientoEvento.FALTO,EstadoSeguimientoEvento.LICENCIA,EstadoSeguimientoEvento.PENDIENTE];
      }else if(this.estadoEvento == EstadoEvento.REALIZADO){
        this.listaEstadoSocio = [EstadoSeguimientoEvento.CUMPLIO,EstadoSeguimientoEvento.RETRASO,EstadoSeguimientoEvento.FALTO,EstadoSeguimientoEvento.LICENCIA];
      }else if(this.estadoEvento == EstadoEvento.CANCELADO){
        this.listaEstadoSocio = [EstadoSeguimientoEvento.CANCELADO];
      }
  }

  public cambioEstadoSeguimientoEvento(){
    this.tablaSocio.limpiarListaItems();
    if(this.eventoSeleccionado!=this.aux){ // && !this.eventoSeleccionado.todosLosSocios      
      let estadoSocioSegEvento:string = this.formularioSociosEvento.get('estadoSeguimientoSocioEvento')?.value;
      this.getListaSociosSeguimientoDeEventoService(this.eventoSeleccionado.idEvento,estadoSocioSegEvento);
    }else{
      this.generalService.mensajeAlerta('Seleccione una reunión');
    }
  }

  public genearteArchivoXLSX():void{
    this.tablaSocio.genearteArchivoXLSX("Socios de reunión");
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  // Mesa directiva service
  private getMesaDirectivaService(){
    this.mesaDirectivaService.getListaMesaDirectivaFiltrado(true).subscribe(
      (resp)=>{
        this.listaMesaDirectiva = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );  
  }

  // Tipo evento service
  private getTipoEventoService(){
    this.tipoEventoService.getListaTipoEvento().subscribe(
      (resp)=>{
        this.listaTipoEvento = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // Evento service
  public getEventoService(id:number) {
    this.eventoService.getEvento(id).subscribe(
      (resp)=>{

      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getCountListaEventoFiltradoService(estadoEvento:string) {
    this.eventoService.getCountListaEventoFiltrado(estadoEvento).subscribe(
      (resp)=>{
        this.countItemsEventos = resp.countItem;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaEventoFiltradoService(estadoEvento:string,pageDto:PageDTO) {
    this.eventoService.getListaEventoFiltrado(estadoEvento,pageDto.page,pageDto.size).subscribe(
      (resp)=>{       
        this.listaEventos = resp;
        if(this.listaEventos.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createEventoService(eventoDTO:EventoDTO) {
    this.eventoService.createEvento(eventoDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se creo el evento correctamente");

        if(this.estadoEvento!=this.aux){
          this.getListaEventoFiltradoService(this.estadoEvento,this.pageEventoDTO);
        }

        let seguimientoEventoTodosLosSociosDTO:SeguimientoEventoTodosLosSociosDTO = {
          idEvento : resp.idEvento,
          estadoSeguimientoEvento : EstadoSeguimientoEvento.PENDIENTE,
          estadoMulta : EstadoMultaSeguimientoEvento.PENDIENTE,
        };
        
        if(this.banderaTodos){
          this.createSeguimientoEventoParaTodosLosSocios(seguimientoEventoTodosLosSociosDTO);
        }else{
          this.listaSocioSeleccionado.forEach(socio => {
            this.createSeguimientoEvento(socio.idSocioMedidor,seguimientoEventoTodosLosSociosDTO);
          });
        }
        this.limpiarFormulario();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateEventoService(eventoVO:EventoVO) : Promise<EventoVO>{
    return this.eventoService.updateEvento(eventoVO).toPromise()
      .then(resp=>{
        this.generalService.mensajeCorrecto("Se actualizo el evento correctamente");
        if(this.estadoEvento!=this.aux){
          this.getListaEventoFiltradoService(this.estadoEvento,this.pageEventoDTO);
        }
        return resp;
      })
      .catch(err=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
        return err;
      });
  }

  public deleteEventoService(id:number){
    this.eventoService.deleteEvento(id).subscribe(
      (resp)=>{

      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cambioEstadoEventoService(idEvento:number,estadoEvento:string) {
    this.eventoService.cambioEstadoEvento(idEvento,estadoEvento).subscribe(
      (resp)=>{
        this.getListaEventoFiltradoService(this.formularioEstadoEvento.get('estadoEvento')?.value,this.pageEventoDTO)
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // Seguimiento evento service
  // RealizarEventoSocioDTO
  public createSeguimientoEvento(idSocioMedidor:number,seguimientoEventoTodosLosSociosDTO:SeguimientoEventoTodosLosSociosDTO) {
    this.seguimientoEventoService.createSeguimientoEvento(idSocioMedidor,seguimientoEventoTodosLosSociosDTO).subscribe(
      (resp)=>{
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createSeguimientoEventoParaTodosLosSocios(seguimientoEventoTodosSociosDTO:SeguimientoEventoTodosLosSociosDTO) {
    this.seguimientoEventoService.createSeguimientoEventoParaTodosLosSocios(seguimientoEventoTodosSociosDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("Se programo el evento para todos los socios");
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaSociosSeguimientoDeEventoService(idEvento:number,estadoSeguimientoEvento:string) {
    this.seguimientoEventoService.getListaSociosSeguimientoDeEvento(idEvento,estadoSeguimientoEvento).subscribe(
      (resp)=>{
        this.listaSocioSeguimientoEvento = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getListaSociosDeEventoService(idEvento:number,estadoSeguimientoEvento:string) {
    this.seguimientoEventoService.getListaSociosDeEvento(idEvento,estadoSeguimientoEvento).subscribe(
      (resp)=>{
        this.listaSocioSeleccionado = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cambioEstadoSeguimientoEventoService(idSeguimientoEvento:number,estadoSeguimientoEvento:string) {
    this.seguimientoEventoService.cambioEstadoSeguimientoEvento(idSeguimientoEvento,estadoSeguimientoEvento).subscribe(
      (resp)=>{
        if(this.eventoSeleccionado!=this.aux)
          this.getListaSociosSeguimientoDeEventoService(this.eventoSeleccionado.idEvento,this.formularioSociosEvento.get('estadoSeguimientoSocioEvento')?.value);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public cambioEstadoSeguimientoTodosService(idEvento:number,estadoSegEvento:string) {
    this.seguimientoEventoService.cambioEstadoSeguimientoTodos(idEvento,estadoSegEvento).subscribe(
      (resp)=>{
        if(this.estadoEvento!=this.aux)
          this.getListaEventoFiltradoService(this.estadoEvento,this.pageEventoDTO);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteTodosLosSeguimientoEventoDeEventoService(idEvento:number) : Promise<BanderaResponseVO> {
    return this.seguimientoEventoService.deleteTodosLosSeguimientoEventoDeEvento(idEvento).toPromise()
      .then(resp=>{
        return resp;
      })
      .catch(err=>{
        return err;
      });
  }

  //  confirm dialog
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

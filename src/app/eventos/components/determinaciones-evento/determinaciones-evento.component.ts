import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { EventoService } from '../../services/evento.service';
import { EventoVO } from '../../models/eventoVO';
import { EstadoEvento } from '../../models/estadoEvento';
import { DeterminacionEventoDTO } from '../../models/determinacionEventoDTO';
import { DeterminacionesEventoService } from '../../services/determinaciones-evento.service';
import { DeterminacionEventoVO } from '../../models/determinacionEventoVO';
import { Observable } from 'rxjs';
import { BuscadorComponent } from 'src/app/shared/components/buscador/buscador.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-determinaciones-evento',
  templateUrl: './determinaciones-evento.component.html',
  styleUrls: ['./determinaciones-evento.component.css']
})
export class DeterminacionesEventoComponent implements OnInit {
  aux           : any = undefined;
  // Buscador
  listaEventosCreate! : Observable<EventoVO[]>;
  listaEventosListar! : Observable<EventoVO[]>;
  eventoSeleccionadoCreate : EventoVO = this.aux;
  eventoSeleccionadoListar : EventoVO = this.aux;
  eventoEditar       : EventoVO = this.aux;

  // Tabla
  tituloDeterminacionEvento  : string[] = [];
  campoDeterminacionEvento   : string[] = [];
  idDeterminacionEvento!     : string;
  listaDeterminacionEvento!  : DeterminacionEventoVO[];

  formularioDeterminacionesEvento!      : FormGroup;

  determinacionEventoEditarSeleccionado : DeterminacionEventoVO = this.aux;

  @ViewChild('tablaDeterminacionReunion') tablaDeterminacionReunion!   : TablaComponent;
  @ViewChild('buscardorCreateComponent') buscardorCreateComponent!     : BuscadorComponent;
  @ViewChild('buscardorListaComponent') buscardorListaComponent!       : BuscadorComponent;

  constructor(private fb                           : FormBuilder,
              private generalService               : GeneralService,
              private determinacionesEventoService : DeterminacionesEventoService,
              private eventoService                : EventoService,
              private dialog                       : MatDialog,
              private router                       : Router) { }

  ngOnInit(): void {
    this.construirFormularioCreateEvento();
    this.setConfiguracionTabla();
  }

  private construirFormularioCreateEvento(){
    this.formularioDeterminacionesEvento = this.fb.group({
      determinacionesEvento : this.fb.array([],[Validators.required],[]),//miembrosMesaDirectiva
    });
    this.aniadirNuevaDeterminacionFormulario();
  }

  public aniadirNuevaDeterminacionFormulario():void{
    this.determinacionesEvento.push(this.nuevaDeterminacionEventoFormulario());
  }

  public  get determinacionesEvento() : FormArray {
    return this.formularioDeterminacionesEvento.get('determinacionesEvento') as FormArray;
  }

  public nuevaDeterminacionEventoFormulario():FormGroup{
    return this.fb.group({
      nro                  : ['', [Validators.required],[]],
      nombreDeterminacion  : ['', [Validators.required]],
    });
  }

  // ---- buscador create ------
  public buscarEventoCreate(texto:string){
    this.busquedaEventoTodosCreateFiltradoService(texto,EstadoEvento.REALIZADO);
  }

  public seleccionarEventoCreate(evento:EventoVO){    
    this.eventoSeleccionadoCreate = evento;
  }

  // ----- buscador listar ------
  public buscarEventoListar(texto:string){
    this.listaDeterminacionEvento = this.aux;
    this.busquedaEventoTodosListarFiltradoService(texto,EstadoEvento.REALIZADO);
  }

  public seleccionarEventoListar(evento:EventoVO){
    this.eventoSeleccionadoListar = evento;
    if(this.eventoSeleccionadoListar!=this.aux){
      this.getListaDeterminacionesDeEventoService(evento.idEvento);
    }
  }
  // ---------------------------
  public eliminarDeterminacionEventoFormulario(index:number):void{
    this.determinacionesEvento.removeAt(index);
  }

  private setConfiguracionTabla(){
    this.tituloDeterminacionEvento = ['#','Nro','Nombre determinación'];
    this.campoDeterminacionEvento = ['nro','nombre'];
    this.idDeterminacionEvento = "id";
  }

  public async crearDeterminacionEvento(){
    if(this.formularioDeterminacionesEvento.invalid || this.eventoSeleccionadoCreate==this.aux){
      if(this.formularioDeterminacionesEvento.invalid){
        this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
        this.formularioDeterminacionesEvento.markAllAsTouched();
      }
      
      if(this.eventoSeleccionadoCreate==this.aux){
        this.generalService.mensajeError("Seleccione un evento");
      }

      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE DETERMINACIONES DE EVENTO','¿Está seguro de registrar las determinaciones del evento?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    let listaDetEvento:DeterminacionEventoDTO[] = this.getListaDeterminacionEventoDTO();

    listaDetEvento.forEach(detEvento => {
      this.createDeterminacionEventoService(detEvento);
    });

    this.limpiarFormulario();
  }
  
  public async actualizarDeterminacionEvento(){
    if(this.formularioDeterminacionesEvento.invalid){
      this.formularioDeterminacionesEvento.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZACION DETERMINACION DE EVENTO','¿Está seguro de actualizar la determinacion del evento?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let detEventoVO:DeterminacionEventoVO = this.getDeterminacionEventoVO();
    this.updateDeterminacionEventoService(detEventoVO);
  }
 
  public cancelarDeterminacionEvento(){
    this.limpiarFormulario();
  }

  public limpiarFormulario(){
    this.formularioDeterminacionesEvento.reset()
    this.determinacionEventoEditarSeleccionado = this.aux;

    while(this.determinacionesEvento.length!=1){
      this.determinacionesEvento.removeAt(1);
    }

    this.buscardorCreateComponent.limpiarFormulario();
  }

  public campoEventoValido(campo:string) : boolean{
    return this.formularioDeterminacionesEvento.controls[campo].valid && this.formularioDeterminacionesEvento.controls[campo].value!="0";
  }

  public campoEventoInvalido(campo:string) : boolean{   
    return  (this.formularioDeterminacionesEvento.controls[campo].invalid && this.formularioDeterminacionesEvento.controls[campo].touched) ||
            (this.formularioDeterminacionesEvento.controls[campo].touched && this.formularioDeterminacionesEvento.controls[campo].value=='0');
  }

  public campoDeterminacionEventoValido(index:number,campo:string) : boolean{
    return (this.determinacionesEvento.at(index) as FormGroup).controls[campo].valid;
  }

  public campoDeterminacionEventoInvalido(index:number,campo:string) : boolean{   
    return (this.determinacionesEvento.at(index) as FormGroup).controls[campo].invalid && 
           (this.determinacionesEvento.at(index) as FormGroup).controls[campo].touched;
  }

  private getListaDeterminacionEventoDTO():DeterminacionEventoDTO[]{
    let listaDeterminacionEvento:DeterminacionEventoDTO[] = [];
    let idEventoSelect:number = this.eventoSeleccionadoCreate.idEvento;

    this.determinacionesEvento.controls.forEach(detEvento=> {
      let nroDet  : number = Number(detEvento.get('nro')?.value);
      let nombreDet : string =  detEvento.get('nombreDeterminacion')?.value;

      let detEventoNuevo:DeterminacionEventoDTO = {
        idEvento : idEventoSelect,
        nro      : nroDet,
        nombre   : nombreDet 
      }

      listaDeterminacionEvento.push(detEventoNuevo);
    });

    return listaDeterminacionEvento;
  }

  private getDeterminacionEventoVO():DeterminacionEventoVO{
    let detEventoVO : DeterminacionEventoVO = {
      id       : this.determinacionEventoEditarSeleccionado.id,
      idEvento : this.determinacionEventoEditarSeleccionado.idEvento, // (this.formularioDeterminacionesEvento.get('nombreEvento')?.value),
      nombre   : this.determinacionesEvento.at(0).get('nombreDeterminacion')?.value,
      nro      : Number(this.determinacionesEvento.at(0).get('nro')?.value),
    }
    return detEventoVO;
  }

  public updateDeterminacionEventoEvent(determinacionEvento:DeterminacionEventoVO){
    this.limpiarFormulario();
    this.determinacionEventoEditarSeleccionado = determinacionEvento;

    this.determinacionesEvento.at(0).get('nro')?.setValue(determinacionEvento.nro);
    this.determinacionesEvento.at(0).get('nombreDeterminacion')?.setValue(determinacionEvento.nombre);

    let nombreEvento:string = this.buscardorListaComponent.getValue();
    this.buscardorCreateComponent.setValue(nombreEvento);
    this.buscardorCreateComponent.disable();
  }

  public async eliminarDeterminacionEventoEvent(determinacionEvento:DeterminacionEventoVO){
    this.limpiarFormulario();

    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR DE DETERMINACION DE EVENTO','¿Está seguro de eliminar esta determinaciones de evento?','Sí, estoy seguro','Cancelar');
    
    if(!afirmativo){ 
      return; 
    }

    this.deleteDeterminacionEventoService(determinacionEvento.id);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }
  
  public genearteArchivoXLSX():void{
    this.tablaDeterminacionReunion.genearteArchivoXLSX("Determinaciones de la reunión");
  }
  // ---------------------- 
  // -- Consumo API-REST --
  // ----------------------
  // Evento service
  public busquedaEventoTodosCreateFiltradoService(texto:string,estadoEvento:string) {
    this.listaEventosCreate = this.eventoService.busquedaEventoTodosFiltradoLista(texto,estadoEvento);
  }

  public busquedaEventoTodosListarFiltradoService(texto:string,estadoEvento:string) {
    this.listaEventosListar = this.eventoService.busquedaEventoTodosFiltradoLista(texto,estadoEvento);
  }

  // determinacion evento
  public getListaDeterminacionesDeEventoService(idEvento:number){
    this.determinacionesEventoService.getListaDeterminacionesDeEvento(idEvento).subscribe(
      (resp)=>{
        this.listaDeterminacionEvento = resp;
        if(this.listaDeterminacionEvento.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createDeterminacionEventoService(determinacionEventoDTO:DeterminacionEventoDTO){
    this.determinacionesEventoService.createDeterminacionEvento(determinacionEventoDTO).subscribe(
      (resp)=>{
        if(this.eventoSeleccionadoListar!=this.aux)
          this.getListaDeterminacionesDeEventoService(this.eventoSeleccionadoListar.idEvento);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateDeterminacionEventoService(determinacionEventoVO:DeterminacionEventoVO) {
    this.determinacionesEventoService.updateDeterminacionEvento(determinacionEventoVO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        if(this.eventoSeleccionadoListar!=this.aux)
          this.getListaDeterminacionesDeEventoService(this.eventoSeleccionadoListar.idEvento);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteDeterminacionEventoService(id:number) {
    this.determinacionesEventoService.deleteDeterminacionEvento(id).subscribe(
      (resp)=>{
        if(this.eventoSeleccionadoListar!=this.aux)
          this.getListaDeterminacionesDeEventoService(this.eventoSeleccionadoListar.idEvento);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
  
  //  ----------------
  // | confirm dialog |
  //  ----------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}
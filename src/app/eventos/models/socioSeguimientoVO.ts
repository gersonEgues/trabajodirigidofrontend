import { LicenciaComponent } from '../components/licencia/licencia.component';
export interface SocioSeguimientoVO {
  idSeguimientoEvento     : number,
  idEvento                : number,
  idSocio                 : number,

  idSocioMedidor          : number,
  idMedidor               : number,
  codigoMedidor           : string,

  codigoSocio             : string,
  nombreCompleto          : string,
  direccion               : string,
  nroCasa                 : string,
  telefonos               : string,
  horaLlegada             : string,
  estadoSeguimientoEvento : string,
  estadoMulta             : string,
  multaCancelada          : number,
  multaCondonada          : number,
  cumplioEnHora           : boolean,
  cumplioConRetraso       : boolean,
  cumplioConRetrasoFinal  : boolean,
  licencia                : boolean,
  falta?                  : boolean,
  listado?                : boolean, // si es oscio ya fue llamado/listado en la reunion
  seleccionado?           : boolean,
  
  cancelado?              : boolean,
  fechaRegistro?          : string,
  multaFalta?             : number,
  multaRetraso?           : number,
  multaRetrasoFinal?      : number,
  nroRecibo?              : number,
  nombreDocumento?        : string,
} 
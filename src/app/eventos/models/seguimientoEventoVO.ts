import { SeguimientoEventoDTO } from "./seguimientoEventoDTO";

export interface SeguimientoEventoVO extends SeguimientoEventoDTO{
  idSeguimientoEvento : number,
}
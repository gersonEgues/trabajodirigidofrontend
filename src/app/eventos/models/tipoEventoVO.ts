import { TipoEventoDTO } from './tipoEventoDTO';

export interface TipoEventoVO extends TipoEventoDTO{
  idTipoEvento : number,
}
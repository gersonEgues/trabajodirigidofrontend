export interface SeguimientoEventoCheckDTO{
  idSeguimientoEvento    : number,
  cumplioEnHora          : boolean,
  cumplioConRetraso      : boolean,
  cumplioConRetrasoFinal : boolean,
  licencia               : boolean,
  falta                  : boolean,
  horaLlegada            : string, 
  estadoMulta            : string,
  estadoSegEvento        : string    
}
export interface DeterminacionEventoDTO{
  idEvento : number,
  nro      : number,
  nombre   : string,
}
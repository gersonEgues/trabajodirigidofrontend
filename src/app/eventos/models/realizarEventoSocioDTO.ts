import { RealizarEventoTodosDTO } from './realizarEventoTodosDTO';

export interface RealizarEventoSocioDTO extends RealizarEventoTodosDTO{
  idSocio : number,
}

export interface CancelarSeguimientoEventoDTO{
  idEvento                : number,
  estadoMulta             : string,
  estadoSeguimientoEvento : string,
}
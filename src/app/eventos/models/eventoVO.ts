import { EventoDTO } from './eventoDTO';
export interface EventoVO extends EventoDTO{
  idEvento      : number,
  nombreActa?   : string,
  hover?        : boolean;
}
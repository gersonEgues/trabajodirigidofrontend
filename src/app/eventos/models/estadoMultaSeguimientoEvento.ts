export enum EstadoMultaSeguimientoEvento {
  NO_APLICA = "NO_APLICA",
  PENDIENTE = "PENDIENTE",
  CANCELADA = "CANCELADO",
  CONDONADA = "CONDONADO",
}
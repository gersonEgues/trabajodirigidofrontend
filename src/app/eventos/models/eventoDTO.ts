import { SocioSeguimientoVO } from "./socioSeguimientoVO";

export interface EventoDTO{
  idMesaDirectiva   : number,
  idTipoEvento      : number,
  nombre            : string,
  ubicacion         : string,
  fechaRegistro     : string,
  fechaRealizacion  : string,
  estadoEvento      : string,
  obligatorio       : boolean,
  todosLosSocios    : boolean,
  multaFalta        : number,
  multaRetraso      : number,
  multaRetrasoFinal : number,
  horaReunion       : string,
  horaRetraso       : string,

  // historial
  nroRowsListaSocioSeguimiento? : number,
  nombreActa?                   : string,
  listaSocioSeguimiento?        : SocioSeguimientoVO[]
}
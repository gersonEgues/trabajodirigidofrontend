import { DeterminacionEventoDTO } from "./determinacionEventoDTO";

export interface DeterminacionEventoVO extends DeterminacionEventoDTO{
  id : number,
}
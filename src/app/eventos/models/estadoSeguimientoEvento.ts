export enum EstadoSeguimientoEvento {
  PENDIENTE     = "PENDIENTE",
  LICENCIA      = "LICENCIA",
  CUMPLIO       = "CUMPLIO",
  RETRASO       = "RETRASO", 
  RETRASO_FINAL = "RETRASO_FINAL", 
  FALTO         = "FALTO",
  CANCELADO     = "CANCELADO",
}
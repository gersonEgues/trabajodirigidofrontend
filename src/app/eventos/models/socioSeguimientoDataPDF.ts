import { DatosGeneralesComiteVO } from 'src/app/shared/models/datosGeneralesComiteVO';
export interface SocioSeguimientoDataPdf {
  idSeguimientoEvento     : number,
  idEvento                : number,
  idSocio                 : number,
  codigoSocio             : string,
  nombreCompleto          : string,
  direccion               : string,
  nroCasa                 : string,
  telefonos               : string,
  cumplioEnHora           : boolean,
  cumplioConRetraso       : boolean,
  licencia                : boolean,
  horaLlegada             : string,
  estadoSeguimientoEvento : string,
  estadoMulta             : string,
  multaCancelada          : number,
  multaCondonada          : number,
  seleccionado?           : boolean,
  
  cancelado?              : boolean,
  fechaRegistro?          : string,
  multaFalta?             : number,
  multaRetraso?           : number,
  multaRetrasoFinal?      : number,
  nroRecibo?              : number,
  nombreDocumento?        : string,
  nombreReunion           : string,

  fechaActualHibirido     : string,
  fechaActual             : string,
  montoMultaNumero        : number,
  montoMultaLetra         : string,
  motivoMulta             : string
  horaActual              : string,
  observacion             : string,
  nombreEncargadoComite   : string,
  rolEncargadoComite      : string,
  
  datosComiteDeAgua       : DatosGeneralesComiteVO;
} 
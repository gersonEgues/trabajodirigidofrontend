export interface SeguimientoEventoTodosLosSociosDTO {
  idEvento : number,
  estadoSeguimientoEvento : string,
  estadoMulta : string,
}
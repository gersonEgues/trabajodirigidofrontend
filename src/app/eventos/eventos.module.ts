import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventosRoutingModule } from './eventos-routing.module';
import { TipoEventoComponent } from './components/tipo-evento/tipo-evento.component';
import { SeguimientoEventoComponent } from './components/seguimiento-evento/seguimiento-evento.component';
import { EventoComponent } from './components/evento/evento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DeterminacionesEventoComponent } from './components/determinaciones-evento/determinaciones-evento.component';
import { ActaEventoComponent } from './components/acta-evento/acta-evento.component';
import { PdfViewerModule }  from  'ng2-pdf-viewer';
import { LicenciaComponent } from './components/licencia/licencia.component';
@NgModule({
  declarations: [
    TipoEventoComponent,
    SeguimientoEventoComponent,
    EventoComponent,
    DeterminacionesEventoComponent,
    ActaEventoComponent,
    LicenciaComponent,
  ],
  imports: [
    CommonModule,
    EventosRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    PdfViewerModule,
  ]
})
export class EventosModule { }

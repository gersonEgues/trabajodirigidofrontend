import { CategoriaDTO } from './categoriaDTO';

export interface CategoriaVO extends CategoriaDTO{
  idCategoria : number
}
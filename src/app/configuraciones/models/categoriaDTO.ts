export interface CategoriaDTO{
  codigo : string,
  nombre : string,
  descripcion : string,
}
export interface PeriodoAnioSinCostoAguaAsignadoVO {
  idPeriodo : number,
  idAnio    : number,
  idMes     : number,
  anio      : number,
  numeroMes : number,
  nombreMes : string,
  check?    : boolean,
}
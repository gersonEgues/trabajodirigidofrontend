import { MesDTO } from './mesDTO';

export interface MesVO extends MesDTO{
  idMes : number,
  nro   : number,
}
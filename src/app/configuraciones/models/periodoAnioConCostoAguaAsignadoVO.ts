import { PeriodoAnioSinCostoAguaAsignadoVO } from "./periodoAnioSinCostoAguaAsignadoVO";

export interface PeriodoAnioConCostoAguaAsignadoVO extends PeriodoAnioSinCostoAguaAsignadoVO{
  idPeriodoCostoAgua : number,
  idCategoria        : number,
  nombrecategoria    : string,
  codigocategoria    : string,
  costoM3            : number,
  volumenMinimoM3    : number,
  costoMinimoM3      : number,
  observacion        : string,
}
import { PeriodoCostoAguaDTO } from "./periodoCostoAguaDTO";

export interface PeriodoCostoAguaVO extends PeriodoCostoAguaDTO{
  idPeriodoCostoAgua : number,
}
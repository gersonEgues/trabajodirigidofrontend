import { AnioDTO } from './anioDTO';

export interface AnioVO extends AnioDTO{
  idAnio : number;
}
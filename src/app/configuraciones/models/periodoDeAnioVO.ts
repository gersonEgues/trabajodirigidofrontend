export interface PeriodoDeAnioVO{
  idPeriodo : number,
  idAnio    : number,
  anio      : number,
  idMes     : number,
  nro       : number,
  nombre    : string,
}
export interface DatosComiteDeAguaDTO{
  nombreComite       : string,
  personeriaJuridica : string,
  fechaFundacion     : string,
  telefono1          : number,
  telefono2          : number,
  nombreDepartamento : string,
}

export interface PeriodoVO {
  idPeriodo : number,
  idAnio    : number,
  idMes     : number,
}
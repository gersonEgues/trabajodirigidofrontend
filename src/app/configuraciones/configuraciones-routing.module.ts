import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MesComponent } from './components/mes/mes.component';
import { GestionComponent } from './components/gestion/gestion.component';
import { CostoAguaComponent } from './components/costo-agua/costo-agua.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { DatosComiteDeAguaComponent } from './components/datos-comite-de-agua/datos-comite-de-agua.component';
import { AuthGuard } from '../login/guards/auth.guard';
import { RolGuard } from '../login/guards/rol.guard';
import { ROL } from '../shared/enums-mensajes/roles';

const routes: Routes = [
  {
    path : '',
    children: [
      { 
        path  : 'configuracion-mes',
        component: MesComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'configuracion-gestion',
        component: GestionComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'configuracion-categoria',
        component: CategoriaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'configuracion-costo-agua',
        component: CostoAguaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE }
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : 'datos-comite-de-agua',
        component: DatosComiteDeAguaComponent,
        data : [
          { nombre : ROL.ADMIN },
          { nombre : ROL.PRESIDENTE },
          { nombre : ROL.SECRETARIA },
        ],
        canActivate : [ AuthGuard, RolGuard ] 
      },
      { 
        path  : '**', 
        redirectTo : 'configuracion-mes' 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionesRoutingModule { }

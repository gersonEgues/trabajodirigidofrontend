import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DatosComiteDeAguaDTO } from '../models/datosComiteDeAguaDTO';

@Injectable({
  providedIn: 'root'
})
export class DatosComiteDeAguaService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/datos-cooperativa",http);
  }

  public getDatosComiteDeAgua() : Observable<DatosComiteDeAguaDTO> {
    return this.http.get<DatosComiteDeAguaDTO>(`${this.url}`).pipe(
      map(response => {
        return response as DatosComiteDeAguaDTO;
      })
    );
  }

  public createDatosComiteDeAgua(datosComiteDeAguaDTO:DatosComiteDeAguaDTO) : Observable<DatosComiteDeAguaDTO>{
    return this.http.post<DatosComiteDeAguaDTO>(`${this.url}`,datosComiteDeAguaDTO).pipe(
      map(resp => {
        return resp as DatosComiteDeAguaDTO;
      })
    );
  }

  public updateDatosComiteDeAgua(datosComiteDeAguaDTO:DatosComiteDeAguaDTO) : Observable<DatosComiteDeAguaDTO>{
    return this.http.put<DatosComiteDeAguaDTO>(`${this.url}`,datosComiteDeAguaDTO).pipe(
      map(resp => {
        return resp as DatosComiteDeAguaDTO;
      })
    );
  }
}

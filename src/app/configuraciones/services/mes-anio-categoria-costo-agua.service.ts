import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RootServiceService } from 'src/app/rootService/root-service.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MesVO } from '../models/mesVO';
import { AnioVO } from '../models/anioVO';
import { CategoriaVO } from '../models/categoriaVO';
import { PeriodoDeAnioVO } from '../models/periodoDeAnioVO';
import { CostoAguaVO } from '../models/costoAguaVO';
import { MesDTO } from '../models/mesDTO';
import { AnioDTO } from '../models/anioDTO';
import { CategoriaDTO } from '../models/categoriaDTO';
import { CostoAguaDTO } from '../models/costoAguaDTO';
import { PeriodoAnioSinCostoAguaAsignadoVO } from '../models/periodoAnioSinCostoAguaAsignadoVO';
import { PeriodoAnioConCostoAguaAsignadoVO } from '../models/periodoAnioConCostoAguaAsignadoVO';



import { PeriodoCostoAguaDTO } from '../models/periodoCostoAguaDTO';
import { PeriodoCostoAguaVO } from '../models/periodoCostoAguaVO';
import { PeriodoVO } from '../models/periodoVO';
import { BanderaResponseVO } from '../../shared/models/banderaResponseVO';

@Injectable({
  providedIn: 'root'
})
export class MesAnioCategoriaCostoAguaService extends RootServiceService{

  constructor(private http: HttpClient) { 
    super("api/configuracion-periodo-categoria-costo-agua",http);
  }

  public getPeriodo(idPeriodo:number) : Observable<PeriodoVO> {
    return this.http.get<PeriodoVO>(`${this.url}/periodo/${idPeriodo}`).pipe(
      map(response => {
        return response as PeriodoVO;
      })
    );
  }

  public getMes(idMes:number) : Observable<MesVO> {
    return this.http.get<MesVO>(`${this.url}/mes/${idMes}`).pipe(
      map(response => {
        return response as MesVO;
      })
    );
  }

  public getMeses() : Observable<MesVO[]> {
    return this.http.get<MesVO[]>(`${this.url}/mes/lista`).pipe(
      map(response => {
        return response as MesVO[];
      })
    );
  }

  public createMes(mesDTO:MesDTO) : Observable<MesVO>{
    return this.http.post<MesVO>(`${this.url}/mes`,mesDTO).pipe(
      map(resp => {
        return resp as MesVO;
      })
    );
  }

  public updateMes(mesVO:MesVO) : Observable<MesVO>{
    return this.http.put<MesVO>(`${this.url}/mes`,mesVO).pipe(
      map(resp => {
        return resp as MesVO;
      })
    );
  }

  public deleteMes(idMes:number) : Observable<BanderaResponseVO>{
    return this.http.delete<BanderaResponseVO>(`${this.url}/mes/${idMes}`).pipe(
      map(resp => {
        return resp as BanderaResponseVO;
      })
    );
  }

  //--- Gestion ---

  public getAnio(idAnio:number) : Observable<AnioVO> {
    return this.http.get<AnioVO>(`${this.url}/anio/${idAnio}`).pipe(
      map(response => {
        return response as AnioVO;
      })
    );
  }

  public getAnios() : Observable<AnioVO[]> {
    return this.http.get<AnioVO[]>(`${this.url}/anio/lista`).pipe(
      map(response => {
        return response as AnioVO[];
      })
    );
  }

  public createAnio(anioDTO:AnioDTO) : Observable<AnioVO> {
    return this.http.post<AnioVO>(`${this.url}/anio`,anioDTO).pipe(
      map(response => {
        return response as AnioVO;
      })
    );
  }

  public updateAnio(anioVO:AnioVO) : Observable<AnioVO> {
    return this.http.put<AnioVO>(`${this.url}/anio`,anioVO).pipe(
      map(response => {
        return response as AnioVO;
      })
    );
  }

  public deleteGestion(idGestion:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/anio/${idGestion}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public getCategoria(idCategoria:number) : Observable<CategoriaVO> {
    return this.http.get<CategoriaVO>(`${this.url}/categoria/${idCategoria}`).pipe(
      map(response => {
        return response as CategoriaVO;
      })
    );
  }

  public getCategorias() : Observable<CategoriaVO[]> {
    return this.http.get<CategoriaVO[]>(`${this.url}/categoria/lista`).pipe(
      map(response => {
        return response as CategoriaVO[];
      })
    );
  }

  public createCategoria(categoriaDTO:CategoriaDTO) : Observable<CategoriaVO> {
    return this.http.post<CategoriaVO>(`${this.url}/categoria`,categoriaDTO).pipe(
      map(response => {
        return response as CategoriaVO;
      })
    );
  }

  public updateCategoria(categoriaVO:CategoriaVO) : Observable<CategoriaVO> {
    return this.http.put<CategoriaVO>(`${this.url}/categoria`,categoriaVO).pipe(
      map(response => {
        return response as CategoriaVO;
      })
    );
  }

  public deleteCategoria(idCategoria:number) : Observable<BanderaResponseVO> {
    return this.http.delete<BanderaResponseVO>(`${this.url}/categoria/${idCategoria}`).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }

  public createCostosAgua(costoAguaDTO:CostoAguaDTO) : Observable<CostoAguaVO> {
    return this.http.post<CostoAguaVO>(`${this.url}/costo-agua`,costoAguaDTO).pipe(
      map(response => {
        return response as CostoAguaVO;
      })
    );
  }

  public updateCostosAgua(costoAguaVO:CostoAguaVO) : Observable<CostoAguaVO> {
    return this.http.put<CostoAguaVO>(`${this.url}/costo-agua`,costoAguaVO).pipe(
      map(response => {
        return response as CostoAguaVO;
      })
    );
  }

  //---------------------



  public getPeriodosDeAnio(idAnio:number) : Observable<PeriodoDeAnioVO[]> {
    return this.http.get<PeriodoDeAnioVO[]>(`${this.url}/periodos-de-anio/${idAnio}`).pipe(
      map(response => {
        return response as PeriodoDeAnioVO[];
      })
    );
  }

  public getPeriodosDeAnioConCostoAguaAsignados(idAnio:number,idCategoria:number) : Observable<PeriodoAnioConCostoAguaAsignadoVO[]> {
    return this.http.get<PeriodoAnioConCostoAguaAsignadoVO[]>(`${this.url}/periodos-con-costo-agua/${idAnio}/${idCategoria}`).pipe(
      map(response => {
        return response as PeriodoAnioConCostoAguaAsignadoVO[];
      })
    );
  }

  public getPeriodosDeAnioSinCostoAguaAsignados(idAnio:number,idCategoria:number) : Observable<PeriodoAnioSinCostoAguaAsignadoVO[]> {
    return this.http.get<PeriodoAnioSinCostoAguaAsignadoVO[]>(`${this.url}/periodos-sin-costo-agua/${idAnio}/${idCategoria}`).pipe(
      map(response => {
        return response as PeriodoAnioSinCostoAguaAsignadoVO[];
      })
    );
  }

  public registrarCostoAguaPorPeriodo(periodoCostoAguaDTO:PeriodoCostoAguaDTO): Observable<PeriodoCostoAguaVO>{
    return this.http.post<PeriodoCostoAguaVO>(`${this.url}/periodo-costo-agua`,periodoCostoAguaDTO).pipe(
      map(response => {
        return response as PeriodoCostoAguaVO;
      })
    );
  }

  public actualizarCostoAguaPorPeriodo(periodoCostoAguaVO:PeriodoCostoAguaVO): Observable<PeriodoCostoAguaVO>{
    return this.http.put<PeriodoCostoAguaVO>(`${this.url}/periodo-costo-agua`,periodoCostoAguaVO).pipe(
      map(response => {
        return response as PeriodoCostoAguaVO;
      })
    );
  }

  // -------- 
  public establecerPeriodoActivoService(idPeriodo:number) : Observable<BanderaResponseVO> {
    return this.http.put<BanderaResponseVO>(`${this.url}/periodo/${idPeriodo}`,null).pipe(
      map(response => {
        return response as BanderaResponseVO;
      })
    );
  }
}

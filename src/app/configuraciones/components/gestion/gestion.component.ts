import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MesAnioCategoriaCostoAguaService } from '../../services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from '../../models/anioVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AnioDTO } from '../../models/anioDTO';
import { PeriodoDeAnioVO } from '../../models/periodoDeAnioVO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.css']
})
export class GestionComponent implements OnInit {
  aux:any = undefined;

  tituloItemsGestion : string[] = [];
  campoItemsGestion  : string[] = [];
  idItemGestion!     : string;
  listaItemsGestion! : AnioVO[];

  gestionSeleccionado : AnioVO = this.aux;
  formularioGestion!  : FormGroup;

  // periodo : Anio
  listaItemsAnioConfiguracionCostoAgua!      : AnioVO[];

  // periodo : Anio
  tituloItemsPeriodoAnio : string[] = [];
  campoItemsPeriodoAnio  : string[] = [];
  idItemPeriodoAnio!     : string;

  // Periodo: Mes
  tituloItemsPeriodoMes : string[] = [];
  campoItemsPeriodoMes  : string[] = [];
  idItemPeriodoMes!     : string;
  listaItemsPeriodoMes! : PeriodoDeAnioVO[];

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configuracionGestion();
    this.configuracionPeriodo();
  }

  private configuracionGestion() : void{
    this.formularioGestion = this.fb.group({
      gestion : ['',[Validators.required],[]],
    });

    this.tituloItemsGestion = ['#','Gestión'];
    this.campoItemsGestion = ['anio'];
    this.idItemGestion  = 'idAnio'; 
    
    this.getListaGestionService();
  }

  public async createGestion(){   
    if(this.formularioGestion.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioGestion.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVA GESTIÓN','¿Está seguro de crear este año?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let gestionDTO : AnioDTO = {
      anio : this.formularioGestion.get('gestion')?.value
    }

    this.createGestionService(gestionDTO);
  }

  public async updateGestion(){
    if(this.formularioGestion.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioGestion.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR GESTIÓN','¿Está seguro de actualizar esta gestión?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let anioVO:AnioVO = {
      idAnio : this.gestionSeleccionado.idAnio,
      anio   : this.formularioGestion.get('gestion')?.value,
    }

    this.updateGestionService(anioVO);
  }

  public cancelGestion(){
    this.limpiarFormularioAnio();
  }

  private limpiarFormularioAnio(){
    let aux:any = undefined;
    this.gestionSeleccionado = aux;
    this.formularioGestion.reset();
  }

  public formularioAnioValido(campo:string) : boolean{
    return this.formularioGestion.controls[campo].valid;
  }

  public formularioAnioInvalido(campo:string) : boolean{   
    return this.formularioGestion.controls[campo].invalid && 
           this.formularioGestion.controls[campo].touched;
  }

  // tabla gestiones
  public updateGestionEvent(anioVO:AnioVO){
    this.gestionSeleccionado = anioVO;
    this.formularioGestion.get('gestion')?.setValue(anioVO.anio);
  }

  public async deleteGestion(anioVO:AnioVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR GESTIÓN','¿Está seguro de eliminar esta gestión?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.deleteGestionService(anioVO.idAnio);
  }

  // Periodo de anio
  private configuracionPeriodo() : void{
    this.tituloItemsPeriodoAnio = ['#','Gestión'];
    this.campoItemsPeriodoAnio = ['anio'];
    this.idItemPeriodoAnio  = 'idAnio'; 
    
    this.tituloItemsPeriodoMes = ['#','Gestion','Numero mes','Nombre Mes','Activo'];
    this.campoItemsPeriodoMes = ['anio','nro','nombre','activo'];
    this.idItemPeriodoMes  = 'idPeriodo';

    this.getListaGestionService();
  }

  public getPeriodosDeAnio(anioVO:AnioVO):void{
    this.getPeriodosDeAnioService(anioVO.idAnio);
  }

  public async establecerPeriodoActivo(periodo:PeriodoDeAnioVO){
    let afirmativo:Boolean = await this.getConfirmacion('ESTABLECER PERIODO ACTIVO',`¿Está seguro de establecer el periodo ${periodo.nombre} - ${periodo.anio}? como activo`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo)
      return; 
    this.establecerPeriodoActivoService(periodo);
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getListaGestionService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaItemsGestion = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private createGestionService(gestionDTO:AnioDTO){
    this.mesAnioCategoriaCostoAguaService.createAnio(gestionDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("La gestión se creo correctamente");
        this.getListaGestionService();
        this.limpiarFormularioAnio();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private updateGestionService(gestionVO:AnioVO){
    this.mesAnioCategoriaCostoAguaService.updateAnio(gestionVO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("La gestión se actualizo correctamente");
        this.getListaGestionService();
        this.limpiarFormularioAnio();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteGestionService(idGestion:number) {
    this.mesAnioCategoriaCostoAguaService.deleteGestion(idGestion).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto('Se elimino de manera correcta la gestion');
        this.getListaGestionService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // periodos
  private getPeriodosDeAnioService(idGestion:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnio(idGestion).subscribe(
      (resp)=>{
        this.listaItemsPeriodoMes = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public establecerPeriodoActivoService(periodo:PeriodoDeAnioVO){
    this.mesAnioCategoriaCostoAguaService.establecerPeriodoActivoService(periodo.idPeriodo).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se establecio ${periodo.nombre} - ${periodo.anio}, como periodo activo`);
        this.getPeriodosDeAnioService(periodo.idAnio);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MesAnioCategoriaCostoAguaService } from '../../services/mes-anio-categoria-costo-agua.service';
import { AnioVO } from '../../models/anioVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoriaVO } from '../../models/categoriaVO';
import { TipoAccionDTO } from '../../models/tipoAccionDTO';
import { PeriodoAnioSinCostoAguaAsignadoVO } from '../../models/periodoAnioSinCostoAguaAsignadoVO';
import { PeriodoAnioConCostoAguaAsignadoVO } from '../../models/periodoAnioConCostoAguaAsignadoVO';
import { PeriodoCostoAguaDTO } from '../../models/periodoCostoAguaDTO';
import { PeriodoCostoAguaVO } from '../../models/periodoCostoAguaVO';
import { TablaComponent } from 'src/app/shared/components/tabla/tabla.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-costo-agua',
  templateUrl: './costo-agua.component.html',
  styleUrls: ['./costo-agua.component.css']
})
export class CostoAguaComponent implements OnInit {
  listaItemsAccionConfiguracionCostoAgua!    : TipoAccionDTO[];
  listaItemsAnioConfiguracionCostoAgua!      : AnioVO[];
  listaItemsCategoriaConfiguracionCostoAgua! : CategoriaVO[];

  formularioBusquedaConfiguracionCostoAgua!   : FormGroup;
  formularioRegistroConfiguracionCostoAgua!   : FormGroup;

  listaTituloPeriodosDeAnioConCostoAguaAsignados! : string[];
  listaTituloPeriodosDeAnioSinCostoAguaAsignados! : string[];

  listaCampoItemPeriodosDeAnioConCostoAguaAsignados! : string[];
  listaCampoItemPeriodosDeAnioSinCostoAguaAsignados! : string[];

  idItemPeriodosDeAnioConCostoAguaAsignados! : string;
  idItemPeriodosDeAnioSinCostoAguaAsignados! : string;

  tipoAccion : number = 1;

  listaItemsPeriodosDeAnioConCostoAguaAsignados! : PeriodoAnioConCostoAguaAsignadoVO[];
  listaItemsPeriodosDeAnioSinCostoAguaAsignados! : PeriodoAnioSinCostoAguaAsignadoVO[];

  @ViewChild('tablaCostoAguaRegistrar') tablaCostoAguaRegistrar! : TablaComponent;
  @ViewChild('tablaCostoAguaActualizar') tablaCostoAguaActualizar! : TablaComponent;
  
  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configuracionCostoAguaPorPeriodo();
  }


  // configuracion  costo agua por periodo
  private configuracionCostoAguaPorPeriodo(){
    this.getListaAniosConfiguracionCostoAguaPeriodoService();
    this.getListaCategoriasConfiguracionCostoAguaPeriodoService();

    this.formularioBusquedaConfiguracionCostoAgua = this.fb.group({
      accion       : ['0',[Validators.required],[]],
      gestion      : ['0',[Validators.required],[]],
      categoria    : ['0',[Validators.required],[]], 
    });

    this.formularioRegistroConfiguracionCostoAgua = this.fb.group({
      costoM3         : ['',[Validators.required,Validators.min(1)],[]],
      costoMinimoM3   : ['',[Validators.required,Validators.min(1)],[]],
      volumenMinimoM3 : ['',[Validators.required,Validators.min(0)],[]],
      observacion     : ['',[Validators.required],[]],
    });

    
    this.listaTituloPeriodosDeAnioConCostoAguaAsignados = ['#','Gestión','Nro. Mes','Nombre Mes','Codigo','Categoria','Costo M3','Costo Min. M3','Vol. Minimo M3','Observación'];
    this.listaCampoItemPeriodosDeAnioConCostoAguaAsignados = ['anio','numeroMes','nombreMes','codigoCategoria','nombreCategoria','costoM3','costoMinimoM3','volumenMinimoM3','observacion'];   
    
    this.listaTituloPeriodosDeAnioSinCostoAguaAsignados = ['#','Gestión','Nro. Mes','Nombre Mes'];
    this.listaCampoItemPeriodosDeAnioSinCostoAguaAsignados = ['anio','numeroMes','nombreMes'];
    
    

    this.idItemPeriodosDeAnioConCostoAguaAsignados = 'idPeriodo';
    this.idItemPeriodosDeAnioSinCostoAguaAsignados = 'idPeriodo';
  }

  public cambioTipoAccion(){
    this.tipoAccion = this.formularioBusquedaConfiguracionCostoAgua.get('accion')?.value;
    this.getConfiguracionCostoAguaPorPeriodo();
  }
  
  public formularioBusquedaConfiguracionCostoAguaValido(campo:string) : boolean{
    return  this.formularioBusquedaConfiguracionCostoAgua.controls[campo].valid && 
            this.formularioBusquedaConfiguracionCostoAgua.controls[campo].value!=0;
  }

  public formularioBusquedaConfiguracionCostoAguaInvalido(campo:string) : boolean{   
    return  (this.formularioBusquedaConfiguracionCostoAgua.controls[campo].invalid && 
            this.formularioBusquedaConfiguracionCostoAgua.controls[campo].touched ) || 
            (this.formularioBusquedaConfiguracionCostoAgua.controls[campo].value==0 &&
            this.formularioBusquedaConfiguracionCostoAgua.controls[campo].touched);
  }

  public formularioRegistroConfiguracionCostoAguaValido(campo:string) : boolean{
    return  this.formularioRegistroConfiguracionCostoAgua.controls[campo].valid;
  }

  public formularioRegistroConfiguracionCostoAguaInvalido(campo:string) : boolean{   
    return  this.formularioRegistroConfiguracionCostoAgua.controls[campo].invalid && 
            this.formularioRegistroConfiguracionCostoAgua.controls[campo].touched;
  }

  public getConfiguracionCostoAguaPorPeriodo(){
    this.listaItemsPeriodosDeAnioConCostoAguaAsignados = [];
    this.listaItemsPeriodosDeAnioSinCostoAguaAsignados = [];
    
    if(this.formularioBusquedaConfiguracionCostoAgua.get('accion')?.value=='0' || this.formularioBusquedaConfiguracionCostoAgua.get('gestion')?.value=='0' || this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.value=='0'){ return; }

    let tipoAccion:number = this.formularioBusquedaConfiguracionCostoAgua.get('accion')?.value;
    let idGestion : number = this.formularioBusquedaConfiguracionCostoAgua.get('gestion')?.value;;
    let idCategoria : number = this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.value;;

    if(tipoAccion==1){// resgistrar
      this.getPeriodosDeAnioSinCostoAguaAsignadosService(idGestion,idCategoria);
    }else{ // actualizar
      this.getPeriodosDeAnioConCostoAguaAsignadosService(idGestion,idCategoria); 
    }
  }

  public async registrarCostoAguaPorPeriodo(){
    if(this.formularioRegistro_Actualizacion_ConfiguracionCostoAguaInvalido()){ 
      return; 
    }
    
    let afirmativo:Boolean = await this.getConfirmacion('REGISTRAR EL COSTO DE AGUA POR PERIODO','¿Está seguro de asignar el costo de agua a los periodos seleccionados?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ return; }

    for (const item of this.listaItemsPeriodosDeAnioSinCostoAguaAsignados) {
      if(item.check){
        let periodoCostoAguaDTO:PeriodoCostoAguaDTO = await this.getPeriodoCostoAguaDTO_forCreate(item);
        await this.registrarCostoAguaPorPeriodoService(periodoCostoAguaDTO);
      }
    }
    this.reloadFormularioConfiguracionCostoAguaPorPeriodo();    
  }

  public async actualizarCostoAguaPorPeriodo(){
    if(this.formularioRegistro_Actualizacion_ConfiguracionCostoAguaInvalido()){ return; }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR EL COSTO DE AGUA POR PERIODO','¿Está seguro de actualizar el costo de agua a los periodos seleccionados?','Sí, estoy seguro','Cancelar');
    if(!afirmativo){ return; }

    for (const item of this.listaItemsPeriodosDeAnioConCostoAguaAsignados) {
      if(item.check){
        let periodoCostoAguaVO:PeriodoCostoAguaVO = this.getPeriodoCostoAguaDTO_forUpdate(item);
        await this.actualizarCostoAguaPorPeriodoService(periodoCostoAguaVO);
      }
    }

    this.reloadFormularioConfiguracionCostoAguaPorPeriodo();
  }

  private formularioRegistro_Actualizacion_ConfiguracionCostoAguaInvalido(){
    let invalido : boolean = false;

    if(this.formularioBusquedaConfiguracionCostoAgua.get('accion')?.value=='0' || this.formularioBusquedaConfiguracionCostoAgua.get('gestion')?.value=='0' || this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.value=='0'){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioBusquedaConfiguracionCostoAgua.markAllAsTouched();
      invalido = true;; 
    }

    if(this.formularioRegistroConfiguracionCostoAgua.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioRegistroConfiguracionCostoAgua.markAllAsTouched();
      invalido = true;
    }

    let itemCheck:boolean = false;
    for (const item of this.listaItemsPeriodosDeAnioConCostoAguaAsignados) {
      if(item.check)
        itemCheck = true;
    }

    for (const item of this.listaItemsPeriodosDeAnioSinCostoAguaAsignados) {
      if(item.check)
        itemCheck = true;
    }

    if(itemCheck==false){
      this.generalService.mensajeAlerta("Seleccione al menos algun periodo de la gestión");
      invalido = true;
    }

    return invalido;
  }

  private getPeriodoCostoAguaDTO_forCreate(item:PeriodoAnioSinCostoAguaAsignadoVO):PeriodoCostoAguaDTO{
    let periodoCostoAguaDTO : PeriodoCostoAguaDTO = {
      idPeriodo       : item.idPeriodo,
      idCategoria     : this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.value,  
      costoM3         : this.formularioRegistroConfiguracionCostoAgua.get('costoM3')?.value,      
      volumenMinimoM3 : this.formularioRegistroConfiguracionCostoAgua.get('volumenMinimoM3')?.value,
      costoMinimoM3   : this.formularioRegistroConfiguracionCostoAgua.get('costoMinimoM3')?.value,
      observacion     : this.formularioRegistroConfiguracionCostoAgua.get('observacion')?.value,
    }

    setTimeout(() => {
    }, 5000);

    
    return periodoCostoAguaDTO;
  }

  private getPeriodoCostoAguaDTO_forUpdate(item:PeriodoAnioConCostoAguaAsignadoVO):PeriodoCostoAguaVO{
    let periodoCostoAguaVO : PeriodoCostoAguaVO = {
      idPeriodoCostoAgua : item.idPeriodoCostoAgua,
      idPeriodo          : item.idPeriodo,
      idCategoria        : this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.value,  
      costoM3            : this.formularioRegistroConfiguracionCostoAgua.get('costoM3')?.value,      
      volumenMinimoM3    : this.formularioRegistroConfiguracionCostoAgua.get('volumenMinimoM3')?.value,
      costoMinimoM3      : this.formularioRegistroConfiguracionCostoAgua.get('costoMinimoM3')?.value,
      observacion        : this.formularioRegistroConfiguracionCostoAgua.get('observacion')?.value,
    }
    return periodoCostoAguaVO;
  }

  private reloadFormularioConfiguracionCostoAguaPorPeriodo(){
    this.getConfiguracionCostoAguaPorPeriodo();
    this.formularioRegistroConfiguracionCostoAgua.reset();
    //this.formularioRegistroConfiguracionCostoAgua.get('costoM3')?.setValue('0');
  }

  public cancelCostoAguaPorPeriodo(){    
    this.formularioRegistroConfiguracionCostoAgua.reset();

    this.formularioBusquedaConfiguracionCostoAgua.reset();
    this.formularioBusquedaConfiguracionCostoAgua.get('accion')?.setValue('0');
    this.formularioBusquedaConfiguracionCostoAgua.get('gestion')?.setValue('0');
    this.formularioBusquedaConfiguracionCostoAgua.get('categoria')?.setValue('0');

    this.tipoAccion=1;
    this.listaItemsPeriodosDeAnioSinCostoAguaAsignados = [];
    this.listaItemsPeriodosDeAnioConCostoAguaAsignados = [];
  }

  private async registrarCostoAguaPorPeriodoService(periodoCostoAguaDTO:PeriodoCostoAguaDTO):Promise<PeriodoCostoAguaVO>{   
    let resp:any = await this.mesAnioCategoriaCostoAguaService.registrarCostoAguaPorPeriodo(periodoCostoAguaDTO).toPromise()
      .then(data=>{
        this.generalService.mensajeCorrecto("Se registro el periodo de manera exitosa.");
        return data;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);
      });
    return resp;
  }

  private async actualizarCostoAguaPorPeriodoService(periodoCostoAguaVO:PeriodoCostoAguaVO):Promise<PeriodoCostoAguaVO>{   
    let resp:any = await this.mesAnioCategoriaCostoAguaService.actualizarCostoAguaPorPeriodo(periodoCostoAguaVO).toPromise()
      .then(data=>{
        this.generalService.mensajeCorrecto("Se actualizo el periodo de manera exiosa.");
        return data;
      })
      .catch(err=>{
        this.generalService.mensajeError(err.message);        
        return err;
      });
    return resp;
  }

  public genearteArchivoXLSX():void{
    if(this.tipoAccion==1){
      this.tablaCostoAguaRegistrar.genearteArchivoXLSX("costoAguaPorPeriodo");
    }else{
      this.tablaCostoAguaActualizar.genearteArchivoXLSX("lista de costo de agua por periodo");
    }
  }
  
  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------

  private getListaAniosConfiguracionCostoAguaPeriodoService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaItemsAnioConfiguracionCostoAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  private getListaCategoriasConfiguracionCostoAguaPeriodoService(){
    this.mesAnioCategoriaCostoAguaService.getCategorias().subscribe(
      (resp)=>{
        this.listaItemsCategoriaConfiguracionCostoAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public getPeriodosDeAnioConCostoAguaAsignadosService(idAnio:number,idCategoria:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnioConCostoAguaAsignados(idAnio,idCategoria).subscribe(
      (resp)=>{
        this.listaItemsPeriodosDeAnioConCostoAguaAsignados = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  public getPeriodosDeAnioSinCostoAguaAsignadosService(idAnio:number,idCategoria:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnioSinCostoAguaAsignados(idAnio,idCategoria).subscribe(
      (resp)=>{
        this.listaItemsPeriodosDeAnioSinCostoAguaAsignados = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  } 

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

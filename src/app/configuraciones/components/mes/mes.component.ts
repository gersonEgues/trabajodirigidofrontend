import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MesAnioCategoriaCostoAguaService } from '../../services/mes-anio-categoria-costo-agua.service';
import { MesVO } from '../../models/mesVO';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { MesDTO } from '../../models/mesDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-mes',
  templateUrl: './mes.component.html',
  styleUrls: ['./mes.component.css']
})
export class MesComponent implements OnInit {
  tituloItemsMes : string[] = [];
  campoItemsMes  : string[] = [];
  idItemMes!     : string;
  listaItemsMes! : any[];

  mesSeleccionado! : MesVO;
  formularioMes!   : FormGroup;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configuracionMes();
  }

  private configuracionMes() : void{
    this.formularioMes = this.fb.group({
      mes : ['',[Validators.required],[]],
    });

    this.tituloItemsMes = ['#','Numero','Nombre del mes'];
    this.campoItemsMes = ['nro','nombre'];
    this.idItemMes  = 'idMes';
    
    this.getListaMesesService();
  }

  public async createMes(){   
    if(this.formularioMes.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioMes.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVO MES','¿Está seguro de crear este mes?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let mesDTO : MesDTO = {
      nombre : this.formularioMes.get('mes')?.value
    }

    this.createMesService(mesDTO);
  }

  public updateMesEvent(mesVO:MesVO){
    this.mesSeleccionado = mesVO;
    this.formularioMes.get('mes')?.setValue(mesVO.nombre);
  }

  public async updateMes(){
    if(this.formularioMes.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioMes.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR MES','¿Está seguro de actualizar el nombre de este mes?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    let mesVO:MesVO = {
      idMes  : this.mesSeleccionado.idMes,
      nro    : this.mesSeleccionado.nro,
      nombre : this.formularioMes.get('mes')?.value,
    }

    this.updateMesService(mesVO);
  }

  public async deleteMes(mes:MesVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR MES','¿Está seguro de eliminar este mes?','Sí, estoy seguro','No, Cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.deleteMesService(mes.idMes);
  }  

  public cancelMes(){
    this.limpiarFormularioMes();
  }

  private limpiarFormularioMes(){
    let aux:any = undefined;
    this.mesSeleccionado = aux;
    this.formularioMes.reset();
  }

  public formularioMesValido(campo:string) : boolean{
    return this.formularioMes.controls[campo].valid
  }

  public formularioMesInvalido(campo:string) : boolean{   
    return this.formularioMes.controls[campo].invalid && 
           this.formularioMes.controls[campo].touched;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getListaMesesService(){
    this.mesAnioCategoriaCostoAguaService.getMeses().subscribe(
      (resp)=>{
        this.listaItemsMes = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private createMesService(mesDTO:MesDTO){
    this.mesAnioCategoriaCostoAguaService.createMes(mesDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("El mes se creo correctamente");
        this.getListaMesesService();
        this.limpiarFormularioMes();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private updateMesService(mesVO:MesVO){
    this.mesAnioCategoriaCostoAguaService.updateMes(mesVO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto("El mes se actualizo correctamente");
        this.getListaMesesService();
        this.limpiarFormularioMes();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteMesService(idMes:number) {
    this.mesAnioCategoriaCostoAguaService.deleteMes(idMes).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto('Se elimino el mes de manera correcta');
        this.getListaMesesService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }      
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

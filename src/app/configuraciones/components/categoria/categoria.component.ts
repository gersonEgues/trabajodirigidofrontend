import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { MesAnioCategoriaCostoAguaService } from '../../services/mes-anio-categoria-costo-agua.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { CategoriaVO } from '../../models/categoriaVO';
import { CategoriaDTO } from '../../models/categoriaDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  aux:any = undefined;
  
  tituloItemsCategoria : string[] = [];
  campoItemsCategoria  : string[] = [];
  idItemCategoria!     : string;
  listaItemsCategoria! : CategoriaVO[];

  categoriaSeleccionada : CategoriaVO = this.aux;
  formularioCategoria!  : FormGroup;

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private dialog                           : MatDialog,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.configuracionCategoria();
  }

  private configuracionCategoria() : void{
    this.formularioCategoria = this.fb.group({
      codigo      : ['',[Validators.required],[]],
      nombre      : ['',[Validators.required],[]],
      descripcion : ['',[Validators.required],[]],
    });

    this.tituloItemsCategoria = ['#','Código','Nombre','Descripción'];
    this.campoItemsCategoria = ['codigo','nombre','descripcion'];
    this.idItemCategoria  = 'idCategoria';
    
    this.getListaCategoriasService();
  }

  public async createCategoria(){   
    if(this.formularioCategoria.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCategoria.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('CREAR NUEVA CATEGORÍA',`¿Está seguro de crear la nueva categoría ${this.formularioCategoria.get('nombre')?.value}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    let categoriaDTO : CategoriaDTO = {
      codigo      : this.formularioCategoria.get('codigo')?.value,
      nombre      : this.formularioCategoria.get('nombre')?.value,
      descripcion : this.formularioCategoria.get('descripcion')?.value,
    }

    this.createCategoriasService(categoriaDTO);
  }

  public async updateCategoria(){
    if(this.formularioCategoria.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioCategoria.markAllAsTouched();
      return;
    }

    let afirmativo : Boolean = await this.getConfirmacion('ACTUALIZAR CATEGORÍA',`¿Está seguro de actualizar la categoría ${this.formularioCategoria.get('nombre')?.value}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    let categoriaVO : CategoriaVO = {
      idCategoria : this.categoriaSeleccionada.idCategoria,
      codigo      : this.formularioCategoria.get('codigo')?.value,
      nombre      : this.formularioCategoria.get('nombre')?.value,
      descripcion : this.formularioCategoria.get('descripcion')?.value,
    }

    this.updateCategoriasService(categoriaVO);
  }

  public cancelCategoria(){
    this.limpiarFormularioCategoria();
  }

  public updateCategoriaEvent(categoriaVO:CategoriaVO){
    this.categoriaSeleccionada = categoriaVO;

    this.formularioCategoria.get('codigo')?.setValue(categoriaVO.codigo);
    this.formularioCategoria.get('nombre')?.setValue(categoriaVO.nombre);
    this.formularioCategoria.get('descripcion')?.setValue(categoriaVO.descripcion);
  }

  public async deleteCategoriaEvent(categoriaVO:CategoriaVO){
    let afirmativo:Boolean = await this.getConfirmacion('ELIMINAR CATEGORÍA',`¿Está seguro de eliminar la categoría ${categoriaVO.nombre}?`,'Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ 
      return; 
    }

    this.deleteCategoriaService(categoriaVO);
  }

  private limpiarFormularioCategoria(){
    let aux:any = undefined;
    this.categoriaSeleccionada = aux;
    this.formularioCategoria.reset();
  }

  public formularioCategoriaValido(campo:string) : boolean{
    return this.formularioCategoria.controls[campo].valid
  }

  public formularioCategoriaInvalido(campo:string) : boolean{   
    return this.formularioCategoria.controls[campo].invalid && 
           this.formularioCategoria.controls[campo].touched;
  }

  public isPresidente():boolean{
    return this.generalService.isPresidente();
  }

  public isAdmin():boolean{
    return this.generalService.isAdmin();
  }

  // ---------------------- 
  // | Consumo API-REST   |
  // ----------------------
  private getListaCategoriasService(){
    this.mesAnioCategoriaCostoAguaService.getCategorias().subscribe(
      (resp)=>{
        this.listaItemsCategoria = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private createCategoriasService(categoriaDTO:CategoriaDTO){
    this.mesAnioCategoriaCostoAguaService.createCategoria(categoriaDTO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se creo correctamente la categoría ${categoriaDTO.nombre}`);
        this.getListaCategoriasService();
        this.limpiarFormularioCategoria();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private updateCategoriasService(categoriaVO:CategoriaVO){
    this.mesAnioCategoriaCostoAguaService.updateCategoria(categoriaVO).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`La se actualizo correctamente la categoría ${resp.nombre}`);
        this.getListaCategoriasService();
        this.limpiarFormularioCategoria();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public deleteCategoriaService(categoriaVO:CategoriaVO) {
    this.mesAnioCategoriaCostoAguaService.deleteCategoria(categoriaVO.idCategoria).subscribe(
      (resp)=>{
        this.generalService.mensajeCorrecto(`Se elimino de manera correcta la categoría ${categoriaVO.nombre}`);
        this.getListaCategoriasService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

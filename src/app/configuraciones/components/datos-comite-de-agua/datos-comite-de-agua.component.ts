import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from 'src/app/shared/services/general.service';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DatosComiteDeAguaDTO } from '../../models/datosComiteDeAguaDTO';
import { DatosComiteDeAguaService } from '../../services/datos-comite-de-agua.service';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from 'src/app/shared/enums-mensajes/info-message';

@Component({
  selector: 'app-datos-comite-de-agua',
  templateUrl: './datos-comite-de-agua.component.html',
  styleUrls: ['./datos-comite-de-agua.component.css']
})
export class DatosComiteDeAguaComponent implements OnInit {
  aux:any = undefined;

  formularioDatosComiteDeAgua! : FormGroup;
  listaDepartamentos           : string[];
  datosComiteAguaEditar        : DatosComiteDeAguaDTO = this.aux;
  
  // tabla datos comite de agua
  listaTituloDatosComite : string[] = [];
  listaCampoDatosComite  : string[] = [];
  idDatosComite!         : string;
  listaItemsDatosComite  : DatosComiteDeAguaDTO[]=[];

  constructor(private fb                       : FormBuilder,
              private generalService           : GeneralService,
              private dialog                   : MatDialog,
              private datosComiteDeAguaService : DatosComiteDeAguaService,
              private router                   : Router) { }

  ngOnInit(): void {
    this.construirFormularioDatosComite();
    this.construirTablaDatosComite();

    this.listaDepartamentos = ['Cochabamba','La Paz','Santa Cruz','Beni','Pando','Tarija','Chuquisaca','Oruro','Potosí'];
    this.listaCampoDatosComite = ['nombreComite','personeriaJuridica','fechaFundacion','telefono1','telefono2','nombreDepartamento'];
    this.getDatosComiteDeAguaService();
  }

  private construirFormularioDatosComite(){
    this.formularioDatosComiteDeAgua = this.fb.group({
      nombreComite       : ['',[Validators.required],[]],
      personeriaJuridica : ['',[Validators.required],[]],
      fechaFundacion     : [this.generalService.getFechaActualFormateado(),[Validators.required],[]],
      telefono1          : ['',[Validators.required,Validators.min(0)],[]],
      telefono2          : ['',[Validators.required,Validators.min(0)],[]],
      nombreDepartamento : ['0',[Validators.required],[]],
    });
  }

  public async createRegistroDatosComite(){
    if(this.formularioDatosComiteDeAgua.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioDatosComiteDeAgua.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('REGISTRO DE DATOS DEL COMITE','¿Está seguro de registrar los datos del comite?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let datosComiteDeAguaDTO : DatosComiteDeAguaDTO = {
      nombreComite       : this.formularioDatosComiteDeAgua.get('nombreComite').value,
      personeriaJuridica : this.formularioDatosComiteDeAgua.get('personeriaJuridica').value,
      fechaFundacion     : this.formularioDatosComiteDeAgua.get('fechaFundacion').value,
      telefono1          : this.formularioDatosComiteDeAgua.get('telefono1').value,
      telefono2          : this.formularioDatosComiteDeAgua.get('telefono2').value,
      nombreDepartamento : this.formularioDatosComiteDeAgua.get('nombreDepartamento').value,
    }

    this.createDatosComiteDeAguaService(datosComiteDeAguaDTO);
  }

  public async updateRegistroDatosComite(){
    if(this.formularioDatosComiteDeAgua.invalid){
      this.generalService.mensajeFormularioInvalido(INFO_MESSAGE.form_invalid,INFO_MESSAGE.title_form_invalid);
      this.formularioDatosComiteDeAgua.markAllAsTouched();
      return;
    }

    let afirmativo:Boolean = await this.getConfirmacion('ACTUALIZAR DATOS DEL COMITE','¿Está seguro de actualizar los datos del comite?','Sí, estoy seguro','No, cancelar');
    if(!afirmativo){ return; }

    let datosComiteDeAguaDTO : DatosComiteDeAguaDTO = {
      nombreComite       : this.formularioDatosComiteDeAgua.get('nombreComite').value,
      personeriaJuridica : this.formularioDatosComiteDeAgua.get('personeriaJuridica').value,
      fechaFundacion     : this.formularioDatosComiteDeAgua.get('fechaFundacion').value,
      telefono1          : this.formularioDatosComiteDeAgua.get('telefono1').value,
      telefono2          : this.formularioDatosComiteDeAgua.get('telefono2').value,
      nombreDepartamento : this.formularioDatosComiteDeAgua.get('nombreDepartamento').value,
    }

    this.updateDatosComiteDeAguaService(datosComiteDeAguaDTO);
  }

  public cancelarRegistroDatosComite(){
    this.limpiarFormulario();
  }

  private limpiarFormulario(){
    this.formularioDatosComiteDeAgua.reset();
    this.formularioDatosComiteDeAgua.get('fechaFundacion').setValue(this.generalService.getFechaActualFormateado());
    this.formularioDatosComiteDeAgua.get('nombreDepartamento').setValue('0');
    this.datosComiteAguaEditar = this.aux;
  }

  public formularioDatosComiteValido(campo:string) : boolean{
    return  this.formularioDatosComiteDeAgua.controls[campo].valid;
  }

  public formularioDatosComiteInvalido(campo:string) : boolean{   
    return  this.formularioDatosComiteDeAgua.controls[campo].invalid && 
            this.formularioDatosComiteDeAgua.controls[campo].touched;
  }


  public formularioDatosComiteDptoValido(campo:string) : boolean{
    return  this.formularioDatosComiteDeAgua.controls[campo].valid && 
            this.formularioDatosComiteDeAgua.controls[campo].value!=0;
  }

  public formularioDatosComiteDptoInvalido(campo:string) : boolean{   
    return  (this.formularioDatosComiteDeAgua.controls[campo].invalid && 
            this.formularioDatosComiteDeAgua.controls[campo].touched) || 
            (this.formularioDatosComiteDeAgua.controls[campo].value==0 &&
            this.formularioDatosComiteDeAgua.controls[campo].touched);
  }

  // tabla datos comite de agua
  private construirTablaDatosComite(){
    this.listaTituloDatosComite = ['#','Nombre Comite de Agua','Personeria Juridíca','Fecha Fundación','telefono #1','Telefono #2','Departamento']
    this.listaCampoDatosComite = [];
  }

  public updateRegistroDatosComiteEvent(datosComiteDeAguaDTO:DatosComiteDeAguaDTO){
    this.datosComiteAguaEditar = datosComiteDeAguaDTO;
    this.formularioDatosComiteDeAgua.get('nombreComite').setValue(datosComiteDeAguaDTO.nombreComite);
    this.formularioDatosComiteDeAgua.get('personeriaJuridica').setValue(datosComiteDeAguaDTO.personeriaJuridica);
    this.formularioDatosComiteDeAgua.get('fechaFundacion').setValue(datosComiteDeAguaDTO.fechaFundacion);
    this.formularioDatosComiteDeAgua.get('telefono1').setValue(datosComiteDeAguaDTO.telefono1);
    this.formularioDatosComiteDeAgua.get('telefono2').setValue(datosComiteDeAguaDTO.telefono2);
    this.formularioDatosComiteDeAgua.get('nombreDepartamento').setValue(datosComiteDeAguaDTO.nombreDepartamento);
  }

  // Consumo API-REST
  public getDatosComiteDeAguaService()  {
    this.datosComiteDeAguaService.getDatosComiteDeAgua().subscribe(
      (resp)=>{
        this.listaItemsDatosComite=[];
        if(resp)
          this.listaItemsDatosComite.push(resp);
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public createDatosComiteDeAguaService(datosComiteDeAguaDTO:DatosComiteDeAguaDTO) {
    this.datosComiteDeAguaService.createDatosComiteDeAgua(datosComiteDeAguaDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getDatosComiteDeAguaService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public updateDatosComiteDeAguaService(datosComiteDeAguaDTO:DatosComiteDeAguaDTO) {
    this.datosComiteDeAguaService.updateDatosComiteDeAgua(datosComiteDeAguaDTO).subscribe(
      (resp)=>{
        this.limpiarFormulario();
        this.getDatosComiteDeAguaService();
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  // ----------------------
  // |  Confirm Dialog    | 
  // ----------------------
  private getConfirmacion(titleDialog:string,messageDialog:string,confirmDialog:string,dismissDialog:string):Promise<Boolean>{
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title   : titleDialog,
        message : messageDialog,
        confirm : confirmDialog,
        dismiss : dismissDialog,
      }
    });

    return dialogRef.afterClosed().toPromise()
      .then(resp=>{
        return (resp)?true:false;
      })
      .catch(err=>{
        return false;
    });
  }
}

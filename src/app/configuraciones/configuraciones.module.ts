import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfiguracionesRoutingModule } from './configuraciones-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MesComponent } from './components/mes/mes.component';
import { GestionComponent } from './components/gestion/gestion.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { CostoAguaComponent } from './components/costo-agua/costo-agua.component';
import { DatosComiteDeAguaComponent } from './components/datos-comite-de-agua/datos-comite-de-agua.component';


@NgModule({
  declarations: [
    MesComponent,
    GestionComponent,
    CategoriaComponent,
    CostoAguaComponent,
    DatosComiteDeAguaComponent
  ],
  imports: [
    CommonModule,
    ConfiguracionesRoutingModule,
    SharedModule,
    AngularMaterialModule, 
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ConfiguracionesModule { }

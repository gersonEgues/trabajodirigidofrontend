export interface DatosGeneralesComiteVO{
  nombreComite       : string,
  personeriaJuridica : string,
  fechaFundacion     : string,
  telefono1          : number,
  telefono2          : number,
  nombreDeparatmento : string,
}
export interface UsuarioVO {
  idUsuario : number,
  nombres   : string,
  apellidoPaterno : string,
  apellidoMaterno : string
}
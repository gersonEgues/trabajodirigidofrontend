export interface SocioVO {
  idSocio         : number,
  codigo          : string,
  nombre          : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  ci              : string,
  extencion       : string,
  direccion       : string,
  nroCasa         : string,
  estado          : boolean,
  fechaCreacion   : string,
  telefono        : number,
}
export interface RangoFechaDTO {
  fechaInicio  : string,
  fechaFin     : string,
  mensaje?     : string,
  rangoValido? : boolean,
  cancelado?   : boolean,
}
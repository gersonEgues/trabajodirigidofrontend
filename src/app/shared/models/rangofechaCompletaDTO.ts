export interface RangoFechaCompletaDTO{
  fechaInicio : string,
  fechaFin    : string,
}
export interface PageDTO{
  page  : number,
  size  : number,
  sort? : string,
}
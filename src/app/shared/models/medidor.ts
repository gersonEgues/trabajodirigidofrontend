export interface Medidor{
  id                : number,
  codigo            : number,
  registrado?       : boolean,
  registroConsumo?  : RegistroConsumo,
}

interface RegistroConsumo{
  volumen         : number,
  costoM3         : number,
  costoTotal      : number,
  lecturaAnterior : number,
  lecturaActual   : number
}
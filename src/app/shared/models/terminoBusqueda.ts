export interface TerminoBusqueda {
  todo          : boolean,
  gestion       : boolean,
  mes           : boolean,
  incio         : string,
  fin           : string,
}
import { Medidor } from "./medidor";

export interface SocioBusqueda{
  id              : number,
  codigo          : number,
  apellidos       : string,
  nombres         : string,
  numMedidores    : number,
  medidores?      : Medidor[]
}
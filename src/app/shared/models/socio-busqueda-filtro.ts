
import { Medidor } from "./medidor";

/**
 * numLista : indice dentro del mismo objeto 
 * numItem  : mismo indice independiente mente cuantas veces se repita el mismo objeto  
 */
export interface SocioBusquedaFiltro{
  id              : number,
  codigo          : number
  nombres         : string,
  apellidos       : string,
  ciSocio?        : string,
  direccionSocio? : string,
  nroCasaSocio?   : string,
  telefonos?      : string,
  numLista        : number,
  numItem?        : number,
  seleccionado?   : boolean,
  numMedidores    : number,
  medidor?        : Medidor,
}

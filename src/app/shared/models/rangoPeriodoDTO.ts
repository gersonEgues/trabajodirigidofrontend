export interface RangoPeriodoDTO {
  idPeriodoInicio     : number,
  idPeriodoFin        : number,
  gestionInicio       : number,
  mesInicio           : number,
  gestionFin          : number,
  mesFin              : number,
  mensaje             : string,
  rangoValido         : boolean,
  bandera?            : boolean,
  todosSeleccionados? : boolean,
  cancelado?          : boolean,
}
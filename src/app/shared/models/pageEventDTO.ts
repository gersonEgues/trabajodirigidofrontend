export interface PageEventDTO{
  length            : number,
  pageIndex         : number,
  pageSize          : number,
  previousPageIndex : number,
}
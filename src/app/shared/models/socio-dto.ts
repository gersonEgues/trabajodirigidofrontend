export interface SocioDTO {
  codigo          : string,
  nombre          : string,
  apellidoPaterno : string,
  apellidoMaterno : string,
  ci              : string,
  extencion       : string,
  direccion       : string,
  nroCasa         : string,
  fechaCreacion   : string,
  telefono        : number,
}
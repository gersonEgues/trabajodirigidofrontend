import { SocioVO } from "./socio-vo";

export interface SocioMedidorVO extends SocioVO{
  idSocioMedidor      : number;
  idMedidor           : number;
  codigoMedidor       : string;

  // seguimiento evento
  idSeguimientoEvento? : number;
}

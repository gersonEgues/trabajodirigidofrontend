import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedRoutingModule } from './shared-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { InputFormComponent } from './components/input-form/input-form.component';
import { TablaComponent } from './components/tabla/tabla.component';
import { TablePaginatorComponent } from './components/table-paginator/table-paginator.component';
import { ImprimirPdfComponent } from './components/imprimir-pdf/imprimir-pdf.component';
import { BuscarSocioComponent } from './components/buscar-socio/buscar-socio.component';
import { FiltroComponent } from './components/filtro/filtro.component';
import { TablePaginatorMultipleRowsComponent } from './components/table-paginator-multiple-rows/table-paginator-multiple-rows.component';
import { TableMultipleRowsComponent } from './components/table-multiple-rows/table-multiple-rows.component';
import { TablePaginatorMultipleRowsV2Component } from './components/table-paginator-multiple-rows-v2/table-paginator-multiple-rows-v2.component';
import { FechaCompletaComponent } from './components/filtro-fecha/fecha-completa/fecha-completa.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { RangoFechaComponent } from './components/rango-fecha/rango-fecha.component';
import { RangoPeriodoComponent } from './components/rango-periodo/rango-periodo.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    InputFormComponent,
    TablaComponent,
    TablePaginatorComponent,
    ImprimirPdfComponent,
    BuscarSocioComponent,
    FiltroComponent,
    TablePaginatorMultipleRowsComponent,
    TableMultipleRowsComponent,
    TablePaginatorMultipleRowsV2Component,
    FechaCompletaComponent,
    ConfirmDialogComponent,
    RangoFechaComponent,
    RangoPeriodoComponent,
    BuscadorComponent,
    SpinnerComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports:[
    FooterComponent,
    InputFormComponent,
    NavbarComponent,
    TablaComponent,
    TablePaginatorComponent,
    TableMultipleRowsComponent,
    ImprimirPdfComponent,
    BuscarSocioComponent,
    FiltroComponent,
    TablePaginatorMultipleRowsComponent,
    TablePaginatorMultipleRowsV2Component,
    FechaCompletaComponent,
    ConfirmDialogComponent,
    RangoFechaComponent,
    RangoPeriodoComponent,
    BuscadorComponent,
    SpinnerComponent
  ]
})
export class SharedModule { }

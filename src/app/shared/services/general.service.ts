import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import * as converterES from 'numero-a-letras';
import { DatosGeneralesComiteVO } from '../models/datosGeneralesComiteVO';
import * as XLSX from 'xlsx'; 
import { UserToken } from 'src/app/login/models/userToken';
import { User } from 'src/app/login/models/user';
import jwt_decode from "jwt-decode";
import { Rol } from 'src/app/login/models/rol';
import { ROL } from '../enums-mensajes/roles';
import { DatosComiteDeAguaService } from 'src/app/configuraciones/services/datos-comite-de-agua.service';
import { DatosComiteDeAguaDTO } from 'src/app/configuraciones/models/datosComiteDeAguaDTO';
import { ERR_MESSAGE } from '../enums-mensajes/err-message';
import { Router } from '@angular/router';
import { PATH } from '../enums-mensajes/path';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  datosComiteDeAgua:DatosComiteDeAguaDTO;

  constructor(private toastr                           : ToastrService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private datosComiteDeAguaService         : DatosComiteDeAguaService,
              private router                           : Router) { 
    this.getDatosComiteDeAguaService();
  }

  public mensajeError(mensaje:string,titulo:string="Error :"){
    this.toastr.error(mensaje,titulo, {
      timeOut: 3500,
    });
  }

  public mensajeCorrecto(mensaje:string,titulo:string="Exito :"){
    this.toastr.success(mensaje,titulo,{
      timeOut:3500,
    });
  }

  public mensajeAlerta(mensaje:string,titulo:string="Alerta :"){
    this.toastr.info(mensaje,titulo, {
      timeOut: 4000,
    });
  }

  public mensajeFormularioInvalido(mensaje:string,titulo:string="Alerta :"){
    this.toastr.warning(mensaje,titulo, {
      timeOut: 4000,
    });
  }

  public  getFechaActual() : string {
    let fecha = new Date(); //Fecha actual
    return fecha.toLocaleDateString();
  }

  public  getFechaActualFormateado() : string {
    let fecha = new Date(); //Fecha actual   
    let fechaArreglo : string[] = fecha.toLocaleDateString().split('/');
    
    let dia  : string = fechaArreglo[0];
    let mes  : string = fechaArreglo[1];
    let anio : string = fechaArreglo[2];

    if(Number(dia)<10){ dia = '0' + dia; }
    if(Number(mes)<10){ mes = '0' + mes; }
    
    return anio+'-'+mes+'-'+dia;
  } 
  
  public  getFechaActualFormateadoDiaMesAnio() : string {
    let fecha = new Date(); //Fecha actual   
    let fechaArreglo : string[] = fecha.toLocaleDateString().split('/');
    
    let dia  : string = fechaArreglo[0];
    let mes  : string = fechaArreglo[1];
    let anio : string = fechaArreglo[2];

    if(Number(dia)<10){ dia = '0' + dia; }
    if(Number(mes)<10){ mes = '0' + mes; }
    
    return `${dia}-${mes}-${anio}`;
  } 

  public  getHoraActualFormateado() : string {
    const date = new Date();
    
    let hrs:number|string = date.getHours();
    let min:number|string = date.getMinutes();
    let sec:number|string = date.getSeconds();

    hrs = (hrs<10)?'0'+hrs:hrs;
    min = (min<10)?'0'+min:min;
    sec = (sec<10)?'0'+sec:sec;

    let hora:string = `${hrs}:${min}:${sec}`;
    return hora;
  }  

  public getFechaHibrido(fecha:string){ 
    let fechaArreglo : string[] = fecha.split('-');
    
    let dia  : number = Number(fechaArreglo[2]);
    let mes  : number = Number(fechaArreglo[1]);
    let anio : number = Number(fechaArreglo[0]);

    let fechaResp:string = `${dia} de ${this.getMesLiteral(mes)} del ${anio}`;
    return fechaResp;
  }

  public getFechaActualHibrido(){
    let fecha = new Date(); //Fecha actual   
    let fechaArreglo : string[] = fecha.toLocaleDateString().split('/');
    
    let dia  : number|string = Number(fechaArreglo[0]);
    let mes  : number|string = Number(fechaArreglo[1]);
    let anio : number|string = Number(fechaArreglo[2]);

    return `${dia} de ${this.getMesLiteral(mes)} del ${anio}`;
  }

  private getMesLiteral(mes:Number){
    let mesLiteral:string = '';
    if(mes==1){
      mesLiteral = 'Enero';
    }else if(mes==2){
      mesLiteral = 'Febrero';
    }else if(mes==3){
      mesLiteral = 'Marzo';
    }else if(mes==4){
      mesLiteral = 'Abril';
    }else if(mes==5){
      mesLiteral = 'Mayo';
    }else if(mes==6){
      mesLiteral = 'Junio';
    }else if(mes==7){
      mesLiteral = 'Julio';
    }else if(mes==8){
      mesLiteral = 'Agosto';
    }else if(mes==9){
      mesLiteral = 'Septiembre';
    }else if(mes==10){
      mesLiteral = 'Octubre';
    }else if(mes==11){
      mesLiteral = 'Noviembre';
    }else{
      mesLiteral = 'Diciembre';
    }
    return mesLiteral;
  }

  public getNumeroLetra(numero:number){
    let numeroLetra:string = converterES.NumerosALetras(numero);
    numeroLetra = numeroLetra.replace('Pesos','');
    numeroLetra = numeroLetra.replace('M.N.','');
    return numeroLetra;
  }

  public getDatosComiteDeAgua():DatosGeneralesComiteVO{
    let datosDepartamento : DatosGeneralesComiteVO = {
      nombreComite        : this.datosComiteDeAgua?.nombreComite,
      personeriaJuridica  : this.datosComiteDeAgua?.personeriaJuridica,
      fechaFundacion      : this.datosComiteDeAgua?.fechaFundacion,
      telefono1           : this.datosComiteDeAgua?.telefono1,
      telefono2           : this.datosComiteDeAgua?.telefono2,
      nombreDeparatmento  : this.datosComiteDeAgua?.nombreDepartamento,
    }
    return datosDepartamento;
  }

  public getBase64ImageFromURL(url:any) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");

      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        resolve(dataURL);
      };

      img.onerror = error => {
        reject(error);
      };

      img.src = url;
    });
  }

  public getDatosDeUsuarioConSessionActiva():User{
    let token : string = localStorage.getItem('token'); 
    let decoded  : UserToken = jwt_decode(token); 
    return decoded.user;
  }

  public getNombreCompletoUsuarioSession():string{
    let user:User = this.getDatosDeUsuarioConSessionActiva();
    let nombreCompleto:string = user.nombre + ' ' + user.apellidoPaterno + ' ' + user.apellidoMaterno;
    return nombreCompleto;
  }

  public getRolCompletoUsuarioSession():string{
    let user:User = this.getDatosDeUsuarioConSessionActiva();
    let rol:string = user.roles[0].nombre;
    return rol;
  }

  public getRolesDeUsuario():string[]{
    let nombreRoles:string[]=[];
    let roles:Rol[] = this.getDatosDeUsuarioConSessionActiva().roles;
    roles.forEach(rol => {
      nombreRoles.push(rol.nombre);
    });
    return nombreRoles;
  }

  public isAdmin():boolean{
    let isAdmin:boolean = false;
    let roles:string[] = this.getRolesDeUsuario();
    roles.forEach(rol => {
      if(rol == ROL.ADMIN)
        isAdmin = true;
    });
    return isAdmin;
  }

  public isPresidente():boolean{
    let isPresidente:boolean = false;
    let roles:string[] = this.getRolesDeUsuario();
    roles.forEach(rol => {
      if(rol == ROL.PRESIDENTE)
        isPresidente = true;
    });
    return isPresidente;
  }

  public isSecretaria():boolean{
    let isSecretaria:boolean = false;
    let roles:string[] = this.getRolesDeUsuario();
    roles.forEach(rol => {
      if(rol == ROL.SECRETARIA)
        isSecretaria = true;
    });
    return isSecretaria;
  }

  public isMiembroMesaDirectiva():boolean{
    let isMiembroMesaDirectiva:boolean = false;
    let roles:string[] = this.getRolesDeUsuario();
    roles.forEach(rol => {
      if(rol == ROL.MIEMBRO_MESA_DIRECTIVA)
      isMiembroMesaDirectiva = true;
    });
    return isMiembroMesaDirectiva;
  }

  // generar archivos xslx
  public genearteArchivoXLSX(tableHtml:HTMLElement,nombreArchivo:string):void{
    let fileName:string= `${nombreArchivo}.xlsx`; 
    //let element = document.getElementById("id"); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(tableHtml);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, fileName);
  }

  public getDatosComiteDeAguaService()  {
    this.datosComiteDeAguaService.getDatosComiteDeAgua().subscribe(
      (resp)=>{
        if(resp)
          this.datosComiteDeAgua = resp;
      },
      (err)=>{
        if(err.status==401){
          this.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.mensajeError(err.message);
        }
      }
    );
  }
}

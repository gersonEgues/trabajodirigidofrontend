export enum INFO_MESSAGE {
  title_not_found_results = "Sin resultados :",
  not_found_results = "No se econtro resultados para la busqueda",

  title_found_results = "Resultados :",
  found_results = "Existe resultados para la busqueda",

  title_form_invalid = "Formulario invalido :",
  form_invalid = "Llene todos los campos requeridos en el formulario",

  success_save = "Se registro los datos con exito!!!",
  success_update = "Se actualizo los datos con exito!!!",
  success_delete = "Se elimino los datos con exito!!!",

  tittle_range_invalid = "Error",
  range_invalid = "Rango seleccionado invalido"
}
export enum ROL {
  ADMIN                  = 'ADMIN',
  PRESIDENTE             = 'PRESIDENTE',
  SECRETARIA             = 'SECRETARIA',
  MIEMBRO_MESA_DIRECTIVA = 'MIEMBRO_MESA_DIRECTIVA'
}
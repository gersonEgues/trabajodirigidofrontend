import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-table-paginator-multiple-rows-v2',
  templateUrl: './table-paginator-multiple-rows-v2.component.html',
  styleUrls: ['./table-paginator-multiple-rows-v2.component.css']
})
export class TablePaginatorMultipleRowsV2Component implements OnInit {
  @Input('tituloItemsTabla') tituloItemsTabla           : string[] = [];
  @Input('campoItemsFiltrado') campoItemsFiltrado       : string[] = [];
  @Input('campoSubItemsFiltrado') campoSubItemsFiltrado : string[] = [];
  @Input('idItemFiltrado')  idItemFiltrado              : string = '';   
  @Input('idItem')  idItem                              : string = '';   
  @Input("countItems") countItems                       : number = 0;
  @Input('idTable') idTable                             : string = '';

  // A nivel de json, todo lo te tiene le json original
  @Input('camposItem') camposItem                   : string[]=[];
  @Input('idCampoSubItem') idCampoSubItem           : string = '';
  @Input('camposSubItem') camposSubItem             : string[] = [];

  @Input('listaItems') listaItems                   : any[] = [];
  @Input('update') update                           : boolean = false;
  @Input('delete') delete                           : boolean = false;

  @Input('pagePaginator') pagePaginator             : boolean = true;

  @Output('itemSeleccionado') itemSeleccionado : EventEmitter<any> = new EventEmitter();
  @Output('cambioDePagina') cambioDePagina     : EventEmitter<any> = new EventEmitter();
  @Output('updateItem') updateItem             : EventEmitter<any> = new EventEmitter();
  @Output('deleteItem') deleteItem             : EventEmitter<any> = new EventEmitter();

  idItemSeleccionado : number = 0;
  listaItemsFiltrado : any[] = [];
  matValidatorRows      : any[]=[];

  lengthList      : number = 0;
  pageSize        : number = 0;
  pageSizeOptions : number[] = [10,20,50,100]

  constructor(private generalService : GeneralService) { }

  ngOnInit(): void {    
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.listaItems){
      if(this.listaItems!= undefined && this.listaItems.length == 0){
        this.idItemSeleccionado = 0;
      }
      this.tratarItemsParaListar();  
    }
    if(changes.countItems){
      this.lengthList = this.countItems;
    }
  }

  private tratarItemsParaListar(){   
    this.listaItemsFiltrado = []; 
    let indexGlobal:number=1;

    this.listaItems.forEach(item => {
      let size:number = item[this.idCampoSubItem].length;

      if(size==0){ 
        let rowsValidator : number[]=[];      
        let itemCopy:any = this.getJsonChildFromJsonParent(item,this.camposItem,rowsValidator);
        itemCopy.indexGlobal = indexGlobal;
        itemCopy.indexLocal  = 1;
        itemCopy.rows        = 1;
        this.listaItemsFiltrado.push(itemCopy);

        rowsValidator.push(0);
        rowsValidator.push(0);
        rowsValidator.push(0);
        this.matValidatorRows.push(rowsValidator);
      }else{
        let indexLocal:number = 1;
        item[this.idCampoSubItem].forEach( (subItem:any) => {
          let rowsValidator : number[]=[];  
          let itemCopy:any = this.getJsonChildFromJsonParent(item,this.camposItem,rowsValidator);
          this.camposSubItem.forEach(campoSI => {
            itemCopy[campoSI+'_si'] = subItem[campoSI];
            rowsValidator.push(size);
          });
          
          itemCopy.indexGlobal = indexGlobal;
          itemCopy.indexLocal  = indexLocal;
          itemCopy.rows        = size;
          this.listaItemsFiltrado.push(itemCopy);

          rowsValidator.push(0);
          rowsValidator.push(0);
          rowsValidator.push(0);
          this.matValidatorRows.push(rowsValidator);

          indexLocal+=1;
        });    
      }
      indexGlobal+=1;
    });
  }

  private getJsonChildFromJsonParent(itemJson:any,campos:string[], rowsValidator : number[]){
    let itemCopy:any = {};
    campos.forEach(campoJ => {
      itemCopy[campoJ+'_i'] = itemJson[campoJ];
      rowsValidator.push(1);
    });
    return itemCopy;
  }
  
  public buscarItem(palabraClave:string, campoItem:string ){
    this.deseleccionarItem();
    this.listaItemsFiltrado.forEach(item => {
      if((item[campoItem].toString().toLowerCase()).includes(palabraClave.toLowerCase()) && palabraClave!=""){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public deseleccionarItemsBusqueda(){
    this.listaItemsFiltrado.forEach(item => {
      item.seleccionado = false;
    });
  }

  public limpiarItemsDeTabla(){
    this.listaItemsFiltrado = [];
    this.listaItems = [];
  }

  public getResultData(data : any){
    let result:any = "-";
    if(data!=null || data!=undefined){
      if(typeof data == "boolean"){
        result = (data)?'Si':'No';
      }else{
        result = data;
      }
    }
    return result;
  }

  public tipoBoolean(data : any){
    let res:boolean = false;
    if(typeof data == "boolean"){
        res = true;
    }
    return res;
  }

  // --- event @output
  //--------- paginator --------
  public cambioSizePaginaOrPagina(event:any){
    this.idItemSeleccionado = -1;
    this.cambioDePagina.emit(event)
  }

  public seleccionarItem(item:any){
    this.deseleccionarItemsBusqueda();
    this.idItemSeleccionado = item[this.idItemFiltrado];
    let itemResp:any = this.getItem(item);
    this.itemSeleccionado.emit(itemResp)
  }

  public deseleccionarItem(){
    this.idItemSeleccionado = 0;
  }

  public updateItemEvent(item:any){
    let respItem:any = this.getItem(item);
    this.updateItem.emit(respItem);
  }

  public deleteItemEvent(item:any){
    let respItem:any = this.getItem(item);
    this.deleteItem.emit(respItem);
  }

  private getItem(item : any){
    let respItem:any;
    for (let i = 0; i < this.listaItems.length; i++) {
      if(this.listaItems[i][this.idItem] == item[this.idItemFiltrado] ){
        respItem = this.listaItems[i];
      }     
    }
    return respItem;
  }  

  public genearteArchivoXLSX(nombre:string){
    let tableElemnt:HTMLElement = document.getElementById(this.idTable);
    this.generalService.genearteArchivoXLSX(tableElemnt,nombre);
  }
}

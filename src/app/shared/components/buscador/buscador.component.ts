import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { EventEmitter,Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';

/**
 * hay dos tipos de eventos
 *  1.- buscar
 *  2.- seleccionar
 * Cada vez que se inserta un caracter en el buscador, se emite eventos
 *  1.- buscar
 *  2.- seleciconar.- envia un item valido, o un item null, ya que anterioremente se pudo haber seleccionado un item valido, 
 *                    y si ahora se esta borrando se lo quiere volver null, ya que ya no es un item valido
 */

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  @Input('titulo') titulo                      : string;
  @Input('type') type                          : string = 'text';
  @Input('placeholder') placeholder            : string = '';
  @Input('idCampo') idCampo                    : string = '';
  @Input('campo') campo                        : string = '';
  @Input('listaResultadoBusqueda') listaItems! :Observable<any[]>;
  
  @Output('buscarTextoEvent') buscarTextoEvent : EventEmitter<any> = new EventEmitter();
  @Output('itemSeleccionadoEvent') itemSeleccionadoEvent : EventEmitter<any> = new EventEmitter();

  aux:any = null;
  formularioBuscadorEvento! : FormGroup;
  itemSeleccionado:any = this.aux;
  buttonDisabled:boolean = false;

  constructor(private fb : FormBuilder,) { }

  ngOnInit(): void {
    this.construirFormulario();
  }

  private construirFormulario(){
    this.formularioBuscadorEvento = this.fb.group({
      texto  : ['',[Validators.required],[]],
    });
  }

  public campoBusquedaValido(campo:string) : boolean{
    return this.formularioBuscadorEvento.controls[campo].valid && this.itemSeleccionado!=this.aux;
  }

  public campoBusquedaInvalido(campo:string) : boolean{   
    return  (this.formularioBuscadorEvento.controls[campo].invalid && this.formularioBuscadorEvento.controls[campo].touched) || 
            (this.formularioBuscadorEvento.controls[campo].touched && this.itemSeleccionado==this.aux);
  }

  public buscartexto(){
    let textoBusqueda:string = this.formularioBuscadorEvento.get('texto').value;

    if(!textoBusqueda){
      this.itemSeleccionado = this.aux;
      this.listaItems = this.aux;
      this.itemSeleccionadoEvent.emit(this.itemSeleccionado);
      return;
    }

    textoBusqueda = textoBusqueda?.toLowerCase();

    if(textoBusqueda?.trim()!='' && textoBusqueda==this.itemSeleccionado?.[this.campo].toLowerCase()){
      this.listaItems = this.aux;
    }else if(textoBusqueda.trim()!=''){
      this.itemSeleccionado = this.aux;
      this.buscarTextoEvent.emit(textoBusqueda);
      this.itemSeleccionadoEvent.emit(this.itemSeleccionado);
    }else if(textoBusqueda.trim()==''){
      this.listaItems = this.aux;
    }
  }

  public limpiarFormulario(){
    this.formularioBuscadorEvento.reset();
    this.itemSeleccionado = this.aux;
    this.listaItems = this.aux;
    this.enable();
  }

  public mouseOver(item:any){
    item.hover = true;
  }

  public mouseOut(item:any){
    item.hover = false;
  }

  public seleccionarEvento(item:any){
    this.itemSeleccionado = item;
    this.formularioBuscadorEvento.get('texto').setValue(this.itemSeleccionado[this.campo]); 
    this.itemSeleccionadoEvent.emit(this.itemSeleccionado);
    this.listaItems = this.aux;
  }

  public limpiarListaResultadoBusqueda(){
    this.listaItems = this.aux;
  }

  public setValue(text:string){
    this.formularioBuscadorEvento.get('texto').setValue(text);
  }

  public getValue(){
    return this.formularioBuscadorEvento.get('texto').value;
  }

  public disable(){
    this.formularioBuscadorEvento.get('texto').disable();
    this.buttonDisabled = true;
  }

  public enable(){
    this.formularioBuscadorEvento.get('texto').enable();
    this.buttonDisabled = false;
  }
}

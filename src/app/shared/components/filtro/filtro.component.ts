import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
//import { EstadoMedidor } from 'src/app/socio-medidor/models/estado-medidor';
import { ToastrService } from 'ngx-toastr';
import { TerminoBusqueda } from '../../models/terminoBusqueda';
import { SocioService } from 'src/app/socios/services/socio.service';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent implements OnInit {
  @Input('item') item!:any;
  @Input('campoIdItem') campoIdItem!:any;
  @Input('mensajeErrorToast') mensajeErrorToast!:string;

  @Output('buscar') buscar:EventEmitter<any> = new EventEmitter();
  
  opcionSeleccionada    : number=1;
  gestionInicioValido   : boolean=false;
  gestionInicioInvalido : boolean=false;
  gestionFinValido      : boolean=false;
  gestionFinInvalido    : boolean=false;

  mesInicioValido   : boolean=false;
  mesInicioInvalido : boolean=false;
  mesFinValido      : boolean=false;
  mesFinInvalido    : boolean=false;
  terminoBusqueda!  : TerminoBusqueda;
  formularioFiltro! : FormGroup;

  constructor(private fb             : FormBuilder,
              private toastr         : ToastrService,
              private generalService : GeneralService) { }

  ngOnInit(): void {
    this.construirFormulario();
  }

  private construirFormulario(){
    this.formularioFiltro = this.fb.group({
      gestionInicio    : [ this.getAnio(),[Validators.required],[]],
      gestionFin       : [ this.getAnio(),[Validators.required],[]],
      mesInicio        : [ this.getAnioMesInicio(), [Validators.required],[]],
      mesFin           : [ this.getAnioMesFin(), [Validators.required],[]],
    });
  }

  private getValorCampo(nombreCampo:string){
    return this.formularioFiltro.get(nombreCampo)?.value;
  }

  private getCampoFormulario(nombreCampo:string):FormControl{
    return this.formularioFiltro.get(nombreCampo) as FormControl;
  }

  public getAnio():number{
    return (new Date).getFullYear();
  }

  public getMes():number{
    return ((new Date).getMonth());
  }

  public getAnioMesInicio():string{
    return this.getAnio()+"-01";
  }

  public getAnioMesFin():string{
    return this.getAnio()+"-"+this.getMes();
  }

  public campoValido(campo:string):boolean{
    return this.formularioFiltro.controls[campo].valid;
  }

  public campoInvalido(campo:string):boolean{
    return  this.formularioFiltro.controls[campo].invalid &&
            this.formularioFiltro.controls[campo].touched;
  }

  cambioOpcionSeleccionadaTodos(event:any):void{
    if(event.checked){
      this.opcionSeleccionada=1;
      this.setValuesGestionIncioFin(false,false,false,false);
      this.setValuesMesIncioFin(false,false,false,false);
    }else{
      this.opcionSeleccionada=0;
    }
  }

  cambioOpcionSeleccionadaGestion(event:any):void{
    this.getCampoFormulario('gestionInicio').setValue(this.getAnio());
    this.getCampoFormulario('gestionFin').setValue(this.getAnio());
    if(event.checked){
      this.opcionSeleccionada=2;  
      this.setValuesGestionIncioFin(true,false,true,false);
      this.setValuesMesIncioFin(false,false,false,false);
    }else{
      this.opcionSeleccionada=0;
      this.setValuesGestionIncioFin(false,false,false,false);
    }
  }

  cambioOpcionSeleccionadaMes(event:any):void{
    this.getCampoFormulario('mesInicio').setValue(this.getAnioMesInicio());
    this.getCampoFormulario('mesFin').setValue(this.getAnioMesFin());
    if(event.checked){
      this.opcionSeleccionada=3;
      this.setValuesMesIncioFin(true,false,true,false);
      this.setValuesGestionIncioFin(false,false,false,false);
    }else{
      this.opcionSeleccionada=0;
      this.setValuesMesIncioFin(false,false,false,false);
    }
  }

  cambioGestion():void{
    if(this.getValorCampo('gestionInicio')>this.getValorCampo('gestionFin'))
      this.setValuesGestionIncioFin(false,true,false,true);
    else
      this.setValuesGestionIncioFin(true,false,true,false);
  }

  cambioMes():void{    
    if(this.getValorCampo('mesInicio')>this.getValorCampo('mesFin'))
      this.setValuesMesIncioFin(false,true,false,true);
    else
      this.setValuesMesIncioFin(true,false,true,false);
  }

  private setValuesGestionIncioFin(giv:boolean,gii:boolean,gfv:boolean,gfi:boolean){
    this.gestionInicioValido = giv;
    this.gestionInicioInvalido = gii;
    this.gestionFinValido = gfv;
    this.gestionFinInvalido = gfi;
  }

  private setValuesMesIncioFin(miv:boolean,mii:boolean,mfv:boolean,mfi:boolean){
    this.mesInicioValido = miv;
    this.mesInicioInvalido = mii;
    this.mesFinValido = mfv;
    this.mesFinInvalido = mfi;
  }

  filtrarTodo():void{
    this.terminoBusqueda = {
      todo    : true,
      gestion : false,
      mes     : false,
      incio   : "",
      fin     : "", 
    }
    this.buscar.emit(this.terminoBusqueda);
  }

  filtrarPorGestion() : void{
    if(this.getValorCampo('gestionInicio')>this.getValorCampo('gestionFin')){
      this.generalService.mensajeAlerta("La gestion inicio debe ser menor o igual a la gestion fin","Error")
      return;
    }

    this.terminoBusqueda = {
      todo    : false,
      gestion : true,
      mes     : false,
      incio   : this.getValorCampo('gestionInicio'),
      fin     : this.getValorCampo('gestionFin'), 
    }
    this.buscar.emit(this.terminoBusqueda);
  }

  filtrarPorMes() : void{
    if(this.getValorCampo('mesInicio')>this.getValorCampo('mesFin')){
      this.generalService.mensajeAlerta("El mes inicio debe ser menor o igual al mes fin","Error")
      return;
    }

    this.terminoBusqueda = {
      todo    : false,
      gestion : false,
      mes     : true,
      incio   : this.getValorCampo('mesInicio'),
      fin     : this.getValorCampo('mesFin'), 
    }
    this.buscar.emit(this.terminoBusqueda);
  }

  filtrar():void{
    if(this.opcionSeleccionada==0 || this.item==undefined){
      if(this.opcionSeleccionada==0)
        this.generalService.mensajeAlerta("Debe seleccionar un tipo de filtrado");

      if(this.item==undefined)
        this.generalService.mensajeAlerta(this.mensajeErrorToast);

      return;
    }

    if(this.opcionSeleccionada == 1)  this.filtrarTodo();
    else if(this.opcionSeleccionada == 2) this.filtrarPorGestion();
    else if(this.opcionSeleccionada ==3 )  this.filtrarPorMes();
  }

  cancelar():void{
    this.opcionSeleccionada=1;
    this.setValuesGestionIncioFin(false,false,false,false);
    this.setValuesMesIncioFin(false,false,false,false);
  }
}

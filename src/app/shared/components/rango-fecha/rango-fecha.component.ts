import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RangoFechaDTO } from '../../models/rangoFechaDTO';


@Component({
  selector: 'app-rango-fecha',
  templateUrl: './rango-fecha.component.html',
  styleUrls: ['./rango-fecha.component.css']
})
export class RangoFechaComponent implements OnInit {
  formularioFecha! : FormGroup;

  @Output('cambioFechaEvent') cambioFechaEvent : EventEmitter<RangoFechaDTO> = new EventEmitter();

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,) { }

  ngOnInit(): void {
    this.construirformularioFecha();
  }

  private construirformularioFecha(){
    this.formularioFecha = this.fb.group({     
      fechaInicio   : [{value:this.generalService.getFechaActualFormateado(),disabled:false}, [Validators.required],[]],
      fechaFin      : [{value:this.generalService.getFechaActualFormateado(),disabled:false}, [Validators.required],[]],
    });
  }

  public getRangoFecha():RangoFechaDTO{
    return this.getRangoFechaDTO();
  }

  public changeFecha(){
    let rangoFechaDTO : RangoFechaDTO = this.getRangoFechaDTO();
    
    if(this.ambasFechasSeleccionadas()){
      if(!this.rangoFechaValida()){
        this.generalService.mensajeAlerta("La fecha incio tiene que ser menor o igual a la fecha fin");
      }
    }

    this.cambioFechaEvent.emit(rangoFechaDTO);
  }

  public limpiarFormulario(){
    this.formularioFecha.reset();
  }

  private getRangoFechaDTO():RangoFechaDTO{
    let rangoFechaDTO! : RangoFechaDTO;
    if(this.formularioFecha.get('fechaInicio')?.invalid || this.formularioFecha.get('fechaFin')?.invalid){
      return rangoFechaDTO = {
        fechaInicio : '',
        fechaFin    : '',
        mensaje     : (this.formularioFecha.get('fechaInicio')?.invalid)?'Seleccione la fecha de incio':'Seleccione la fecha de fin',
        rangoValido : false
      };
    }
    
    let fechaIncio : string = this.formularioFecha.get('fechaInicio')?.value;
    let fechaFin : string   = this.formularioFecha.get('fechaFin')?.value;
    let mensaje : string    = '';
    let rangoValido : boolean = true;

    if(fechaIncio>fechaFin){
      mensaje = 'La fecha inicio tiene que ser menor o igual que la fecha fin';
      rangoValido = false;
    }else{
      mensaje = 'Rango de fecha valido';
    }

    rangoFechaDTO = {
      fechaInicio : fechaIncio,
      fechaFin    : fechaFin,
      mensaje     : mensaje,
      rangoValido : rangoValido,
    };

    return rangoFechaDTO;
  }

  private rangoFechaValida():boolean{
    let bandera:boolean = true;
    if(this.formularioFecha.get('fechaInicio')?.invalid || this.formularioFecha.get('fechaFin')?.invalid){
      bandera = false;
      return bandera;
    }

    let fechaIncio : string = this.formularioFecha.get('fechaInicio')?.value;
    let fechaFin : string   = this.formularioFecha.get('fechaFin')?.value;

    if(fechaIncio>fechaFin){
      bandera = false;
      return bandera;
    }

    return bandera;
  }

  public ambasFechasSeleccionadas():boolean{
    if(this.formularioFecha.get('fechaInicio')?.valid && this.formularioFecha.get('fechaFin')?.valid){return true;}
    else{ return false;}
  }

  public campoValido(nombreCampo:string):boolean{
    return  this.formularioFecha.controls[nombreCampo].valid && ( (this.ambasFechasSeleccionadas())?  this.rangoFechaValida() : true);
              
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  (this.formularioFecha.controls[nombreCampo].touched &&
            this.formularioFecha.controls[nombreCampo].invalid)  || ( (this.ambasFechasSeleccionadas())?  !this.rangoFechaValida() : false);
  }
} 

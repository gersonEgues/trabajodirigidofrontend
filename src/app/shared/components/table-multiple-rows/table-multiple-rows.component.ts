import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-table-multiple-rows',
  templateUrl: './table-multiple-rows.component.html',
  styleUrls: ['./table-multiple-rows.component.css']
})
export class TableMultipleRowsComponent implements OnInit {
  @Input('tituloItems') tituloItems                 : string[] = [];
  @Input('campoItems') campoItems                   : string[] = [];
  @Input('idItem')  idItem                          : string = '';   
  @Input("countItems") countItems                   : number = 0;
  @Input('idFilaDividida') idFilaDividida           : string = '';
  @Input('idCampoFilaDividida') idCampoFilaDividida : string = '';
  @Input('listaItems') listaItems                   : any[] = [];

  @Output('itemSeleccionado') itemSeleccionado : EventEmitter<any> = new EventEmitter();

  idItemSeleccionado : number = 0;
  listaItemsFiltrado : any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.listaItems){
      if(this.listaItems!= undefined && this.listaItems.length == 0){
        this.idItemSeleccionado = 0;
      }
      this.tratarItemsParaListar();       
    }
  }

  private tratarItemsParaListar(){   
    this.listaItemsFiltrado = []; 
    let ig:number=1;
    this.listaItems.forEach(item => {
      let size:number = item[this.idFilaDividida].length;
      if(size==0){
        let itemCopy:any = JSON.parse(JSON.stringify(item));
        itemCopy[this.idCampoFilaDividida] = "-";         
        itemCopy.indexGlobal = ig;
        itemCopy.indexLocal  = 0;
        itemCopy.rows        = 1;
        this.listaItemsFiltrado.push(itemCopy);
      }else{
        for (let i = 0; i < size; i++) {
          let itemCopy:any = JSON.parse(JSON.stringify(item));
          itemCopy[this.idCampoFilaDividida] = item[this.idFilaDividida][i][this.idCampoFilaDividida];         
          itemCopy.indexGlobal = ig;
          itemCopy.indexLocal  = i;
          itemCopy.rows        = size;
          this.listaItemsFiltrado.push(itemCopy);
        }        
      }
      ig+=1;
    });
  }

  public buscarItem(palabraClave:string, campoItem:string ){
    this.deseleccionarItem();
    this.listaItemsFiltrado.forEach(item => {
      if((item[campoItem].toString()).includes(palabraClave)){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public deseleccionarItemsBusqueda(){
    this.listaItemsFiltrado.forEach(item => {
      item.seleccionado = false;
    });
  }

  // --- event @output
  //--------- paginator --------
  public seleccionarItem(item:any){
    this.deseleccionarItemsBusqueda();
    this.idItemSeleccionado = item[this.idItem];
    let itemResp:any = this.getItem(item);
    this.itemSeleccionado.emit(itemResp)
  }

  public deseleccionarItem(){
    this.idItemSeleccionado = -1;
  }

  private getItem(item : any){
    let respItem:any;
    for (let i = 0; i < this.listaItems.length; i++) {
      if(this.listaItems[i][this.idItem] == item[this.idItem] ){
        respItem = this.listaItems[i];
      }     
    }
    return respItem;
  }
}

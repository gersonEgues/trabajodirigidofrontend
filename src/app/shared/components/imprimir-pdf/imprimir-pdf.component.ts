import { Component, EventEmitter, OnInit , Output} from '@angular/core';

@Component({
  selector: 'app-imprimir-pdf',
  templateUrl: './imprimir-pdf.component.html',
  styleUrls: ['./imprimir-pdf.component.css']
})
export class ImprimirPdfComponent implements OnInit {
  @Output('generateArchivoXSLS') generateArchivoXSLS : EventEmitter<any> = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  public generarArchivoXSLXEvent(){
    this.generateArchivoXSLS.emit();
  }
}

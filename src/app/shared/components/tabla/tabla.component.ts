import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {
  @Input('tituloItems') tituloItems!   : string[];
  @Input('campoItems') campoItems!     : string[];
  @Input('idItem') idItem!             : string;
  @Input('listaItems') listaItems!     : any[];
  @Input('editar') editar              : boolean = false;
  @Input('eliminar') eliminar          : boolean = false;
  @Input('habilitar') habilitar        : boolean = false;
  @Input('inhabilitar') inhabilitar    : boolean = false;
  @Input('reload') reload              : boolean = false;
  @Input('labelCheck') labelCheck!     : string;
  @Input('disableCheck') disableCheck  : boolean = true;
  @Input('checkItems') checkItems      : boolean = false;
  @Input('ver') ver                    : boolean = false;
  @Input('descargar') descargar        : boolean = false;
  @Input('idTable') idTable            : string = '';
  @Input('textPosition') textPosition : string = 'text-start';

  @Output('itemSeleccionado') itemSeleccionado:EventEmitter<any>                       = new EventEmitter();
  @Output('editarItemSeleccionado') editarItemSeleccionado:EventEmitter<any>           = new EventEmitter();
  @Output('eliminarItemSeleccionado') eliminarItemSeleccionado:EventEmitter<any>       = new EventEmitter();
  @Output('habilitarItemSeleccionado') habilitarItemSeleccionado:EventEmitter<any>     = new EventEmitter();
  @Output('inhabilitarItemSeleccionado') inhabilitarItemSeleccionado:EventEmitter<any> = new EventEmitter();
  @Output('verItem') verItem : EventEmitter<any>                                       = new EventEmitter();
  @Output('descargarItem') descargarItem : EventEmitter<any>                           = new EventEmitter();
  @Output('reloadItem') reloadItem : EventEmitter<any>                                 = new EventEmitter();
  

  idItemSeleccionado:string='';

  constructor(private generalService : GeneralService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){
  }

  public tipoBoolean(data : any){
    let res:boolean = false;
    if(typeof data == "boolean"){
        res = true;
    }
    return res;
  }

  public tipoNumero(data : any){
    let res:boolean = false;
    if(typeof data == "number"){
        res = true;
    }
    return res;
  }

  public esAcronimo(data : any){
    let res:boolean = false;
    if(typeof data == "string"){
        if(data.length<=3){
          res=true;
        }
    }
    return res;
  }

  public getResultData(data : any){
    let result:any = '';
    if(data!=null || data!=undefined){
      if(typeof data == "boolean"){
        result = (data)?'Si':'No';
      }else{
        result = data;
      }
    }    
    return result;
  }

  public seleccionarItem(item:any){
    this.idItemSeleccionado = item[this.idItem];    
    this.itemSeleccionado.emit(item);
  }

  public deseleccionarItem(){
    this.idItemSeleccionado = '';
  }

  public deseleccionarItems(){
    this.idItemSeleccionado = '';
    this.listaItems?.forEach(item => {
      item.seleccionado = false;
    });
  }

  public limpiarListaItems(){
    this.listaItems = [];
    this.idItemSeleccionado = '';
  }

  public editarItem(item:any){
    this.editarItemSeleccionado.emit(item);
  }

  public eliminarItem(item:any){
    this.eliminarItemSeleccionado.emit(item);
  }

  public habilitarItem(item:any){
    this.habilitarItemSeleccionado.emit(item);
  }

  public inhabilitarItem(item:any){
    this.inhabilitarItemSeleccionado.emit(item);
  }

  public verItemEvent(item:any):void{
    this.verItem.emit(item);
  }

  public descargarteItemEvent(item:any):void{
    this.descargarItem.emit(item);
  }

  public reloadItemEvent(item:any):void{
    this.reloadItem.emit(item);
  }

  public itemCheck(item:any){ 
    let element = <HTMLInputElement> document.getElementById(item[this.idItem]);
    if(element.checked){
      item.check = true;
    }else{
      item.check = false;
    }
  }

  public cambioEstadoEditar(){
    this.editar= !this.editar;
  }

  public cambioEstadoEliminar(){
    this.eliminar = !this.eliminar;
  }

  public cambioEstadoHabilitar(){
    this.habilitar= !this.habilitar;
  }

  public genearteArchivoXLSX(nombre:string){
    let tableElemnt:HTMLElement = document.getElementById(this.idTable);
    this.generalService.genearteArchivoXLSX(tableElemnt,nombre);
  }
}

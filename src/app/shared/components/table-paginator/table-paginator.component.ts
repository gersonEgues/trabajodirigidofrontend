import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { GeneralService } from '../../services/general.service';


@Component({
  selector: 'app-table-paginator',
  templateUrl: './table-paginator.component.html',
  styleUrls: ['./table-paginator.component.css']
})
export class TablePaginatorComponent implements OnInit {
  @Input('tituloItems') tituloItems   : string[] = [];
  @Input('campoItems') campoItems     : string[] = [];
  @Input('idItem') idItem             : string = '';     
  @Input('listaItems') listaItems     : any[] = [];
  @Input("countItems") countItems     : number = 0;
  @Input('update') update             : boolean = false;
  @Input('delete') delete             : boolean = false;
  @Input('ver') ver                   : boolean = false;
  @Input('descargar') descargar       : boolean = false;
  @Input('genearteXSLX') genearteXSLX : boolean = false;
  @Input('idTable') idTable           : string = '';
  
  @Output('itemSeleccionado') itemSeleccionado : EventEmitter<any> = new EventEmitter();
  @Output('cambioDePagina') cambioDePagina     : EventEmitter<any> = new EventEmitter();
  @Output('verItem') verItem                   : EventEmitter<any> = new EventEmitter();
  @Output('descargarItem') descargarItem       : EventEmitter<any> = new EventEmitter();
  @Output('updateItem') updateItem             : EventEmitter<any> = new EventEmitter();
  @Output('deleteItem') deleteItem             : EventEmitter<any> = new EventEmitter();
  @Output('generateFileXLSX') generateFileXLSX : EventEmitter<any> = new EventEmitter();

  idItemSeleccionado : number=0;
  
  lengthList                    : number = 0;
  pageSize                      : number = 0;
  pageSizeOptions               : number[] = [10,20,50,100]

  constructor(private generalService : GeneralService) { }

  ngOnInit(): void {
    this.configurarOpcionesTabla();
  }

  public ngOnChanges(changes: SimpleChanges){
    if(changes.listaItems){
      if(this.listaItems!= undefined && this.listaItems.length == 0){
        this.idItemSeleccionado = 0;
      }
      this.configurarOpcionesTabla();
    }

    if(changes.countItems){
      this.lengthList = this.countItems;
    }
  }

  public configurarOpcionesTabla(){
    if(this.countItems > 100){
      this.pageSizeOptions.push(this.countItems);
    } 
  }

  public getResultData(data : any){
    let result:any = "-";
    if(data!=null || data!=undefined){
      if(typeof data == "boolean"){
        result = (data)?'Si':'No';
      }else{
        result = data;
      }
    }
    return result;
  }

  public tipoBoolean(data : any):boolean{
    let res:boolean = false;
    if(typeof data == "boolean"){
        res = true;
    }
    return res;
  }

  public seleccionarItem(item:any):void{
    this.deseleccionarItems();
    this.idItemSeleccionado = item[this.idItem];
    this.itemSeleccionado.emit(item)
  }

  public deseleccionarItem():void{
    this.idItemSeleccionado = -1;
  }

  public verItemEvent(item:any):void{
    this.verItem.emit(item);
  }

  public descargarteItemEvent(item:any):void{
    this.descargarItem.emit(item);
  }

  public updateItemEvent(item:any):void{
    this.updateItem.emit(item);
  }

  public deleteItemEvent(item:any):void{
    this.deleteItem.emit(item);
  }

  public buscarItem(palabraClave:string, campoItem:string ):void{
    this.deseleccionarItem();

    if(!this.listaItems)
      return;

    if(palabraClave==''){
      this.deseleccionarItems();
      return;
    }
    
    palabraClave = palabraClave.toLowerCase();
    this.listaItems.forEach(item => {
      if((item[campoItem].toString()).toLowerCase().includes(palabraClave)){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public buscarItemEnRango(rangoInicio:string, rangoFin:string,campoItem:string):void{
    this.deseleccionarItem();
    
    this.listaItems.forEach(item => {
      let value:string = item[campoItem].toString();
      if(value>=rangoInicio && value<=rangoFin){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public deseleccionarItems(){
    this.deseleccionarItem();
    this.listaItems?.forEach(item => {
      item.seleccionado = false;
    });
  }

  public cambioEstadoEditar(){
    this.update = !this.update;
  }

  public cambioEstadoEliminar(){
    this.delete = !this.delete;
  }

  public cambioEstadoVer(){
    this.ver= !this.ver;
  }

  //---- paginator -----
  public cambioSizePaginaOrPagina(event:PageEvent){
    this.idItemSeleccionado = -1;
    this.cambioDePagina.emit(event)
  }

  public genearteArchivoXLSX(nombre:string){
    let tableElemnt:HTMLElement = document.getElementById(this.idTable);
    this.generalService.genearteArchivoXLSX(tableElemnt,nombre);
  }
}

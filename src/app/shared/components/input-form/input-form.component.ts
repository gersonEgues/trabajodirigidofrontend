import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {
  @Input('title') title: string = '';
  @Input('type') type : string = 'text';
  @Input('placeHolder') placeHolder : string = '';

  @Output('dataOutput') dataOutput:EventEmitter<string> = new EventEmitter();

  buscador!:FormGroup;

  constructor(private fb:FormBuilder) { 
    this.buscador = this.fb.group({
      texto : ['',[],[]]
    });
  }

  ngOnInit() : void {
  }

  public buscarTexto() : void {
    let textoInput:string = (String(this.buscador.controls.texto.value)).toLowerCase();
    this.dataOutput.emit(textoInput);
  }

  public limpiarTexto() : void {
    this.buscador.get('texto')?.setValue('');
  }
}

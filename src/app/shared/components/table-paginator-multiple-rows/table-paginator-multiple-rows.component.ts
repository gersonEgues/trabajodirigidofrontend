import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-table-paginator-multiple-rows',
  templateUrl: './table-paginator-multiple-rows.component.html',
  styleUrls: ['./table-paginator-multiple-rows.component.css']
})
export class TablePaginatorMultipleRowsComponent implements OnInit {
  @Input('tituloItems') tituloItems                 : string[] = [];
  @Input('campoItems') campoItems                   : string[] = [];
  @Input('idItem')  idItem                          : string = '';   
  @Input("countItems") countItems                   : number = 0;
  @Input('idFilaDividida') idFilaDividida           : string = '';
  @Input('idCampoFilaDividida') idCampoFilaDividida : string = '';
  @Input('listaItems') listaItems                   : any[] = [];
  @Input('update') update                           : boolean = false;
  @Input('delete') delete                           : boolean = false;
  @Input('show') show                               : boolean = false;
  @Input('pagePaginator') pagePaginator             : boolean = true;
  @Input('idTable') idTable                         : string = '';

  @Output('itemSeleccionado') itemSeleccionado : EventEmitter<any> = new EventEmitter();
  @Output('cambioDePagina') cambioDePagina     : EventEmitter<any> = new EventEmitter();
  @Output('updateItem') updateItem             : EventEmitter<any> = new EventEmitter();
  @Output('deleteItem') deleteItem             : EventEmitter<any> = new EventEmitter();
  @Output('showItem') showItem                 : EventEmitter<any> = new EventEmitter();

  idItemSeleccionado : number = 0;
  listaItemsFiltrado : any[] = [];

  lengthList      : number = 0;
  pageSize        : number = 0;
  pageSizeOptions : number[] = [10,20,50,100]

  constructor(private generalService : GeneralService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes.listaItems){
      if(this.listaItems!= undefined && this.listaItems.length == 0){
        this.idItemSeleccionado = 0;
      }
      this.tratarItemsParaListar();       
    }
    if(changes.countItems){
      this.lengthList = this.countItems;
    }
  }

  private tratarItemsParaListar(){   
    this.listaItemsFiltrado = []; 
    let ig:number=1;
    this.listaItems.forEach(item => {
      let size:number = item[this.idFilaDividida].length;
      if(size==0){
        let itemCopy:any = JSON.parse(JSON.stringify(item));
        itemCopy[this.idCampoFilaDividida] = "-";         
        itemCopy.indexGlobal = ig;
        itemCopy.indexLocal  = 0;
        itemCopy.rows        = 1;
        this.listaItemsFiltrado.push(itemCopy);
      }else{
        for (let i = 0; i < size; i++) {
          let itemCopy:any = JSON.parse(JSON.stringify(item));
          itemCopy[this.idCampoFilaDividida] = item[this.idFilaDividida][i][this.idCampoFilaDividida];         
          itemCopy.indexGlobal = ig;
          itemCopy.indexLocal  = i;
          itemCopy.rows        = size;
          this.listaItemsFiltrado.push(itemCopy);
        }        
      }
      ig+=1;
    });
  }
  
  public buscarItem(palabraClave:string, campoItem:string ){
    this.deseleccionarItem();
    this.listaItemsFiltrado.forEach(item => {      
      if((item[campoItem].toString().toLowerCase()).includes(palabraClave)){
        item.seleccionado = true;
      }else{
        item.seleccionado = false;
      }
    });
  }

  public deseleccionarItemsBusqueda(){
    this.listaItemsFiltrado.forEach(item => {
      item.seleccionado = false;
    });
  }

  public cambioEstadoUpdate(){
    this.update = !this.update;  
  }

  public cambioEstadoDelete(){
    this.delete = !this.delete; 
  }

  public cambioEstadoShow(){
    this.show = !this.show;  
  }


  // --- event @output
  //--------- paginator --------
  public cambioSizePaginaOrPagina(event:any){
    this.idItemSeleccionado = -1;
    this.cambioDePagina.emit(event)
  }

  public seleccionarItem(item:any){
    this.deseleccionarItemsBusqueda();
    this.idItemSeleccionado = item[this.idItem];
    let itemResp:any = this.getItem(item);
    this.itemSeleccionado.emit(itemResp)
  }

  public deseleccionarItem(){
    this.idItemSeleccionado = 0;
  }

  public updateItemEvent(item:any){
    let respItem:any = this.getItem(item);
    this.updateItem.emit(respItem);
  }

  public deleteItemEvent(item:any){
    let respItem:any = this.getItem(item);
    this.deleteItem.emit(respItem);
  }

  public showItemEvent(item:any){
    let respItem:any = this.getItem(item);
    this.showItem.emit(respItem);
  }

  private getItem(item : any){
    let respItem:any;
    for (let i = 0; i < this.listaItems.length; i++) {
      if(this.listaItems[i][this.idItem] == item[this.idItem] ){
        respItem = this.listaItems[i];
      }     
    }
    return respItem;
  }

  public genearteArchivoXLSX(nombre:string){
    let tableElemnt:HTMLElement = document.getElementById(this.idTable);
    this.generalService.genearteArchivoXLSX(tableElemnt,nombre);
  }
}

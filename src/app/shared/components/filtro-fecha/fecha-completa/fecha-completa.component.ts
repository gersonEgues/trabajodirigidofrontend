import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { RangoFechaCompletaDTO } from 'src/app/shared/models/rangofechaCompletaDTO';

import { GeneralService } from '../../../services/general.service';
@Component({
  selector: 'app-fecha-completa',
  templateUrl: './fecha-completa.component.html',
  styleUrls: ['./fecha-completa.component.css']
})
export class FechaCompletaComponent implements OnInit {
  rangoFechaCompletaDTO! : RangoFechaCompletaDTO;

  @Input('onOff') onOff                                        : boolean = false;
  @Output('buscarEnIntervaloDeFecha') buscarEnIntervaloDeFecha : EventEmitter<any> = new EventEmitter();


  formularioFiltro!:FormGroup;

  constructor(private fb             : FormBuilder,
              private generalService : GeneralService) { }

  ngOnInit(): void {
    this.construirFormulario();

    this.rangoFechaCompletaDTO = {
      fechaInicio : this.getFormulario().get('fechaInicio')?.value,
      fechaFin : this.getFormulario().get('fechaFin')?.value,
    }
  }

  private construirFormulario(){
    this.formularioFiltro = this.fb.group({
      fechaInicio    : [ {value:this.generalService.getFechaActualFormateado(), disabled:!this.onOff}, [Validators.required],[]],
      fechaFin       : [ {value:this.generalService.getFechaActualFormateado(), disabled:!this.onOff}, [Validators.required],[]],
    });
  }

  public getFormulario():FormGroup{
    return this.formularioFiltro;
  }

  public busqueda(){
    
  }

  public rangofechaValida():boolean{
    let bandera:boolean = false;
    let fechaIni = this.getFormulario().get('fechaInicio')?.value;
    let fechaFin = this.getFormulario().get('fechaFin')?.value;

    if(fechaIni<=fechaFin){
      bandera = true;
    }    
    return bandera;
  }


  public cambioFechaEvento(){
    this.actualizarRangoDeFechaDelFormulario();
    
    if(!this.rangofechaValida()){
      this.generalService.mensajeAlerta("La fecha inicio tiene que ser menor o igual a la fecha fin");
      return;
    }
    this.buscarEnIntervaloDeFecha.emit(this.rangoFechaCompletaDTO);
  }

  private actualizarRangoDeFechaDelFormulario() : void{
    this.rangoFechaCompletaDTO = {
      fechaInicio : this.getFormulario().get('fechaInicio')?.value,
      fechaFin : this.getFormulario().get('fechaFin')?.value,
    }
  }

  public switchOnOff(event:any) : void{
    if(event.checked){
      this.activarEstadoFechaCompleta(); 
      this.cambioFechaEvento();
    }else{
      this.desactivarEstadoFechaCompleta();
    }
  }

  public formatearEstadoFecha() : void{
    this.getFormulario().get('fechaInicio')?.setValue(this.generalService.getFechaActualFormateado());
    this.getFormulario().get('fechaFin')?.setValue(this.generalService.getFechaActualFormateado());
  }
  
  public activarEstadoFechaCompleta() : void{
    this.onOff = true;
    this.getFormulario().get('fechaInicio')?.enable();
    this.getFormulario().get('fechaFin')?.enable();
  }

  public desactivarEstadoFechaCompleta() : void{
    this.onOff = false;
    this.getFormulario().get('fechaInicio')?.disable();
    this.getFormulario().get('fechaFin')?.disable();
    this.formatearEstadoFecha();
  }
}
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { EventEmitter,Output } from '@angular/core';
import { GeneralService } from '../../services/general.service';

import { SocioService } from 'src/app/socios/services/socio.service';
import { TableMultipleRowsComponent } from '../table-multiple-rows/table-multiple-rows.component';
import { InputFormComponent } from '../input-form/input-form.component';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';
import { INFO_MESSAGE } from '../../enums-mensajes/info-message';
import { SocioMedidorVO } from '../../models/socio-medidor-vo';
import { SocioVO } from '../../models/socio-vo';

@Component({
  selector: 'app-buscar-socio',
  templateUrl: './buscar-socio.component.html',
  styleUrls: ['./buscar-socio.component.css']
})
export class BuscarSocioComponent implements OnInit {
  estadoSocio   : boolean = true;
  tipoBusqueda  : number  = 0; // 1.- codigo, 2.- apellido, 3.- nombre 
  textoBusqueda : string  = "";

  @Input('busquedaTodos') busquedaTodos               : boolean = false;
  @Input('busquedaSocioMedidor') busquedaSocioMedidor : boolean = false;

  @Input('opcionEstado') opcionEstado : boolean = false;
  @Input('idTable') idTable           : string = '';
  

  // tabla
  @Input('tituloItems') tituloItems : string[]  = [];
  @Input('campoItems') campoItems   : string[]  = [];
  idItemSocio                       : string    = 'idSocio';
  listaDeSocios                     : SocioVO[] = [];

  idItemSocioMedidor                : string    = 'idSocioMedidor';
  listaDeSociosMedidor              : SocioMedidorVO[] = [];

  @Output('itemSeleccionado') itemSeleccionado : EventEmitter<any> = new EventEmitter();
  @Output('nuevaBusqueda') nuevaBusqueda : EventEmitter<any> = new EventEmitter();

  @ViewChild(TableMultipleRowsComponent) tableMultipleRowsComponent! : TableMultipleRowsComponent;

  @ViewChild('InputFormComponentCodigo') InputFormComponentCodigo!     : InputFormComponent;
  @ViewChild('InputFormComponentApellido') InputFormComponentApellido! : InputFormComponent;
  @ViewChild('InputFormComponentNombre') InputFormComponentNombre!     : InputFormComponent;
  
  idItemSeleccionado : number = 0;
  
  
  constructor(private socioService   : SocioService,
              private generalService : GeneralService,
              private router         : Router) { }

  ngOnInit(): void { }

  public buscarSocioPorCodigo(codigo:string) : void {
    this.tipoBusqueda = 1;
    this.textoBusqueda = codigo;

    let valNull:any = null;
    if(codigo.trim()==""){
      this.listaDeSocios        = [];
      this.listaDeSociosMedidor = [];
    }else{
      if(this.busquedaSocioMedidor)
        this.buscarSocioMedidorService(codigo,valNull,valNull,this.estadoSocio);
      else if(this.busquedaTodos)
        this.buscarSociosTodosService(codigo,valNull,valNull);
      else
        this.buscarSocioService(codigo,valNull,valNull,this.estadoSocio);    

      this.InputFormComponentNombre.limpiarTexto();
      this.InputFormComponentApellido.limpiarTexto();
    }

    this.nuevaBusqueda.emit(codigo);
  }

  public buscarSocioPorNombre(nombre:string) : void {
    this.tipoBusqueda = 3;
    this.textoBusqueda = nombre;

    let valNull:any = null;
    if(nombre.trim()==""){
      this.listaDeSocios        = [];
      this.listaDeSociosMedidor = [];
    } else {
      if(this.busquedaSocioMedidor)
        this.buscarSocioMedidorService(valNull,nombre,valNull,this.estadoSocio);
      else if(this.busquedaTodos)
        this.buscarSociosTodosService(valNull,nombre,valNull);
      else
        this.buscarSocioService(valNull,nombre,valNull,this.estadoSocio);

      this.InputFormComponentCodigo.limpiarTexto();
      this.InputFormComponentApellido.limpiarTexto();
    }

    this.nuevaBusqueda.emit(nombre);
  }

  public buscarSocioPorApellido(apellido:string) : void {
    this.tipoBusqueda = 2;
    this.textoBusqueda = apellido;

    let valNull:any = null;
    if(apellido.trim()==""){
      this.listaDeSocios        = [];
      this.listaDeSociosMedidor = [];
    }else{
      if(this.busquedaSocioMedidor)
        this.buscarSocioMedidorService(valNull,valNull,apellido,this.estadoSocio);
      else if(this.busquedaTodos)
        this.buscarSociosTodosService(valNull,valNull,apellido);
      else
        this.buscarSocioService(valNull,valNull,apellido,this.estadoSocio);

      this.InputFormComponentCodigo.limpiarTexto();
      this.InputFormComponentNombre.limpiarTexto();      
    }

    this.nuevaBusqueda.emit(apellido);
  }

  //-------------------
  // Consumo API-REST |
  // ------------------ 
  private buscarSocioService(codigo:string, nombre:string, apellido:string, estado:boolean) {
    this.socioService.buscarSocio(codigo,nombre,apellido,estado).subscribe(
      (resp:SocioVO[])=>{
        this.listaDeSocios = resp;
        if(resp.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },(err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private buscarSociosTodosService(codigo:string, nombre:string, apellido:string) {
    this.socioService.buscarSocioTodos(codigo,nombre,apellido).subscribe(
      (resp:SocioVO[])=>{
        this.listaDeSocios = resp;
        if(resp.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },(err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private buscarSocioMedidorService(codigo:string, nombre:string, apellido:string, estado:boolean) {
    this.socioService.buscarSocioMedidor(codigo,nombre,apellido,estado).subscribe(
      (resp)=>{
        this.listaDeSociosMedidor = resp;
        
        if(resp.length==0){
          this.generalService.mensajeAlerta(INFO_MESSAGE.not_found_results,INFO_MESSAGE.title_not_found_results);
        }else{
          this.generalService.mensajeCorrecto(INFO_MESSAGE.found_results,INFO_MESSAGE.title_found_results);
        }
      },(err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  public itemSeleccionadoEvent(item:any) : void {    
    this.itemSeleccionado.emit(item);
  } 

  public deseleccionarItem() : void {
    this.tableMultipleRowsComponent.deseleccionarItem();
  }

  public reloadBusqueda() : void {
    if(this.tipoBusqueda == 1) {
      this.buscarSocioPorCodigo(this.textoBusqueda);
    }else if(this.tipoBusqueda == 2) {
      this.buscarSocioPorApellido(this.textoBusqueda);
    }else if(this.tipoBusqueda == 3) {
      this.buscarSocioPorNombre(this.textoBusqueda);
    }
  }

  public limpiarBusqueda() : void {
    this.InputFormComponentCodigo.limpiarTexto();
    this.InputFormComponentNombre.limpiarTexto();
    this.InputFormComponentApellido.limpiarTexto();
  }

  public cambioEstadoSocio(){
    this.listaDeSocios = [];
  }
}

import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AnioVO } from 'src/app/configuraciones/models/anioVO';
import { PeriodoDeAnioVO } from 'src/app/configuraciones/models/periodoDeAnioVO';
import { MesAnioCategoriaCostoAguaService } from 'src/app/configuraciones/services/mes-anio-categoria-costo-agua.service';
import { GeneralService } from 'src/app/shared/services/general.service';
import { RangoPeriodoDTO } from '../../models/rangoPeriodoDTO';
import { Router } from '@angular/router';
import { ERR_MESSAGE } from 'src/app/shared/enums-mensajes/err-message';
import { PATH } from 'src/app/shared/enums-mensajes/path';


@Component({
  selector: 'app-rango-periodo',
  templateUrl: './rango-periodo.component.html',
  styleUrls: ['./rango-periodo.component.css']
})
export class RangoPeriodoComponent implements OnInit {
  formularioPeriodo!  : FormGroup;
  listaGestionInicio! : AnioVO[];
  listaGestionFin!    : AnioVO[];
  listaMesInicio!     : PeriodoDeAnioVO[];
  listaMesFin!        : PeriodoDeAnioVO[];

  idGestionInicioSeleccionado! : number;
  idGestionFinSeleccionado!    : number;
  idMesInicioSeleccionado!     : number;
  idMesFinSeleccionado!        : number;

  aux:any = undefined;
  rangoPeriodoDTO:RangoPeriodoDTO = this.aux;

  @Output('cambioPeriodoEvent') cambioPeriodoEvent : EventEmitter<RangoPeriodoDTO> = new EventEmitter();

  constructor(private fb                               : FormBuilder,
              private generalService                   : GeneralService,
              private mesAnioCategoriaCostoAguaService : MesAnioCategoriaCostoAguaService,
              private router                           : Router) { }

  ngOnInit(): void {
    this.construirformularioFecha();
    this.getListaAniosService();
  }

  private construirformularioFecha(){
    this.formularioPeriodo = this.fb.group({     
      gestionInicio  : ['0', [Validators.required],[]],
      mesInicio      : [{value : '0', disabled:true}, [Validators.required],[]],
      gestionFin     : ['0', [Validators.required],[]],
      mesFin         : [{value : '0', disabled:true}, [Validators.required],[]],
    });
  }

  public changeGestionIncio(){
    this.formularioPeriodo.get('mesInicio')?.enable();
    this.idGestionInicioSeleccionado = this.formularioPeriodo.get('gestionInicio')?.value;
    
    this.limpiarMesIncio();
    this.getListaMesInicioService(this.idGestionInicioSeleccionado);

    this.rangoPeriodoDTO = this.getRangoPeriodoDTO();
    this.cambioPeriodoEvent.emit(this.rangoPeriodoDTO);
  }

  public changeGestionFin(){
    this.formularioPeriodo.get('mesFin')?.enable();
    this.idGestionFinSeleccionado = this.formularioPeriodo.get('gestionFin')?.value;
    
    this.limpiarMesFin();
    this.getListaMesFinService(this.idGestionFinSeleccionado);

    this.rangoPeriodoDTO = this.getRangoPeriodoDTO();
    this.cambioPeriodoEvent.emit(this.rangoPeriodoDTO);
  }
  
  public changeMesInicio(){
    this.rangoPeriodoDTO = this.getRangoPeriodoDTO();
    this.cambioPeriodoEvent.emit(this.rangoPeriodoDTO);
  }

  public changeMesFin(){
    this.rangoPeriodoDTO = this.getRangoPeriodoDTO();
    this.cambioPeriodoEvent.emit(this.rangoPeriodoDTO);  
  }

  private limpiarMesIncio(){
    this.formularioPeriodo.get('mesInicio')?.reset();
    this.formularioPeriodo.get('mesInicio')?.setValue('0');
    this.idMesInicioSeleccionado = this.aux;
  }

  private limpiarMesFin(){
    this.formularioPeriodo.get('mesFin')?.reset();
    this.formularioPeriodo.get('mesFin')?.setValue('0');
    this.idMesFinSeleccionado = this.aux;
  }

  public limpiarFormulario(){
    this.limpiarMesIncio();
    this.limpiarMesFin();

    this.formularioPeriodo.get('gestionInicio')?.reset();
    this.formularioPeriodo.get('gestionFin')?.reset();
    this.formularioPeriodo.get('gestionInicio')?.setValue("0");
    this.formularioPeriodo.get('gestionFin')?.setValue("0");

    this.idGestionInicioSeleccionado = this.aux;
    this.idGestionFinSeleccionado = this.aux;
  }

  public campoValido(nombreCampo:string):boolean{
    let bandera1:boolean = (this.formularioPeriodo.controls[nombreCampo].valid && 
                            this.formularioPeriodo.controls[nombreCampo].value != '0') 
    let bandera2:boolean = ((this.ambosPeriodosSeleccionados())?this.getRangoPeriodoDTO().rangoValido:true);

    return bandera1 && bandera2;
  }

  public campoInvalido(nombreCampo:string):boolean{
    return  (this.formularioPeriodo.controls[nombreCampo].touched && 
            this.formularioPeriodo.controls[nombreCampo].value == '0') || 
            ((this.ambosPeriodosSeleccionados() )?!this.getRangoPeriodoDTO().rangoValido:false);
  }

  public ambosPeriodosSeleccionados(){
    return this.idMesInicioSeleccionado!=undefined && this.idMesFinSeleccionado!=undefined;
  }

  public getRangoPeriodoDTO():RangoPeriodoDTO{
    let idGestionIncio: number = Number(this.formularioPeriodo.get('gestionInicio').value);
    let idGestionFin: number = Number(this.formularioPeriodo.get('gestionFin').value);
    
    let idPeriodoInicio:number = Number(this.formularioPeriodo.get('mesInicio').value);
    let idPeriodoFin:number = Number(this.formularioPeriodo.get('mesFin').value);

    let rangoValido:boolean = false;
    let todasLasOpcionesSeleccionadas : boolean = false;
    if(!(idGestionIncio==0 || idGestionFin==0 || idPeriodoInicio==0 || idPeriodoFin == 0)){
      todasLasOpcionesSeleccionadas = true;
      rangoValido = idPeriodoFin>=idPeriodoInicio;
    }

    let rangoPeriodoDTO:RangoPeriodoDTO = {
      idPeriodoInicio    : idPeriodoInicio,
      idPeriodoFin       : idPeriodoFin,
      gestionInicio      : idGestionIncio,
      mesInicio          : idGestionFin,
      gestionFin         : 0,
      mesFin             : 0,
      mensaje            : '',
      rangoValido        : rangoValido,
      todosSeleccionados : todasLasOpcionesSeleccionadas,
    }

    return rangoPeriodoDTO;
  }

  // consumo API-REST
  private getListaAniosService(){
    this.mesAnioCategoriaCostoAguaService.getAnios().subscribe(
      (resp)=>{
        this.listaGestionInicio = resp;
        this.listaGestionFin    = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getListaMesInicioService(idAnio:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnio(idAnio).subscribe(
      (resp)=>{
        this.listaMesInicio = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }

  private getListaMesFinService(idAnio:number){
    this.mesAnioCategoriaCostoAguaService.getPeriodosDeAnio(idAnio).subscribe(
      (resp)=>{
        this.listaMesFin = resp;
      },
      (err)=>{
        if(err.status==401){
          this.generalService.mensajeError(ERR_MESSAGE.err_401);
          this.router.navigate([PATH.log_out]);
        }else if(err.status==0){
          this.generalService.mensajeError(ERR_MESSAGE.err_0);
          this.router.navigate([PATH.log_out]);          
        }else{
          this.generalService.mensajeError(err.message);
        }
      }
    );
  }
}
